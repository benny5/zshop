package com.enation.app.javashop.consumer.shop.promotion;

import com.enation.app.javashop.consumer.core.event.DistributionBindingMessageEvent;
import com.enation.app.javashop.core.distribution.model.vo.DistributionRelationshipVO;
import com.enation.app.javashop.core.distribution.service.DistributionManager;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DistributionBindingMessageInvokeConsumer implements DistributionBindingMessageEvent {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DistributionManager distributionManager;

    @Override
    public void DistributionBindingMessageInvoke(List<DistributionRelationshipVO> distributionRelationshipVOList) {
        distributionManager.addDistributionRelationshipVO(distributionRelationshipVOList);
    }
}
