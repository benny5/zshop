package com.enation.app.javashop.consumer.shop.goods;

import com.enation.app.javashop.consumer.core.event.GoodsCommentEvent;
import com.enation.app.javashop.core.base.message.GoodsCommentMsg;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.trade.OrderClient;
import com.enation.app.javashop.core.member.model.enums.CommentTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.CommentStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v1.0
 * @Description: 更改商品的评论数量
 * @date 2018/6/25 10:23
 * @since v7.0.0
 */
@Service
public class GoodsCommentNumConsumer implements GoodsCommentEvent {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private OrderClient orderClient;

    @Override
    public void goodsComment(GoodsCommentMsg goodsCommentMsg) {

        if (goodsCommentMsg.getComment() == null || goodsCommentMsg.getComment().isEmpty()) {
            return;
        }
        goodsCommentMsg.getComment().forEach(comment -> {
            //如果评论不为空  且是初评
            if (comment != null && CommentTypeEnum.INITIAL.name().equals(comment.getCommentsType())) {
                this.goodsClient.updateCommentCount(comment.getGoodsId());
            }
        });
    }
}
