package com.enation.app.javashop.consumer.job.execute.impl;

import com.enation.app.javashop.consumer.job.execute.EveryHourExecute;
import com.enation.app.javashop.core.promotion.luck.service.LuckManager;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: zhou
 * @Date: 2021/3/4
 * @Description: 下线已经过期的抽奖活动
 */
public class LuckJob implements EveryHourExecute {

    @Autowired
    private LuckManager luckManager;

    @Override
    public void everyHour() {
        luckManager.expireLuck();
    }
}
