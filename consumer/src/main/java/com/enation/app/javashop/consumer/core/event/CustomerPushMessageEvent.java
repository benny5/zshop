package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageDO;

/**
 * @author xlg
 * @since 2020-11-24 19:55
 * 用户推送活动消息事件
 */
public interface CustomerPushMessageEvent {
    /**
     * 给用户推送活动消息需要执行的方法
     */
    void customerPushMessageInvoke(ActivityMessageDO activityMessageDO);
}
