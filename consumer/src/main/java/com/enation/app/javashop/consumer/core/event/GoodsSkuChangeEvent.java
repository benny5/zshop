package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.base.message.GoodsSkuChangeMsg;

/**
 * @author JFeng
 * @date 2020/8/17 15:48
 */
public interface GoodsSkuChangeEvent {

    void goodsSkuChange(GoodsSkuChangeMsg goodsSkuChangeMsg);

}
