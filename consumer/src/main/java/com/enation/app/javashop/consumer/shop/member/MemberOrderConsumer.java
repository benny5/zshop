package com.enation.app.javashop.consumer.shop.member;

import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.member.service.MemberCouponManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.service.OrderMemberManager;
import com.enation.app.javashop.core.trade.order.service.OrderProfitManager;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.util.DictUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员订单状态汇总
 *
 * @author duanmingyu
 * @version v7.1.4
 * @since v7.0.0
 * 2019-06-24
 */
@Service
public class MemberOrderConsumer implements OrderStatusChangeEvent, RefundStatusChangeEvent {

    @Autowired
    private OrderQueryManager orderQueryManager;
    @Autowired
    private OrderMemberManager orderMemberManager;
    @Autowired
    private OrderProfitManager orderProfitManager;
    @Autowired
    private MemberCouponManager memberCouponManager;


    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        logger.info("订单状态变更-用户订单统计："+orderMessage.toString());
        //获取订单信息
        OrderDO order = orderMessage.getOrderDO();

        orderMemberManager.orderStatistic(order.getMemberId());

        // 更新状态
        OrderDetailDTO orderDetailDTO = orderQueryManager.getModel(order.getSn());
        orderProfitManager.updateOrderProfitStatus(orderDetailDTO);

        if(OrderStatusEnum.PAID_OFF.value().equals(order.getOrderStatus())){
            // 根据memberId查询下单数  订单数小于2 是新用户
            Integer orderNum = orderQueryManager.getOrderNumberByMemberId(order.getMemberId(),order.getSellerId());
            if(orderNum != null && orderNum != 0 && orderNum < 2){
                String firstOrder = DictUtils.getDictValue(null, "首单", "ORDER_FLAG");
                // 标识 订单是首单
                orderQueryManager.updateOrderFlag(order.getSn(),firstOrder);
            }
        }

        if(OrderStatusEnum.CANCELLED.value().equals(order.getOrderStatus())){
            // 订单取消 退回优惠券
            logger.info("订单取消 退回优惠券"+order.getSn());
            memberCouponManager.retroBackCoupon(order.getSn());
        }
    }

    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {
        logger.info("订单退款-用户订单统计："+refundChangeMsg.toString());
        //获取订单信息
        Integer memberId = refundChangeMsg.getRefund().getMemberId();
        orderMemberManager.orderStatistic(memberId);
    }
}
