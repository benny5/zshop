package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.SpreadRefundCompleteEvent;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author 王校长
 * @Date 2020/9/10
 * @Descroption 多退少补完成后消费者(退款失败成功均发消息)
 */
@Component
public class SpreadRefundCompleteReceiver {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired(required = false)
    private List<SpreadRefundCompleteEvent> events;

    /**
     * 多退少补完成
     * @param refund
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SPREAD_REFUND_COMPLETE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SPREAD_REFUND_COMPLETE, type = ExchangeTypes.FANOUT)
    ))
    public void spreadRefundComplete(RefundDO refund) {
        logger.info("【消息内容】:"+refund.toString());

        if (events != null) {
            for (SpreadRefundCompleteEvent event : events) {
                try {
                    event.spreadRefundComplete(refund);
                } catch (Exception e) {
                    logger.error("多退少补完成消息出错", e);
                }
            }
        }

    }


}
