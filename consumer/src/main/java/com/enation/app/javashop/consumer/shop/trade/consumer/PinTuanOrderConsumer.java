package com.enation.app.javashop.consumer.shop.trade.consumer;

import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.PintuanSuccessEvent;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.promotion.pintuan.service.PintuanOrderManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by kingapex on 2019-01-25.
 * 拼团订单消费者<br/>
 * 如果是拼团订单，检测相应的拼团活动是否已经参团成功<br/>
 * 如果成功，要更新相应数据
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-25
 */
@Component
public class PinTuanOrderConsumer implements OrderStatusChangeEvent, PintuanSuccessEvent {

    @Autowired
    private PintuanOrderManager pintuanOrderManager;

    @Autowired
    private AmqpTemplate amqpTemplate;


    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        //对已付款的订单
        if (orderMessage.getNewStatus().equals(OrderStatusEnum.PAID_OFF)) {
            OrderDO orderDO = orderMessage.getOrderDO();
            if (orderDO.getOrderType().equals(OrderTypeEnum.pintuan.name())) {
                Integer pinTuanOrderId = pintuanOrderManager.payOrder(orderDO);
                if (pinTuanOrderId != null) {
                    this.amqpTemplate.convertAndSend(AmqpExchange.PINTUAN_SUCCESS, AmqpExchange.PINTUAN_SUCCESS + "_ROUTING", pinTuanOrderId);
                }
            }

        }

    }

    @Override
    public void success(Integer pintuanOrderId) {
        pintuanOrderManager.calDistributionCommission(pintuanOrderId);
    }
}
