package com.enation.app.javashop.consumer.job.execute.impl;

import com.enation.app.javashop.consumer.job.execute.EveryHourExecute;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.service.AfterSaleManager;
import com.enation.app.javashop.core.promotion.luck.service.LuckRecordManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 每小时执行
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-25 上午10:21
 */
@Component
public class RefundStatusJob implements EveryHourExecute {

    @Autowired
    private AfterSaleManager afterSaleManager;
    @Autowired
    private LuckRecordManager luckRecordManager;
    /**
     * 每小时执行
     */
    @Override
    public void everyHour() {
        // 修改订单退款的完成状态
        List<RefundDO> refundDOList = afterSaleManager.queryRefundStatus();
        // 订单退款完成之后回收抽奖奖品
        if(!CollectionUtils.isEmpty(refundDOList)){
            luckRecordManager.LuckRefund(refundDOList);
        }
    }
}
