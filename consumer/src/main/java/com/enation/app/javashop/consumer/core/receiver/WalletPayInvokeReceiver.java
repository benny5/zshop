package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.WalletPayInvokeEvent;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.vo.PayBill;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 王校长
 * @since 2020年10月17日 10:37:25
 * 调用钱包支付消费者
 */
@Component
public class WalletPayInvokeReceiver {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired(required = false)
    private List<WalletPayInvokeEvent> events;

    /**
     * 调用钱包支付
     * @param paymentbill 支付单实体
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.WALLET_PAY_INVOKE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.WALLET_PAY_INVOKE, type = ExchangeTypes.FANOUT)
    ))
    public void spreadRefundComplete(PaymentBillDO paymentbill) {
        logger.info("【消息内容】:"+paymentbill.toString());

        if (events != null) {
            for (WalletPayInvokeEvent event : events) {
                try {
                    event.walletPayInvoke(paymentbill);
                } catch (Exception e) {
                    logger.error("调用钱包支付消息出错", e);
                }
            }
        }

    }


}
