package com.enation.app.javashop.consumer.core.receiver;


import com.enation.app.javashop.consumer.core.event.PintuanSuccessEvent;
import com.enation.app.javashop.consumer.core.event.ShetuanChangeEvent;
import com.enation.app.javashop.core.base.message.ShetuanChangeMsg;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 社区团购消息接收
 */
@Component
public class ShetuanReceiver {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired(required = false)
    private List<ShetuanChangeEvent> events;

    /**
     * 社区团购变更消息
     * @param shetuanChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHETUAN_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SHETUAN_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void  receiveMessage(ShetuanChangeMsg shetuanChangeMsg){
        logger.info("【消息内容】:"+shetuanChangeMsg.toString());

        if (events != null) {
            for (ShetuanChangeEvent event : events) {
                try {
                    event.shetuanChange(shetuanChangeMsg);
                } catch (Exception e) {
                    logger.error("社区团购商品成功消息", e);
                }
            }
        }

    }

}
