package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.distribution.model.vo.DistributionRelationshipVO;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageDO;

import java.util.List;

/**
 * @author xlg
 * @since 2020-12-03 11:28
 * 会员团长绑定记录  事件
 */
public interface DistributionBindingMessageEvent {
    /**
     * 会员团长绑定记录需要执行的方法
     */
    void DistributionBindingMessageInvoke(List<DistributionRelationshipVO> distributionRelationshipVOList);
}
