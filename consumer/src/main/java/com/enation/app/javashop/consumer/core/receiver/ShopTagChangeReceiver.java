package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.ShopTagChangeEvent;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/12/10
 * @Description: 查询店铺中关联所有商品的标签，并放入redis
 */
@Component
public class ShopTagChangeReceiver {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private List<ShopTagChangeEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHOP_TAG_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SHOP_TAG_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void cartGoodsCount(List<Integer> sellerIds) {

        for (ShopTagChangeEvent event : events) {
            try {
                event.shopTagChangeCache(sellerIds);
            }catch (Exception e){
                logger.error("查询店铺中关联所有商品的标签出错："+e.getMessage());
            }
        }
    }
}
