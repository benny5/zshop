package com.enation.app.javashop.consumer.shop.goods;

import com.enation.app.javashop.consumer.core.event.GoodsChangeEvent;
import com.enation.app.javashop.core.base.message.GoodsChangeMsg;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.member.ShopClient;
import com.enation.app.javashop.core.client.system.PageDataClient;
import com.enation.app.javashop.core.goods.model.vo.CacheGoods;
import com.enation.app.javashop.core.pagedata.model.PageData;
import com.enation.app.javashop.core.payment.model.enums.ClientType;
import com.enation.app.javashop.framework.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: 店铺商品数量消费者
 * @date 2018/9/14 14:20
 * @since v7.0.0
 */
@Component
public class ShopGoodsCountConsumer implements GoodsChangeEvent {

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private ShopClient shopClient;

    @Autowired
    private PageDataClient pageDataClient;

    @Override
    public void goodsChange(GoodsChangeMsg goodsChangeMsg) {

        //删除操作不需要重新统计店铺的商品数量
        if (GoodsChangeMsg.DEL_OPERATION == goodsChangeMsg.getOperationType()) {
            return;
        }

        //获取商品id
        Integer[] goodsIds = goodsChangeMsg.getGoodsIds();
        if (goodsIds.length > 0) {
            Integer goodsId = goodsIds[0];
            //获取商品店铺id
            CacheGoods goods = goodsClient.getFromCache(goodsId);
            Integer sellerGoodsCount = goodsClient.getSellerGoodsCount(goods.getSellerId());
            //更新店铺信息
            shopClient.updateShopGoodsNum(goods.getSellerId(), sellerGoodsCount);
            //更新wap首页楼层数据 -------------------JFENG  因为空指针删除
            // PageData pageData = pageDataClient.getByType(ClientType.WAP.name(), "INDEX");
            // String data = pageData.getPageData();
            // if (!data.isEmpty()) {
            //     List<HashMap> dataList = JsonUtil.jsonToList(data, HashMap.class);
            //     for (Map map : dataList) {
            //         List<Map> blockList = (List<Map>) map.get("blockList");
            //         for (Map blockMap : blockList) {
            //             String blockType = (String) blockMap.get("block_type");
            //             switch (blockType) {
            //                 case "GOODS":
            //                     Map blockValue = (Map) blockMap.get("block_value");
            //                     Integer pageGoodsId = (Integer) blockValue.get("goods_id");
            //                     if (pageGoodsId.equals(goodsId)) {
            //                         blockValue.put("goods_name", goods.getGoodsName());
            //                         blockValue.put("sn", goods.getSn());
            //                         blockValue.put("goods_image", goods.getThumbnail());
            //                         blockValue.put("enable_quantity", goods.getEnableQuantity());
            //                         blockValue.put("quantity", goods.getQuantity());
            //                         blockValue.put("price", goods.getPrice());
            //                         blockValue.put("market_enable", goods.getMarketEnable());
            //                         blockValue.put("goods_price", goods.getPrice());
            //                         break;
            //                     }
            //
            //             }
            //         }
            //     }
            //     //重新渲染
            //     pageData.setPageData(JsonUtil.objectToJson(dataList));
            //     pageDataClient.edit(pageData, pageData.getPageId());
            // }
        }

    }
}
