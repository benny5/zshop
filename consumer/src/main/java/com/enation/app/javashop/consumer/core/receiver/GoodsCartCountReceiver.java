package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.GoodsCartCountEvent;
import com.enation.app.javashop.core.base.message.GoodsCartCountMsg;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/11
 * @Description: 购物车商品加购统计
 */
@Component
public class GoodsCartCountReceiver {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private List<GoodsCartCountEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.GOODS_CART_COUNT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.GOODS_CART_COUNT, type = ExchangeTypes.FANOUT)
    ))
    public void cartGoodsCount(GoodsCartCountMsg goodsCartCountMsg) {

        for (GoodsCartCountEvent event : events) {
            try {
                event.goodsCartCount(goodsCartCountMsg);
            }catch (Exception e){
                logger.error("购物车商品加购统计消息出错："+e.getMessage());
            }
        }
    }
}
