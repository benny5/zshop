package com.enation.app.javashop.consumer.shop.message;


import com.enation.app.javashop.consumer.core.event.*;
import com.enation.app.javashop.consumer.core.receiver.OrderIntoDbReceiver;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.RefuseTypeEnum;
import com.enation.app.javashop.core.base.SettingGroup;
import com.enation.app.javashop.core.base.message.*;
import com.enation.app.javashop.core.base.model.vo.SmsSendVO;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.member.MemberClient;
import com.enation.app.javashop.core.client.member.ShopClient;
import com.enation.app.javashop.core.client.system.MessageTemplateClient;
import com.enation.app.javashop.core.client.system.SettingClient;
import com.enation.app.javashop.core.client.system.SmsClient;
import com.enation.app.javashop.core.client.trade.OrderClient;
import com.enation.app.javashop.core.goods.model.vo.CacheGoods;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.vo.MemberLoginMsg;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.system.enums.MessageCodeEnum;
import com.enation.app.javashop.core.system.enums.MessageOpenStatusEnum;
import com.enation.app.javashop.core.system.model.dos.MessageTemplateDO;
import com.enation.app.javashop.core.system.model.vo.SiteSetting;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderItemsDO;
import com.enation.app.javashop.core.trade.order.model.dto.OrderDTO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderOperateEnum;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipStatusEnum;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.entity.DictDTO;
import com.enation.app.javashop.framework.util.*;
import org.apache.commons.lang.text.StrSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * 消息模版发送短信
 *
 * @author zjp
 * @version v7.0
 * @since v7.0
 * 2018年3月25日 下午3:15:01
 */
@Component
public class SmsSendMessageConsumer implements OrderStatusChangeEvent, RefundStatusChangeEvent, GoodsChangeEvent, MemberLoginEvent, MemberRegisterEvent, TradeIntoDbEvent, GoodsCommentEvent {

    @Autowired
    private MessageTemplateClient messageTemplateClient;

    @Autowired
    private SettingClient settingClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private ShopClient shopClient;

    @Autowired
    private MemberClient memberClient;

    @Autowired
    private SmsClient smsClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private OrderQueryManager orderQueryManager;

    private void sendSms(SmsSendVO smsSendVO) {
        smsClient.send(smsSendVO);
    }

    // 接收短信提醒的客服手机号字典类型
    private static final String CUSTOMER_SERVICE_MOBILE = "customer_service_mobile";
    // 不发送短信提醒的店铺ID字典类型
    private static final String SHETUAN_SELLER_ID = "shetuan_seller_id";

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        OrderDO orderDO = orderMessage.getOrderDO();
        String siteSettingJson = settingClient.get(SettingGroup.SITE);
        //获取系统配置
        SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson, SiteSetting.class);
        MessageTemplateDO messageTemplate;
        //获取当前下单会员信息
        Member member = memberClient.getModel(orderDO.getMemberId());
        //获取当前店铺所有者的联系方式
        ShopVO shopVO = shopClient.getShop(orderDO.getSellerId());
        //订单支付提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.PAID_OFF.name())) {
            Map<String, Object> valuesMap = new HashMap<>(4);
            valuesMap.put("ordersSn", orderDO.getSn());
            valuesMap.put("paymentTime", DateUtil.toString(orderDO.getPaymentTime(), "yyyy-MM-dd"));
            valuesMap.put("siteName", siteSetting.getSiteName());

            // 店铺订单支付提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSPAY);
            // 判断短信是否开启
            if (messageTemplate != null) {
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    // 发送短信
                    this.sendSms(this.getSmsMessage(shopVO.getLinkPhone(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }
            // 会员订单支付提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSPAY);
            if (messageTemplate != null) {
                // 判断短信是否开启
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    this.sendSms(this.getSmsMessage(member.getMobile(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }

        }

        //订单收货提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.ROG.name())) {
            Map<String, Object> valuesMap = new HashMap<>(4);
            valuesMap.put("ordersSn", orderDO.getSn());
            valuesMap.put("finishTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));
            valuesMap.put("siteName", siteSetting.getSiteName());
            // 店铺订单收货提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSRECEIVE);
            if (messageTemplate != null) {
                // 判断短信是否开启
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    this.sendSms(this.getSmsMessage(shopVO.getLinkPhone(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }
            //会员订单收货提醒
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSRECEIVE);
            if (messageTemplate != null) {
                // 判断短信是否开启
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    this.sendSms(this.getSmsMessage(member.getMobile(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }
        }

        //订单取消提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.CANCELLED.name())) {
            Map<String, Object> valuesMap = new HashMap<>(4);
            valuesMap.put("ordersSn", orderDO.getSn());
            valuesMap.put("cancelTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));
            valuesMap.put("siteName", siteSetting.getSiteName());
            // 发送会员消息
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSCANCEL);
            if (messageTemplate != null) {
                // 判断短信是否开启
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    this.sendSms(this.getSmsMessage(member.getMobile(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }

            // 发送店铺消息
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSCANCEL);
            if (messageTemplate != null) {
                // 判断短信是否开启
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    this.sendSms(this.getSmsMessage(shopVO.getLinkPhone(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }
        }

        //订单发货提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.SHIPPED.name())) {
            // 会员消息发送
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERSSEND);
            if (messageTemplate != null) {
                // 判断短信是否开启
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    Map<String, Object> valuesMap = new HashMap<>(4);
                    valuesMap.put("ordersSn", orderDO.getSn());
                    valuesMap.put("shipSn", orderDO.getShipNo());
                    valuesMap.put("sendTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));
                    valuesMap.put("siteName", siteSetting.getSiteName());
                    this.sendSms(this.getSmsMessage(member.getMobile(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }
        }

        // 订单分包裹发货提醒
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.PART_SHIPPED.name())) {
            // 1.获取会员消息发送模板
            messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERORDERPARTSHIP);
            if (messageTemplate != null) {
                // 判断短信是否开启
                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                    // 2.从redis中获取部分发货的商品IDs
                    OrderDO order = orderMessage.getOrderDO();
                    String key = OrderOperateEnum.SPLIT_SHIP.name() + "_" + order.getSn();
                    String skuIdsString = stringRedisTemplate.opsForValue().get(key);
                    if (StringUtil.isEmpty(skuIdsString)) {
                        return;
                    }
                    String[] skuIds = skuIdsString.split(",");
                    // 2.根据orderSn查询orderItem集合，查询订单中所有商品的数量
                    List<OrderItemsDO> orderItems = orderQueryManager.getOrderItems(order.getSn());
                    List<OrderItemsDO> unShipOrderItemList = orderItems.stream()
                            .filter(orderItemsDO -> orderItemsDO.getShipStatus() == null || orderItemsDO.getShipStatus().equals(ShipStatusEnum.SHIP_NO.value()))
                            .collect(toList());

                    // 3.构建短信内容替换参数map
                    Map<String, Object> valuesMap = new HashMap<>(4);
                    valuesMap.put("ordersSn", orderDO.getSn());
                    valuesMap.put("shipNum", skuIds.length);
                    Integer unShipNum = unShipOrderItemList.size() - skuIds.length;
                    valuesMap.put("unShipNum", unShipNum < 0 ? 0 : unShipNum);

                    // 4.发送短信
                    this.sendSms(this.getSmsMessage(member.getMobile(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
                }
            }
        }
    }


    /**
     * 售后消息
     */
    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {

        //退货/款提醒
        // *******************************************正式环境 退货/款提醒给企业微信群发送消息**************************************************
        if (refundChangeMsg.getRefundStatusEnum().equals(RefundStatusEnum.APPLY)) {
            RefundDO refund = refundChangeMsg.getRefund();
            if (EnvironmentUtils.isProd()) {
                String qySendMessageData = "【退款审核提醒】「" + refund.getSellerName() + "」有1个新的退款申请，订单号" + refund.getOrderSn() + "，请及时审核~";
                QyWeixinSendMessageUtil.sendMessageToGroupChat(QyWeixinSendMessageUtil.RefundMessage, qySendMessageData);
            }
        }


// *******************************************退货/款提醒给固定手机号发送提醒短信**************************************************
//        SmsSendVO smsSendVO = new SmsSendVO();
//        RefundDO refund = refundChangeMsg.getRefund();
//        OrderDetailDTO orderDetailDTO = orderClient.getModel(refund.getOrderSn());
//        smsSendVO.setMobile(orderDetailDTO.getShipTel());
//        String siteSettingJson = settingClient.get(SettingGroup.SITE);
//        //获取当前下单会员信息
//        Member member = memberClient.getModel(refund.getMemberId());
//        //获取当前店铺所有者的联系方式
//        ShopVO shopVO = shopClient.getShop(refund.getSellerId());
//        SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson, SiteSetting.class);
//        //退货/款提醒
//        if (refundChangeMsg.getRefundStatusEnum().equals(RefundStatusEnum.APPLY)) {
//            MessageTemplateDO messageTemplate = null;
//            Map<String, Object> valuesMap = new HashMap<>(4);
//            valuesMap.put("refundSn", refund.getSn());
//            valuesMap.put("siteName", siteSetting.getSiteName());
//            // 会员信息发送
//            // 记录会员订单取消信息（会员中心查看）
//            if (refund.getRefuseType().equals(RefuseTypeEnum.RETURN_GOODS.name())) {
//                messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERRETURNUPDATE);
//            }
//            if (refund.getRefuseType().equals(RefuseTypeEnum.RETURN_MONEY.name())) {
//                messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERREFUNDUPDATE);
//            }
//            if (messageTemplate != null) {
//                // 判断短信是否开启
//                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
//                    this.sendSms(this.getSmsMessage(member.getMobile(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
//                }
//            }
//            // 店铺信息发送
//            messageTemplate = null;
//            if (refund.getRefuseType().equals(RefuseTypeEnum.RETURN_GOODS.name())) {
//                messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPRETURN);
//            }
//            if (refund.getRefuseType().equals(RefuseTypeEnum.RETURN_MONEY.name())) {
//                messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPREFUND);
//            }
//            // 记录店铺订单取消信息（商家中心查看）
//            if (messageTemplate != null) {
//                // 判断短信是否开启
//                if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
//                    this.sendSms(this.getSmsMessage(shopVO.getLinkPhone(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));
//                }
//            }
//            // 取消订单，申请退款，申请退货后，发送提示退款审核的消息给客服---》只有商城的订单发送短信提醒
//            // 1.判断是不是社区团购的订单，社区团购的订单不发短信提醒
//            Integer sellerId = refund.getSellerId();
//            List<DictDTO> sellerIdDictList = DictUtils.getDictList("", SHETUAN_SELLER_ID);
//            if (!CollectionUtils.isEmpty(sellerIdDictList)) {
//                for (DictDTO sellerIdDict : sellerIdDictList) {
//                    if (sellerId.toString().equals(sellerIdDict.getValue())) {
//                        return;
//                    }
//                }
//            }
//            // 2.获取接收消息客服的手机号
//            List<DictDTO> mobileDictList = DictUtils.getDictList("", CUSTOMER_SERVICE_MOBILE);
//            if (CollectionUtils.isEmpty(mobileDictList)) {
//                return;
//            }
//            // 3.获取消息模板，消息内容
//            MessageTemplateDO approvalMessageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPREFUNDAPPROVAL);
//            if (approvalMessageTemplate == null) {
//                return;
//            }
//            // 判断短信是否开启
//            if (approvalMessageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
//                // 4.构建参数
//                valuesMap.clear();
//                valuesMap.put("shopName", refund.getSellerName());
//                valuesMap.put("orderSn", refund.getOrderSn());
//                // 5.发送短信
//                for (DictDTO mobileDict : mobileDictList) {
//                    this.sendSms(this.getSmsMessage(mobileDict.getValue(), this.replaceContent(approvalMessageTemplate.getSmsContent(), valuesMap)));
//                }
//            }
//
//        }

    }

    @Override
    public void goodsChange(GoodsChangeMsg goodsChangeMsg) {
        SmsSendVO smsSendVO = new SmsSendVO();
        //商品审核失败提醒
        String siteSettingJson = settingClient.get(SettingGroup.SITE);

        SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson, SiteSetting.class);
        if (GoodsChangeMsg.GOODS_VERIFY_FAIL == goodsChangeMsg.getOperationType()) {
            //发送店铺消息
            for (Integer goodsId : goodsChangeMsg.getGoodsIds()) {
                CacheGoods goods = goodsClient.getFromCache(goodsId);
                ShopVO shop = shopClient.getShop(goods.getSellerId());
                smsSendVO.setMobile(shop.getLinkPhone());
                // 记录店铺订单取消信息（商家中心查看）
                MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPGOODSVERIFY);
                if (messageTemplate != null) {
                    // 判断短信是否开启
                    if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                        Map<String, Object> valuesMap = new HashMap<>(4);
                        valuesMap.put("siteName", siteSetting.getSiteName());
                        valuesMap.put("name", goods.getGoodsName());
                        valuesMap.put("message", goodsChangeMsg.getMessage());
                        smsSendVO.setContent(this.replaceContent(messageTemplate.getSmsContent(), valuesMap));
                        this.sendSms(smsSendVO);
                    }
                }
            }
        }
        //商品下架消息提醒
        if (GoodsChangeMsg.UNDER_OPERATION == goodsChangeMsg.getOperationType() && !StringUtil.isEmpty(goodsChangeMsg.getMessage())) {
            //发送店铺消息
            for (Integer goodsId : goodsChangeMsg.getGoodsIds()) {
                CacheGoods goods = goodsClient.getFromCache(goodsId);
                ShopVO shop = shopClient.getShop(goods.getSellerId());
                smsSendVO.setMobile(shop.getLinkPhone());
                // 记录店铺订单取消信息（商家中心查看）
                MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPGOODSMARKETENABLE);
                if (messageTemplate != null) {
                    // 判断短信是否开启
                    if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                        Map<String, Object> valuesMap = new HashMap<>(4);
                        valuesMap.put("siteName", siteSetting.getSiteName());
                        valuesMap.put("name", goods.getGoodsName());
                        valuesMap.put("reason", goodsChangeMsg.getMessage());
                        smsSendVO.setContent(this.replaceContent(messageTemplate.getSmsContent(), valuesMap));
                        this.sendSms(smsSendVO);
                    }
                }
            }
        }
    }

    @Override
    public void memberLogin(MemberLoginMsg memberLoginMsg) {

        String siteSettingJson = settingClient.get(SettingGroup.SITE);

        SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson, SiteSetting.class);
        Member member = memberClient.getModel(memberLoginMsg.getMemberId());

        SmsSendVO smsSendVO = new SmsSendVO();
        smsSendVO.setMobile(member.getMobile());

        MessageTemplateDO messageTemplate;

        // 记录会员登录成功信息（会员中心查看）
         messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERLOGINSUCCESS);
        // 判断站内信是否开启
        if (messageTemplate != null) {
            // 判断短信是否开启
            if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                Map<String, Object> valuesMap = new HashMap<>(4);
                valuesMap.put("name", member.getUname());
                valuesMap.put("loginTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));
                valuesMap.put("siteName", siteSetting.getSiteName());
                smsSendVO.setContent(this.replaceContent(messageTemplate.getSmsContent(), valuesMap));
                this.sendSms(smsSendVO);
            }
        }
    }

    @Override
    public void memberRegister(MemberRegisterMsg memberRegisterMsg) {
        String siteSettingJson = settingClient.get(SettingGroup.SITE);

        SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson, SiteSetting.class);
        Member member = memberRegisterMsg.getMember();
        SmsSendVO smsSendVO = new SmsSendVO();
        smsSendVO.setMobile(member.getMobile());

        //会员注册成功提醒
        MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.MEMBERREGISTESUCCESS);
        if (messageTemplate != null) {
            // 判断短信是否开启
            if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                Map<String, Object> valuesMap = new HashMap<String, Object>(4);
                valuesMap.put("name", member.getUname());
                valuesMap.put("siteName", siteSetting.getSiteName());
                valuesMap.put("loginTime", DateUtil.toString(DateUtil.getDateline(), "yyyy-MM-dd"));
                smsSendVO.setContent(this.replaceContent(messageTemplate.getSmsContent(), valuesMap));
                this.sendSms(smsSendVO);
            }
        }
    }

    @Override
    public void onTradeIntoDb(TradeVO tradeVO) {
        OrderIntoDbReceiver.THREAD_POOL.execute(() -> {
            String siteSettingJson = settingClient.get(SettingGroup.SITE);
            SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson, SiteSetting.class);
            //店铺新订单创建提醒
            List<OrderDTO> orderList = tradeVO.getOrderList();
            SmsSendVO smsSendVO = new SmsSendVO();
            MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSNEW);
            if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                for (OrderDTO orderDTO : orderList) {
                    // 判断是否开启
                    ShopVO shop = shopClient.getShop(orderDTO.getSellerId());
                    Map<String, Object> valuesMap = new HashMap<>(4);
                    valuesMap.put("ordersSn", orderDTO.getSn());
                    valuesMap.put("createTime", DateUtil.toString(orderDTO.getCreateTime(), "yyyy-MM-dd"));
                    valuesMap.put("siteName", siteSetting.getSiteName());
                    smsSendVO.setMobile(shop.getLinkPhone());
                    smsSendVO.setContent(this.replaceContent(messageTemplate.getSmsContent(), valuesMap));
                    this.sendSms(smsSendVO);
                }
            }
        });
    }

    /**
     * 商品评论
     *
     * @param goodsCommentMsg 商品评论消息
     */
    @Override
    public void goodsComment(GoodsCommentMsg goodsCommentMsg) {
        if(goodsCommentMsg.getComment() == null || goodsCommentMsg.getComment().isEmpty()){
            return;
        }
        String siteSettingJson = settingClient.get(SettingGroup.SITE);
        SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson, SiteSetting.class);
        //获取坪林的消息模板
        MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSEVALUATE);
        if (messageTemplate != null) {
            if (messageTemplate.getSmsState().equals(MessageOpenStatusEnum.OPEN.value())) {
                goodsCommentMsg.getComment().forEach(comment -> {
                    Map<String, Object> valuesMap = new HashMap<>(4);
                    valuesMap.put("siteName", siteSetting.getSiteName());
                    valuesMap.put("ordersSn", comment.getSellerId());
                    valuesMap.put("userName", comment.getMemberName());
                    valuesMap.put("evalTime", DateUtil.toString(comment.getCreateTime(), "yyyy-MM-dd"));
                    //获取当前店铺所有者的联系方式
                    ShopVO shopVO = shopClient.getShop(comment.getSellerId());
                    this.sendSms(this.getSmsMessage(shopVO.getLinkPhone(), this.replaceContent(messageTemplate.getSmsContent(), valuesMap)));

                });
            }
        }
    }

    /**
     * 替换短信中的内容
     *
     * @param content 短信
     * @param map     替换的文本内容
     * @return 替换后的文本
     */
    private String replaceContent(String content, Map map) {
        StrSubstitutor strSubstitutor = new StrSubstitutor(map);
        return strSubstitutor.replace(content);
    }

    /**
     * 组织短信发送的相关信息
     *
     * @param mobile  手机号
     * @param content 内容
     * @return 短信业务传递参数vo
     */
    private SmsSendVO getSmsMessage(String mobile, String content) {
        SmsSendVO smsSendVO = new SmsSendVO();
        smsSendVO.setMobile(mobile);
        smsSendVO.setContent(content);
        return smsSendVO;
    }


}
