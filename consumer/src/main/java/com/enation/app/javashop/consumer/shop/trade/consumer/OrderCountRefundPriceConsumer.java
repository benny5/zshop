package com.enation.app.javashop.consumer.shop.trade.consumer;

import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.shop.message.WechatMiniproMessageConsumer;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.client.member.ShopClient;
import com.enation.app.javashop.core.client.trade.BillClient;
import com.enation.app.javashop.core.client.trade.OrderClient;
import com.enation.app.javashop.core.distribution.model.dos.DistributionDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionOrderDO;
import com.enation.app.javashop.core.distribution.service.DistributionManager;
import com.enation.app.javashop.core.distribution.service.DistributionOrderManager;
import com.enation.app.javashop.core.member.model.dos.LeaderDO;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.enums.CommissionTypeEnum;
import com.enation.app.javashop.core.member.service.LeaderManager;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.orderbill.model.dos.BillItem;
import com.enation.app.javashop.core.orderbill.model.enums.BillType;
import com.enation.app.javashop.core.orderbill.service.BillItemManager;
import com.enation.app.javashop.core.orderbill.service.SettleAccountsManager;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanOrderDO;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanOrderManager;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.system.enums.WechatMiniproTemplateTypeEnum;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderProfitDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.PaymentTypeEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.trade.order.service.OrderProfitManager;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.util.CurrencyUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 订单状态改变消费
 * 订单付款后修改订单项的可退款金额
 * @author duanmingyu
 * @version v1.0
 * @Description:
 * @since v7.1
 * @date 2019-05-10
 */
@Component
public class OrderCountRefundPriceConsumer implements OrderStatusChangeEvent {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private OrderClient orderClient;
    @Autowired
    private DistributionOrderManager distributionOrderManager;
    @Autowired
    private BillItemManager billItemManager;
    @Autowired
    private BillClient billClient;

    @Override
    public void orderChange(OrderStatusChangeMsg orderStatusChangeMsg) {
        try {
            OrderDO orderDO = orderStatusChangeMsg.getOrderDO();
            String paymentType = orderDO.getPaymentType();
            OrderStatusEnum orderStatus = orderStatusChangeMsg.getNewStatus();
            //在线支付&&订单已支付
            boolean online = PaymentTypeEnum.ONLINE.value().equals(paymentType) && OrderStatusEnum.PAID_OFF.equals(orderStatus);
            //货到付款&&订单已收货
            boolean cod = PaymentTypeEnum.COD.value().equals(paymentType) && OrderStatusEnum.ROG.equals(orderStatus);
            //在线支付&&订单已支付 或者  货到付款&&订单已收货
            if (online || cod) {
                //  更新订单项可退款金额
                this.orderClient.addOrderItemRefundPrice(orderStatusChangeMsg.getOrderDO());

                //解决订单状态改变重复生成es_bill_item和es_order_profit
                if(orderStatusChangeMsg.getNewStatus().equals(OrderStatusEnum.PAID_OFF) && !orderDO.getOrderType().equals(OrderTypeEnum.pintuan.name())){
                    // 真正防止重复生成
                    BillItem oldBillItem = billItemManager.getModelByOrderSnAndItemType(orderDO.getSn(), BillType.PAYMENT.name());
                    if(oldBillItem != null){
                        return;
                    }

                    BillItem billItem = billClient.buildBillItem(orderDO.getSn(), BillType.PAYMENT);

                    distributionOrderManager.calculateDistributionCommission(orderDO,billItem);

                    this.billClient.add(billItem);
                }
            }
        } catch (Exception e) {
            logger.error("订单变更消息异常:",e);
            e.printStackTrace();
        }
    }

}
