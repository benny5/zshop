package com.enation.app.javashop.consumer.job.handler;

import com.enation.app.javashop.core.base.JobAmqpExchange;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 每5分钟执行
 */
@Component
public class EveryFiveMinExecuteJobHandler {


    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AmqpTemplate amqpTemplate;
    @XxlJob(value = "everyFiveMinExecuteJobHandler")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            amqpTemplate.convertAndSend(JobAmqpExchange.EVERY_5_MIN_EXECUTE,
                    JobAmqpExchange.EVERY_5_MIN_EXECUTE + "_ROUTING",
                    "");
        } catch (Exception e) {
            this.logger.error("每5分钟任务AMQP消息发送异常：", e);
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }
}

