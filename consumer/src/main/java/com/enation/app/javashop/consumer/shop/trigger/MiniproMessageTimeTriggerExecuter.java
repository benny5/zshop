package com.enation.app.javashop.consumer.shop.trigger;

import com.enation.app.javashop.core.system.model.dto.MiniproSendMsgDTO;
import com.enation.app.javashop.core.system.service.WechatMiniproTemplateManager;
import com.enation.app.javashop.framework.trigger.Interface.TimeTriggerExecuter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 小程序订阅消息延迟加载执行器
 */
@Component("miniproMessageTimeTriggerExecuter")
public class MiniproMessageTimeTriggerExecuter implements TimeTriggerExecuter {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private WechatMiniproTemplateManager wechatMiniproTemplateManager;

    /**
     * 小程序订阅消息延迟加载执行器
     */
    @Override
    public void execute(Object object) {
        try {
            MiniproSendMsgDTO miniproSendMsgDTO = (MiniproSendMsgDTO) object;
            wechatMiniproTemplateManager.send(miniproSendMsgDTO);
        } catch (Exception e) {
            logger.error("小程序消息发送失败", e);
        }
    }
}
