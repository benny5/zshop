package com.enation.app.javashop.consumer.shop.trigger;

import com.enation.app.javashop.core.system.model.dos.WxPublicTemplate;
import com.enation.app.javashop.core.system.model.dto.MiniproSendMsgDTO;
import com.enation.app.javashop.core.system.service.WechatPublicManager;
import com.enation.app.javashop.framework.trigger.Interface.TimeTriggerExecuter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 王志杨
 * @since 2020年11月11日 19:12:41
 * 微信公众号消息延迟加载执行器
 */
@Component("publicMessageTimeTriggerExecuter")
public class PublicMessageTimeTriggerExecuter implements TimeTriggerExecuter {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private WechatPublicManager wechatPublicManager;

    /**
     *  微信公众号消息延迟加载执行器
     */
    @Override
    public void execute(Object object) {
        try {
            WxPublicTemplate wxPublicTemplate = (WxPublicTemplate) object;
            wechatPublicManager.publicSendMessage(wxPublicTemplate);
        } catch (Exception e) {
            logger.error("微信公众号模板消息发送失败", e);
        }
    }
}
