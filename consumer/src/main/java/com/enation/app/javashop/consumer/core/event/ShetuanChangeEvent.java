package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.message.ShetuanChangeMsg;

/**
 * @author JFeng
 * @date 2020/6/3 13:40
 */
public interface ShetuanChangeEvent {

    void shetuanChange(ShetuanChangeMsg shetuanChangeMsg);
}
