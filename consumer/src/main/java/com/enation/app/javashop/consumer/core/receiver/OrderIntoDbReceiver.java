package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.TradeIntoDbEvent;
import com.enation.app.javashop.consumer.shop.goods.GoodsStockLockConsumer;
import com.enation.app.javashop.consumer.shop.trade.consumer.AccountFreezeConsumer;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;
import com.enation.app.javashop.framework.cache.Cache;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 订单入库消息处理
 *
 * @author Snow create in 2018/5/10
 * @version v2.0
 * @since v7.0.0
 */
@Component
public class OrderIntoDbReceiver {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired(required = false)
    private List<TradeIntoDbEvent> events;

    @Autowired
    private Cache cache;

    /** 关闭下单无用功能 */
    public static final boolean IS_CLOSE = true;
    /** 下单线程池 */
    public static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(20);

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.ORDER_CREATE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.ORDER_CREATE, type = ExchangeTypes.FANOUT)
    ))
    public void tradeIntoDb(String tradeVOKey) {
        logger.info("【消息内容】:"+tradeVOKey);

        TradeVO tradeVO = (TradeVO) this.cache.get(tradeVOKey);
        if (events != null) {
            for (TradeIntoDbEvent event : events){
                if (event instanceof AccountFreezeConsumer) {
                    try {
                        event.onTradeIntoDb(tradeVO);
                    } catch (Exception e) {
                        logger.error("冻结账户金额出错", e);
                    }
                    break;
                }
            }

            for (TradeIntoDbEvent event : events){
                if (event instanceof GoodsStockLockConsumer) {
                    try {
                        event.onTradeIntoDb(tradeVO);
                    } catch (Exception e) {
                        logger.error("扣减库存出错", e);
                    }
                    break;
                }
            }

            for (TradeIntoDbEvent event : events) {
                try {
                    if (event instanceof AccountFreezeConsumer || event instanceof GoodsStockLockConsumer) {
                        continue;
                    }
                    event.onTradeIntoDb(tradeVO);
                } catch (Exception e) {
                    logger.error("交易入库消息出错", e);
                } finally {
                    cache.remove(tradeVOKey);
                }
            }
        }

    }

}
