package com.enation.app.javashop.consumer.shop.trade.consumer;

import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.ShopChangeEvent;
import com.enation.app.javashop.consumer.core.event.ShopStatusChangeEvent;
import com.enation.app.javashop.consumer.core.receiver.ShopChangeReceiver;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.RefundTypeEnum;
import com.enation.app.javashop.core.app.JPushService;
import com.enation.app.javashop.core.app.model.PushNotice;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.base.message.ShopStatusChangeMsg;
import com.enation.app.javashop.core.client.system.MessageTemplateClient;
import com.enation.app.javashop.core.shop.model.enums.ShopStatusEnum;
import com.enation.app.javashop.core.shop.model.vo.ShopChangeMsg;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.system.enums.MessageCodeEnum;
import com.enation.app.javashop.core.system.model.dos.MessageTemplateDO;
import com.enation.app.javashop.core.trade.cart.model.dos.OrderPermission;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ServiceStatusEnum;
import com.enation.app.javashop.core.trade.order.model.vo.CancelVO;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.core.trade.sdk.model.OrderSkuDTO;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.JsonUtil;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单变更通知
 *
 * @author JFENG
 */
@Component
public class JPushConsumer implements OrderStatusChangeEvent , RefundStatusChangeEvent, ShopChangeEvent {

    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private JPushService pushService;

    @Autowired
    private MessageTemplateClient messageTemplateClient;

    /**
     * 订单通知
     * @param orderMessage
     */
    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        //订单已付款
        if(orderMessage.getNewStatus().name().equals(OrderStatusEnum.PAID_OFF.name())){
            // 店铺订单支付提醒
            OrderDO orderDO = orderMessage.getOrderDO();
            Map<String, Object> valuesMap = new HashMap<String, Object>(4);
            valuesMap.put("ordersSn", orderDO.getSn());
            valuesMap.put("paymentTime", DateUtil.toString(orderDO.getPaymentTime(), "yyyy-MM-dd"));

            MessageTemplateDO messageTemplate = messageTemplateClient.getModel(MessageCodeEnum.SHOPORDERSPAY);
            StrSubstitutor strSubstitutor = new StrSubstitutor(valuesMap);
            String content = strSubstitutor.replace(messageTemplate.getContent());

            PushNotice pushNotice = new PushNotice();
            pushNotice.setPlatform(new String[]{});
            pushNotice.setAudience(new String[]{orderDO.getSellerId()+""});
            pushNotice.setTitle("新订单");
            pushNotice.setContent(content);
            pushNotice.setOptions(true);
            pushNotice.setType(2);
            pushService.jpush(pushNotice);
        }

        // TODO: 2020/8/31 订单申请退款

    }


    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {
        RefundDO refund = refundChangeMsg.getRefund();
        RefundStatusEnum refundStatusEnum = refundChangeMsg.getRefundStatusEnum();
        if (RefundStatusEnum.APPLY.equals(refundStatusEnum)) {
            PushNotice pushNotice=new PushNotice();
            pushNotice.setPlatform(new String[]{});
            pushNotice.setAudience(new String[]{refund.getSellerId()+""});
            pushNotice.setTitle("售后订单");
            pushNotice.setContent(refund.getOrderSn()+"  "+refund.getRefundReason());
            pushNotice.setOptions(true);
            pushNotice.setType(3);
            pushService.jpush(pushNotice);
        }

    }

    @Override
    public void shopChange(ShopChangeMsg shopChangeMsg) {
        ShopVO newShop = shopChangeMsg.getNewShop();
        ShopVO originalShop = shopChangeMsg.getOriginalShop();
        if((originalShop.getShopDisable().equals(ShopStatusEnum.APPLYING.name())||originalShop.getShopDisable().equals(ShopStatusEnum.APPLY.name()))
                && newShop.getShopDisable().equals(ShopStatusEnum.REFUSED.name())){
            PushNotice pushNotice=new PushNotice();
            pushNotice.setPlatform(new String[]{});
            pushNotice.setAudience(new String[]{newShop.getShopId()+""});
            pushNotice.setTitle("店铺审核");
            pushNotice.setContent("店铺审核结果:"+newShop.getShopDisable());
            pushNotice.setOptions(true);
            if (newShop.getShopDisable().equals(ShopStatusEnum.REFUSED)) {
                //店铺审核成功
                pushNotice.setType(1);
            } else {
                //店铺审核失败
                pushNotice.setType(4);
            }
            pushService.jpush(pushNotice);
        }
    }
}
