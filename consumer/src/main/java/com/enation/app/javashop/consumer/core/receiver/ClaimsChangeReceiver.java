package com.enation.app.javashop.consumer.core.receiver;

import com.enation.app.javashop.consumer.core.event.ClaimsChangeEvent;
import com.enation.app.javashop.consumer.core.event.ShopChangeEvent;
import com.enation.app.javashop.core.aftersale.model.vo.ClaimsChangeMsg;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.shop.model.vo.ShopChangeMsg;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 理赔单变更消费者
 */
@Component
public class ClaimsChangeReceiver {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired(required = false)
    private List<ClaimsChangeEvent> events;

    /**
     * 理赔单消息处理
     *
     * @param claimsChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.CLAIMS_STATUS_CHANGE + "_ROUTING"),
            exchange = @Exchange(value = AmqpExchange.CLAIMS_STATUS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void shopChange(ClaimsChangeMsg claimsChangeMsg) {

        if (events != null) {
            for (ClaimsChangeEvent event : events) {
                try {
                    event.claimsChange(claimsChangeMsg);
                } catch (Exception e) {
                    logger.error("理赔单变更消息出错", e);
                }
            }
        }

    }

}
