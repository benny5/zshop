package com.enation.app.javashop.consumer.shop.message;

import com.enation.app.javashop.consumer.core.event.OrderStatusChangeEvent;
import com.enation.app.javashop.consumer.core.event.PintuanSuccessEvent;
import com.enation.app.javashop.consumer.core.event.RefundStatusChangeEvent;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.dos.RefundGoodsDO;
import com.enation.app.javashop.core.aftersale.model.enums.*;
import com.enation.app.javashop.core.aftersale.service.AfterSaleManager;
import com.enation.app.javashop.core.base.message.OrderStatusChangeMsg;
import com.enation.app.javashop.core.base.message.RefundChangeMsg;
import com.enation.app.javashop.core.member.model.dos.ConnectDO;
import com.enation.app.javashop.core.member.model.enums.ConnectTypeEnum;
import com.enation.app.javashop.core.member.service.ConnectManager;
import com.enation.app.javashop.core.payment.plugin.weixin.signaturer.WechatSignaturer;
import com.enation.app.javashop.core.promotion.pintuan.model.PintuanOrderDetailVo;
import com.enation.app.javashop.core.promotion.pintuan.service.PintuanOrderManager;
import com.enation.app.javashop.core.system.enums.WechatMsgTemplateTypeEnum;
import com.enation.app.javashop.core.system.sendMessage.WechatSendMessage;
import com.enation.app.javashop.core.system.service.WechatMsgTemplateManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 微信发送模板消息
 *
 * @author fk
 * @version v7.1.4
 * @since v7.1.4
 * 2019年6月18日 下午3:15:01
 */
@Component
public class WechatMessageConsumer implements OrderStatusChangeEvent, RefundStatusChangeEvent, PintuanSuccessEvent {

    @Autowired
    private WechatMsgTemplateManager wechatMsgTemplateManager;
    @Autowired
    private WechatSignaturer wechatSignaturer;
    @Autowired
    private WechatSendMessage wechatSendMessage;
    @Autowired
    private AfterSaleManager afterSaleManager;
    @Autowired
    private PintuanOrderManager pintuanOrderManager;
    @Autowired
    private OrderQueryManager orderQueryManager;
    @Autowired
    private ConnectManager connectManager;
    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport daoSupport;

    /**
     * 订单状态改变
     *
     * @param orderMessage
     */
    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        OrderStatusEnum status = orderMessage.getNewStatus();
        OrderDO order = orderMessage.getOrderDO();
        //获取会员的openid
        String openId = wechatSignaturer.getMemberOpenid(order.getMemberId());
        WechatMsgTemplateTypeEnum noticeEnum = null;
        List<Object> keywords = new ArrayList<>();
        //商品信息
        String itemsJson = order.getItemsJson();
        List<OrderSkuVO> list = JsonUtil.jsonToList(itemsJson, OrderSkuVO.class);
        switch (status) {
            case PAID_OFF:
                //订单支付成功通知
                noticeEnum = WechatMsgTemplateTypeEnum.PAY_NOTICE;
                keywords.add(order.getMemberName());
                keywords.add(order.getSn());
                keywords.add(String.format("%.2f", order.getOrderPrice())+"元");
                //商品信息
                StringBuffer goodsInfo = new StringBuffer();
                for (OrderSkuVO sku : list) {
                    goodsInfo.append(sku.getName() + "*" + sku.getNum() + " ");
                }
                keywords.add(goodsInfo);
                break;
            case SHIPPED:
                //订单发货提醒
                noticeEnum = WechatMsgTemplateTypeEnum.SHIP_NOTICE;
                keywords.add(order.getSn());
                keywords.add(order.getLogiName());
                keywords.add(order.getShipNo());
                break;
            case ROG:
                //确认收货通知
                noticeEnum = WechatMsgTemplateTypeEnum.ROG_NOTICE;
                keywords.add(order.getSn());
                //商品信息
                StringBuffer goodsInfoRog = new StringBuffer();
                for (OrderSkuVO sku : list) {
                    goodsInfoRog.append(sku.getName() + "*" + sku.getNum() + " ");
                }
                keywords.add(goodsInfoRog);
                keywords.add(DateUtil.toString(order.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                keywords.add(DateUtil.toString(order.getShipTime(), "yyyy-MM-dd HH:mm:ss"));
                keywords.add(DateUtil.toString(order.getSigningTime(), "yyyy-MM-dd HH:mm:ss"));
                break;
            case CANCELLED:
                //订单取消通知
                noticeEnum = WechatMsgTemplateTypeEnum.CANCEL_NOTICE;
                keywords.add(order.getSn());
                //商品信息
                StringBuffer goodsInfoCancel = new StringBuffer();
                for (OrderSkuVO sku : list) {
                    goodsInfoCancel.append(sku.getName() + "*" + sku.getNum() + " ");
                }
                keywords.add(goodsInfoCancel);
                keywords.add(order.getCancelReason());
                break;
            default:
        }
        if (noticeEnum != null) {
            wechatMsgTemplateManager.send(openId, noticeEnum, keywords.toArray());
        }
    }

    /**
     * 退货退款消息
     *
     * @param refundChangeMsg
     */
    @Override
    public void refund(RefundChangeMsg refundChangeMsg) {

        RefundStatusEnum refundStatus = refundChangeMsg.getRefundStatusEnum();
        RefundDO refund = refundChangeMsg.getRefund();
        //退货审核通知
        if (RefundStatusEnum.PASS.equals(refundStatus) && RefuseTypeEnum.RETURN_GOODS.name().equals(refund.getRefuseType())) {

            //获取会员的openid
            String openId = wechatSignaturer.getMemberOpenid(refund.getMemberId());
            List<Object> keywords = new ArrayList<>();

            keywords.add("审核通过");
            //商品信息
            List<RefundGoodsDO> goodsDetail = afterSaleManager.getRefundGoods(refund.getSn());
            StringBuffer goodsInfo = new StringBuffer();
            for (RefundGoodsDO goods : goodsDetail) {
                goodsInfo.append(goods.getGoodsName() + "*" + goods.getReturnNum() + " ");
            }
            keywords.add(goodsInfo);
            //审核时间
            keywords.add(DateUtil.toString(new Date(), "yyyy-MM-dd HH:mm:ss"));
            //退款通知
            wechatMsgTemplateManager.send(openId, WechatMsgTemplateTypeEnum.REFUND_GOOD_NOTICE, keywords.toArray());
            return;
        }

        //退款通知
        if (RefundStatusEnum.REFUNDING.equals(refundStatus)) {

            //获取会员的openid
            String openId = wechatSignaturer.getMemberOpenid(refund.getMemberId());

            // 1.拼团订单自动取消发送微信小程序订阅消息提醒
            OrderDetailDTO orderDetailDTO = orderQueryManager.getModel(refund.getOrderSn());
            if (OrderTypeEnum.pintuan.name().equals(orderDetailDTO.getOrderType())) {
                // 获取拼团VO，用于传递发送消息字段
                ConnectDO connectDO = connectManager.getConnect(refund.getMemberId(), ConnectTypeEnum.WECHAT.value());
                PintuanOrderDetailVo pintuanOrderDetailVo = pintuanOrderManager.getMainOrderBySn(orderDetailDTO.getSn());
                wechatSendMessage.sendGroupWorkMessage(pintuanOrderDetailVo, connectDO.getOpenId(), false, refund.getMemberId());
            }

            List<Object> keywords = new ArrayList<>();
            //商品信息
            List<RefundGoodsDO> goodsDetail = afterSaleManager.getRefundGoods(refund.getSn());
            StringBuffer goodsInfo = new StringBuffer();
            for (RefundGoodsDO goods : goodsDetail) {
                goodsInfo.append(goods.getGoodsName() + "*" + goods.getReturnNum() + " ");
            }
            keywords.add(goodsInfo);
            //订单号
            keywords.add(refund.getOrderSn());
            //退款金额
            keywords.add(refund.getRefundPrice());
            //退款方式  原路退回或者线下支付
            String refundWay = RefundWayEnum.valueOf(refund.getRefundWay()).description();

            if (!StringUtil.isEmpty(refund.getAccountType())) {
                String text = AccountTypeEnum.valueOf(refund.getAccountType()).description();
                keywords.add(refundWay + "-" + text);
            } else {
                keywords.add("未知");
            }
            // 2.退款通知
            wechatMsgTemplateManager.send(openId, WechatMsgTemplateTypeEnum.RETUND_NOTICE, keywords.toArray());
        }


    }

    /**
     * 发送拼团成功消息
     *
     * @param pintuanOrderId
     */
    @Override
    public void success(Integer pintuanOrderId) {
        // 1.拼团主id查询相关的拼团订单，循环给每个人发送消息
        String sql = "select o.* from es_pintuan_child_order pc inner join es_order o on pc.order_sn = o.sn where pc.order_id = ? ";
        List<OrderDO> orders = this.daoSupport.queryForList(sql, OrderDO.class, pintuanOrderId);
        // 2.获取拼团VO，用于传递发送消息字段
        PintuanOrderDetailVo pintuanOrderDetailVo = pintuanOrderManager.getMainOrderBySn(orders.get(0).getSn());
        for (OrderDO order : orders) {
            // 3.获取openID发送消息
            ConnectDO connectDO = connectManager.getConnect(order.getMemberId(), ConnectTypeEnum.WECHAT.value());
            wechatSendMessage.sendGroupWorkMessage(pintuanOrderDetailVo, connectDO.getOpenId(), true, order.getMemberId());
        }


//        *************************************原有发送消息的方法***************************************
//        查询订单
//        for (OrderDO order : orders) {
//
//            String openId = wechatSignaturer.getMemberOpenid(order.getMemberId());
//
//            List<Object> keywords = new ArrayList<>();
//            //商品信息
//            String itemsJson = order.getItemsJson();
//            List<OrderSkuVO> list = JsonUtil.jsonToList(itemsJson, OrderSkuVO.class);
//            StringBuffer goodsInfo = new StringBuffer();
//            for (OrderSkuVO sku : list) {
//                goodsInfo.append(sku.getName() + "*" + sku.getNum() + " ");
//            }
//            keywords.add(goodsInfo);
//            keywords.add(order.getSn());
//            keywords.add(order.getOrderPrice());
//            keywords.add(DateUtil.toString(order.getPaymentTime(), "yyyy-MM-dd HH:mm:ss"));
//
//            wechatMsgTemplateManager.send(openId, WechatMsgTemplateTypeEnum.PINTUAN_NOTICE, keywords.toArray());
//        }


    }

}
