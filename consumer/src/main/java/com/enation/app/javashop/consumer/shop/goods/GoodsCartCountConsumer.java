package com.enation.app.javashop.consumer.shop.goods;

import com.enation.app.javashop.consumer.core.event.GoodsCartCountEvent;
import com.enation.app.javashop.core.base.message.GoodsCartCountMsg;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.statistics.model.dto.GoodsCartData;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/9/11
 * @Description: 购物车商品加购统计实际方法
 */
@Component
public class GoodsCartCountConsumer implements GoodsCartCountEvent {

    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    @Qualifier("sssDaoSupport")
    private DaoSupport daoSupport;

    @Override
    public void goodsCartCount(GoodsCartCountMsg goodsCartCountMsg) {
        GoodsSkuVO goodsSkuVO = goodsCartCountMsg.getSkuVO();
        LocalDateTime nowTime = LocalDateTime.now();
        int year = nowTime.getYear();
        int month=nowTime.getMonthValue();
        int day=nowTime.getDayOfMonth();
        String updateSql="update es_sss_goods_cart_data set cart_goods_num = cart_goods_num+? where goods_id=? and member_id=? and year=? and month=? and day=?";
        int updateState=this.daoSupport.execute(updateSql, goodsCartCountMsg.getNum(),goodsSkuVO.getGoodsId(), goodsCartCountMsg.getMemberId(),year,month,day);
        if(updateState<=0){
            GoodsCartData goodsCart=new GoodsCartData();
            goodsCart.setMemberId(goodsCartCountMsg.getMemberId());
            goodsCart.setGoodsId(goodsSkuVO.getGoodsId());
            goodsCart.setGoodsName(goodsSkuVO.getGoodsName());
            goodsCart.setCartGoodsNum(goodsCartCountMsg.getNum());
            goodsCart.setYear(year);
            goodsCart.setMonth(month);
            goodsCart.setDay(day);
            daoSupport.insert("es_sss_goods_cart_data", goodsCart);
        }
    }
}
