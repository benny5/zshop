package com.enation.app.javashop.consumer.core.event;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/24
 * @Description: 理赔单状态改变
 */
public interface ClaimsStatusChangeEvent {
    /**
     * 理赔单审核成功之后，需要执行的操作
     * @param claimsDOList
     */
    void claimsChange(List<ClaimsDO> claimsDOList);
}
