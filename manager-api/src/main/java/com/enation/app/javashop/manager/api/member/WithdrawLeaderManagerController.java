package com.enation.app.javashop.manager.api.member;

import com.dag.eagleshop.core.account.model.dto.base.PageDTO;
import com.dag.eagleshop.core.account.model.dto.base.ViewPage;
import com.dag.eagleshop.core.account.model.dto.withdraw.MarkTransferredDTO;
import com.dag.eagleshop.core.account.model.dto.withdraw.QueryWithdrawBillDTO;
import com.dag.eagleshop.core.account.model.dto.withdraw.WithdrawBillDTO;
import com.dag.eagleshop.core.account.service.AccountManager;
import com.enation.app.javashop.core.distribution.exception.DistributionErrorCode;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 提现控制器
 * @author 孙建
 */
@Api(description = "提现控制器")
@RestController
@RequestMapping("/admin/leader/members/withdraw")
public class WithdrawLeaderManagerController {

    @Autowired
    private AccountManager accountManager;

    protected final Log logger = LogFactory.getLog(this.getClass());


    @ApiOperation("提现申请审核列表")
    @GetMapping(value = "/apply/list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_size", value = "页码大小", required = false, paramType = "query", dataType = "int", allowMultiple = false),
            @ApiImplicitParam(name = "page_no", value = "页码", required = false, paramType = "query", dataType = "int", allowMultiple = false),
            @ApiImplicitParam(name = "status", value = "状态 全部的话，不要传递参数即可 APPLY:申请中/VIA_AUDITING:审核成功/FAIL_AUDITING:审核失败/RANSFER_ACCOUNTS:已转账 ", required = false, paramType = "query", dataType = "String", allowMultiple = false)
    })
    public ViewPage<WithdrawBillDTO> pageList(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize, String status) {
        // 查询列表
        PageDTO<QueryWithdrawBillDTO> pageDTO = new PageDTO<>();
        pageDTO.setPageNum(pageNo);
        pageDTO.setPageSize(pageSize);
        pageDTO.setBody(new QueryWithdrawBillDTO());
        return accountManager.queryWithdrawList(pageDTO);
    }


    @ApiOperation("标记为已转账")
    @PostMapping(value = "/markTransfer")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "withdraw_bill_id", value = "提现记录id", required = true, paramType = "query", dataType = "String", allowMultiple = false),
            @ApiImplicitParam(name = "remark", value = "备注", required = false, paramType = "query", dataType = "int", allowMultiple = false),
    })
    public void markTransfer(@ApiIgnore String withdrawBillId, String remark) {
        try {
            Buyer buyer = UserContext.getBuyer();
            MarkTransferredDTO markTransferredDTO = new MarkTransferredDTO();
            markTransferredDTO.setWithdrawBillId(withdrawBillId);
            markTransferredDTO.setTransferRemark(remark);
            markTransferredDTO.setOperatorId(String.valueOf(buyer.getUid()));
            markTransferredDTO.setOperatorName(buyer.getUsername());
            accountManager.markTransfer(markTransferredDTO);
        } catch (DistributionException e) {
            logger.error("标记为已转账异常：", e);
            throw e;
        } catch (Exception e) {
            logger.error("标记为已转账异常：", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }

    }
}
