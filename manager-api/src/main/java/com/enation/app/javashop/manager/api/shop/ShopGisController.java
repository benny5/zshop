package com.enation.app.javashop.manager.api.shop;

import com.enation.app.javashop.core.shop.model.dos.ShopGisDO;
import com.enation.app.javashop.core.shop.model.dto.ShopGisDTO;
import com.enation.app.javashop.core.shop.model.vo.ShopGisVO;
import com.enation.app.javashop.core.shop.service.ShopGisManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author 王志杨
 * @Date 2020/8/29 16:06
 * @Descroption 平台自营店铺服务范围控制器
 * @Since
 * @Version
 */
@Api(description = "店铺服务范围API")
@RestController
@RequestMapping("/admin/shops/gis")
@Validated
public class ShopGisController {

    @Autowired
    private ShopGisManager shopGisManager;

    @ApiOperation(value = "添加店铺服务范围", response = ShopGisDTO.class)
    @PostMapping
    public ShopGisDTO insertShopGis(@Valid @RequestBody ShopGisDTO shopGisDTO) {
        shopGisManager.insertShopGis(shopGisDTO);
        return shopGisDTO;
    }

    @ApiOperation(value = "通过shop_id获取店铺服务范围节点坐标集合", response = ShopGisVO.class)
    @GetMapping(value = "/shopPolygon/{shop_id}")
    public ShopGisVO getShopGisByShopId(@PathVariable("shop_id") Integer shop_id) {
        return shopGisManager.getShopGisByShopId(shop_id);
    }

    /**
     * 根据市级地区ID查询范围内所有自提点的服务范围的集合
     * @param areaId
     * @return
     */
    @ApiOperation(value = "根据市级地区ID查询范围内所有自提点的服务范围的集合", response = ShopGisVO.class)
    @GetMapping(value = "/cityPolygon/{areaId}")
    public List<ShopGisVO> getMultiPolygonByAreaId(@PathVariable("areaId") Integer areaId){
        return shopGisManager.getMultiPolygonByAreaId(areaId);
    }


    @ApiOperation(value = "判断买家要求配送位置的经纬坐标属于哪些自提点的配送范围内", response = ShopGisDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coord", value = "收货点的坐标", required = true, allowMultiple = true, dataType = "double", paramType = "path")
    })
    @GetMapping(value = "/belong/{coord}")
    public List<ShopGisVO> getPointBelongMultiPolygon(@PathVariable("coord") Double[] coord) {
        return shopGisManager.checkPointBelongMultiPolygon(coord);
    }


    @ApiOperation(value = "检测指定收货点的坐标是否在shop_id店铺的配送范围", response = Integer.class, responseReference = "200 在范围内，444不在配送范围内")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coord", value = "收货点的坐标", required = true, allowMultiple = true, dataType = "double", paramType = "path"),
            @ApiImplicitParam(name = "shop_id", value = "店铺Id", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping(value = "/{coord}/{shop_id}")
    public Integer getShopGisByShopId(@PathVariable("coord") Double[] coord,
                                      @PathVariable("shop_id") Integer shop_id) {
        return shopGisManager.checkPointWithinMultiPolygon(coord, shop_id);
    }

}
