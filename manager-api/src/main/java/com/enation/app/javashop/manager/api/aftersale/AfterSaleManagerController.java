package com.enation.app.javashop.manager.api.aftersale;

import com.enation.app.javashop.core.aftersale.model.dto.RefundDetailDTO;
import com.enation.app.javashop.core.aftersale.model.vo.*;
import com.enation.app.javashop.core.aftersale.service.AfterSaleManager;
import com.enation.app.javashop.core.goods.model.enums.Permission;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.redis.transactional.RedisTransactional;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zjp
 * @version v7.0
 * @Description 售后相关api
 * @ClassName AfterSaleManagerController
 * @since v7.0 上午10:53 2018/5/10
 */

@Api(description = "售后相关API")
@RestController
@RequestMapping("/admin/after-sales")
@Validated
public class AfterSaleManagerController {

    @Autowired
    private AfterSaleManager afterSaleManager;

    @ApiOperation(value = "系统退款")
    @PostMapping(value = "/systemRefund")
    public void systemRefund(@Valid BuyerRefundApplyVO refundApply){
        this.afterSaleManager.systemRefund(refundApply);
    }

    @ApiOperation(value = "平台审核退款/退货", response = RefundApprovalVO.class)
    @PostMapping(value = "/refunds/{sn}")
    @ApiImplicitParam(name = "sn", value = "退款(货)编号", required =true, dataType = "String" ,paramType="path")
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "refund_approval_",value = "#sn")
    public RefundApprovalVO audit(@Valid RefundApprovalVO refundApply, @PathVariable("sn") @ApiIgnore String sn){
        refundApply.setSn(sn);
        this.afterSaleManager.approval(refundApply, Permission.ADMIN);
        return refundApply;
    }

    @ApiOperation(value = "平台入库操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sn", value = "退款单编号", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "remark", value = "入库备注", required = false, dataType = "String", paramType = "query")
    })
    @PostMapping(value = "/stock-ins/{sn}")
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "stock_in_",value = "#sn")
    public String stockIn(@PathVariable("sn") String sn, String remark) {
        afterSaleManager.sellerStockIn(sn, remark, Permission.ADMIN);
        return "";
    }

    @ApiOperation(value = "管理员的退款(货)详细", response = RefundDetailDTO.class)
    @ApiImplicitParam(name = "sn", value = "退款(货)编号", required =true, dataType = "String" ,paramType="path")
    @GetMapping(value = "/refund/{sn}")
    public RefundDetailDTO adminDetail(@PathVariable("sn")  String sn  ) {
        RefundDetailDTO detail  = this.afterSaleManager.getDetail(sn);
        return detail;
    }

    @ApiOperation(value = "管理员查询全部退货/退款单")
    @GetMapping(value = "/refund")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no" , value = "页码" , required = true , dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size" , value = "分页数" , required = true , dataType = "int", paramType = "query")
    })
    public Page listReturnByAdmin(RefundQueryParamVO queryParam, @ApiIgnore @NotNull(message = "页码不能为空") Integer pageNo, @ApiIgnore @NotNull(message = "每页数量不能为空") Integer pageSize) {
        queryParam.setPageNo(pageNo);
        queryParam.setPageSize(pageSize);
        return this.afterSaleManager.query(queryParam);
    }

    @ApiOperation(value = "退款单导出excel",response = ExportRefundExcelVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start_time" , value = "开始时间" , required = true , dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "end_time" , value = "结束时间" , required = true , dataType = "long", paramType = "query")
    })
    @GetMapping(value = "/exports/excel")
    public List<ExportRefundExcelVO> exportExcel(@ApiIgnore @NotNull(message = "开始时间不能为空") long startTime, @ApiIgnore @NotNull(message = "结束时间不能为空") long endTime){
           return afterSaleManager.exportExcel(startTime,endTime);
    }

    @ApiOperation(value = "批量取消订单-付款之后", response = FinanceRefundApprovalVO.class)
    @PostMapping(value = "/sellerRefund")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn_string", value = "订单编号,用【,】分割", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "cancel_reason", value = "退款原因", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "remark", value = "退款备注", required = false, dataType = "String", paramType = "query")
    })
    public String sellerRefund(String orderSnString, String remark, String cancelReason) {
        String[] orderArray=orderSnString.split(",");
        for(int i=0;i<orderArray.length;i++){
            BuyerCancelOrderVO cancelOrderVO=new BuyerCancelOrderVO();
            cancelOrderVO.setOrderSn(orderArray[i]);
            cancelOrderVO.setRefundReason(cancelReason);
            cancelOrderVO.setCustomerRemark(remark);
            this.afterSaleManager.sysCancelOrder(cancelOrderVO);
        }

        return "";
    }
}
