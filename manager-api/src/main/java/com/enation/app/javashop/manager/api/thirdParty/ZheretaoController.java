package com.enation.app.javashop.manager.api.thirdParty;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.enation.app.javashop.core.thirdParty.model.dto.Result;
import com.enation.app.javashop.core.thirdParty.model.enums.ResultCodeEnum;
import com.enation.app.javashop.core.thirdParty.model.vo.ZheretaoRequestVo;
import com.enation.app.javashop.core.thirdParty.service.ZheretaoManager;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/10/13
 * @Description:
 */
@Api(description = "获取浙里淘商品API")
@RestController
@RequestMapping("/admin/third-party/zheretao")
public class ZheretaoController {
    @Autowired
    private ZheretaoManager zheretaoManager;


    @ApiOperation(value = "分页获取浙里淘数据测试接口")
    @GetMapping
    public void zheretao() {
        zheretaoManager.getZheretaoAllGoods();
    }

    @ApiOperation(value = "更新商品信息")
    @ApiImplicitParam(name = "data", value = "商品参数", required = true, dataType = "ZheretaoRequestVo", paramType = "body")
    @PostMapping(value = "/updateGoods")
    public Result updateGoods(@RequestBody String data) {
        Result result = Result.success();
        if (StringUtil.notEmpty(data)) {
            ZheretaoRequestVo zheretaoRequestVo = JSON.parseObject(data, new TypeReference<ZheretaoRequestVo>() {
            });
            result = zheretaoManager.updateGoods(zheretaoRequestVo);
        } else {
            result.setCode(ResultCodeEnum.E614.getCode());
            result.setMessage(ResultCodeEnum.E615.getDescription());
        }
        return result;
    }


    @ApiOperation(value = "映射浙里淘分类ID和商城的分类ID")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uploadFile", value = "映射文件.xlsx，第一张表格格式为：第一列为浙里淘二级分类的Id，第二列为商城三级分类的名字", paramType = "form", dataType = "__File", required = true)
    })
    @PostMapping(value = "/categoryMapping", headers = "content-type=multipart/form-data")
    public String categoryMapping(@RequestParam("uploadFile") MultipartFile uploadFile) {
        List<Integer> failCell = zheretaoManager.categoryMapping(uploadFile);
        if (!CollectionUtils.isEmpty(failCell)) {
            return "失败的行：" + failCell.toString() + "  失败原因为：未找到该三级分类";
        } else {
            return "映射保存成功！";
        }
    }
}
