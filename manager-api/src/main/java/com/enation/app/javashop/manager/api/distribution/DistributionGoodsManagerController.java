package com.enation.app.javashop.manager.api.distribution;

import com.enation.app.javashop.core.distribution.model.dos.DistributionGoods;
import com.enation.app.javashop.core.distribution.service.DistributionGoodsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 分销商品
 * @author liushuai
 * @version v1.0
 * @since v7.0
 * 2018/8/30 上午9:42
 * @Description:
 *
 */
@RestController
@RequestMapping("/admin/distribution/goods")
@Api(description = "平台分销商品API")
public class DistributionGoodsManagerController {

    @Autowired
    private DistributionGoodsManager distributionGoodsManager;

    @ApiOperation(value = "商品返利配置获取")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goods_id", value = "商品id", required = true, dataType = "int", paramType = "path"),
    })
    @GetMapping(value = "/{goods_id}")
    public DistributionGoods querySetting(@PathVariable("goods_id") Integer goodsId) {
        return distributionGoodsManager.getModel(goodsId);
    }

    @ApiOperation(value = "分销商品返利设置")
    @PutMapping(value = "/batchEditGoods/{goods_ids}")
    public Boolean batchEditGoods(@PathVariable("goods_ids") Integer[] goodsIds,
                                  @RequestParam(name = "subsidy_rate", required = true) Double subsidyRate) {
        if(subsidyRate < -1 || subsidyRate > 100){
            throw new RuntimeException("比例配置范围有误");
        }
        DistributionGoods distributionGoods = new DistributionGoods();
        distributionGoods.setSubsidyRate(subsidyRate);
        distributionGoodsManager.batchEditRate(goodsIds, distributionGoods);
        return true;
    }

}
