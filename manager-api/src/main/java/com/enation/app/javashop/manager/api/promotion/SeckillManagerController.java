package com.enation.app.javashop.manager.api.promotion;

import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillDO;
import com.enation.app.javashop.core.promotion.seckill.model.enums.SeckillGoodsApplyStatusEnum;
import com.enation.app.javashop.core.promotion.seckill.model.enums.SeckillStatusEnum;
import com.enation.app.javashop.core.promotion.seckill.model.enums.SeckillTypeEnum;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillVO;
import com.enation.app.javashop.core.promotion.seckill.service.SeckillManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.exception.SystemErrorCodeV1;
import com.enation.app.javashop.framework.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 限时抢购活动控制器
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-02 17:30:23
 */
@RestController
@RequestMapping("/admin/promotion/seckills")
@Api(description = "限时抢购活动相关API")
@Validated
public class SeckillManagerController	{

	@Autowired
	private SeckillManager seckillManager;


	@ApiOperation(value	= "查询限时抢购列表", response = SeckillDO.class)
	@ApiImplicitParams({
		 @ApiImplicitParam(name	= "page_no",	value =	"页码", dataType = "int",	paramType =	"query"),
		 @ApiImplicitParam(name	= "page_size",	value =	"每页显示数量", dataType = "int",	paramType =	"query")
	})
	@GetMapping
	public Page<SeckillVO> list(@ApiIgnore Integer pageNo,@ApiIgnore Integer pageSize)	{
		return	this.seckillManager.list(pageNo,pageSize,null);
	}


	@ApiOperation(value	= "添加限时抢购入库", response = SeckillVO.class)
	@PostMapping
	public SeckillVO add(@Valid @RequestBody SeckillVO seckill)	{
		seckill.verifyParam(seckill);
		seckill.setSeckillStatus(SeckillStatusEnum.EDITING.name());
		this.seckillManager.add(seckill);
		return	seckill;
	}

	@PutMapping(value = "/{id}")
	@ApiOperation(value	= "修改限时抢购入库", response = SeckillDO.class)
	@ApiImplicitParams({
		 @ApiImplicitParam(name	= "id",	value =	"主键",	required = true, dataType = "int",	paramType =	"path")
	})
	public	SeckillVO edit(@Valid @RequestBody SeckillVO seckill, @PathVariable @NotNull(message = "限时抢购ID参数错误") Integer id) {
		seckill.verifyParam(seckill);
		this.seckillManager.edit(seckill,id);
		return	seckill;
	}


	@DeleteMapping(value = "/{id}")
	@ApiOperation(value	= "删除限时抢购入库")
	@ApiImplicitParams({
		 @ApiImplicitParam(name	= "id",	value =	"要删除的限时抢购入库主键",	required = true, dataType = "int",	paramType =	"path")
	})
	public	String	delete(@PathVariable Integer id) {

		this.seckillManager.delete(id);

		return "";
	}

	@DeleteMapping(value = "/{id}/close")
	@ApiOperation(value	= "关闭限时抢购")
	@ApiImplicitParams({
			@ApiImplicitParam(name	= "id",	value =	"要关闭的限时抢购入库主键",	required = true, dataType = "int",	paramType =	"path")
	})
	public	String	close(@PathVariable Integer id) {

		this.seckillManager.close(id);

		return "";
	}


	@GetMapping(value =	"/{id}")
	@ApiOperation(value	= "查询一个限时抢购入库")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id",	value = "要查询的限时抢购入库主键",	required = true, dataType = "int",	paramType = "path")
	})
	public	SeckillVO get(@PathVariable	Integer	id)	{
		SeckillVO seckillVO = this.seckillManager.getModel(id);
		return	seckillVO;
	}


	@ApiOperation(value = "发布限时抢购活动")
	@PostMapping("/{seckill_id}/release")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "seckill_id",	value = "要查询的限时抢购入库主键",	required = true, dataType = "int",	paramType = "path")
	})
	public SeckillVO publish(@Valid @RequestBody SeckillVO seckill,@ApiIgnore @PathVariable("seckill_id") Integer seckillId){

		seckill.verifyParam(seckill);
		//发布状态
		seckill.setSeckillStatus(SeckillStatusEnum.RELEASE.name());
		seckill.setSeckillType(SeckillTypeEnum.PLATFORM.value());
		seckill.setSellerIds(","+UserContext.getSeller().getSellerId()+",");
		if(seckillId==null || seckillId==0){
			seckillManager.add(seckill);
		}else{
			seckillManager.edit(seckill,seckillId);
		}

		return seckill;
	}


	@ApiOperation(value	= "审核商品")
	@PostMapping(value = "/review/{apply_id}")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "apply_id",	value = "限时抢购申请ID",	required = true, dataType = "int",	paramType = "path"),
			@ApiImplicitParam(name = "fail_reason",	value = "驳回原因",	 dataType = "String",	paramType = "query"),
			@ApiImplicitParam(name = "status",	value = "审核状态结果,yes为通过,no为不通过",	required = true, dataType = "String",allowableValues = "yes,no",paramType = "query")
	})
	public String reviewGoods(@ApiIgnore @PathVariable(name = "apply_id") @NotNull(message = "团购活动ID参数错误") Integer applyId,@ApiIgnore String failReason,
							@NotEmpty(message = "请选择审核状态结果") String status){

		if("yes".equals(status)){
			status = SeckillGoodsApplyStatusEnum.PASS.name();
		}else{
			status = SeckillGoodsApplyStatusEnum.FAIL.name();
		}

		this.seckillManager.reviewApply(applyId,status,failReason);
		return "";
	}



}
