package com.enation.app.javashop.manager.api.trade;

import com.alibaba.excel.EasyExcel;
import com.enation.app.javashop.core.excel.ExportOrderSkuVO;
import com.enation.app.javashop.core.promotion.shetuan.model.vo.OrderProfitVO;
import com.enation.app.javashop.core.trade.order.model.dto.OrderProfitQueryParam;
import com.enation.app.javashop.core.trade.order.service.OrderProfitManager;
import com.enation.app.javashop.framework.context.ThreadContextHolder;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.CurrencyUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(description = "订单收益管理API")
@RestController
@RequestMapping("/admin/trade/profit")
@Validated
public class OrderProfitController {

    @Autowired
    private OrderProfitManager orderProfitManager;

    @ApiOperation(value = "查询收益明细", response = OrderProfitVO.class)
    @PostMapping("query_profit_list")
    public Map<String, Object> queryProfitList(@RequestBody OrderProfitQueryParam queryParam){
        Map<String,Object> map = new HashMap<>();
        queryParam.setIsZero(1);
        Page page = orderProfitManager.queryProfitList(queryParam);
        map.put("data",page.getData());
        map.put("data_total",page.getDataTotal());
        map.put("page_no",page.getPageNo());
        map.put("page_size",page.getPageSize());
        // 合计佣金
        Double totalCommissionMoney = orderProfitManager.getTotalCommissionMoney(queryParam);
        // 已结算佣金
        Double settledCommissionMoney = orderProfitManager.getSettledCommissionMoney(queryParam);
        // 未结算佣金
        Double unsettledCommissionMoney = CurrencyUtil.sub(totalCommissionMoney,settledCommissionMoney);
        map.put("total_commission_money",totalCommissionMoney);
        map.put("settled_commission_money",settledCommissionMoney);
        map.put("unsettled_commission_money",unsettledCommissionMoney);
        return map;
    }

    @ApiOperation(value = "导出佣金明细", response = OrderProfitVO.class)
    @PostMapping("/export_profit_list")
    public void exportProfitList(@RequestBody OrderProfitQueryParam queryParam){
        queryParam.setIsZero(1);
        queryParam.setPageNo(1);
        queryParam.setPageSize(5000);
        orderProfitManager.exportProfitList(queryParam);
    }

}
