package com.enation.app.javashop.manager.api.pagedata;

import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.base.context.RegionFormat;
import com.enation.app.javashop.core.base.model.vo.SuccessMessage;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.core.pagedata.constraint.annotation.ClientAppType;
import com.enation.app.javashop.core.pagedata.constraint.annotation.PageType;
import com.enation.app.javashop.core.pagedata.model.PageData;
import com.enation.app.javashop.core.pagedata.model.enums.OwnerTypeEnum;
import com.enation.app.javashop.core.pagedata.model.enums.PageClientTypeEnum;
import com.enation.app.javashop.core.pagedata.model.enums.PageHomeTypeEnum;
import com.enation.app.javashop.core.pagedata.model.enums.PageStateEnum;
import com.enation.app.javashop.core.pagedata.model.vo.PageListQueryVo;
import com.enation.app.javashop.core.pagedata.service.PageDataManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.runtime.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

/**
 * 楼层控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@RestController
@RequestMapping("/admin/pages")
@Api(description = "楼层相关API")
@Validated
public class PageDataManagerController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private PageDataManager pageManager;

    /**
     * @param page
     * @return
     * @author john
     */
    @PutMapping(value = "/add")
    @ApiOperation(value = "新增页面", response = PageData.class)
    public PageData add(@Valid PageData page) {
        //新增的页面默认为草稿状态
        page.setState(PageStateEnum.SCRIPT.getValue());
        //新增的页面归属平台
        page.setOwnerType(OwnerTypeEnum.DEFINED.value());
        page.setClientType(PageClientTypeEnum.APP.value());
        page.setPageType(PageHomeTypeEnum.DEFINED.value().toString());
        page.setPageTypeName(PageHomeTypeEnum.DEFINED.description());

        this.pageManager.add(page);
        return page;
    }

    /**
     * @param pageId
     * @return
     * @author john
     * 将单个页面设置成上架状态
     */
    @PutMapping(value = "/putaway/{page_id}")
    @ApiOperation(value = "页面上架", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_id", value = "主键", required = true, dataType = "int", paramType = "path")
    })
    public boolean putaway(@PathVariable("page_id") Integer pageId) {
        return this.pageManager.putaway(pageId);
    }

    /**
     * @param pageId
     * @return
     * @author john
     * 将单个页面设置成上架状态
     */
    @PutMapping(value = "/sold_out/{page_id}")
    @ApiOperation(value = "页面下架", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_id", value = "主键", required = true, dataType = "int", paramType = "path")
    })
    public boolean soldOut(@PathVariable("page_id") Integer pageId) {
        return this.pageManager.soldOut(pageId);
    }


    @PutMapping(value = "/{page_id}")
    @ApiOperation(value = "修改页面并保存草稿", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_id", value = "主键", required = true, dataType = "int", paramType = "path")
    })
    public PageData edit(@Valid PageData page, @PathVariable("page_id") Integer pageId) {
        page.setStoreId(1); //存入草稿状态
        this.pageManager.edit(page, pageId);

        return page;
    }

    @PutMapping(value = "/{page_id}/type=2")
    @ApiOperation(value = "修改页面并发布", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_id", value = "主键", required = true, dataType = "int", paramType = "path")
    })
    public PageData edit2(@Valid PageData page, @PathVariable("page_id") Integer pageId) {
        page.setStoreId(2); //存入草稿状态
        this.pageManager.edit(page, pageId);

        return page;
    }


    @GetMapping(value = "/{page_id}")
    @ApiOperation(value = "查询一个楼层")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_id", value = "要查询的楼层主键", required = true, dataType = "int", paramType = "path")
    })
    public PageData get(@PathVariable("page_id") Integer pageId) {

        PageData page = this.pageManager.getModel(pageId);

        return page;
    }


    /**
     * jfeng 修改
     * 查询出所有的页面配置列表
     *
     * @return
     */
    @PostMapping(value = "/list")
    public Page getPageListByStore(@RequestBody PageListQueryVo pageVO) {
        Page page = this.pageManager.listOfPlatform(pageVO);
        return page;
    }

    /**
     * JFENG 删除页面
     *
     * @param pageId
     */
    @DeleteMapping(value = "/{page_id}")
    @ApiOperation(value = "删除页面")
    public void deletePage(@PathVariable("page_id") Integer pageId) {
        this.pageManager.delete(pageId);
    }

    @PutMapping(value = "/{client_type}/{page_type}")
    @ApiOperation(value = "使用客户端类型和页面类型修改楼层", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "要查询的客户端类型 APP/WAP/PC", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "page_type", value = "要查询的页面类型 INDEX 首页/SPECIAL 专题", required = true, dataType = "string", paramType = "path")
    })
    public PageData editByType(@Valid PageData pageData, @ClientAppType @PathVariable("client_type") String clientType, @PageType @PathVariable("page_type") String pageType) {
        pageData.setClientType(clientType);
        pageData.setPageType(pageType);
        this.pageManager.editByType(pageData);
        return pageData;
    }

    @GetMapping(value = "/{client_type}/{page_type}")
    @ApiOperation(value = "使用客户端类型和页面类型查询一个楼层")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "要查询的客户端类型 APP/WAP/PC", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "page_type", value = "要查询的页面类型 INDEX 首页/SPECIAL 专题", required = true, dataType = "string", paramType = "path")
    })
    public PageData getByType(@ClientAppType @PathVariable("client_type") String clientType, @PageType @PathVariable("page_type") String pageType) {

        PageData page = this.pageManager.getByType(clientType, pageType);

        return page;
    }

    /**
     * jfeng
     * 设置APP端首页（平台首页和城市首页）
     *
     * @return
     */
    @PutMapping(value = {"/index/{page_id}/{home_page_type}"})
    @ApiOperation(value = "设置APP端首页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "home_page_type", value = "首页类型", required = true, dataType = "string", paramType = "path")
    })
   public Boolean setIndexPage(@PathVariable("page_id") Integer pageId, @PathVariable("home_page_type") String homePageType,
                                @RegionFormat @RequestParam(value = "index_region", required = false) Region region) {
        return pageManager.setIndexPage(pageId,homePageType, region);
    }

    /**
     *xlg
     * 自定义页面复制
     */
    @GetMapping(value = {"/copy_page_index"})
    @ApiOperation(value = "自定义页面复制")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_id", value = "数据id", required = true, dataType = "int", paramType = "query")

    })
    public SuccessMessage copyPageIndex(@ApiIgnore Integer pageId) {
        try {
            pageManager.copyPageIndex(pageId);
            return  new SuccessMessage("微页面复制成功！");
        }catch (Exception e){
            logger.error("微页面复制失败:"+e.getMessage());
            throw new DistributionException("-1", "微页面复制失败:" + e.getMessage());
        }
    }
}
