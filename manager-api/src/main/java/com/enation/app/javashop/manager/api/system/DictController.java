package com.enation.app.javashop.manager.api.system;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.system.model.dos.SmsPlatformDO;
import com.enation.app.javashop.core.system.model.dto.DictDTO;
import com.enation.app.javashop.core.system.model.vo.ExpressPlatformVO;
import com.enation.app.javashop.framework.cache.Cache;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import sun.security.krb5.internal.crypto.KeyUsage;

import java.security.Key;
import java.util.Set;

/**
 * 数据字典
 * @author JFeng
 * @date 2020/8/13 13:59
 */
@RestController
@RequestMapping("/admin/cache")
@Api(description = "数据字典")
public class DictController {
    @Autowired
    private Cache cache;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @ApiOperation(value = "键值对")
    @PostMapping(value = "/keyValue")
    public String keyValue(@RequestBody DictDTO dictDTO) {
        String key = dictDTO.getKey();
        this.cache.put(key, JSON.toJSONString(dictDTO.getValue()));
        return (String) this.cache.get(key);
    }

    @ApiOperation(value = "批量删除key")
    @PostMapping(value = "/batchDelete")
    public Set<String> batchDelete(@RequestParam String keyPattern) {
        //批量删除
        Set<String> keys = redisTemplate.keys(keyPattern.trim());
        if(!CollectionUtils.isEmpty(keys)){
            redisTemplate.delete(keys);
        }
        return keys;
    }
}
