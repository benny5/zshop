package com.enation.app.javashop.base.listener;

import com.dag.eagleshop.core.dict.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 系统启动类
 * @author ChienSun
 * @date 2019/1/3
 */
@Component
@Order(1)
public class ApplicationStartupRunner implements ApplicationRunner {

    @Autowired
    private DictService dictService;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // 初始化字典
                dictService.initDictRedisCache();
            }
        }).start();
    }
}
