package com.enation.app.javashop.base.api;

import com.dag.eagleshop.core.dict.model.dos.Dict;
import com.dag.eagleshop.core.dict.model.vo.DictVO;
import com.dag.eagleshop.core.dict.service.DictService;
import com.enation.app.javashop.core.base.model.vo.SuccessMessage;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dict")
@Api(description = "数据字典")
public class DictBaseController {

    @Autowired
    private DictService dictService;

    protected final Log logger = LogFactory.getLog(this.getClass());

    @ApiOperation("数据字典列表")
    @PostMapping("/query_dict_list")
    public Page<DictVO> queryDictList( DictVO dictVO,Integer pageNo,Integer pageSize) {
        dictVO.setPageNo(pageNo);
        dictVO.setPageSize(pageSize);
        return dictService.queryDictList(dictVO);
    }

    @ApiOperation("数据字典类型")
    @GetMapping("/query_all_type")
    public List<String> queryAllType() {
        return dictService.queryAllType();
    }

    @ApiOperation("根据type查询字典列表")
    @PostMapping("/query_list_by_type")
    public Page<DictVO> queryListByType( DictVO dictVO) {
        dictVO.setPageNo(1);
        dictVO.setPageSize(10);
        return dictService.queryDictList(dictVO);
    }

    @ApiOperation("添加数据字典")
    @PostMapping("/add_dict")
    public SuccessMessage addDict( Dict dict) {
        try {
            dictService.addDict(dict);
            // 初始化缓存
            dictService.initDictRedisCache();
            return new SuccessMessage("添加成功!");
        } catch (Exception e) {
            logger.error("添加数据字典", e);
            throw new DistributionException("-1", "添加数据字典" + e.getMessage());
        }
    }

    @ApiOperation("修改数据字典")
    @PostMapping("/udp_dict")
    public SuccessMessage udpDict( Dict dict) {
        try {
            dictService.updateDict(dict);
            // 初始化缓存
            dictService.initDictRedisCache();
            return new SuccessMessage("修改成功!");
        } catch (Exception e) {
            logger.error("修改数据字典: ", e);
            throw new DistributionException("-1", "修改数据字典 :" + e.getMessage());
        }
    }

    @ApiOperation("删除数据")
    @GetMapping("/del_dict/{id}")
    public SuccessMessage delDict(@PathVariable(name = "id")  Integer id) {
        try {
            dictService.delDict(id);
            // 初始化缓存
            dictService.initDictRedisCache();
            return new SuccessMessage("删除成功!");
        } catch (Exception e) {
            logger.error("删除成功: ", e);
            throw new DistributionException("-1", "删除成功 :" + e.getMessage());
        }
    }

    @ApiOperation("根据id查询")
    @GetMapping("/query_dict_by_id/{id}")
    public DictVO queryDictById(@PathVariable(name = "id")  Integer id) {
            return dictService.queryDictById(id);
    }

    @ApiOperation("初始化字典缓存")
    @GetMapping("/initDictRedisCache")
    public boolean initDictRedisCache() {
        // 初始化缓存
        dictService.initDictRedisCache();
        return true;
    }
}
