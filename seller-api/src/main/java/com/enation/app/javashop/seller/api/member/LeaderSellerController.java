package com.enation.app.javashop.seller.api.member;

import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.base.context.RegionFormat;
import com.enation.app.javashop.core.member.model.dos.LeaderDO;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.vo.LeaderQueryVO;
import com.enation.app.javashop.core.member.model.vo.LeaderVO;
import com.enation.app.javashop.core.member.service.LeaderManager;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.BeanUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/seller/members/leader")
@Api(description = "团长管理相关API")
public class LeaderSellerController {

    @Autowired
    private LeaderManager leaderManager;
    @Autowired
    private MemberManager memberManager;


    @ApiOperation(value = "根据id查看团长详情", response = LeaderDO.class)
    @GetMapping(value = "/get/{leader_id}")
    public Map<String, Object> getById(@PathVariable("leader_id") Integer leaderId){
        LeaderDO leaderDO = this.leaderManager.getById(leaderId);

        Integer memberId = leaderDO.getMemberId();
        Member member = null;
        if(memberId != null){
            member = memberManager.getModel(leaderDO.getMemberId());
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("leader", leaderDO);
        resultMap.put("member", member);
        return resultMap;
    }


    @ApiOperation(value = "添加团长", response = Void.class)
    @PostMapping(value = "add_leader")
    public void addLeader(LeaderVO leaderVO, @RegionFormat @RequestParam(value = "region", required = true) Region region){
        LeaderDO leaderDO = new LeaderDO();
        BeanUtil.copyProperties(leaderVO, leaderDO);

        Member member = memberManager.getModel(leaderDO.getMemberId());
        if(member == null){
            throw new RuntimeException("会员不存在");
        }

        Seller seller = UserContext.getSeller();

        leaderDO.setLeaderMobile(member.getMobile());
        leaderDO.setProvince(region.getProvince());
        leaderDO.setCity(region.getCity());
        leaderDO.setCounty(region.getCounty());
        leaderDO.setTown(region.getTown());
        leaderDO.setProvinceId(region.getProvinceId());
        leaderDO.setCityId(region.getCityId());
        leaderDO.setCountyId(region.getCountyId());
        leaderDO.setTownId(region.getTownId());
        leaderDO.setOpStatus(1);
        leaderDO.setSourceFrom(1);
        leaderDO.setSellerId(seller.getSellerId());
        leaderDO.setCreaterName(seller.getSellerName());
        leaderDO.setAuditStatus(2);
        leaderManager.addLeader(leaderDO);
    }


    @ApiOperation(value = "修改团长", response = Void.class)
    @PostMapping(value = "update_leader")
    public void updateLeader(LeaderVO leaderVO, @RegionFormat @RequestParam(value = "region", required = true) Region region){
        LeaderDO leaderDO = new LeaderDO();
        BeanUtil.copyProperties(leaderVO, leaderDO);

        Member member = memberManager.getModel(leaderDO.getMemberId());
        if(member == null){
            throw new RuntimeException("会员不存在");
        }

        leaderDO.setLeaderMobile(member.getMobile());
        leaderDO.setProvince(region.getProvince());
        leaderDO.setCity(region.getCity());
        leaderDO.setCounty(region.getCounty());
        leaderDO.setTown(region.getTown());
        leaderDO.setProvinceId(region.getProvinceId());
        leaderDO.setCityId(region.getCityId());
        leaderDO.setCountyId(region.getCountyId());
        leaderDO.setTownId(region.getTownId());
        leaderManager.edit(leaderDO, leaderDO.getLeaderId());
    }


    @ApiOperation(value = "查询团长列表", response = LeaderDO.class)
    @GetMapping(value = "list")
    public Page list(LeaderQueryVO leaderQueryVO, @RegionFormat @RequestParam(value = "region", required = false) Region region){
        leaderQueryVO.setSellerId(UserContext.getSeller().getSellerId());

        if(region != null){
            leaderQueryVO.setProvinceId(region.getProvinceId());
            leaderQueryVO.setCityId(region.getCityId());
            leaderQueryVO.setCountyId(region.getCountyId());
            leaderQueryVO.setTownId(region.getTownId());
        }

        return this.leaderManager.list(leaderQueryVO);
    }


    @ApiOperation(value = "删除团长")
    @DeleteMapping(value = "/{leader_id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "leader_id", value = "团长id", required = true, dataType = "int", paramType = "path")
    })
    public boolean delete(@ApiIgnore @PathVariable("leader_id") Integer leaderId){
        return this.leaderManager.delete(leaderId);
    }

}
