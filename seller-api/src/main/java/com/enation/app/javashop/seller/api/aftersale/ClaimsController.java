package com.enation.app.javashop.seller.api.aftersale;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.aftersale.model.enums.ClaimsStatusEnum;
import com.enation.app.javashop.core.aftersale.model.vo.ClaimsParamVO;
import com.enation.app.javashop.core.aftersale.model.vo.ClaimsQueryVO;
import com.enation.app.javashop.core.aftersale.model.vo.ClaimsVO;
import com.enation.app.javashop.core.aftersale.model.vo.Result;
import com.enation.app.javashop.core.aftersale.service.ClaimsManager;
import com.enation.app.javashop.core.excel.ExcelManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/21
 * @Description:
 */
@RestController
@RequestMapping("/seller/after-sales/claims")
@Api(tags = "理赔单相关API")
public class ClaimsController {
    @Autowired
    private ClaimsManager claimsManager;
    @Autowired
    private ExcelManager excelManager;

    @ApiOperation(value = "理赔单分页展示列表，查询接口")
    @GetMapping(value = "/getClaimsList")
    public Page<ClaimsVO> getClaimsList(ClaimsQueryVO claimsQueryVO) {
        Seller seller = UserContext.getSeller();
        Page<ClaimsVO> claimsVOList = claimsManager.getClaimsList(claimsQueryVO,seller);
        return claimsVOList;
    }

    @ApiOperation(value = "添加理赔单")
    @ApiImplicitParam(name = "claimsParam", value = "理赔单数组", required = true, dataType = "ClaimsParam", paramType = "body")
    @PostMapping(value = "/addClaims")
    public Result<String> addClaims(@RequestBody ClaimsParamVO claimsParam) {
        Seller seller = UserContext.getSeller();
        return claimsManager.addClaims(claimsParam, seller);
    }

    @ApiOperation(value = "审核理赔单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "claims_ids", value = "理赔单id 数组", allowMultiple = true, required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "claims_status", value = "审核状态", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "audit_describe", value = "审核备注", required = false, dataType = "String", paramType = "query")
    })
    @PostMapping(value = "/audit/{claims_ids}")
    public void audit(@PathVariable("claims_ids") List<Integer> claimsIds, @RequestParam(value = "claims_status") String claimsStatus, @RequestParam(value = "audit_describe") String auditDescribe) {
        Seller seller = UserContext.getSeller();
        Assert.notNull(claimsStatus, "获取理赔单状态异常");
        //理赔单状态如果是审核成功或失败，调用审核方法，更新审核人信息
        if (claimsStatus.equals(ClaimsStatusEnum.AUDIT_SUCCESS.value()) || claimsStatus.equals(ClaimsStatusEnum.AUDIT_FAIL.value())) {
            claimsManager.auditClaims(claimsIds, claimsStatus, auditDescribe, seller);
        } else {
            //调用理赔方法，更新理赔人信息
            claimsManager.claimsStatus(claimsIds, claimsStatus, auditDescribe, seller);
        }

    }

    /**
     * 异常单处理结果展示页面
     * @param exceptionId
     * @return
     */
    @ApiOperation(value = "理赔单处理结果，根据异常单ID查询理赔单")
    @ApiImplicitParam(name = "exception_id", value = "异常单ID", required = true, dataType = "int", paramType = "path")
    @GetMapping(value = "/{exception_id}")
    public ClaimsParamVO getByExceptionId(@PathVariable("exception_id") @NotNull(message = "异常单ID不得为空") Integer exceptionId) {
        ClaimsParamVO claimsParamVO = claimsManager.getClaimsParamVOByExceptionId(exceptionId);
        return claimsParamVO;
    }

    /**
     *
     * @param claimsQueryVO
     */
    @ApiOperation(value = "导出理赔单信息")
    @GetMapping(value = "/claimsExport")
    public void claimsExport(ClaimsQueryVO claimsQueryVO){
        Seller seller = UserContext.getSeller();
        claimsQueryVO.setPageSize(1000);
        excelManager.claimsExport(claimsQueryVO,seller);
    }

}
