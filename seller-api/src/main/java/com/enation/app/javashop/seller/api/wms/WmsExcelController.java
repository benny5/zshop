package com.enation.app.javashop.seller.api.wms;

import com.enation.app.javashop.core.shop.model.dto.FixTimeSection;
import com.enation.app.javashop.core.shop.service.impl.OrderReceiveTimeManager;
import com.enation.app.javashop.core.wms.model.dto.WmsOrderQueryParam;
import com.enation.app.javashop.core.wms.model.enums.WmsExportType;
import com.enation.app.javashop.core.wms.service.WmsOrderExcelManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * 出库管理
 * @author JFeng
 * @date 2020/9/21 16:50
 */
@Api(description = "出库管理API")
@RestController
@RequestMapping("/seller/wms/excel")
@Validated
public class WmsExcelController {
    @Autowired
    private WmsOrderExcelManager wmsOrderExcelManager;
    @Autowired
    private OrderReceiveTimeManager orderReceiveTimeManager;


    @ApiOperation(value = "导出分拣清单")
    @PostMapping("/skuOrder")
    public void exportSkuOrderList(@RequestBody WmsOrderQueryParam  wmsOrderQueryParam) {
        if(wmsOrderQueryParam.getReceiveTimeSeq()==null){
           throw  new ServiceException("1000","请选择送达时间段");
        }
        Seller seller = UserContext.getSeller();
        wmsOrderQueryParam.setSellerId(seller.getSellerId());
        Integer deliveryDate = Integer.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")));
        wmsOrderQueryParam.setDeliveryDate(deliveryDate);

        WmsExportType wmsExportType = WmsExportType.valueOf(wmsOrderQueryParam.getExportType());

        FixTimeSection timeSection = orderReceiveTimeManager.resolveReceiveTimeSeq(wmsOrderQueryParam.getSellerId(), wmsOrderQueryParam.getReceiveTimeSeq());
        wmsOrderQueryParam.setReceiveTime(timeSection.getReceiveTimeStamp());
        wmsOrderQueryParam.setReceiveTimeType(timeSection.getReceiveTimeType());
        wmsOrderQueryParam.setReceiveTimeName(timeSection.getSortingTimeText());

        if(LocalTime.now().getHour()>21){
            // 如果11点之后导出，需要读取次日出库商品采购
            Long receiveTimeStamp = timeSection.getReceiveTimeStamp();
            wmsOrderQueryParam.setReceiveTime(receiveTimeStamp+24*60*60);
            wmsOrderQueryParam.setReceiveTimeName("明日商品总数汇总");
        }

        switch (wmsExportType){
            case SKU_ORDER:
                /* 每日商品找格子数据 --- 商品分拣 */
                this.wmsOrderExcelManager.exportSkuOrderList(wmsOrderQueryParam);
                break;
            case SKU_SUPPLIER:
                /* 每日sku统计表 --- 商品采购单 */
                this.wmsOrderExcelManager.exportSkuSupplierList(wmsOrderQueryParam);
                break;
            case ORDER_SORTING:
                /* 每日订单找格子 --- 订单对应分拣网格 */
                this.wmsOrderExcelManager.exportOrderSortingList(wmsOrderQueryParam);
                break;
            case ORDER_SITE:
                /* B模式数据 --- B模式按照站点归类汇总商品总数*/
                this.wmsOrderExcelManager.exportBModelList(wmsOrderQueryParam);
                break;
        }
    }

}
