package com.enation.app.javashop.seller.api.promotion;

import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.enation.app.javashop.core.promotion.luck.model.dos.LuckDO;
import com.enation.app.javashop.core.promotion.luck.model.dos.LuckPrizeDO;
import com.enation.app.javashop.core.promotion.luck.model.vo.QueryLuckRecordVO;
import com.enation.app.javashop.core.promotion.luck.model.vo.QueryLuckVO;
import com.enation.app.javashop.core.promotion.luck.service.LuckManager;
import com.enation.app.javashop.core.promotion.luck.service.LuckPrizeManager;
import com.enation.app.javashop.core.promotion.luck.service.LuckRecordManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


/**
 * 抽奖活动
 * @author 孙建
 */
@RestController
@RequestMapping("/seller/promotion/luck")
@Api(description = "抽奖活动API")
public class LuckSellerController {

    @Autowired
    private LuckManager luckManager;
    @Autowired
    private LuckPrizeManager luckPrizeManager;
    @Autowired
    private LuckRecordManager luckRecordManager;

    @GetMapping("query_luck_list")
    @ApiOperation(value = "查询抽奖活动列表")
    public Page queryLuckList(QueryLuckVO queryLuckVO) {
        Seller seller = UserContext.getSeller();
        queryLuckVO.setSellerId(seller.getSellerId());
        Page<LuckDO> page = luckManager.queryLuckList(queryLuckVO);
        return page;
    }


    @GetMapping("query_luck_prize_list")
    @ApiOperation(value = "查询抽奖活动奖品列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "luck_id", value = "抽奖活动Id", required = true, dataType = "int", paramType = "query")
    })
    public List<LuckPrizeDO> queryLuckPrizeList(@ApiIgnore Integer luckId) {
        Seller seller = UserContext.getSeller();
        return luckPrizeManager.queryListByLuckId(luckId);
    }


    @GetMapping("query_luck_record_list")
    @ApiOperation(value = "查询抽奖记录列表")
    public Page queryLuckRecordList(QueryLuckRecordVO queryLuckRecordVO) {
        Seller seller = UserContext.getSeller();
        queryLuckRecordVO.setSellerId(seller.getSellerId());
        return luckRecordManager.queryLuckRecordList(queryLuckRecordVO);
    }


    @PostMapping(value = "/on_off_line")
    @ApiOperation(value = "上线或下线")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "luck_id", value = "抽奖活动Id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "luck_status", value = "上线或下线", required = true, dataType = "int", paramType = "query"),
    })
    public JsonBean onOffLine(@ApiIgnore Integer luckId, @ApiIgnore Integer luckStatus){
        luckManager.onOffLine(luckId, luckStatus);
        return new JsonBean();
    }

    @ApiOperation(value = "创建或修改抽奖活动（新增时不传luck_id）", response = LuckDO.class)
    @PostMapping(value = "/addOrUpdateLuck")
    public JsonBean addOrUpdateLuck(@RequestBody LuckDO luckDO){
        luckManager.addOrUpdateLuck(luckDO);
        return new JsonBean();
    }

    @ApiOperation(value = "删除抽奖活动")
    @PostMapping(value = "/deleteLuck/{luck_id}")
    public JsonBean deleteLuck(@PathVariable("luck_id") Integer luckId){
        luckManager.deleteLuck(luckId);
        return new JsonBean();
    }

    @GetMapping("luck_record_export")
    @ApiOperation(value = "抽奖记录导出")
    public void luckRecordExport(QueryLuckRecordVO queryLuckRecordVO) {
        Seller seller = UserContext.getSeller();
        queryLuckRecordVO.setSellerId(seller.getSellerId());
        queryLuckRecordVO.setPageNo(1);
        queryLuckRecordVO.setPageSize(2000);
        luckRecordManager.luckRecordExport(queryLuckRecordVO);
    }

    @ApiOperation(value = "添加和修改抽奖奖品")
    @PostMapping(value = "/addLuckPrize")
    public JsonBean addAndUpdateLuckPrize(@RequestBody List<LuckPrizeDO> prizeDOList){
        luckPrizeManager.addAndUpdateLuckPrize(prizeDOList);
        return new JsonBean();
    }

    @ApiOperation(value = "兑换抽奖奖品")
    @PostMapping(value = "/exchangePrize")
    public JsonBean exchangePrize(@RequestParam(value = "luckRecordIds") String luckRecordIds){
        luckRecordManager.exchangePrize(luckRecordIds);
        return new JsonBean();
    }

}
