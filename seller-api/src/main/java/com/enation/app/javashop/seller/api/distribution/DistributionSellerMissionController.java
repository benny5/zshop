package com.enation.app.javashop.seller.api.distribution;

import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionDetailDO;
import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionScheduleDO;
import com.enation.app.javashop.core.distribution.model.dto.CreateMissionDetailsDTO;
import com.enation.app.javashop.core.distribution.model.vo.*;
import com.enation.app.javashop.core.distribution.service.DistributionMissionDetailService;
import com.enation.app.javashop.core.distribution.service.DistributionMissionScheduleService;
import com.enation.app.javashop.core.distribution.service.DistributionMissionService;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.BeanUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;


/**
 * @author 王志杨
 * @since 2021/1/22 17:18
 * 团长任务API
 */
@RestController
@RequestMapping("/seller/distribution/mission")
@Api(description = "团长任务API")
public class DistributionSellerMissionController {

    @Autowired
    private DistributionMissionService distributionMissionService;
    @Autowired
    private DistributionMissionDetailService distributionMissionDetailService;
    @Autowired
    private DistributionMissionScheduleService distributionMissionScheduleService;

    @PostMapping("/create_mission")
    @ApiOperation(value = "创建团长任务")
    public JsonBean createMission(@RequestBody @Validated DistributionMissionVO distributionMissionVO){

        Seller seller = UserContext.getSeller();
        DistributionMissionDO distributionMissionDO = new DistributionMissionDO();
        BeanUtil.copyProperties(distributionMissionVO, distributionMissionDO);
        distributionMissionService.insertDistributionMission(distributionMissionDO, seller);
        return new JsonBean();
    }

    @PostMapping("/update_mission")
    @ApiOperation(value = "修改团长任务")
    public JsonBean updateMission(@RequestBody DistributionMissionVO distributionMissionVO){

        Seller seller = UserContext.getSeller();
        DistributionMissionDO distributionMissionDO = new DistributionMissionDO();
        BeanUtil.copyProperties(distributionMissionVO, distributionMissionDO);
        distributionMissionService.updateDistributionMission(distributionMissionDO, seller);
        return new JsonBean();
    }


    @PostMapping(value = "/delete_mission")
    @ApiOperation(value = "删除团长任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mission_id", value = "团长任务Id", required = true, dataType = "int", paramType = "query"),
    })
    public JsonBean deleteMission(Integer missionId){

        Seller seller = UserContext.getSeller();
        distributionMissionService.deleteDistributionMission(missionId, seller);
        return new JsonBean();
    }


    @GetMapping("query_mission_list")
    @ApiOperation(value = "查询团长任务列表")
    public Page queryMissionList(QueryDistributionMissionVO queryDistributionMissionVO) {
        Seller seller = UserContext.getSeller();
        queryDistributionMissionVO.setSellerId(seller.getSellerId());
        Page<DistributionMissionVO> page = distributionMissionService.queryDistributionMissionList(queryDistributionMissionVO);
        return page;
    }


    @PostMapping(value = "/on_off_line")
    @ApiOperation(value = "上线或下线")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mission_id", value = "团长任务Id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "mission_status", value = "上线或下线", required = true, dataType = "int", paramType = "query"),
    })
    public JsonBean onOffLine(@ApiIgnore Integer missionId,@ApiIgnore Integer missionStatus){
        distributionMissionService.onOffLine(missionId, missionStatus);
        return new JsonBean();
    }


    @PostMapping("/create_mission_details")
    @ApiOperation(value = "创建团长任务详情")
    public JsonBean createMissionDetails(@RequestBody CreateMissionDetailsDTO createMissionDetailsDTO) {
        List<DistributionMissionDetailDO> distributionMissionDetailDOList = new ArrayList<>();

        // 日任务
        DistributionMissionDetailVO dayMissionDetailVO = createMissionDetailsDTO.getDayMissionDetail();
        if(dayMissionDetailVO != null){
            DistributionMissionDetailDO dayMissionDetailDO = new DistributionMissionDetailDO();
            BeanUtil.copyProperties(dayMissionDetailVO, dayMissionDetailDO);
            distributionMissionDetailDOList.add(dayMissionDetailDO);
        }

        // 月任务
        DistributionMissionDetailVO monthMissionDetailVO = createMissionDetailsDTO.getMonthMissionDetail();
        if(monthMissionDetailVO != null){
            DistributionMissionDetailDO monthMissionDetailDO = new DistributionMissionDetailDO();
            BeanUtil.copyProperties(monthMissionDetailVO, monthMissionDetailDO);
            distributionMissionDetailDOList.add(monthMissionDetailDO);
        }

        distributionMissionDetailService.createMissionDetails(distributionMissionDetailDOList);
        return new JsonBean();
    }

    @GetMapping("/getMissionDetailByMisId/{mission_id}")
    @ApiOperation(value = "查询任务详情")
    public CreateMissionDetailsDTO getMissionDetailByMisId(@PathVariable("mission_id") Integer missionId){
        return distributionMissionDetailService.getMissionDetailByMisId(missionId);
    }

    @PostMapping("/update_mission_details")
    @ApiOperation(value = "更新团长任务详情")
    public JsonBean updateMissionDetails(@RequestBody CreateMissionDetailsDTO createMissionDetailsDTO) {
        List<DistributionMissionDetailDO> distributionMissionDetailDOList = new ArrayList<>();

        // 日任务
        DistributionMissionDetailVO dayMissionDetailVO = createMissionDetailsDTO.getDayMissionDetail();
        if(dayMissionDetailVO != null){
            DistributionMissionDetailDO dayMissionDetailDO = new DistributionMissionDetailDO();
            BeanUtil.copyProperties(dayMissionDetailVO, dayMissionDetailDO);
            distributionMissionDetailDOList.add(dayMissionDetailDO);
        }

        // 月任务
        DistributionMissionDetailVO monthMissionDetailVO = createMissionDetailsDTO.getMonthMissionDetail();
        if(monthMissionDetailVO != null){
            DistributionMissionDetailDO monthMissionDetailDO = new DistributionMissionDetailDO();
            BeanUtil.copyProperties(monthMissionDetailVO, monthMissionDetailDO);
            distributionMissionDetailDOList.add(monthMissionDetailDO);
        }

        distributionMissionDetailService.updateMissionDetails(distributionMissionDetailDOList);
        return new JsonBean();
    }


    @GetMapping("query_mission_schedule_list")
    @ApiOperation(value = "查询团长任务进度列表")
    public Page queryMissionScheduleList(QueryDistributionMissionScheduleVO queryDistributionMissionScheduleVO) {
        Seller seller = UserContext.getSeller();
        queryDistributionMissionScheduleVO.setSellerId(seller.getSellerId());
        Page<DistributionMissionScheduleVO> page = distributionMissionScheduleService.queryMissionScheduleList(queryDistributionMissionScheduleVO);
        return page;
    }

}
