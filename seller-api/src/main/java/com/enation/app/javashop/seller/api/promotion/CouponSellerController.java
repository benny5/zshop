package com.enation.app.javashop.seller.api.promotion;

import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.enation.app.javashop.core.promotion.PromotionErrorCode;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.core.promotion.coupon.service.CouponManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.NoPermissionException;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.exception.SystemErrorCodeV1;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.DictUtils;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * 优惠券控制器
 *
 * @author Snow
 * @version v2.0
 * @since v7.0.0
 * 2018-04-17 23:19:39
 */
@RestController
@RequestMapping("/seller/promotion/coupons")
@Api(description = "优惠券相关API")
@Validated
public class CouponSellerController {
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private CouponManager couponManager;


    @ApiOperation(value = "查询优惠券列表", response = CouponDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "开始时间", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "截止时间", dataType = "Long", paramType = "query"),
            @ApiImplicitParam(name = "coupon_type", value = "优惠券类型", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataType = "String", paramType = "query")
    })
    @GetMapping
    public Page list(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize,
                     @ApiIgnore Long startTime, @ApiIgnore Long endTime,@ApiIgnore String couponType, @ApiIgnore String keyword) {

        return this.couponManager.list(pageNo, pageSize, startTime, endTime, keyword,couponType);
    }

    @ApiOperation(value = "根据优惠券类型查询可用的优惠券列表")
    @ApiImplicitParam(name = "coupon_type", value = "优惠券类型", dataType = "String", paramType = "query")
    @GetMapping("/getListByType")
    public List<CouponDO> getListByType(String couponType) {
        Seller seller = UserContext.getSeller();
        return couponManager.getList(seller.getSellerId(), couponType);
    }


    @ApiOperation(value = "添加优惠券", response = CouponDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "优惠券名称", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "coupon_price", value = "优惠券面额", required = true, dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "coupon_threshold_price", value = "优惠券门槛价格", required = true, dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "coupon_type", value = "优惠券类型", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "使用起始时间", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "使用截止时间", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "create_num", value = "发行量", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit_num", value = "每人限领数量", required = true, dataType = "int", paramType = "query"),

    })
    @PostMapping
    public CouponDO add(@ApiIgnore @NotEmpty(message = "请填写优惠券名称") String title,
                        @ApiIgnore @NotNull(message = "请填写优惠券面额")@Max(value = 99999999, message = "优惠券面额不能超过99999999") Double couponPrice,
                        @ApiIgnore @NotNull(message = "请填写优惠券门槛价格") @Max(value = 99999999, message = "优惠券门槛价格不能超过99999999") Double couponThresholdPrice,
                        @ApiIgnore @NotNull(message = "请选择优惠券类型") String couponType,
                        @ApiIgnore @NotNull(message = "请填写起始时间") Long startTime,
                        @ApiIgnore @NotNull(message = "请填写截止时间") Long endTime,
                        @ApiIgnore  Long useStartTime, @ApiIgnore Long useEndTime,
                        @ApiIgnore Integer usePeriod, @ApiIgnore String useTimeType,
                        @ApiIgnore @NotNull(message = "请填写发行量") Integer createNum,
                        @ApiIgnore @NotNull(message = "请填写每人限领数量") Integer limitNum) {
        if (limitNum < 0) {
            throw new ServiceException(PromotionErrorCode.E406.code(), "限领数量不能为负数");
        }
        //校验每人限领数是都大于发行量
        if (limitNum > createNum) {
            throw new ServiceException(PromotionErrorCode.E405.code(), "限领数量超出发行量");
        }
        //校验优惠券面额是否小于门槛价格
        //if(couponPrice >= couponThresholdPrice){
        //    throw  new ServiceException(PromotionErrorCode.E409.code(),"优惠券面额必须小于优惠券门槛价格");
        //}

        CouponDO couponDO = new CouponDO();
        couponDO.setTitle(title);
        couponDO.setCouponPrice(couponPrice);
        couponDO.setCouponThresholdPrice(couponThresholdPrice);
        couponDO.setCreateNum(createNum);
        couponDO.setLimitNum(limitNum);
        couponDO.setSellerId(UserContext.getSeller().getSellerId());
        couponDO.setSellerName(UserContext.getSeller().getSellerName());
        couponDO.setCouponType(couponType);
        couponDO.setDisabled(0);
        couponDO.setCreateTime(DateUtil.getDateline());
        couponDO.setUseTimeType(useTimeType);
        couponDO.setUsePeriod(usePeriod);
        couponDO.setUseStartTime(useStartTime);
        couponDO.setUseEndTime(useEndTime);

        //开始时间取前段+00:00:00 结束时间取前段+23:59:59
        String startStr = DateUtil.toString(startTime, "yyyy-MM-dd");
        String endStr = DateUtil.toString(endTime, "yyyy-MM-dd");

        couponDO.setStartTime(DateUtil.getDateline(startStr + " 00:00:00"));
        couponDO.setEndTime(DateUtil.getDateline(endStr + " 23:59:59", "yyyy-MM-dd HH:mm:ss"));

        this.paramValid(startTime, endTime);

        couponDO.setReceivedNum(0);
        couponDO.setUsedNum(0);
        this.couponManager.add(couponDO);

        return couponDO;
    }

    @PutMapping(value = "/{coupon_id}")
    @ApiOperation(value = "修改优惠券", response = CouponDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coupon_id", value = "优惠券id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "title", value = "优惠券名称", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "coupon_price", value = "优惠券面额", required = true, dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "coupon_threshold_price", value = "优惠券门槛价格", required = true, dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "coupon_type", value = "优惠券类型", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "使用起始时间", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "使用截止时间", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "create_num", value = "发行量", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit_num", value = "每人限领数量", required = true, dataType = "int", paramType = "query"),

    })
    public CouponDO edit(@ApiIgnore @PathVariable("coupon_id") @NotNull(message = "请填写优惠券id") Integer couponId,
                        @ApiIgnore @NotEmpty(message = "请填写优惠券名称") String title,
                        @ApiIgnore @NotNull(message = "请填写优惠券面额") Double couponPrice,
                        @ApiIgnore @NotNull(message = "请填写优惠券门槛价格") Double couponThresholdPrice,
                        @ApiIgnore @NotNull(message = "请填写起始时间") Long startTime,
                         @ApiIgnore @NotNull(message = "请填写截止时间") Long endTime,
                         @ApiIgnore @NotNull(message = "请填写发行量") Integer createNum,
                         @ApiIgnore @NotNull(message = "请选择优惠券类型") String couponType,
                         @ApiIgnore  Long useStartTime, @ApiIgnore Long useEndTime,
                         @ApiIgnore Integer usePeriod, @ApiIgnore String useTimeType,
                         @ApiIgnore @NotNull(message = "请填写每人限领数量") Integer limitNum) {
        //校验每人限领数是都大于发行量
        if (limitNum > createNum) {
            throw new ServiceException(PromotionErrorCode.E405.code(), "限领数量超出发行量");
        }

        CouponDO oldCoupon = this.couponManager.getModel(couponId);
        long currTime = DateUtil.getDateline();
        if (oldCoupon.getStartTime() <= currTime && oldCoupon.getEndTime() >= currTime) {
            //throw new ServiceException(PromotionErrorCode.E405.code(), "优惠券已生效，不可进行编辑操作");
        }

        CouponDO couponDO = new CouponDO();
        couponDO.setCouponId(couponId);
        couponDO.setTitle(title);
        couponDO.setCouponPrice(couponPrice);
        couponDO.setCouponThresholdPrice(couponThresholdPrice);
        couponDO.setStartTime(startTime);
        couponDO.setEndTime(endTime);
        couponDO.setCreateNum(createNum);
        couponDO.setLimitNum(limitNum);
        couponDO.setSellerId(UserContext.getSeller().getSellerId());
        couponDO.setSellerName(UserContext.getSeller().getSellerName());
        couponDO.setCouponType(couponType);
        couponDO.setDisabled(0);
        couponDO.setUseTimeType(useTimeType);
        couponDO.setUsePeriod(usePeriod);
        couponDO.setUseStartTime(useStartTime);
        couponDO.setUseEndTime(useEndTime);

        this.paramValid(couponDO.getStartTime(), couponDO.getEndTime());
        this.couponManager.verifyAuth(couponId);
        this.couponManager.edit(couponDO, couponId);
        return couponDO;
    }


    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "删除优惠券")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要删除的优惠券主键", required = true, dataType = "int", paramType = "path")
    })
    public String delete(@PathVariable Integer id) {

        this.couponManager.verifyAuth(id);
        this.couponManager.delete(id);

        return "";
    }

    @PutMapping(value = "/end/{id}")
    @ApiOperation(value = "修改优惠券结束时间")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要修改优惠券的主键", required = true, dataType = "int", paramType = "path")
    })
    public JsonBean endCoupon(@PathVariable Integer id) {
        this.couponManager.endCoupon(id);
        return new JsonBean("结束成功");
    }


    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查询一个优惠券")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要查询的优惠券主键", required = true, dataType = "int", paramType = "path")
    })
    public CouponDO get(@PathVariable Integer id) {

        CouponDO coupon = this.couponManager.getModel(id);
        if (coupon == null || !coupon.getSellerId().equals(UserContext.getSeller().getSellerId())) {
            throw new NoPermissionException("无权操作或者数据不存在");
        }

        return coupon;
    }

    @GetMapping(value = "/{status}/list")
    @ApiOperation(value = "根据状态获取优惠券数据集合")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "优惠券状态 0：全部，1：有效，2：失效", required = true, dataType = "int", paramType = "path", allowableValues = "0,1,2", example = "0：全部，1：有效，2：失效")
    })
    public List<CouponDO> getByStatus(@PathVariable Integer status) {

        return this.couponManager.getByStatus(status);
    }

    /**
     * 参数验证
     *
     * @param startTime
     * @param endTime
     */
    private void paramValid(Long startTime, Long endTime) {

        long nowTime = DateUtil.getDateline();

        //如果活动起始时间小于现在时间
        if (startTime.longValue() < nowTime) {
            //throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动起始时间必须大于当前时间");
        }

        // 开始时间不能大于结束时间
        if (startTime.longValue() > endTime.longValue()) {
            throw new ServiceException(SystemErrorCodeV1.INVALID_REQUEST_PARAMETER, "活动起始时间不能大于活动结束时间");
        }
    }

    @PostMapping(value = "batch_push")
    @ApiOperation(value = "批量给用户推优惠卷")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "coupon_id", value = "优惠卷id", required = true, dataType = "string",paramType = "query"),
            @ApiImplicitParam(name = "content", value = "短信内容", required = true, dataType = "string",paramType = "query"),
            @ApiImplicitParam(name = "mobile", value = "要查询的会员的手机号", required = true, dataType = "string",paramType = "query")
    })
    public Map<String,Object> getByStatus(@ApiIgnore @RequestParam(value = "coupon_id") String couponId, @ApiIgnore  @RequestParam(value = "content") String content, @ApiIgnore  @RequestParam(value = "mobile") String mobile) {
        Map<String, Object> map = new HashMap<>();
        try {

            Assert.isTrue(StringUtil.notEmpty(couponId),"请选择优惠卷!");
            Assert.isTrue(StringUtil.notEmpty(mobile),"请填写手机号!");

            // 1.0 将符号进行转换
            mobile = mobile.trim().replace("，", ",");
            couponId = couponId.trim().replace("，", ",");
            // 1.1 将字符串 转为数组
            String[] mobiles = mobile.split(",");
            String[] couponIds = couponId.split(",");

            String couponsPushedNum = DictUtils.getDictValue("", "优惠券推送", "COUPONS_PUSHED_NUM");
            Assert.isTrue(mobiles.length <= Integer.valueOf(couponsPushedNum),"填入的手机号不能大于"+couponsPushedNum);

            this.couponManager.batchPush(couponIds, content, mobiles);
            map.put("success", true);
            map.put("message", "推送成功!");
            return map;
        } catch (Exception e) {
            logger.error("批量推送优惠券:" + e.getMessage());
            map.put("success", false);
            map.put("message", e.getMessage());
        }
        return map;
    }
}
