package com.enation.app.javashop.seller.api.app;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.base.SettingGroup;
import com.enation.app.javashop.core.client.system.SettingClient;
import com.enation.app.javashop.core.promotion.luck.model.dos.LuckRecordDO;
import com.enation.app.javashop.core.shop.model.dos.ShopSildeDO;
import com.enation.app.javashop.core.shop.model.vo.operator.SellerEditShop;
import com.enation.app.javashop.core.system.model.dto.UpgradeConfig;
import com.enation.app.javashop.core.system.model.dto.UpgradeRespDTO;
import com.enation.app.javashop.core.system.model.vo.PointSetting;
import com.enation.app.javashop.core.trade.order.model.enums.ShipStatusEnum;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.core.wms.model.dos.WmsOrderDO;
import com.enation.app.javashop.core.wms.model.dos.WmsOrderItemsDO;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.enation.app.javashop.framework.util.SqlUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 店铺幻灯片控制器
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 18:50:58
 */
@RestController
@RequestMapping("/seller/app")
@Api(description = "商家APP升级")
public class SellerAppController {
	private static final UpgradeRespDTO DEFAULT_VERSION = new UpgradeRespDTO();
	static{
		DEFAULT_VERSION.setCastUpdate(false);
		DEFAULT_VERSION.setNeedUpgrade(false);
		DEFAULT_VERSION.setLastVersion("0.0.0");
		DEFAULT_VERSION.setUpgradeContent("无");
		DEFAULT_VERSION.setDownloadUrl("");
		DEFAULT_VERSION.setAppName("APP");
	}


	@Autowired
	private SettingClient settingClient;

	@ApiOperation(value = "检测升级", response = UpgradeRespDTO.class)
	@GetMapping("/upgrade")
	public UpgradeRespDTO judgeUpgrade(@RequestParam("current_version") String currentVersion) {
		currentVersion=currentVersion.replaceAll("[a-zA-Z]","" );

		String upgradeJson = settingClient.get(SettingGroup.APP_UPGRADE);
		UpgradeConfig upgradeConfig = JSON.parseObject(upgradeJson, UpgradeConfig.class);

		UpgradeRespDTO upgradeRespDTO = new UpgradeRespDTO();

		// 无需升级
		if(upgradeConfig == null){
			return DEFAULT_VERSION;
		}

		// 判断是否需要升级
		boolean isUpgrade = this.isUpgrade(currentVersion, upgradeConfig.getLastVersion());
		if(isUpgrade){
			upgradeRespDTO.setNeedUpgrade(true);
			// 判断是否需要强制升级
			upgradeRespDTO.setCastUpdate(isCastUpgrade(currentVersion, upgradeConfig.getWithoutVersion()));
		}else{
			upgradeRespDTO.setNeedUpgrade(false);
			upgradeRespDTO.setCastUpdate(false);
		}
		upgradeRespDTO.setAppName(upgradeConfig.getAppName());
		upgradeRespDTO.setDownloadUrl(upgradeConfig.getDownloadUrl());
		upgradeRespDTO.setUpgradeContent(upgradeConfig.getUpgradeContent());
		upgradeRespDTO.setLastVersion(upgradeConfig.getLastVersion());
		return upgradeRespDTO;
	}

	// 判断是否需要升级
	private boolean isUpgrade(String userVersion, String lineVersion){
		if(userVersion == null || lineVersion == null) {
			return false;
		}
		if(userVersion.equalsIgnoreCase(lineVersion)){
			return false;
		}

		String[] userVersionArray = userVersion.split("\\.");
		String[] lineVersionArray = lineVersion.split("\\.");
		for (int i = 0; i < userVersionArray.length; i++) {
			// 防止越界
			if((i + 1) > lineVersionArray.length){
				return false;
			}

			int miniUserVersion = Integer.parseInt(userVersionArray[i]);
			int miniLineVersion = Integer.parseInt(lineVersionArray[i]);
			if(miniUserVersion < miniLineVersion){  // 小需要升级
				return true;
			}else if(miniUserVersion > miniLineVersion){ // 大不需要
				return false;
			}
			// == 就继续 代码不用写
		}
		return true;
	}

	// 判断是否需要强制升级
	private boolean isCastUpgrade(String userVersion, String ignoreVersion){
		if (userVersion == null || ignoreVersion == null) {
			return false;
		}

		if(userVersion.equalsIgnoreCase(ignoreVersion)){
			return false;
		}
		return true;
	}

}
