package com.enation.app.javashop.seller.api.passport;

import com.enation.app.javashop.core.promotion.coupon.model.dos.SendCouponDTO;
import com.enation.app.javashop.core.promotion.coupon.service.CouponManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author JFeng
 * @date 2020/9/29 11:27
 */
@RestController
@RequestMapping("/seller/nosec")
@Api(description = "NOSEC")
public class NoSecurityController {
    @Autowired
    private CouponManager couponManager;


    @PostMapping(value = "/sendCoupon")
    @ApiOperation(value = "僵尸用户发券")
    public void sendCoupon( @Valid @RequestBody SendCouponDTO sendCouponDTO) {
        couponManager.sendCoupon(sendCouponDTO);
    }

}
