package com.enation.app.javashop.seller.api.wms;

import com.enation.app.javashop.core.shop.model.dto.FixTimeSection;
import com.enation.app.javashop.core.shop.service.impl.OrderReceiveTimeManager;
import com.enation.app.javashop.core.wms.model.dto.SortDirectParam;
import com.enation.app.javashop.core.wms.model.dto.WmsOrderQueryParam;
import com.enation.app.javashop.core.wms.model.vo.WmsOrderDetail;
import com.enation.app.javashop.core.wms.model.vo.WmsOrderVO;
import com.enation.app.javashop.core.wms.service.WmsOrderManager;
import com.enation.app.javashop.core.wms.utils.WmsSqlBuilder;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.redis.transactional.RedisTransactional;
import com.enation.app.javashop.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 出库管理
 * @author JFeng
 * @date 2020/9/21 16:50
 */
@Api(description = "出库管理API")
@RestController
@RequestMapping("/seller/wms/order")
@Validated
public class WmsOrderController {
    @Autowired
    private WmsOrderManager wmsOrderManager;
    @Autowired
    private OrderReceiveTimeManager receiveTimeManager;

    @ApiOperation(value = "查询出库单列表")
    @PostMapping("/list")
    public Page<WmsOrderVO> list(@RequestBody  WmsOrderQueryParam param ) {
        Seller seller = UserContext.getSeller();
        param.setSellerId(seller.getSellerId());

        Page<WmsOrderVO> page = this.wmsOrderManager.queryDeliverOrderPage(param);
        return page;
    }

    @ApiOperation(value = "生成出库分拣编号")
    @PostMapping("/sorting")
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "wms_sorting")
    public void createSorting(@RequestBody SortDirectParam sortDirectParam)  {
        Seller seller = UserContext.getSeller();
        sortDirectParam.setSellerId(seller.getSellerId());
        this.wmsOrderManager.createSorting(sortDirectParam);
        // 防止用户一直点击
        try {
            Thread.sleep(10000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @ApiOperation(value = "出库确认")
    @PutMapping("/outStock")
    public Boolean outStock(@RequestParam("sn_list") Integer[]   deliverySns, @RequestParam (value = "delivery_status") String deliveryStatus , @RequestParam (value = "refuse_reason",required = false) String refuseReason) {
        this.wmsOrderManager.outStock(deliverySns,deliveryStatus,refuseReason);
        return true;
    }

    @ApiOperation(value = "查询打印详情")
    @PostMapping("/queryList/{delivery_ids}")
    public List<WmsOrderVO> queryList(@PathVariable("delivery_ids") Integer[] deliveryIds) {
        List<WmsOrderVO> wmsOrderVOS = this.wmsOrderManager.queryList(deliveryIds);
        return wmsOrderVOS;


    }

    @ApiOperation(value = "打印次数+1")
    @PutMapping("/printTimes/{delivery_ids}")
    public Integer printTimes(@PathVariable("delivery_ids") Integer[] deliveryIds) {
        this.wmsOrderManager.updatePrintTimes(deliveryIds);
        return 1;
    }

    @ApiOperation(value = "分拣波次列表")
    @GetMapping("/sortBatchList")
    public List<FixTimeSection> sortBatchList() {
        Seller seller = UserContext.getSeller();
        List<FixTimeSection> sortBatchList = this.receiveTimeManager.getSortBatchList(seller.getSellerId());
        return sortBatchList;
    }


    @ApiOperation(value = "出库确认")
    @PutMapping("/updateReceiveTime")
    public Boolean updateReceiveTime(@RequestParam("delivery_ids") Integer[]   deliveryIds, @RequestParam (value = "receive_day") String receiveDay ) {
        this.wmsOrderManager.updateReceiveTimeByIds(deliveryIds,receiveDay);
        return true;
    }

    @ApiOperation(value = "延迟出库")
    @PutMapping("/delayDispatch")
    public Boolean delayDispatch(@RequestParam("delivery_ids") Integer[]   deliveryIds) {
        this.wmsOrderManager.delayDispatch(deliveryIds);
        return true;
    }
}
