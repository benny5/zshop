package com.enation.app.javashop.seller.api.goods;

import com.enation.app.javashop.core.goods.model.dos.TagsDO;
import com.enation.app.javashop.core.goods.model.vo.GoodsTagVO;
import com.enation.app.javashop.core.goods.service.TagsManager;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 商品标签控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-28 14:49:36
 */
@RestController
@RequestMapping("/seller/goods/tags")
@Api(description = "商品标签相关API")
public class TagsSellerController {

    @Autowired
    private TagsManager tagsManager;


    @ApiOperation(value = "查询商品标签列表", response = TagsDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping
    public Page list(GoodsTagVO goodsTagVO, @ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize) {
        return this.tagsManager.list(goodsTagVO,pageNo, pageSize);
    }

    @ApiOperation(value = "新增商品标签")
    @PostMapping(value = "/addAndEditGoodsTag")
    public void addAndEditGoodsTag(@RequestBody GoodsTagVO goodsTagVO) {
        this.tagsManager.addAndEditGoodsTags(goodsTagVO);
    }

    @ApiOperation(value = "删除商品标签")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tag_ids", value = "标签ID", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = true, dataType = "int", paramType = "query")
    })
    @PostMapping(value = "/deleteTag")
    public void addAndEditGoodsTag(@ApiIgnore String tagIds,@ApiIgnore Integer status) {
        if(StringUtil.isEmpty(tagIds)){
            throw new ServiceException("601","参数问题");
        }
        this.tagsManager.deleteTag(tagIds,status);
    }

    @ApiOperation(value = "查询某标签下的商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "tag_id", value = "标签id", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping("/{tag_id}/goods")
    public Page listGoods(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize, @PathVariable("tag_id") Integer tagId) {

        return this.tagsManager.queryTagGoods(tagId, pageNo, pageSize);
    }

    @ApiOperation(value = "保存某标签下的商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tag_id", value = "标签id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "goods_ids", value = "要保存的商品id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "all_goods", value = "是否选择全部商品", required = true, dataType = "int", paramType = "query"),
    })
    @PutMapping("/{tag_id}/goods/{goods_ids}")
    public String saveGoods(@PathVariable("tag_id") Integer tagId, @PathVariable("goods_ids") Integer[] goodsIds,@RequestParam("all_goods") Integer allGoods) {
        this.tagsManager.saveTagGoods(tagId, goodsIds,allGoods);

        return null;
    }


}
