package com.enation.app.javashop.seller.api.goods;

import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.base.context.RegionFormat;
import com.enation.app.javashop.core.goods.model.vo.SupplierVO;
import com.enation.app.javashop.core.goods.service.SupplierManager;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/seller/goods/supplier")
@Api(description = "供应商相关API")
public class SupplierSellerController {

    @Autowired
    private SupplierManager supplierManager;

    @ApiOperation(value = "查询供应商列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "region", value = "地区id", dataType = "int", paramType = "query"),
    })
    @PostMapping("/query_supplier_list")
    public Page<SupplierVO>  querySupplierList(SupplierVO supplierVO,@RegionFormat @RequestParam(value = "region", required = false) Region region){
       return supplierManager.querySupplierList(supplierVO);
    }
}
