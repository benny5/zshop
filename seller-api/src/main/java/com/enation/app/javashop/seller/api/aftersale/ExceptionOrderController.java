package com.enation.app.javashop.seller.api.aftersale;

import com.enation.app.javashop.core.aftersale.model.dto.ExceptionOrderDTO;
import com.enation.app.javashop.core.aftersale.model.vo.*;
import com.enation.app.javashop.core.aftersale.service.ExceptionOrderManager;
import com.enation.app.javashop.core.base.model.vo.SmsSendVO;
import com.enation.app.javashop.core.base.service.SmsManager;
import com.enation.app.javashop.core.excel.ExcelManager;
import com.enation.app.javashop.core.system.enums.MessageCodeEnum;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;
import io.jsonwebtoken.lang.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 王志杨
 * @since 2020年9月21日 11:45:26
 * 异常订单业务层
 */
@RestController
@RequestMapping("/seller/after-sales")
@Api(description = "异常单相关API")
public class ExceptionOrderController {

    @Autowired
    private ExceptionOrderManager exceptionOrderManager;
    @Autowired
    private ExcelManager excelManager;
    @Autowired
    private SmsManager smsManager;

    @ApiOperation(value = "异常上报", response = Result.class)
    @PostMapping(value = "/exception_order/add_exception_order")
    public Result<ExceptionOrderDTO> addExceptionOrder(@Valid ExceptionOrderDTO exceptionOrderDTO){
        Seller seller = UserContext.getSeller();
        return exceptionOrderManager.insertExceptionOrder(exceptionOrderDTO, seller);
    }

    @ApiOperation(value = "查询异常单列表", response = Page.class)
    @PostMapping(value = "/exception_order/list")
    public Page<ExceptionOrderVO> list(@RequestBody ExceptionQueryParamVO exceptionQueryParamVO){
        Seller seller = UserContext.getSeller();
        Assert.notNull(seller, "商家登录信息获取异常");
        exceptionQueryParamVO.setSellerId(seller.getSellerId());
        return exceptionOrderManager.pageExceptionOrder(exceptionQueryParamVO);
    }

    @ApiOperation(value = "查询处理异常展示信息", response = Result.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "exception_id", value = "异常单ID", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping(value = "/exception_order/query_slove_message/{exception_id}")
    public Result queryExceptionSloveVO(@PathVariable("exception_id") @NotNull(message = "异常单ID不得为空")Integer exceptionId){
        return exceptionOrderManager.queryExceptionStructVO(exceptionId);
    }

    @ApiOperation(value = "根据异常单ID数组批量处理完成修改异常单状态", response = String.class)
    @PutMapping(value = "/exception_order/process_success/{exception_ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "exception_ids", value = "异常Ids数组", allowMultiple = true, required = true, dataType = "int", paramType = "path")
    })
    public Result batchProcessSuccess(@PathVariable("exception_ids") Integer[] exceptionIds) {
        Seller seller = UserContext.getSeller();
        return exceptionOrderManager.batchUpdateExceptionOrderStatus(exceptionIds, seller);
    }

    @ApiOperation(value = "批量异常单撤销", response = String.class)
    @PutMapping(value = "/exception_order/cancel/{exception_ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "exception_ids", value = "异常Ids数组", allowMultiple = true, required = true, dataType = "String", paramType = "path")
    })
    public Result batchCancel(@PathVariable("exception_ids") Integer[] exceptionIds) {
        Seller seller = UserContext.getSeller();
        return exceptionOrderManager.batchCancel(exceptionIds, seller);
    }

    @ApiOperation(value = "导入异常单")
    @PostMapping(value = "/exception_order/import")
    public void importExceptionOrder(@RequestParam("file") MultipartFile file) {
        System.out.println("file = " + file);
        excelManager.importExceptionOrder(file);
    }

    @ApiOperation(value = "异常单导出excle", notes = "export", produces = "application/octet-stream")
    @PostMapping(value = "/exception_order/export")
    public void exceptionOrderExport(@RequestBody ExceptionQueryParamVO exceptionQueryParamVO){
        Seller seller = UserContext.getSeller();
        Assert.notNull(seller, "商家登录信息获取异常");
        exceptionQueryParamVO.setSellerId(seller.getSellerId());
        exceptionQueryParamVO.setPageSize(1000);
        excelManager.exceptionOrderExport(exceptionQueryParamVO);
    }

    @ApiOperation(value = "查询售后服务评分消息模版", response = String.class)
    @GetMapping(value = "/message/template/{order_sn}")
    public String list(@PathVariable("order_sn") @NotBlank(message = "订单号获取异常") String orderSn) {
        return exceptionOrderManager.getAfterSaleTemplate(MessageCodeEnum.AFTERSALEEVALUATION, orderSn);
    }

    @ApiOperation(value = "发送评价短信")
    @PostMapping(value = "/exception_order/send_message")
    public void sendMessage(@RequestBody @Valid SmsSendVO smsSendVO){
        smsManager.send(smsSendVO);
    }

    //加载异常列表所需枚举集合
    @ApiOperation(value = "加载异常列表所需枚举集合", response = Result.class)
    @GetMapping(value = "/exception_order/query_exception_enums")
    public List<Map<String, List<ExceptionEnumStructs>>> queryExceptionEnums(){
        ArrayList<Map<String, List<ExceptionEnumStructs>>> list = new ArrayList<>();
        Map<String, List<ExceptionEnumStructs>> sourcesMap = exceptionOrderManager.queryExceptionSources();
        Map<String, List<ExceptionEnumStructs>> typesMap = exceptionOrderManager.queryExceptionTypes();
        Map<String, List<ExceptionEnumStructs>> statusMap = exceptionOrderManager.queryExceptionStatus();
        Map<String, List<ExceptionEnumStructs>> timeTypeMap = exceptionOrderManager.queryExceptionTimeType();
        list.add(sourcesMap);
        list.add(typesMap);
        list.add(statusMap);
        list.add(timeTypeMap);
        return list;
    }
}
