package com.dag.account.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.account.dao.entity.Member;


public interface MemberMapper extends BaseMapper<Member> {

}