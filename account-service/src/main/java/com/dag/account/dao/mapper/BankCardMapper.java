package com.dag.account.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.account.dao.entity.BankCard;

public interface BankCardMapper extends BaseMapper<BankCard> {

}
