package com.dag.account.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.account.api.dto.account.CountAccountDTO;
import com.dag.account.dao.entity.Account;
import com.dag.account.dao.vo.OperateBalanceVO;
import com.dag.account.dao.vo.OperateFreezeVO;

public interface AccountMapper extends BaseMapper<Account> {

    /** 账户汇总 */
    CountAccountDTO summary(String memberId);

    /** 操作余额 */
    void operateBalance(OperateBalanceVO operateBalanceVO);

    /** 操作冻结 */
    void operateFreeze(OperateFreezeVO operateFreezeVO);

    /** 操作解冻 */
    void operateUnfreeze(OperateFreezeVO operateFreezeVO);

    /** 操作解冻扣除 */
    void operateUnfreezeDeduction(OperateFreezeVO operateFreezeVO);


}
