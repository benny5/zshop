package com.dag.account.dao.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 操作余额
 */
@Data
public class OperateBalanceVO {

    private String accountId;

    private BigDecimal balanceAmount;

    public OperateBalanceVO(String accountId, BigDecimal balanceAmount){
        this.accountId = accountId;
        this.balanceAmount = balanceAmount;
    }

}
