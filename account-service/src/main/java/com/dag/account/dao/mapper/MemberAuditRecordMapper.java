package com.dag.account.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.account.dao.entity.MemberAuditRecord;


public interface MemberAuditRecordMapper extends BaseMapper<MemberAuditRecord> {

}