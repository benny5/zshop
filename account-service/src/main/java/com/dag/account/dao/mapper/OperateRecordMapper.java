package com.dag.account.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.account.dao.entity.OperateRecord;

/**
 * @author 孙建
 * 2020-5-30
 */
public interface OperateRecordMapper extends BaseMapper<OperateRecord> {


}