package com.dag.account.dao.mapper;

import com.dag.account.dao.entity.FundPool;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dag.account.dao.vo.OperateFundPoolVO;

/**
 * @author 孙建
 * @since 2020-05-30
 */
public interface FundPoolMapper extends BaseMapper<FundPool> {

    void operateFundPool(OperateFundPoolVO operateFundPoolVO);

}