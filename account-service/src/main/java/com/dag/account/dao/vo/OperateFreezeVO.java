package com.dag.account.dao.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 操作余额
 */
@Data
public class OperateFreezeVO {

    private String accountId;

    private BigDecimal freezeAmount;

    public OperateFreezeVO(String accountId, BigDecimal freezeAmount){
        this.accountId = accountId;
        this.freezeAmount = freezeAmount;
    }

}
