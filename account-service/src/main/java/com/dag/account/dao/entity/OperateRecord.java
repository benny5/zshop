package com.dag.account.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.dag.common.entity.DataEntity;
import lombok.Data;

/**
 * 操作记录
 * @author 孙建
 */
@Data
@TableName("acc_operate_record")
public class OperateRecord extends DataEntity {

    /**
     * 对象id
     */
    private String objId;
    /**
     * 对象类型
     */
    private Integer objType;
    /**
     * 事件
     */
    private String eventName;
    /**
     * 描述内容
     */
    private String description;
    /**
     * 操作人
     */
    private String createrName;


}
