package com.dag.account.constants;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 提现渠道
 **/
public enum WithdrawChannelEnum {

    BANK_CARD(1, "银行卡"),
    ALIPAY(2, "支付宝"),
    WECHAT(3, "微信");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (WithdrawChannelEnum item : WithdrawChannelEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    WithdrawChannelEnum(int index, String text){
        this.index = index;
        this.text = text;
    }

    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return index == null ? StringUtils.EMPTY : enumMap.get(index);
    }


}
