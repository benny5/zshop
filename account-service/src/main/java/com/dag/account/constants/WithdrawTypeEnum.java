package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 提现类型
 **/
public enum WithdrawTypeEnum {

    ONLINE(1, "线上"),
    OFFLINE(2, "线下");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (WithdrawTypeEnum item : WithdrawTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    WithdrawTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }

    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
