package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 操作对象类型
 */
public enum ObjTypeEnum {

    ACCOUNT(1, "账户");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (ObjTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    ObjTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }

}
