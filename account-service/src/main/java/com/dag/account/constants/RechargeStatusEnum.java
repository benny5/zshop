package com.dag.account.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 充值状态
 * @author 孙建
 */
public enum RechargeStatusEnum {

    /** 充值中 */
    RECHARGING(1, "充值中"),
    /** 充值完成 */
    SUCCESS(2, "充值完成"),
    /** 充值关闭 */
    CLOSE(3, "充值关闭");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (RechargeStatusEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    RechargeStatusEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }

}
