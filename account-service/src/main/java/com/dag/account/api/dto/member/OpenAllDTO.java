package com.dag.account.api.dto.member;

import com.dag.account.api.dto.account.OpenAccountDTO;
import com.dag.common.entity.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 开通会员DTO
 */
@Data
public class OpenAllDTO extends SaaSDTO {

    @NotNull(message = "会员信息不能为空")
    private OpenMemberDTO openMember;

    @NotNull(message = "账户信息不能为空")
    private OpenAccountDTO openAccount;


    @Override
    public void validate() {
        super.validate();

        openMember.validate();

        openAccount.validate();
    }
}
