package com.dag.account.api.dto.account;

import com.dag.common.entity.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 转账DTO
 */
@Data
public class TransferReqDTO extends SaaSDTO{

    /**
     * 交易凭证单号
     */
    @NotNull(message = "交易凭证单号不能为空")
    private String tradeVoucherNo;
    /**
     * 付款人id
     */
    @NotNull(message = "付款人id不能为空")
    private String draweeMemberId;
    /**
     * 付款人账户类型
     */
    @NotNull(message = "付款人账户类型")
    private Integer draweeAccountType;
    /**
     * 收款人id
     */
    @NotNull(message = "收款人id不能为空")
    private String payeeMemberId;
    /**
     * 收款人账户类型
     */
    @NotNull(message = "账户类型不能为空")
    private Integer payeeAccountType;
    /**
     * 付款金额
     */
    @NotNull(message = "付款金额不能为空")
    private BigDecimal amount;
    /**
     * 交易主题
     */
    @NotNull(message = "交易主题不能为空")
    private String tradeSubject;
    /**
     * 交易内容
     */
    @NotNull(message = "交易内容不能为空")
    private String tradeContent;
    /**
     * 转账方式 实时和非实时 TransferTypeEnum
     */
    @NotNull(message = "转账类型不能为空")
    private Integer transferType;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;
    /**
     * 付款人姓名
     */
    private String draweeMemberName;
    /**
     * 收款人姓名
     */
    private String payeeMemberName;

}
