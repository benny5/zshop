package com.dag.account.api.dto.account;

import com.dag.common.entity.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 业务转账DTO
 */
@Data
public class BusinessTransferReqDTO extends SaaSDTO{

    /**
     * 交易凭证单号
     */
    @NotNull(message = "交易凭证单号不能为空")
    private String tradeVoucherNo;
    /**
     * 业务转账明细
     */
    @NotNull(message = "转账明细不能为空")
    private List<TransferReqDTO> transferReqList;

}
