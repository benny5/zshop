package com.dag.account.api;

import com.dag.account.api.dto.payment.*;
import com.dag.account.api.dto.refund.RefundRecordDTO;
import com.dag.account.service.IPaymentService;
import com.dag.account.service.IRefundService;
import com.dag.lock.redis.annotation.LockAction;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付管理
 */
@Slf4j
@RestController
@RequestMapping("/account/payment")
public class PaymentApi {

    @Autowired
    private IPaymentService paymentService;
    @Autowired
    private IRefundService refundService;


    @LockAction(key = "unifiedPay", value = "#payRequestDTO.tradeVoucherNo")
    @ApiOperation(value = "账户统一支付（立即支付场景）", notes = "账户统一支付（立即支付场景）")
    @PostMapping(value = "unifiedPay")
    public UnifiedPayRespDTO unifiedPay(@RequestBody UnifiedPayReqDTO payRequestDTO) {
        payRequestDTO.validate();
        return paymentService.unifiedPay(payRequestDTO);
    }


    @LockAction(key = "freezePrePay", value = "#freezePrePayReqDTO.tradeVoucherNo")
    @ApiOperation(value = "账户冻结预支付（先冻结后支付场景）", notes = "账户冻结预支付（先冻结后支付场景）")
    @PostMapping(value = "freezePrePay")
    public FreezePrePayRespDTO freezePrePay(@RequestBody FreezePrePayReqDTO freezePrePayReqDTO) {
        freezePrePayReqDTO.validate();
        return paymentService.freezePrePay(freezePrePayReqDTO);
    }


    @LockAction(key = "unfreezeConfirmPay", value = "#unfreezeConfirmPayReqDTO.tradeVoucherNo")
    @ApiOperation(value = "账户解冻并确认支付（先冻结后支付场景）", notes = "账户解冻并确认支付（先冻结后支付场景）")
    @PostMapping(value = "unfreezeConfirmPay")
    public UnfreezeConfirmPayRespDTO unfreezeConfirmPay(@RequestBody UnfreezeConfirmPayReqDTO unfreezeConfirmPayReqDTO) {
        unfreezeConfirmPayReqDTO.validate();
        return paymentService.unfreezeConfirmPay(unfreezeConfirmPayReqDTO);
    }


    @LockAction(key = "unfreezeCancelPay", value = "#unfreezeCancelPayReqDTO.tradeVoucherNo")
    @ApiOperation(value = "账户解冻并取消支付（先冻结后支付场景）", notes = "账户解冻并取消支付（先冻结后支付场景）")
    @PostMapping(value = "unfreezeCancelPay")
    public UnfreezeCancelPayRespDTO unfreezeCancelPay(@RequestBody UnfreezeCancelPayReqDTO unfreezeCancelPayReqDTO) {
        unfreezeCancelPayReqDTO.validate();
        return paymentService.unfreezeCancelPay(unfreezeCancelPayReqDTO);
    }


    @LockAction(key = "paymentRecord", value = "#paymentRecordDTO.tradeVoucherNo")
    @ApiOperation(value = "其他支付渠道生成支付记录", notes = "其他支付渠道生成支付记录")
    @PostMapping(value = "paymentRecord")
    public boolean paymentRecord(@RequestBody PaymentRecordDTO paymentRecordDTO) {
        paymentRecordDTO.validate();
        paymentService.paymentRecord(paymentRecordDTO);
        return true;
    }



    /**
     * 接口废弃 移动到RefundApi 下一个版本删除
     */
    @Deprecated
    @ApiOperation(value = "其他支付渠道生成退款记录", notes = "其他支付渠道生成退款记录")
    @PostMapping(value = "refundRecord")
    public boolean refundRecord(@RequestBody RefundRecordDTO refundRecordDTO) {
        refundRecordDTO.validate();
        refundService.refundRecord(refundRecordDTO);
        return true;
    }

}
