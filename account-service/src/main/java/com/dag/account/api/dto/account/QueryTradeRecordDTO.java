package com.dag.account.api.dto.account;

import com.dag.common.entity.SaaSDTO;
import lombok.Data;

/**
 * 交易记录查询DTO
 */
@Data
public class QueryTradeRecordDTO extends SaaSDTO {

    /**
     * 交易会员id
     */
    private String memberId;
    /**
     * 交易账户id
     */
    private String accountId;
    /**
     * 交易类型
     */
    private Integer tradeType;
    /**
     * 资金流向
     */
    private Integer fundFlowType;
    /**
     * 交易渠道
     */
    private Integer tradeChannel;
    /**
     * 支付状态
     */
    private Integer tradeStatus;

}
