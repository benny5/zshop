package com.dag.account.api.dto.payment;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UnfreezeConfirmPayRespDTO extends BaseDTO {

    /**
     * 支付流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 支付金额
     */
    private BigDecimal amount;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static UnfreezeConfirmPayRespDTO bySuccess(String tradeVoucherNo, String message){
        UnfreezeConfirmPayRespDTO unfreezeConfirmPayRespDTO = by(tradeVoucherNo , message);
        unfreezeConfirmPayRespDTO.setSuccess(true);
        return unfreezeConfirmPayRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static UnfreezeConfirmPayRespDTO byFail(String tradeVoucherNo, String message){
        UnfreezeConfirmPayRespDTO unfreezeConfirmPayRespDTO = by(tradeVoucherNo, message);
        unfreezeConfirmPayRespDTO.setSuccess(false);
        return unfreezeConfirmPayRespDTO;
    }


    private static UnfreezeConfirmPayRespDTO by(String tradeVoucherNo, String message){
        UnfreezeConfirmPayRespDTO unfreezeConfirmPayRespDTO = new UnfreezeConfirmPayRespDTO();
        unfreezeConfirmPayRespDTO.setTradeVoucherNo(tradeVoucherNo);
        unfreezeConfirmPayRespDTO.setMessage(message);
        return unfreezeConfirmPayRespDTO;
    }

}
