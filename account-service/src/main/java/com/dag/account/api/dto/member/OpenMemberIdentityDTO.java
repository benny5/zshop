package com.dag.account.api.dto.member;

import com.dag.account.constants.IdentityTypeEnum;
import com.dag.common.entity.BaseDTO;
import com.dag.common.exception.BusinessRuntimeException;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 开通会员身份DTO
 */
@Data
public class OpenMemberIdentityDTO extends BaseDTO {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 身份类型
     */
    @NotNull(message = "身份类型不能为空")
    private Integer identityType;
    /**
     * 身份类型名称
     */
    @NotNull(message = "身份类型名称不能为空")
    private String identityName;


    @Override
    public void validate() {
        super.validate();

        if(identityType == IdentityTypeEnum.PLATFORM.getIndex()){
            throw new BusinessRuntimeException("不能创建此身份的会员");
        }
    }
}
