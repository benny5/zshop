package com.dag.account.api;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dag.account.api.dto.withdraw.AddWithdrawAccountDTO;
import com.dag.account.api.dto.withdraw.QueryWithdrawAccountDTO;
import com.dag.account.api.dto.withdraw.UpdateWithdrawAccountDTO;
import com.dag.account.dao.entity.WithdrawAccount;
import com.dag.account.service.IWithdrawAccountService;
import com.dag.common.entity.IdDTO;
import com.dag.common.entity.PageDTO;
import com.dag.common.entity.ViewPage;
import com.dag.common.utils.ModelMapperUtils;
import com.dag.mybatis.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 提现账户
 * @author chien
 * @since 2020-07-22
 */
@Slf4j
@RestController
@RequestMapping("/account/withdrawAccount")
public class WithdrawAccountApi {

    @Autowired
    private IWithdrawAccountService withdrawAccountService;

    @ApiOperation(value = "查询提现账号列表", notes = "查询提现账号列表")
    @PostMapping(value = "queryWithdrawAccountList")
    public ViewPage<WithdrawAccount> queryWithdrawAccountList(@RequestBody PageDTO<QueryWithdrawAccountDTO> pageDTO) {
        pageDTO.validate();

        QueryWithdrawAccountDTO queryWithdrawAccountDTO = pageDTO.getBody();

        // 封装参数
        WithdrawAccount withdrawAccount = ModelMapperUtils.map(queryWithdrawAccountDTO, WithdrawAccount.class);

        // 根据条件查询列表
        QueryWrapper<WithdrawAccount> queryWrapper = new QueryWrapper<>(withdrawAccount);
        // 封装查询条件
        buildQuery(queryWrapper, withdrawAccount, queryWithdrawAccountDTO);

        PageUtils.sortFiledToQueryWrapper(pageDTO.getOrderBys(), queryWrapper);
        PageHelper.startPage(pageDTO.getPageNum(), pageDTO.getPageSize());
        return PageUtils.convert(new PageInfo<>(withdrawAccountService.list(queryWrapper)));
    }


    @ApiOperation(value = "添加提现账号", notes = "添加提现账号")
    @PostMapping(value = "addWithdrawAccount")
    public boolean addWithdrawAccount(@RequestBody AddWithdrawAccountDTO addWithdrawAccountDTO) {
        addWithdrawAccountDTO.validate();
        WithdrawAccount withdrawAccount = ModelMapperUtils.map(addWithdrawAccountDTO, WithdrawAccount.class);
        withdrawAccount.setCreateTime(new Date());
        return withdrawAccountService.save(withdrawAccount);
    }


    @ApiOperation(value = "根据id更新提现账号", notes = "根据id更新提现账号")
    @PostMapping(value = "updateWithdrawAccountById")
    public boolean updateWithdrawAccountById(@RequestBody UpdateWithdrawAccountDTO updateWithdrawAccountDTO){
        updateWithdrawAccountDTO.validate();
        WithdrawAccount withdrawAccount = ModelMapperUtils.map(updateWithdrawAccountDTO, WithdrawAccount.class);
        return withdrawAccountService.updateById(withdrawAccount);
    }


    @ApiOperation(value = "删除提现账户", notes = "删除提现账户")
    @PostMapping(value = "delWithdrawAccountById")
    public void delWithdrawAccountById(@RequestBody IdDTO<String> idDTO) {
        withdrawAccountService.removeById(idDTO.getId());
    }

    // 特殊查询条件
    private void buildQuery(QueryWrapper<WithdrawAccount> queryWrapper, WithdrawAccount withdrawAccount, QueryWithdrawAccountDTO queryWithdrawAccountDTO) {

    }

}
