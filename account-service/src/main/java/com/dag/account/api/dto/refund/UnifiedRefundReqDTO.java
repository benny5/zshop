package com.dag.account.api.dto.refund;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class UnifiedRefundReqDTO extends BaseDTO {

    /**
     * 支付流水号
     */
    @NotNull(message = "支付流水号不能为空")
    private String serialNo;
    /**
     * 交易凭证单号
     */
    @NotNull(message = "交易凭证单号不能为空")
    private String tradeVoucherNo;
    /**
     * 付款人id
     */
    @NotNull(message = "退款人id不能为空")
    private String refundMemberId;
    /**
     * 付款账户类型
     */
    @NotNull(message = "付款账户类型")
    private Integer accountType;
    /**
     * 退款金额
     */
    @NotNull(message = "退款金额不能为空")
    private BigDecimal amount;
    /**
     * 交易主题
     */
    @NotNull(message = "交易主题不能为空")
    private String tradeSubject;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;

}
