package com.dag.account.api;

import com.dag.account.api.dto.member.OpenAllDTO;
import com.dag.account.api.dto.member.OpenMemberDTO;
import com.dag.account.dao.entity.Member;
import com.dag.account.service.IMemberService;
import com.dag.common.entity.IdDTO;
import com.dag.lock.redis.annotation.LockAction;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 会员操作类
 * @author 孙建
 */
@Slf4j
@RestController
@RequestMapping("/account/member")
public class MemberApi {

    @Autowired
    private IMemberService memberService;


    @LockAction(key = "openMember", value = "#openMemberDTO.mobile")
    @ApiOperation(value = "开通会员", notes = "开通会员")
    @PostMapping(value = "openMember")
    public Member openMember(@RequestBody OpenMemberDTO openMemberDTO) {
        openMemberDTO.validate();
        return memberService.openMember(openMemberDTO);
    }


    @ApiOperation(value = "开通会员和账户", notes = "开通会员和账户")
    @PostMapping(value = "openAll")
    public Member openAll(@RequestBody OpenAllDTO openAllDTO) {
        openAllDTO.validate();
        return memberService.openAll(openAllDTO);
    }


    @ApiOperation(value = "根据ID查询成员", notes = "根据ID查询成员")
    @PostMapping(value = "queryById")
    public Member queryById(@RequestBody IdDTO<String> idDTO) {
        // 验证
        idDTO.validate();
        return memberService.getById(idDTO.getId());
    }


    @ApiOperation(value = "编辑会员资料", notes = "编辑会员资料")
    @PostMapping(value = "updateById")
    public void updateById(@RequestBody Member member) {
        memberService.updateById(member);
    }


    @ApiOperation(value = "删除会员和账户所有信息", notes = "删除会员和账户所有信息")
    @PostMapping(value = "delMemberById")
    public void delMemberById(@RequestBody IdDTO<String> idDTO) {
        memberService.delById(idDTO.getId());
    }
}
