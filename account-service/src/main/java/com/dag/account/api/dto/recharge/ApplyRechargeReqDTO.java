package com.dag.account.api.dto.recharge;

import com.dag.common.entity.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 申请充值DTO
 */
@Data
public class ApplyRechargeReqDTO extends BaseDTO {

    /**
     * 账户id
     */
    @NotNull(message = "账户id不能为空")
    private String accountId;
    /**
     * 充值金额
     */
    @NotNull(message = "充值金额不能为空")
    private BigDecimal amount;
    /**
     * 交易渠道
     */
    @NotNull(message = "交易渠道不能为空")
    private Integer tradeChannel;

}
