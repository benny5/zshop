package com.dag.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dag.account.dao.entity.OperateRecord;
import com.dag.account.dao.mapper.OperateRecordMapper;
import com.dag.account.service.IOperateRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 孙建
 * @since 2020-5-30
 */
@Slf4j
@Service
public class OperateRecordServiceImpl extends ServiceImpl<OperateRecordMapper, OperateRecord> implements IOperateRecordService {

}
