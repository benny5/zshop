package com.dag.account.service.impl;

import com.dag.account.dao.entity.WithdrawAccount;
import com.dag.account.dao.mapper.WithdrawAccountMapper;
import com.dag.account.service.IWithdrawAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chien
 * @since 2020-07-22
 */
@Slf4j
@Service
public class WithdrawAccountServiceImpl extends ServiceImpl<WithdrawAccountMapper, WithdrawAccount> implements IWithdrawAccountService {
	
}
