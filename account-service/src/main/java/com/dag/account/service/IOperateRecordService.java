package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.dao.entity.OperateRecord;

/**
 * @author 孙建
 * @since 2020-5-30
 */
public interface IOperateRecordService extends IService<OperateRecord> {
	
}
