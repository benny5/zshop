package com.dag.account.service;

import com.dag.account.dao.entity.SettleBillItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Administrator
 * @since 2020-07-26
 */
public interface ISettleBillItemService extends IService<SettleBillItem> {

    List<SettleBillItem> listBySettleBillId(String settleBillId);

}
