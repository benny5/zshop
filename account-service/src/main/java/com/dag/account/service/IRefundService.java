package com.dag.account.service;


import com.dag.account.api.dto.refund.RefundRecordDTO;
import com.dag.account.api.dto.refund.UnifiedRefundReqDTO;
import com.dag.account.api.dto.refund.UnifiedRefundRespDTO;

/**
 * @author 孙建
 * @since 2020-06-01
 */
public interface IRefundService {

    UnifiedRefundRespDTO unifiedRefund(UnifiedRefundReqDTO refundRequestDTO);

    void refundRecord(RefundRecordDTO refundRecordDTO);

}
