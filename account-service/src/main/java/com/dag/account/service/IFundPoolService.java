package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.dao.entity.FundPool;

import java.math.BigDecimal;

/**
 * @author 孙建
 * @since 2020-05-30
 */
public interface IFundPoolService extends IService<FundPool> {

    void add(BigDecimal amount, String operatorName);

    void deduction(BigDecimal amount, String operatorName);

    FundPool getFundPool();

}
