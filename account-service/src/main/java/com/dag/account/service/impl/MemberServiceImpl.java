package com.dag.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dag.account.api.dto.account.OpenAccountDTO;
import com.dag.account.api.dto.member.OpenAllDTO;
import com.dag.account.api.dto.member.OpenMemberDTO;
import com.dag.account.api.dto.member.OpenMemberIdentityDTO;
import com.dag.account.dao.entity.Account;
import com.dag.account.dao.entity.BankCard;
import com.dag.account.dao.entity.Member;
import com.dag.account.dao.entity.MemberIdentity;
import com.dag.account.dao.mapper.MemberMapper;
import com.dag.account.service.IAccountService;
import com.dag.account.service.IBankCardService;
import com.dag.account.service.IMemberIdentityService;
import com.dag.account.service.IMemberService;
import com.dag.common.constants.ActiveStatusEnum;
import com.dag.common.constants.AuditStatusEnum;
import com.dag.common.exception.BusinessRuntimeException;
import com.dag.common.utils.IDataUtils;
import com.dag.common.utils.ModelMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 会员操作
 * @author 孙建
 */
@Slf4j
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements IMemberService {

    @Autowired
    private IMemberIdentityService memberIdentityService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IBankCardService bankCardService;

    /**
     * 开通会员
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Member openMember(OpenMemberDTO openMemberDTO) {
        // 保存会员
        Member member = ModelMapperUtils.map(openMemberDTO, Member.class);
        member.setCreateTime(new Date());
        member.setMemberStatus(ActiveStatusEnum.OPEN.getIndex());
        save(member);

        // 保存会员身份
        List<OpenMemberIdentityDTO> identityList = openMemberDTO.getIdentityList();
        if(IDataUtils.isNotEmpty(identityList)){
            List<MemberIdentity> saveIdentityList = new ArrayList<>();
            for (OpenMemberIdentityDTO identityDTO : identityList) {
                MemberIdentity identity = ModelMapperUtils.map(identityDTO, MemberIdentity.class);
                identity.setMemberId(member.getId());
                identity.setAuditStatus(AuditStatusEnum.NO_SUBMIT.getIndex());
                identity.setCreateTime(new Date());
                identity.setOrgId(member.getOrgId());
                identity.setMerchantId(member.getMerchantId());
                saveIdentityList.add(identity);
            }
            memberIdentityService.saveBatch(saveIdentityList);
        }
        return member;
    }

    /**
     * 开通会员和账户
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Member openAll(OpenAllDTO openAllDTO) {
        OpenMemberDTO openMember = openAllDTO.getOpenMember();
        OpenAccountDTO openAccount = openAllDTO.getOpenAccount();

        // 开通会员
        Member member = openMember(openMember);

        // 开通账户
        openAccount.setMemberId(member.getId());
        accountService.openAccount(openAccount);

        return member;
    }


    /**
     * 删除会员和账户所有信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delById(String memberId) {
        List<Account> accountList = accountService.queryByMemberId(memberId);
        if(IDataUtils.isNotEmpty(accountList)){
            for (Account account : accountList) {
                if(account.getTotalAmount().doubleValue() > 0){
                    throw new BusinessRuntimeException("会员账户尚有资金，不能删除会员信息");
                }
            }
        }

        // 删除账户
        Account deleteAccount = new Account();
        deleteAccount.setMemberId(memberId);
        accountService.remove(new QueryWrapper<>(deleteAccount));

        // 删除会员
        removeById(memberId);

        // 删除会员类型
        MemberIdentity deleteMemberIdentity = new MemberIdentity();
        deleteMemberIdentity.setMemberId(memberId);
        memberIdentityService.remove(new QueryWrapper<>(deleteMemberIdentity));

        // 删除银行卡
        BankCard deleteBankCard = new BankCard();
        deleteBankCard.setMemberId(memberId);
        bankCardService.remove(new QueryWrapper<>(deleteBankCard));
    }

}
