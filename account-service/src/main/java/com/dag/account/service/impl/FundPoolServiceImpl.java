package com.dag.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dag.account.dao.entity.FundPool;
import com.dag.account.dao.mapper.FundPoolMapper;
import com.dag.account.dao.vo.OperateFundPoolVO;
import com.dag.account.service.IFundPoolService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 资金池操作
 * @author 孙建
 * @since 2020-05-30
 */
@Slf4j
@Service
public class FundPoolServiceImpl extends ServiceImpl<FundPoolMapper, FundPool> implements IFundPoolService {


    /**
     * 资金池加钱
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(BigDecimal amount, String operatorName) {
        FundPool fundPool = getFundPool();
        amount = amount.abs();
        OperateFundPoolVO operateFundPoolVO = new OperateFundPoolVO(fundPool.getId(), amount);
        baseMapper.operateFundPool(operateFundPoolVO);
    }


    /**
     * 资金池减钱
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deduction(BigDecimal amount, String operatorName) {
        FundPool fundPool = getFundPool();
        amount = amount.abs().multiply(BigDecimal.valueOf(-1));
        OperateFundPoolVO operateFundPoolVO = new OperateFundPoolVO(fundPool.getId(), amount);
        baseMapper.operateFundPool(operateFundPoolVO);
    }


    /**
     * 获取资金池账户
     */
    @Override
    public FundPool getFundPool() {
        return getOne(new QueryWrapper<>());
    }
}
