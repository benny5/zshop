package com.dag.account.service;

import com.dag.account.dao.entity.Account;
import com.dag.account.dao.entity.FundPool;
import com.dag.account.dao.entity.Member;

public interface IPlatformService {

    Member openPlatformMember();

    Member queryPlatformMember();

    Account openPlatformAccount();

    Account openProfitAccount();

    Account queryPlatformAccount();

    Account queryPlatformAccount(Integer accountType);

    FundPool openFundPoolAccount();

}
