package com.dag.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dag.account.api.dto.bankcard.BindBankCardDTO;
import com.dag.account.dao.entity.BankCard;
import com.dag.account.dao.entity.Member;
import com.dag.account.dao.mapper.BankCardMapper;
import com.dag.account.service.IBankCardService;
import com.dag.account.service.IMemberService;
import com.dag.common.constants.ActiveStatusEnum;
import com.dag.common.exception.BusinessRuntimeException;
import com.dag.common.utils.IDataUtils;
import com.dag.common.utils.ModelMapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 银行卡操作
 * @author 孙建
 */
@Slf4j
@Service
public class BankCardServiceImpl extends ServiceImpl<BankCardMapper, BankCard> implements IBankCardService {

    @Autowired
    private IMemberService memberService;

    /**
     * 绑定银行卡
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public BankCard bind(BindBankCardDTO bindBankCardDTO) {
        // 查询会员
        Member member = memberService.getById(bindBankCardDTO.getMemberId());
        if(member == null){
            throw new BusinessRuntimeException("会员不存在");
        }

        // 判断银行卡是否已经存在
        BankCard existsBankCard = this.queryByCardNo(bindBankCardDTO.getCardNo());
        if(existsBankCard != null){
            throw new BusinessRuntimeException("银行卡已存在");
        }

        // 处理默认银行卡
        Integer isDefault = bindBankCardDTO.getIsDefault();
        if(isDefault != null && isDefault == 1){
            // 更新内容
            BankCard updateBankCard = new BankCard();
            updateBankCard.setIsDefault(0);
            // 更新条件
            BankCard conditionBankCard = new BankCard();
            conditionBankCard.setMemberId(member.getId());
            // 更新
            update(updateBankCard, new UpdateWrapper<>(conditionBankCard));
        }

        // 保存银行卡
        BankCard bankCard = ModelMapperUtils.map(bindBankCardDTO, BankCard.class);
        bankCard.setCreateTime(new Date());
        bankCard.setCardStatus(ActiveStatusEnum.OPEN.getIndex());
        save(bankCard);
        return bankCard;
    }



    /**
     * 解绑银行卡
     * @param id 银行卡id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void unbind(String id) {
        BankCard bankCard = getById(id);
        if(bankCard == null){
            throw new BusinessRuntimeException("银行卡不存在");
        }

        // 删除银行卡
        removeById(id);
    }


    /**
     * 根据卡号查询银行卡
     */
    @Override
    public BankCard queryByCardNo(String cardNo) {
        BankCard validateBankCard = new BankCard();
        validateBankCard.setCardNo(cardNo);
        List<BankCard> bankCardList = list(new QueryWrapper<>(validateBankCard));
        return IDataUtils.isNotEmpty(bankCardList) ? bankCardList.get(0) : null;
    }


    /**
     * 根据卡号批量查询银行卡列表
     */
    @Override
    public List<BankCard> queryByCardNoList(List<String> cardNoList) {
        if(IDataUtils.isEmpty(cardNoList)){
            throw new BusinessRuntimeException("查询卡号列表不能为空");
        }
        QueryWrapper<BankCard> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("card_no", cardNoList);
        return list(queryWrapper);
    }

}
