package com.dag.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dag.account.dao.entity.SettleBillItem;
import com.dag.account.dao.mapper.SettleBillItemMapper;
import com.dag.account.service.ISettleBillItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author Administrator
 * @since 2020-07-26
 */
@Slf4j
@Service
public class SettleBillItemServiceImpl extends ServiceImpl<SettleBillItemMapper, SettleBillItem> implements ISettleBillItemService {

    @Override
    public List<SettleBillItem> listBySettleBillId(String settleBillId) {
        SettleBillItem settleBillItem = new SettleBillItem();
        settleBillItem.setSettleBillId(settleBillId);
        QueryWrapper<SettleBillItem> queryWrapper = new QueryWrapper<>(settleBillItem);
        queryWrapper.orderByAsc("create_time");
        return list(queryWrapper);
    }
}
