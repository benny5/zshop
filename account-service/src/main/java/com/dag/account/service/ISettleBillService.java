package com.dag.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dag.account.api.dto.settlebill.CreateSettleBillDTO;
import com.dag.account.api.dto.settlebill.SettleBillDTO;
import com.dag.account.dao.entity.SettleBill;

/**
 * @author Administrator
 * @since 2020-07-26
 */
public interface ISettleBillService extends IService<SettleBill> {

    SettleBill createSettleBill(CreateSettleBillDTO createSettleBillDTO);

    SettleBill next(String id);

    SettleBill queryLastSettleBill(String memberId);

}
