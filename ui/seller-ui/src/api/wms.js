/**
 * 仓库管理相关API
 */

import request from '@/utils/request'

/**
 * 获取出库单列表
 * @param params
 * @returns {Promise<any>}
 */
export function getWmsOrderList(params) {
  return request({
    url: '/seller/wms/order/list',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 确认出库
 * @param params
 * @returns {Promise<any>}
 */
export function putOrderOutStock(params) {
  return request({
    url: '/seller/wms/order/outStock',
    method: 'put',
    loading: false,
    data: {
      delivery_status: params.delivery_status,
      refuse_reason: params.refuse_reason,
      sn_list: params.deliverys
    }
  })
}

/**
 * 导出出库单
 * @param params
 */
export function exportWmsOrder(params) {
  return request({
    responseType: 'blob',
    url: '/seller/wms/excel/skuOrder',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    timeout: 0,
    loading: true,
    data: params
  })
}
/**
 * 生成分拣批次
 * @param params
 */
export function sortingBatch(params) {
  return request({
    url: '/seller/wms/order/sorting',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    timeout: 0,
    loading: true,
    data: params
  })
}
/**
 * 
 * @param params
 */
export function getWmsOrderDetail(params) {
  return request({
    url: '/seller/wms/order/queryList/' + params,
    method: 'post',
    timeout: 0,
    loading: true
  })
}
/**
 * @param {*} params
 * @return {*} 
 */
export function getOrderSorting(params) {
  return request({
    url: '/seller/wms/order/sorting',
    headers: { 'Content-Type': 'application/json' },
    method: 'post',
    loading: false,
    data: params
  })
}

/**
 * @param {*} params
 * @return {*} 
 */
export function getOrderPrintTimes(params) {
  return request({
    url: `/seller/wms/order/printTimes/${params}`,
    method: 'put',
    loading: false
  })
}

/**
 * @param {*} params
 * @return {*} 
 */
export function updateReceiveTime(params) {
  return request({
    url: `/seller/wms/order/updateReceiveTime`,
    method: 'put',
    loading: false,
    params
  })
}

/**
 * @param {*} params
 * @return {*} 
 */
export function delayDispatch(params) {
  return request({
    url: `/seller/wms/order/delayDispatch`,
    method: 'put',
    loading: false,
    params
  })
}

