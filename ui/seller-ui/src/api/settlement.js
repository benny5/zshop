/**
 * 结算单相关API
 */

import request from '@/utils/request'
import md5 from 'js-md5'

/**
 * 账单汇总
 */
export function getCountAccount() {
  return request({
    url: `/seller/order/bills/countAccount`,
    method: 'get',
    loading: false
  })
}

/**
 * 获取结算账单列表
 * @param params
 * @returns {Promise<any>}
 */
export function getSettleMentList(params) {
  return request({
    url: 'seller/order/bills',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 导出结算单
 * @param params
 */
export function exportSettleMentExcel(id) {
  return request({
    url: `seller/order/bills/${id}/export`,
    method: 'get',
    loading: false
  })
}

/**
 * 商家查看某详细账单
 * @param ids
 * @param params
 * @returns {Promise<any>}
 */
export function getBillDetails(ids, params) {
  return request({
    url: `seller/order/bills/${ids}`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 查看本期账单中的订单列表或者退款单列表
 * @param id
 * @param type
 * @param params
 * @returns {Promise<any>}
 */
export function getOrderList(id, type, params) {
  return request({
    url: `seller/order/bills/${id}/${type}`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 卖家对账单进行下一步操作 确认结算
 * @param id
 * @param params
 * @returns {Promise<any>}
 */
export function confirmSettle(id, params) {
  return request({
    url: `seller/order/bills/${id}/next`,
    method: 'put',
    loading: false,
    params
  })
}

/*
  * @date 2020-02-22
  * @author wolfMan
  * @description {获取提现账号信息}
*/
export function getViewWithdraw() {
  return request({
    url: 'seller/members/withdraw/view-withdraw',
    method: 'post',
    loading: false
  })
}

/*
  * @date 2020-02-22
  * @author wolfMan
  * @description {获取提现账号信息}
*/
export function getWithdrawInfo() {
  return request({
    url: 'account/withdraw/queryWithdrawAcount',
    method: 'post',
    loading: false
  })
}

/*
  * @date 2020-02-22
  * @author wolfMan
  * @description {获取提现账号信息}
*/
export function getWithdrawCode() {
  return request({
    url: 'seller/members/withdraw/sms-code',
    method: 'post',
    loading: false
  })
}

/*
  * @date 2020-12-08
  * @author luocheng
  * @description {获取提现账号信息}
*/
export function applyWithdraw(params) {
  return request({
    url: 'seller/members/withdraw/apply-withdraw',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/*
  * @date 2020-12-10
  * @author luocheng
  * @description {提交支付宝提现账号}
*/
export function submitWithdrawAccount(params) {
  return request({
    url: '/account/submitWithdrawAccount',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: false,
    data: params
  })
}

/*
  * @date 2020-12-10
  * @author luocheng
  * @description {提交银行卡提现账号}
*/
export function bindBankCard(params) {
  return request({
    url: '/account/bindBankCard',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: false,
    data: params
  })
}

/*
  * @date 2020-12-10
  * @author luocheng
  * @description {修改支付宝提现账号}
*/
export function updateWithdrawAccount(params) {
  return request({
    url: '/account/updateWithdrawAccount',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: false,
    data: params
  })
}

/*
  * @date 2020-12-10
  * @author luocheng
  * @description {修改银行卡提现账号}
*/
export function updateBankCard(params) {
  return request({
    url: '/account/updateBankCard',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: false,
    data: params
  })
}

/*
* @date 2020-12-10
* @author luocheng
* @description {提现接口}
*/
export function applyWithdrawSubmit(params) {
  return request({
    url: '/account/withdraw/applyWithdraw',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    loading: false,
    data: params
  })
}

/*
  * @date 2020-12-08
  * @author luocheng
  * @description {提现记录}
*/
export function checkPassword(password) {
  password = md5(password);
  return request({
    url: `/seller/login/checkPassword?password=${password}`,
    method: 'get',
    loading: false
  })
}

/*
  * @date 2020-12-08
  * @author luocheng
  * @description {提现记录}
*/
export function getWithdrawApplyList(params) {
  return request({
    url: 'seller/members/withdraw/apply-history',
    method: 'get',
    loading: false,
    params
  })
}
