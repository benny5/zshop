/**
 * 商品规格选择器
 */
import Vue from 'vue'
import UploadSortableOld from './src/main'

UploadSortableOld.install = () => {
  Vue.component(UploadSortableOld.name, UploadSortableOld)
}

export default UploadSortableOld
