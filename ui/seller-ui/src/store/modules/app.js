import Cookies from 'js-cookie'

const app = {
  state: {
    sidebar: {
      opened: !+Cookies.get('sidebarStatus'),
      withoutAnimation: false
    },
    device: 'desktop',
    recruitScrollY: 0,
    searchGoodsName: {},
    language: Cookies.get('adminLanguage') || 'zh'
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
      state.sidebar.withoutAnimation = false
    },
    CLOSE_SIDEBAR: (state, withoutAnimation) => {
      Cookies.set('sidebarStatus', 1)
      state.sidebar.opened = false
      state.sidebar.withoutAnimation = withoutAnimation
    },
    TOGGLE_DEVICE: (state, device) => {
      state.device = device
    },
    SET_LANGUAGE: (state, language) => {
      state.language = language
      Cookies.set('language', language)
    },
    RECRUIT_SCROLLY(state, recruitScrollY) { 
      state.recruitScrollY = recruitScrollY
    },
    SEARCH_GOODS_NAME(state, searchGoodsName) { 
      state.searchGoodsName = searchGoodsName
    }
  },
  actions: {
    toggleSideBar({ commit }) {
      commit('TOGGLE_SIDEBAR')
    },
    closeSideBar({ commit }, { withoutAnimation }) {
      commit('CLOSE_SIDEBAR', withoutAnimation)
    },
    toggleDevice({ commit }, device) {
      commit('TOGGLE_DEVICE', device)
    },
    setLanguage({ commit }, language) {
      commit('SET_LANGUAGE', language)
    },
    changeRecruitScrollY({ commit }, recruitScrollY) {
      commit('RECRUIT_SCROLLY', recruitScrollY)
    },
    saveSearchGoodsName({ commit }, searchGoodsName) {
      commit('SEARCH_GOODS_NAME', searchGoodsName)
    }
  }
}

export default app
