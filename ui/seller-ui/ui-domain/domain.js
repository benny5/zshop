/**
 * Created by Andste on 2018/7/2.
 * buyer_pc : 买家PC端
 * buyer_wap: 买家WAP端
 * seller   : 商家中心
 * admin    : 后台管理
 */

const env = process.server
  ? process.env
  : (global.window ? window.__NUXT__.state.env : {})

module.exports = {
  // 开发环境
  dev: {
    buyer_pc: 'http://localhost',
    buyer_wap: 'http://localhost',
    seller: 'http://localhost',
    admin: 'http://localhost'
  },
  // 生产环境
  pro: {
    buyer_pc : env.DOMAIN_BUYER_PC || 'http://localhost',
    buyer_wap: env.DOMAIN_BUYER_WAP || 'http://localhost:7002',
    seller   : env.DOMAIN_SELLER || 'http://localhost',
    admin    : env.DOMAIN_ADMIN || 'localhost'
  }
}
