import Vue from 'vue'
import RadioColor from './src/main'

RadioColor.install = () => {
  Vue.component(RadioColor.name, RadioColor)
}

export default RadioColor
