/**
 * Created by Andste on 2018/6/15.
 * 优惠券
 * 主要用于楼层
 */

import Vue from 'vue'
import FollowPicker from './src/main'

FollowPicker.install = () => {
  Vue.component(FollowPicker.name, FollowPicker)
}

export default FollowPicker
