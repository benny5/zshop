/*
  * @date 2020-02-03
  * @author wolfMan
  * @description {拼团商品选择器}
*/

import Vue from 'vue'
import EnPintuanDialog from './src/main.vue'

EnPintuanDialog.install = () => {
  Vue.component(EnPintuanDialog.name, EnPintuanDialog)
}

export default EnPintuanDialog
