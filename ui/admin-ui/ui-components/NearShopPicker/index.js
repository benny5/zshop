/*
  * @date 2020-02-20
  * @author wolfMan
  * @description {附近门店}
*/

import Vue from 'vue'
import NearShopPicker from './src/main'

NearShopPicker.install = () => {
  Vue.component(NearShopPicker.name, NearShopPicker)
}

export default NearShopPicker
