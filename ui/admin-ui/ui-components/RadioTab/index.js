import Vue from 'vue'
import RadioTab from './src/main'

RadioTab.install = () => {
  Vue.component(RadioTab.name, RadioTab)
}

export default RadioTab
