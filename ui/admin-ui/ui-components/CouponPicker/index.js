/**
 * Created by Andste on 2018/6/15.
 * 优惠券
 * 主要用于楼层
 */

import Vue from 'vue'
import CouponPicker from './src/main'

CouponPicker.install = () => {
  Vue.component(CouponPicker.name, CouponPicker)
}

export default CouponPicker
