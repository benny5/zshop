/**
 * Created by Andste on 2018/6/15.
 * 文本选择器
 * 主要用于楼层
 */

import Vue from 'vue'
import AdvertPicker from './src/main'

AdvertPicker.install = () => {
  Vue.component(AdvertPicker.name, AdvertPicker)
}

export default AdvertPicker
