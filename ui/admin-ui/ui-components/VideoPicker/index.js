/**
 * Created by Andste on 2018/6/15.
 * 文本选择器
 * 主要用于楼层
 */

import Vue from 'vue'
import GoodsPicker from './src/main'

GoodsPicker.install = () => {
  Vue.component(GoodsPicker.name, GoodsPicker)
}

export default GoodsPicker
