
import request from '@/utils/request'
import { api } from '~/ui-domain'

// 字典模块接口
// export const getDict = params => { return https.post(`${urlHead}/dict/queryListByType`, params); };

export function getDict(params) {
  return request({
    url: `${api.base}/dict/query_list_by_type`,
    method: 'post',
    loading: false,
    params
  })
}
// 储存已获取的字典
let _hasDictArray = [];
// 获取字典
export async function getDictData (dictType) {
  let _params = {
    type: dictType
  };
  await getDict(_params).then(res => {
    let _data = res.data;
    if (_data.length > 0) {
      _hasDictArray[dictType] = _data;
    }
  });
}

// 获取字典并返回
export async function backDictData (dictType) {
  let _params = {
    type: dictType
  };
  let _backVal = null;
  await getDict(_params).then(res => {
    let _data = res.data;
    if (_data.success) {
      _backVal = _data.data;
    }
  });
  return _backVal;
}

// 格式化字典数据
export function filterShowDict (dictType, filterVal) {
  let _dictData = _hasDictArray[dictType];
  let _back = filterVal;
  if (_dictData) {
    for (let item of _dictData) {
      if (filterVal !== null && filterVal !== undefined && item.value.toString() === filterVal.toString()) {
        _back = item.label;
        break;
      }
    }
  }
  return _back;
}

// 值为空显示为0
export const showNumberEmpty = params => { 
  return params || 0; 
}
