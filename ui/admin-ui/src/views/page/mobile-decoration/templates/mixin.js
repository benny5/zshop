/*
  * @date 2020-02-05
  * @author wolfMan
  * @description {}
*/

/** mixin */
export default {
  props: {
    /** 数据 */
    data: {
      type: Object,
      default: () => ({})
    },
    /** 是否为编辑模式 */
    isEdit: {
      type: Boolean,
      default: false
    }
  },
  components: {
    'layout-item': {
      props: ['block', 'isEdit', 'blockHref'],
      template: `<div class="layout-item">
                   <template v-if="isEdit || $parent.isEdit">
                     <slot :block="block"></slot>
                     <template v-if="block.block_type === 'IMAGE'">
                       <img v-if="block.block_value.img_url" :src="block.block_value.img_url">
                       <div v-else-if="isEdit || $parent.isEdit" class="no-image"></div>
                     </template>
                   </template>
                   <a v-else :href="blockHref || $parent.blockHref(block)">
                     <slot :block="block"></slot>
                     <img v-if="block.block_type === 'IMAGE'" :src="block.block_value.img_url">
                   </a>
                 </div>`
    }
  },
  methods: {
    /** 获取区块链接 */
    blockHref(block) {
      if (block.block_type === 'GOODS') {
        if (!block.block_value) return '#'
        return `/goods/${block.block_value.goods_id}`
      }
      if (!block || !block.block_opt) return '#'
      const { opt_type, opt_value } = block.block_opt
      switch (opt_type) {
        // 链接地址
        case 'URL': return opt_value
        // 商品
        case 'GOODS': return `/goods/${opt_value}`
        // 微页面
        case 'OWNPAGE': return `/goods/${opt_value}`
        // 限时秒杀
        case 'SECKILL': return `/seckill`
        // 关键字
        case 'KEYWORD': return `/goods?keyword=${opt_value}`
        // 店铺
        case 'SHOP': return `/shop/${opt_value}`
        // 分类
        case 'CATEGORY': return `/goods?category=${opt_value}`
        default: return '/'
      }
    }
  }
}
