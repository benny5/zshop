/*
  * @date 2020-02-20
  * @author wolfMan
  * @description {楼层公共模块}
*/

import tpl_60 from './tpl_60';
import tpl_61 from './tpl_61';
import tpl_62 from './tpl_62';
import tpl_63 from './tpl_63';
import tpl_64 from './tpl_64';
import tpl_65 from './tpl_65';
import tpl_70 from './tpl_70';
import tpl_71 from './tpl_71';
import tpl_73 from './tpl_73';
import tpl_76 from './tpl_76';

export default {
  60: tpl_60,
  61: tpl_61,
  62: tpl_62,
  63: tpl_63,
  64: tpl_64,
  65: tpl_65,
  70: tpl_70,
  71: tpl_71,
  73: tpl_73,
  76: tpl_76
}
