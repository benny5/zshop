/**
 * Created by Andste on 2018/5/16.
 */

import request from '@/utils/request'

/**
 * 获取页面数据
 * @param client_type
 * @param page_type
 */
export function getPage(client_type, page_type, id) {
  return request({
    url: `admin/pages/${id}`,
    method: 'get'
  })
}
/**
 * 设置APP首页
 * @param id
 */
export function setAppIndex(id, type, params) {
  return request({
    url: `/admin/pages/index/${id}/${type}`,
    method: 'put',
    data: params
  })
}
/**
 * 页面上架
 * @param id
 */
export function setPageUp(id) {
  return request({
    url: `/admin/pages/putaway/${id}`,
    method: 'put'
  })
}
/**
 * 页面下架
 * @param id
 */
export function setPageDown(id) {
  return request({
    url: `/admin/pages/sold_out/${id}`,
    method: 'put'
  })
}

/**
 * 新增页面
 * @param client_type
 * @param page_type
 * @param params
 */
export function addPage(params) {
  return request({
    url: `admin/pages/add`,
    method: 'put',
    data: params
  })
}
/**
 * 页面上架
 * @param id
 * @param page_type
 * @param params
 */
export function putPage(id) {
  return request({
    url: `/admin/pages/putaway/${id}`,
    method: 'put'
  })
}
/**
 * 修改页面
 * @param params
 */
export function editPage(id, params) {
  return request({
    url: `admin/pages/${id}`,
    method: 'put',
    data: params
  })
}
/**
 * 修改页面发布
 * @param params
 */
export function editPutPage(id, params) {
  return request({
    url: `admin/pages/${id}/type=2`,
    method: 'put',
    data: params
  })
}

/**
  * @date 2020/10/29
  * @author kaiqiang
  * @description { 页面复制 }
*/
export function copyPageIndex(params) {
  return request({
    url: '/admin/pages/copy_page_index',
    method: 'get',
    params
  })
}

/**
  * @date 2020/11/10
  * @author luocheng
  * @description { 页面删除 }
*/
export function deletePageIndex(ids) {
  return request({
    url: `/admin/pages/${ids}`,
    method: 'delete'
  })
}

/**
 * ----------以下接口是二次新增接口-------------
 **/

/**
 * 获取楼层列表
 * @param client_type
 * @param page_type
 */
export function getPageList(_params) {
  return request({
    url: `admin/pages/list`,
    params: _params,
    method: 'get'
  })
}
