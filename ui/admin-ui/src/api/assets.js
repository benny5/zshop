/**
 * 资产相关API
 */

import request from '@/utils/request'

/**
 * 获取提现申请审核列表
 * @param params
 * @returns {Promise<any>}
 */
export function getSettleMentList(params) {
  return request({
    url: 'account/withdraw/queryWithdrawList',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 提现申请审核
 * @param params
 * @returns {Promise<any>}
 */
export function withdrawAuditing(params) {
  return request({
    url: 'account/withdraw/auditing',
    method: 'post',
    loading: false,
    data: params
  })
}

/**
 * 标记为已转账
 * @param params
 * @returns {Promise<any>}
 */
export function withdrawMarkTransfer(params) {
  return request({
    url: 'account/withdraw/markTransfer',
    method: 'post',
    loading: false,
    data: params
  })
}

/**
 * 批量导出
 * @param params
 * @returns {Promise<any>}
 */
export function withdrawBillExport(params) {
  return request({
    headers: { 'Content-Type': 'application/json' },
    url: '/account/withdraw/withdrawBillExport',
    responseType: 'blob',
    loading: false,
    method: 'post',
    data: params
  })
}
