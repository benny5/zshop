/**
 * 店铺相关API
 */

import request from '@/utils/request'
import md5 from 'js-md5'

/**
 * 获取店铺列表
 * @param params
 * @returns {Promise<any>}
 */
export function getShopList(params) {
  return request({
    url: 'admin/shops',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取所有店铺
 */
export function getAllShopList() {
  return request({
    url: 'admin/shops/list',
    method: 'get',
    loading: false
  })
}

/**
 * 获取店铺详情
 * @param shop_id
 */
export function getShopDetail(shop_id) {
  return request({
    url: `admin/shops/${shop_id}`,
    method: 'get'
  })
}

/**
 * 关闭店铺
 * @param shop_id
 */
export function closeShop(shop_id) {
  return request({
    url: `admin/shops/disable/${shop_id}`,
    method: 'put'
  })
}

/**
 * 恢复店铺
 * @param shop_id
 */
export function recoverShop(shop_id) {
  return request({
    url: `admin/shops/enable/${shop_id}`,
    method: 'put'
  })
}

/**
 * 修改审核店铺
 * @param shop_id
 * @param params
 */
export function editAuthShop(shop_id, params) {
  return request({
    url: `admin/shops/${shop_id}`,
    method: 'put',
    data: params
  })
}

/**
 * 获取店铺模板列表
 * @param params
 * @returns {Promise<any>}
 */
export function getShopThemeList(params) {
  return request({
    url: 'admin/shops/themes',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 添加店铺模板
 * @param params
 * @returns {*}
 */
export function addShopTheme(params) {
  return request({
    url: 'admin/shops/themes',
    method: 'post',
    data: params
  })
}

/**
 * 获取店铺模板详情
 * @param id
 * @returns {Promise<any>}
 */
export function getShopThemeDetail(id) {
  return request({
    url: `admin/shops/themes/${id}`,
    method: 'get'
  })
}

/**
 * 修改店铺模板
 * @param id
 * @param params
 * @returns {*}
 */
export function editShopTheme(id, params) {
  return request({
    url: `admin/shops/themes/${id}`,
    method: 'put',
    data: params
  })
}

/**
 * 删除店铺模板
 * @param id
 * @returns {*}
 */
export function deleteShopTheme(id) {
  return request({
    url: `admin/shops/themes/${id}`,
    method: 'delete'
  })
}

/**
 * 获取店铺标签
 * @param
 * @returns {Promise<any>}
 */
export function getShopTags() {
  return request({
    url: `/admin/shops/tags`,
    method: 'get'
  })
}

/**
 * 查询所有开启状态的店铺
 * @param str
 */
export function copyShopList(str) {
  return request({
    url: `/admin/shops/getAllShopList?disabled=${str}`,
    method: 'get'
  })
}

/**
 * 迁移老账号至新账号
 * @param params
 * @returns {*}
 */
export function addNewMemberAndMove(params) {
  params.password = md5(params.password)
  return request({
    headers: { 'Content-Type': 'application/json' },
    url: '/admin/members/addNewMemberAndMove',
    method: 'post',
    data: params
  })
}

/**
  * @date 2021/03/10
  * @author kaiqiang
  * @description {  }
*/
export function getAdminShops(params) {
  return request({
    url: '/admin/shops',
    method: 'get',
    params
  })
}

