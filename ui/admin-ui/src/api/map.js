import request from '@/utils/request'

/**
 * 登录
 * @param params
 * @returns { 添加覆盖物 }
 */
export function addSuperStratum(params) {
  return request({
    url: '/admin/shops/gis',
    method: 'post',
    loading: true,
    data: params
  })
}
