
import request from '@/utils/request'
import { api } from '~/ui-domain'

// 字典模块接口
export function getDict(params) {
  return request({
    url: `${api.base}/dict/query_list_by_type`,
    method: 'post',
    loading: false,
    params
  })
}
