// components/group-address/group-address.js
const app = getApp();
let log = require('../../log.js'); // 引用上面的log.js文件

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 获取位置权限
    getLocation: {
      type: Boolean,
      value: false
    },
    // 位置信息
    locationData: {
      type: Object,
      value: {}
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    showChange: false,
    addressShow: '',
    fixedTop: ''
  },
  // 生命周期
  lifetimes: {
    async ready() {
      if (app.globalData.systemInfo) {
        await app.getHeader();
      };
      this.setData({
        fixedTop: app.globalData.navbar.height - 1
      });
      const _locationData = app.globalData.locationData;
      this.setData({
        addressShow: _locationData.addressName
      });
    }
  },
  // 数据监听器
  observers: {
    getLocation(newValue) {
      this.setData({
        showChange: newValue
      });
    },
    locationData() {
      const { locationData } = this.properties;
      if (!locationData.hasAuth || (!locationData.latitude && !locationData.longitude)) {
        return;
      };
      this.setData({
        addressShow: locationData.addressName
      });
      let _history = wx.getStorageSync('locationHistory') || [];
      if (_history.length && _history[0].addressName === locationData.addressName) {
        return;
      };
      _history = _history.filter(item => item.addressName !== locationData.addressName);
      _history.unshift(locationData);
      if (_history.length > 4) {
        _history = _history.splice(0, 4);
      };
      wx.setStorageSync('locationHistory', _history);
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    openSetting: function (res) {
      if (res.detail.authSetting['scope.userLocation'] === true) {
        // 储存获取位置权限信息
        wx.setStorageSync('getLocation', true);
        wx.navigateTo({
          url: '/pages/city-list/city-list'
        });
      } else {
        // 储存获取位置权限信息
        wx.setStorageSync('getLocation', false);
      };
    },
    // 切换选择地址页面
    changeLocation: function () {
      // 获取用户网路状态
      wx.getNetworkType({
        success(response){
          if (response.networkType === 'none') {
            wx.showToast({
              title: '无法连接网络，请检查您的网络',
              icon: 'none'
            });
            setTimeout(() => {
              wx.hideToast();
            }, 1500);
            return;
          };
          // 获取用户授权
          wx.getSetting({
            success(res) {
              wx.navigateTo({
                url: '/pages/city-list/city-list'
              });
              console.log(res);
            }, fail(error) {
              if (error.errMsg === 'getSetting:fail getaddrinfo ENOTFOUND servicewechat.com') {
                wx.showToast({
                  title: '无法连接网络，请检查您的网络',
                  icon: 'none'
                });
              } else {
                wx.showToast({
                  title: '获取位置信息失败，请打开手机定位服务',
                  icon: 'none'
                });
              };
              setTimeout(() => {
                wx.hideToast();
              }, 1500);
            }
          });
        }
      });
    }
  }
})
