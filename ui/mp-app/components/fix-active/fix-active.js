// components/fix-active/fix-active.js
import * as API_Member from '../../api/members'
import { Foundation } from '../../ui-utils/index.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 订单号
    orderSn: {
      type: String,
      value: ''
    },
    // 优惠券信息
    couponData: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    hideCoupon: false,
    order_sn: '',
    coupon_id: '',
    coupon: {}
  },
  // 数据监听器
  observers: {
    orderSn (newVal) {
      this.setData({
        order_sn: newVal
      });
    },
    couponData (newVal) {
      if (newVal) {
        newVal.start_time = Foundation.unixToDate(newVal.start_time, 'yyyy.MM.dd');
        newVal.end_time = Foundation.unixToDate(newVal.end_time, 'yyyy.MM.dd');
      };
      this.setData({
        coupon_id: newVal.coupon_id,
        coupon: newVal
      });
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    openCoupon () {
      const { order_sn, coupon_id } = this.data;
      API_Member.getShareCoupon(order_sn, coupon_id).then(res => {
        this.setData({
          hideCoupon: true
        });
      });
    },
    closeCoupon () {
      this.triggerEvent('hideReceive', false);
    }
  }
})
