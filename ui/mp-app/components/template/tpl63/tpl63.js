// pages/floor/template/tpl63/tpl63.js
import * as API_Goods from '../../../api/goods.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tplItemData: {
      type: Object,
      value: {}
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    outStyle: '',
    boxStyle: '',
    cardTitleStyle: '',
    titleStyle: '',
    layoutBoxStyle: '',
    goodItemStyle: '',
    goodPicWrapStyle: '',

    goodsList: []
  },
  // 生命周期
  lifetimes: {
    async ready() {
      const _this = this;
      const { blockStyle, blockTitle, blockList } = { ...this.properties.tplItemData };
      const _outCom = `padding-left:${blockStyle.pagePadding}px;padding-right:${blockStyle.pagePadding}px;`;
      const _titleCom = `color:${blockTitle.color};font-size:${blockTitle.fontSize * 2}rpx;font-weight:${blockTitle.fontWeight};`;
      this.setData({
        outStyle: blockTitle.showTitle ? _outCom : `${_outCom}margin-left:-${blockStyle.proPadding}rpx;margin-right:-${blockStyle.proPadding}rpx;`,
        boxStyle: blockTitle.showBackground ? `padding:${blockStyle.proPadding}rpx;border-radius:${blockTitle.backRadius}rpx;background:${blockTitle.backColor}` : `margin-left:-${blockStyle.proPadding}rpx;margin-right:-${blockStyle.proPadding}rpx;`,
        cardTitleStyle: `padding-left:${blockStyle.proPadding}rpx;padding-right:${blockStyle.proPadding}rpx;margin-bottom:-${blockStyle.proPadding}rpx;text-align:${blockTitle.titleAlign};`,
        titleStyle: blockTitle.showBackground ? `margin-top:-${blockStyle.proPadding}rpx;${_outCom}` : _titleCom,
        layoutBoxStyle: `padding:${blockStyle.proPadding}rpx;`,
        goodItemStyle: `border-radius:${blockStyle.proRadius*2}rpx;`,
        goodPicWrapStyle: `padding-top:${blockStyle.imgPercent}%;`
      });
      let _list = JSON.parse(JSON.stringify(blockList));
      let _ids = '';
      _list.forEach(item => {
        _ids = _ids === '' ? item.block_value.id : `${_ids},${item.block_value.id}`;
      });
      let response = await API_Goods.getIdsGoodsList(_ids);
      const _data = response.data;
      _data.forEach((item, index) => {
        let show_tag = item.self_operated == 1 ? 1 : item.is_local == 1 ? 2 : 0;
        _list[index].block_value = {
          'id': item.goods_id,
          'goods_image': item.small,
          'goods_name': item.name,
          'goods_des': item.meta_description,
          'goods_price': item.price,
          'show_tag': show_tag,
          'video_url': item.video_url,
          'showVideo': false
        };
      });
      // 筛选无效值
      _list = _list.filter(item => item.block_value.goods_image);

      this.setData({
        goodsList: _list
      });
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getHref: function (e) {
      const _operate = e.currentTarget.dataset.operate;
      this.triggerEvent('hrefEvent', _operate)
    },
    btnclick: function (e) {
      if(!e.currentTarget.dataset.type){
        wx.navigateTo({
          url: e.currentTarget.dataset.link,
        })
      }
    },
    btnclickLink: function (e) {
      wx.navigateTo({
        url: e.currentTarget.dataset.link,
      })
    },
    playVideo: function (e) {
      const { id, index } = e.currentTarget.dataset;
      let { goodsList } = this.data;
      goodsList[index].block_value.showVideo = true;
      let videoplay = wx.createVideoContext(id, this);
      videoplay.play();
      this.setData({
        goodsList
      });
    }
  }
})
