// pages/floor/template/template-components.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tplData: {
      type: Object,
      value: {}
    },
    catsTypeData: {
      type: Array,
      value: []
    },
    cartData: {
      type: Array,
      value: []
    },
    heightData: {
      type: Object,
      value: {}
    }
  },
  // 数据监听器
  observers: {
    catsTypeData(value) {
      this.setData({
        catsTypeList: value
      })
    },
    cartData(value) {
      this.setData({
        cartList: value
      })
    },
    heightData(value) {
      this.setData({
        heightInfo: value
      })
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    heightInfo: {}
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /** 获取区块链接 */
    showHrefEvent: function(obj) {
      const block = obj.detail;
      if (!block || !block.block_opt) {
        return;
      };
      const { opt_type, opt_value } = block.block_opt;
      let _toUrl = '';
      switch (opt_type) {
        // 链接地址
        case 'URL':
          _toUrl = opt_value;
          break;
        // 链接地址
        case 'APPLY':
          _toUrl = '/pages/leader/recruit-show/recruit-show';
          break;
        // 商品
        case 'GOODS':
          _toUrl = `/pages/goods/goods?goods_id=${opt_value}`;
          break;
        // 微页面
        case 'OWNPAGE':
          _toUrl = `/pages/customized/customized?page_id=${opt_value}`;
          break;
        // 限时秒杀
        case 'SECKILL': 
          _toUrl = '/pages/seckill/seckill';
          break;
        // 关键字
        case 'KEYWORD':
          _toUrl = `/pages/goods/goods?keyword=${opt_value}`;
          break;
        // 店铺
        case 'SHOP':
          _toUrl = `/pages/shop/shop_id/shop_id?id=${opt_value}`;
          break;
        // 分类
        case 'CATEGORY':
          _toUrl = `/pages/category/category?category=${opt_value}`;
          break;
        // 直播
        case 'LIVE':
          _toUrl = '/pages/live-list/live-list';
          break;
        default:
          break;
      };
      if (_toUrl) {
        wx.navigateTo({
          url: _toUrl
        })
      };
    },
    /** 滚动区域团购模块置顶 */
    getNextList: function() {
      let myComponent = this.selectComponent('#tpl9001');
      myComponent.getNextList();
    }
  }
})
