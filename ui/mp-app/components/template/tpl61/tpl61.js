// pages/floor/template/tpl61/tpl61.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tplItemData: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    advertList: [],
    pagePadding: 0,
    rowWidth: 0,
    rowHeight: 0,
    cellStyle: ''
  },
  // 生命周期
  lifetimes: {
    ready() {
      const _this = this;
      const { blockStyle, blockList } = { ...this.properties.tplItemData };
      const _windowWidth = wx.getSystemInfoSync().windowWidth;
      // 处理http图片历史数据
      let _blockList = blockList.map(item => {
        if (item.block_value && item.block_value.img_url && item.block_value.img_url.split('http://')[1]) {
          item.block_value.img_url = `https://${item.block_value.img_url.split('http://')[1]}`;
        }
        return item;
      });
      const _imgUrl = _blockList[0].block_value.img_url;
      wx.getImageInfo({
        src: _imgUrl,
        success: function (res) {
          let _useWidth = !blockStyle.pagePadding ? _windowWidth : _windowWidth - blockStyle.pagePadding * 2;
          let _height = res.height / res.width * _useWidth / _blockList.length;
          const _cellCom = `padding:0 ${blockStyle.imgPadding / 2}px;width:${(_useWidth + blockStyle.imgPadding) / _blockList.length}px;height:${_height}px;`;
          let _advertList = _blockList.map((item, index) => {
            item.cellStyle = `left:${(_useWidth + blockStyle.imgPadding) / _blockList.length * index - blockStyle.imgPadding/2}px;${_cellCom}`;
            return item;
          });
          _this.setData({
            advertList: _advertList,
            pagePadding: blockStyle.pagePadding ? blockStyle.pagePadding : 0,
            rowWidth: _useWidth,
            rowHeight: _height
          });
        }
      });
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getHref: function (e) {
      const _operate = e.currentTarget.dataset.operate;
      this.triggerEvent('hrefEvent', _operate)
    }
  }
})
