// components/title-capsule/title-capsule.js
const app = getApp();

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    pageType: {
      type: String,
      value: 'grouping'
    }
  },
  data: {
    imgHttp: 'https://localhost',
    pageLogo: 'logo-grouping',
    navbar: app.globalData.navbar,
    statusBarHeight: 0, // 状态栏高度
    navbarHeight: 0, // 顶部导航栏高度
    navbarBtn: { // 胶囊位置信息
      height: 0,
      width: 0,
      top: 0,
      bottom: 0,
      right: 0
    }
  },
  // 微信7.0.0支持wx.getMenuButtonBoundingClientRect()获得胶囊按钮高度
  attached: async function () {
    if (app.globalData.systemInfo) {
      await app.getHeader();
    };
    this.setData({
      navbar: app.globalData.navbar
    });
  },
  // 生命周期
  lifetimes: {
    ready() {
      const { pageType } = this.properties;
      let _pageLogo = pageType === 'grouping' ? 'logo-grouping' : 'logo-shoping';
      this.setData({
        pageLogo: _pageLogo
      });
    }
  },
  methods: {
  }
})

