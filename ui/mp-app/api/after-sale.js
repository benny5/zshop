/**
 * 申请售后相关API
 */

import request from '../utils/request'

/**
 * 获取售后列表
 * @param params
 * @returns {AxiosPromise}
 */
export function getAfterSale(params) {
  return request.ajax({
    url: '/after-sales/refunds',
    method: 'get',
    loading: true,
    needToken: true ,
    params
  })
}

/**
 * 获取售后申请数据
 * @param order_sn
 * @param sku_id
 */
export function getAfterSaleData(order_sn, sku_id) {
  const params = sku_id ? { sku_id } : {}
  return request.ajax({
    url: `/after-sales/refunds/apply/${order_sn}`,
    method: 'get',
    needToken: true ,
    params
  })
}

/**
 * 获取售后详情
 * @param sn 订单编号
 * @returns {AxiosPromise}
 */
export function getAfterSaleDetail(sn) {
  return request.ajax({
    url: `/after-sales/refund/${sn}`,
    method: 'get',
    needToken: true 
  })
}

/**
 * 申请退款
 * @param params
 */
export function applyAfterSaleMoney(params) {
  return request.ajax({
    url: '/after-sales/refunds/apply',
    method: 'post',
    needToken: true ,
    params
  })
}

/**
 * 申请退货
 * @param params
 */
export function applyAfterSaleGoods(params) {
  return request.ajax({
    url: '/after-sales/return-goods/apply',
    method: 'post', 
    needToken: true ,
    params
  })
}

/**
 * 申请取消订单
 * @param params
 */
export function applyAfterSaleCancel(params) {
  return request.ajax({
    url: '/after-sales/refunds/cancel-order',
    method: 'post', 
    needToken: true ,
    params
  })
}
