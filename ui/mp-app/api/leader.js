/**
 * Created by wolfMan on 2020/05/29.
 */
import request from '../utils/request'

/**
 * 绑定客户与团长关系
 * @param params
 */
export function leaderBand(params) {
  return request.ajax({
    url: '/members/leader/band',
    method: 'post',
    needToken: true,
    loading: true,
    params
  })
}

/**
 * 获取当前团长
 * @param params
 */
export function getLastLeader(params) {
  return request.ajax({
    url: '/members/leader/lastLeader',
    method: 'get',
    needToken: true,
    loading: true,
    params
  })
}

/**
 * 获取附近团长
 * @param params
 * @returns {AxiosPromise}
 */
export function getNearLeader(params) {
  return request.ajax({
    url: '/members/leader/near_leaders',
    method: 'get',
    needToken: true,
    loading: true,
    params
  })
}

/**
 * 团长招募
 * @param params
 */
export function enrollLeader(params){
  return request.ajax({
    url: '/members/leader/leader_enroll',
    method: 'post',
    needToken: true,
    loading: true,
    params
  })
}
