import request from '../utils/request'
import { md5 } from '../lib/md5'

/**
 * 账户密码登录绑定
 * @param uuid
 */
export function loginBindConnectByAccount(uuid, params) {
  const _params = { ...params }
  _params.password = md5(_params.password)
  return request.ajax({
    url: `/passport/login-binder/wap/${uuid}`,
    method: 'post',
    params: _params
  })
}

/**
 * 注册绑定
 * @param uuid
 * @param params
 */
export function registerBindConnect(uuid,params) {
  const _params = { ...params }
  _params.password = md5(_params.password)
  return request.ajax({
    url: `/passport/mini-program/register-bind/${uuid}`,
    method: 'post',
    params:_params
  })
}

/**
 * 微信小程序自动登录
 */
export function loginByAuto(params) {
  return request.ajax({
    url: '/passport/mini-program/auto-login',
    method: 'get',
    loading: true,
    params
  })
}

/**
 * 加密数据解密验证
 * @param params
 */
export function accessUnionID(params) {
  return request.ajax({
    url: '/passport/mini-program/decrypt',
    method: 'get',
    loading: true,
    params
  })
}