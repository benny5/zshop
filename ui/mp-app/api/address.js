/**
 * 收货地址相关API
 */

import request from '../utils/request'
import { api } from '../config/config'

/**
 * 获取收货地址列表
 * @returns {AxiosPromise}
 */
export function getAddressList() {
  return request.ajax({
    url: '/members/addresses',
    method: 'get',
    loading: true,
    needToken: true
  })
}

/**
 * 添加收货地址
 * @param params 地址参数
 * @returns {AxiosPromise}
 */
export function addAddress(params) {
  return request.ajax({
    url: '/members/address',
    method: 'post',
    loading: true,
    needToken: true,
    params
  })
}

/**
 * 编辑地址
 * @param id 地址ID
 * @param params 地址参数
 * @returns {AxiosPromise}
 */
export function editAddress(id, params) {
  return request.ajax({
    url: `/members/address/${id}`,
    method: 'put',
    loading: true,
    needToken: true,
    params
  })
}

/**
 * 删除收货地址
 * @param id
 */
export function deleteAddress(id) {
  return request.ajax({
    url: `/members/address/${id}`,
    method: 'delete',
    needToken: true
  })
}

/**
 * 设置默认地址
 * @param id
 */
export function setDefaultAddress(id) {
  return request.ajax({
    url: `/members/address/${id}/default`,
    method: 'put',
    needToken: true
  })
}

/**
 * 获取某个地址详情
 * @param id
 */
export function getAddressDetail(id) {
  return request.ajax({
    url: `/members/address/${id}`,
    method: 'get',
    loading: true,
    needToken: true
  })
}

/**
 * 获取地区列表
 * @param id
 */
export function getAreas(id) {
  const _api = `${api.base}/regions/@id/children`
  return request.ajax({
    url: _api.replace('@id', id),
    method: 'get'
  })
}

/**
 * 高德地图相关API
 * @param id
 */
export function geoAddress(address, city) {
  const amap = 'https://restapi.amap.com';
  const web_key = '716a1b9c5ab75fc7fcd2ce1c58d3dbc3';
  return request.ajax({
    baseURL: amap,
    url: `${amap}/v3/geocode/geo?key=${web_key}&address=${address}`,
    method: 'get',
    headers: { 'Content-Type': 'application/json' }
  });
}
/**
 * 获取当前位置地标
 * @param lng
 * @param lat
 */
export function getNearbyOld(lng, lat) {
  return request.ajax({
    url: `/map/gaode/${lng}/${lat}/`,
    method: 'get',
    loading: true
  })
}
/**
 * 查询位置所得地标列表
 */
export function getNearbyByKey(params) {
  return request.ajax({
    url: '/map/gaode/inputtips',
    method: 'get',
    loading: true,
    params
  })
}
/**
 * 获取当前位置地标[腾讯地图]
 * @param lng
 * @param lat
 */
export function getNearby(lng, lat) {
  return request.ajax({
    url: `/map/tencent/${lng}/${lat}/`,
    method: 'get',
    loading: true
  })
}
/**
 * 根据省市区名称查区编号
 * @param id
 */
export function getAreasNum(params) {
  return request.ajax({
    url: `${api.base}/regions/area`,
    method: 'get',
    params
  })
}
