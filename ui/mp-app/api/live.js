/**
 * Created by wolfMan on 2020/09/28.
 */
import request from '../utils/request'

/**
 * 获取直播间列表
 * @param params
 */
export function getLiveListInfo(params) {
  return request.ajax({
    url:  '/liveplayer/minipro/getliveinfo',
    method: 'get',
    loading: true,
    params
  })
}