import request from '../utils/request'

export function getWalletIndex() {
  return request.ajax({
    url: '/account/walletIndex',
    method: 'get',
    loading: true,
    needToken: true,
    // isJson: true
  })
}

/**
 * 查询交易记录列表
 * @returns {AxiosPromise}
 */
export function queryTradeRecordList(params) {
  return request.ajax({
    url: '/account/queryTradeRecordList',
    method: 'post',
    loading: true,
    needToken: true,
    isJson: true,
    params
  })
}


/**
 * 查询提现帐号信息
 * @returns {AxiosPromise}
 */
export function queryWithdrawAcount() {
  return request.ajax({
    url: '/account/withdraw/queryWithdrawAcount',
    method: 'post',
    loading: true,
    needToken: true,
    isJson: true
  })
}

/**
 * 申请提现
 */
export function applyWithdraw(params) {
  return request.ajax({
    url: '/account/withdraw/applyWithdraw',
    method: 'post',
    loading: true,
    needToken: true,
    isJson: true,
    params
  })
}

/**
 * 绑定银行卡
 */
export function bindBankCard(params) {
  return request.ajax({
    url: '/account/bindBankCard',
    method: 'post',
    loading: true,
    needToken: true,
    isJson: true,
    params
  })
}

/**
 * 绑定帐户信息（支付宝/微信)
 */
export function bindAccount(params) {
  return request.ajax({
    url: '/account/submitWithdrawAccount',
    method: 'post',
    loading: true,
    needToken: true,
    isJson: true,
    params
  })
}

/**
 * 查询帐户信息
 */
export function getMemberAccount() {
  return request.ajax({
    url: '/account/queryAccount',
    method: 'get',
    needToken: true
  })
}