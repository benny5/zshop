import * as API_Shop from '../../../api/shop.js'
import * as API_Goods from '../../../api/goods.js'
import { Foundation } from '../../../ui-utils/index.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: [],
    searchStatus: false,    //搜索状态-是否已经在搜索了
    categoryFilter: false,  // 分类筛选是否打开
    currentSortType: 'def_desc',    
    scrollTop: 0,
    scrollHeight: 0,
    loading:false,
    finished:false,
    params:{
      seller_id:0,
      keyword: '',//搜索关键字
      page_no:1,
      page_size:10,
      sort: 'def_desc'//筛选条件类型
    },
    pageCount:0,
    msg:''
  },
  getShopInfo:function(){
    let that = this;
    API_Shop.getShopBaseInfo(this.data.params.seller_id).then(response=>{
      wx.setNavigationBarTitle({ title: response.shop_name})
    })
  },
  getGoodsList: function () {
    API_Goods.getGoodsList(this.data.params).then(response=>{
      let _pageCount = Math.ceil(response.data_total / this.data.params.page_size)
      this.setData({ pageCount: _pageCount })
      const data = response.data
      if(data && data.length){
        data.forEach(key=>{
          key.price = Foundation.formatPrice(key.price)
        })
        this.data.goodsList.push(...data)
        this.setData({
          categoryFilter: false,
          goodsList: this.data.goodsList
        });
      }else{
        this.setData({ finished: true })
      }
    })
  },
  openSortFilter: function (event) {
    let currentId = event.currentTarget.id;
    switch (currentId) {
      case 'categoryFilter':
        this.setData({
          categoryFilter: !this.data.categoryFilter
        });
        break;
      case 'defSort':
        var sort = '';
        if (this.data.params.sort == 'def_asc') {
          sort = 'def_desc';
        } else {
          sort = 'def_asc';
        }
        this.setData({
          'params.sort': sort,
          'params.page_no': 1,
          categoryFilter: false,
          finished: false,
          goodsList: []
        });
        this.getGoodsList();
        break;
      case 'priceSort':
        var sort = '';
        if (this.data.params.sort == 'price_asc') {
          sort = 'price_desc';
        } else {
          sort = 'price_asc';
        }
        this.setData({
          'params.sort': sort,
          categoryFilter: false,
          finished: false,
          'params.page_no': 1,
          goodsList: []
        });

        this.getGoodsList();
        break;
      case 'gradeSort':
        var sort = '';
        if (this.data.params.sort == 'grade_asc') {
          sort = 'grade_desc';
        } else {
          sort = 'grade_asc';
        }
        this.setData({
          'params.sort': sort,
          categoryFilter: false,
          finished: false,
          'params.page_no': 1,
          goodsList: []
        });

        this.getGoodsList();
        break;
      default:
        var sort = '';
        if (this.data.params.sort == 'buynum_asc') {
          sort = 'buynum_desc';
        } else {
          sort = 'buynum_asc';
        }
        //销量排序
        this.setData({
          'params.sort': sort,
          categoryFilter: false,
          'params.page_no': 1,
          finished: false,
          goodsList: []
        });
        this.getGoodsList();
    }
  },
  inputChange: function (e) {
    this.setData({
      'params.keyword': e.detail.value || '',
      searchStatus: false
    });
  },
  onKeywordConfirm:function(){
    this.setData({
      'params.page_no': 1,
      goodsList:[]
    })
    this.getGoodsList()
  },
  //店内搜素
  inStoreSearch:function(){
    this.setData({
      'params.page_no':1,
      goodsList: []
    })
    this.getGoodsList()
  },
  //全站搜索
  allSearch:function(){
    wx.navigateTo({
      url: '/pages/category/category?keyword=' + this.data.params.keyword,
    })
  },
  scroll: function (e) {
    let that = this
    if (e.detail.scrollTop > 200) {
      that.setData({
        showGoTop: true
      })
    } else {
      that.setData({
        showGoTop: false
      })
    }
  },
  goTop: function () {this.setData({scrollTop: 0})},
  loadMore:function(e){
    this.setData({ 'params.page_no': this.data.params.page_no += 1});
    if (this.data.pageCount >= this.data.params.page_no){
      this.getGoodsList();
    }else{
      this.setData({msg:'已经到底了~'})
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    that.setData({
      'params.seller_id': options.shop_id
    });
    wx.getSystemInfo({
      success: function(res) {
        that.setData({scrollHeight:res.windowHeight + 'px'})
      },
    })
    this.getShopInfo()
    this.getGoodsList()
  }
})