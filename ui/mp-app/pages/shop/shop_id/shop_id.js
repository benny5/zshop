import * as API_Shop from '../../../api/shop.js'
import * as API_Promotions from '../../../api/promotions.js'
import * as API_Goods from '../../../api/goods.js'
import * as API_Members from '../../../api/members.js'
import { Foundation } from '../../../ui-utils/index.js'
var WxParse = require('../../../lib/wxParse/wxParse.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    shop_id: 0,
    shop: {},
    is_collection: false, //店铺是否收藏
    collection_num: 0, //店铺收藏数
    topNavActive: 'all',
    topNavData: [{
      key: 'all',
      name: '全部商品',
      data: []
    }, {
      key: 'new',
      name: '首发新品',
      data: []
    }, {
      key: 'recommend',
      name: '店主推荐',
      data: []
    }, {
      key: 'hot',
      name: '热卖疯抢',
      data: []
    }],
    shopsildes: [],//店铺幻灯片
    coupons: '',//优惠券列表
    goodsList: [], //显示商品列表
    allGoods: [],//全部商品
    newGoods: [],// 新品上架
    hotGoods: [],// 热销商品
    recGoods: [],// 推荐商品
    scrollHeight: 0,
    scrollTop: 0,
    showGoTop: false //返回顶部按钮
  },
  // 菜单切换
  navToggle(e) {
    const _currentData = e.currentTarget.dataset.current;
    const _current = this.data.topNavActive;
    if (_currentData.key === _current) return;
    this.setData({
      topNavActive: _currentData.key,
      goodsList: _currentData.data
    });
  },
  //获取店铺基本信息
  getShopInfo(){
    let that = this;
    API_Shop.getShopBaseInfo(that.data.shop_id).then(response =>{
      that.setData({
        shop: response,
        collection_num: response.shop_collect
      })
      wx.setNavigationBarTitle({ title: response.shop_name})
      WxParse.wxParse('shopDetail', 'html', response.shop_desc, that)
    })
    // that.getShopSildes()
    // that.getShopCoupons()
    that.getShopTagGoods()
    that.getShopIsCollect()
  },
  //获取店铺幻灯片
  getShopSildes(){
    API_Shop.getShopSildes(this.data.shop_id).then(response => {
      this.setData({ shopsildes: response })
    })
  },
  //获取店铺优惠券
  getShopCoupons(){
    API_Promotions.getShopCoupons(this.data.shop_id).then(response =>{
      this.setData({ coupons: response})
    })
  },
  //领取店铺优惠券
  handleReceiveCoupon(e) {
    const coupon = e.currentTarget.dataset.coupon
    if (!wx.getStorageSync('refresh_token')) {
      wx.showToast({ title: '您还未登录!', image: 'https://localhost/images/icon_error.png' })
      return false
    }
    API_Members.receiveCoupons(coupon.coupon_id).then(() => {
      wx.showToast({ title: '领取成功' })
    })
  },
  //获取店铺标签商品
  async getShopTagGoods() {
    const values = await Promise.all([
      API_Goods.getGoodsList({
        seller_id: this.data.shop_id,
        page_no: 1,
        page_size: 100
      }),
      API_Goods.getTagGoods(this.data.shop_id, 'new', 20),
      API_Goods.getTagGoods(this.data.shop_id, 'recommend', 20),
      API_Goods.getTagGoods(this.data.shop_id, 'hot', 20)
    ]);
    let _goodsAllList = values[0].data;
    if (_goodsAllList && _goodsAllList.length) {
      _goodsAllList.forEach(key => {
        key.price = Foundation.formatPrice(key.price);
        key.priceInt = key.price ? key.price.split('.')[0] : '0';
        key.pricePoint = key.price ? key.price.split('.')[1] : '00';
        key.goods_name = key.name
      });
    };
    let _goodsNewList = values[1].map(key => {
      key.price = Foundation.formatPrice(key.price);
      key.priceInt = key.price ? key.price.split('.')[0] : '0';
      key.pricePoint = key.price ? key.price.split('.')[1] : '00';
      return key;
    });
    let _goodsRecommendList = values[2].map(key => {
      key.price = Foundation.formatPrice(key.price);
      key.priceInt = key.price ? key.price.split('.')[0] : '0';
      key.pricePoint = key.price ? key.price.split('.')[1] : '00';
      return key;
    });
    let _goodsHotList = values[3].map(key => {
      key.price = Foundation.formatPrice(key.price);
      key.priceInt = key.price ? key.price.split('.')[0] : '0';
      key.pricePoint = key.price ? key.price.split('.')[1] : '00';
      return key;
    });
    let _topNavData = this.data.topNavData;
    _topNavData[0].data = _goodsAllList;
    _topNavData[1].data = _goodsNewList;
    _topNavData[2].data = _goodsRecommendList;
    _topNavData[3].data = _goodsHotList;
    let _goodsList = [];
    _topNavData.forEach(item => {
      if (item.key == this.data.topNavActive) {
        _goodsList = item.data;
      };
    });
    this.setData({
      topNavData: _topNavData,
      goodsList: _goodsList
    });
  },
  //获取店铺是否收藏
  getShopIsCollect(){
    if (wx.getStorageSync('refresh_token')){
      API_Members.getShopIsCollect(this.data.shop_id).then(response => {
        this.setData({ is_collection: response.message })
      })
    }
  },
  //收藏店铺
  collectionShop(){
    if(!wx.getStorageSync('refresh_token')){
      wx.showToast({ title: '您还未登录!', image:'https://localhost/images/icon_error.png'})
      return false
    }
    if(this.data.is_collection){
      API_Members.deleteShopCollection(this.data.shop_id).then(()=>{
        this.setData({is_collection:false,collection_num:this.data.collection_num -= 1})
        wx.showToast({title: '取消收藏成功!'})
      })
    }else{
      API_Members.collectionShop(this.data.shop_id).then(()=>{
        this.setData({ is_collection: true, collection_num: this.data.collection_num += 1 })
        wx.showToast({ title: '收藏成功!' })
      })
    }
  },
  //滚动到指定位置显示返回顶部按钮
  scroll: function (e) {
    let that = this
    if (e.detail.scrollTop > 200) {
      that.setData({showGoTop: true})
    } else {
      that.setData({showGoTop: false})
    }
  },
  //返回顶部
  goTop() {this.setData({scrollTop: 0})},
  //模板三中点击导航跳转到指定位置
  hotanchor() {this.setData({scrollTop: 400 })},
  recanchor() {this.setData({scrollTop: 3028})},
  newanchor() {this.setData({scrollTop: 6142})},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    that.setData({shop_id: parseInt(options.id)})
    wx.getSystemInfo({
      success: function (res) {
        that.setData({scrollHeight: res.windowHeight + 'px'});
      }
    });
    that.getShopInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
