/**
 * 结算页
 * 支付配送组件
 */
import * as API_Trade from '../../../../../api/trade'
Component({
  properties: {
    // 社区团购类型
    typeGroup: String,
    // 支付配送数据
    paymentData: Object,
    // 是否显示
    show: Boolean
  },
  data: {
    currentShipFee: {},
    shipFeeList: [],
    hasChecked: ''
  },
  // 数据监听器
  observers: {
    show() {
      const { currentShipFee, shipFeeList } = this.data.paymentData;
      this.setData({
        currentShipFee,
        shipFeeList,
        hasChecked: currentShipFee ? currentShipFee.type : ''
      })
    }
  },
  methods: {
    // 关闭弹窗
    handleClose() {
      this.setData({ show: false })
    },
    // 选择支付方式
    handleChoosePayment(e) {
      const _payment = e.target.dataset.payment;
      if (_payment.enable) {
        this.setData({
          hasChecked: _payment.type,
          currentShipFee: _payment
        })
      }
    },
    // 保存支付配送信息
    handleSavePayment() {
      const { typeGroup, currentShipFee } = this.data;
      if (typeGroup === 'grouping') {
        this.setData({ show: false });
        this.triggerEvent('changed', { currentShipFee })
      } else {
        const shipWayList = [{
          ship_way: currentShipFee.type,
          seller_id: currentShipFee.seller_id
        }];
        API_Trade.setShipWay(shipWayList).then(() => {
          this.setData({ show: false });
          this.triggerEvent('changed', { currentShipFee });
        });
      };
    }
  }
})

