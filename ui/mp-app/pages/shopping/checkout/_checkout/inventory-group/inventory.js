/**
 * 结算页
 * 购物车清单组件
 */
import * as API_Trade from '../../../../../api/trade'
import { Foundation } from '../../../../../ui-utils/index.js'
Component({
  options: {
    addGlobalClass: true,
  },
  properties: {
    inventories: Array,
    inventoriesTotal: Object
  },
  data: {
    inventoriesData: [],
    shopName: ''
  },
  // 生命周期
  lifetimes: {
    ready() {
    }
  },
  // 监听父级传入值变化
  observers: {
    'inventories': function (data) {
      const _inventories = data;
      this.setData({
        shopName: _inventories[0].seller_name
      });
      _inventories.map(item => {
        // 新增allow_local 支持同城=1 allow_global 支持快递=1
        const { price, allow_local, allow_global, allow_self, active_ship_way, seller_id } = item;
        const { local_freight_price, global_freight_price } = price;
        let _shipFeeList = [{
          type: 'GLOBAL',
          name: '快递',
          showName: `快递${global_freight_price || 0}元`,
          feeStr: Foundation.formatPrice(global_freight_price) || '0',
          fee: global_freight_price || 0,
          enable: allow_global === 1,
          seller_id,
        }, {
          type: 'SELF',
          name: '自提',
          showName: `自提(免运费)`,
          feeStr: '0',
          fee: 0,
          enable: allow_self === 1,
          seller_id,
        }];
        const _currentShipFee = _shipFeeList.filter(filterItem => filterItem.type === active_ship_way)[0] || null;
        item.shipFeeList = _shipFeeList;
        item.currentShipFee = _currentShipFee;
        item.currentShipFeeStr = _currentShipFee ? _currentShipFee.name : '';
        return item;
      });
      this.setData({
        inventoriesData: _inventories
      });
    },
  },
  methods: {
    // 显示隐藏商品
    toggleShow(e) {
      let _index = e.currentTarget.dataset.index;
      let { inventoriesData } = this.data;
      inventoriesData[_index].showOpen = !inventoriesData[_index].showOpen;
      this.setData({ inventoriesData });
    },
    /* 显示支付配送选择弹窗 */
    handleShowPayment(e) {
      const _data = e.currentTarget.dataset.payment;
      const _paymentData = {
        currentShipFee: _data.currentShipFee,
        shipFeeList: _data.shipFeeList
      };
      this.triggerEvent('showPayment', _paymentData)
    }
  }
})
