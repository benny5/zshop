// pages/leader-list/leader-list.js
import * as API_Leader from '../../api/leader'
const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 获取位置权限信息
    getLocation: false,
    // 页面涞源
    from: '',
    // 当前团长信息
    currentData: {},
    // 附近团长列表
    leaderList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      from: options.from
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取位置权限
    let _getLocation = wx.getStorageSync('getLocation');
    this.setData({
      getLocation: _getLocation
    });
    this.getCurrentLeader();
    this.getLeaderList();
  },
  // 获取当前团长信息
  async getCurrentLeader() {
    let currentData = await app.getCurrentLeader() || {};
    this.setData({
      currentData
    });
  },
  // 获取附近团长
  async getLeaderList() {
    let leaderList = await app.getLeaderList();
    this.setData({
      leaderList: leaderList.data || []
    });
  },
  // 选中团长
  async selectedLeader(e) {
    const leaderid = e.currentTarget.dataset.leaderid;
    if (leaderid || leaderid === 0) {
      const uuid = wx.getStorageSync('uuid');
      let _params = {
        'leader_id':leaderid,
        'uuid': uuid
      };
      await API_Leader.leaderBand(_params);
    };
    wx.navigateBack({ delta: 1 })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getCurrentLeader();
    this.getLeaderList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})