// pages/active/active.js
import * as API_Floor from '../../api/floor.js'
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageBack: '#f2f2f2',
    floorData: [],
    pageDesc:'',
    shareImage: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.getShareKey('pages/active/active', 'shareActive');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getPageData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getPageData();
    // 停止下拉[回弹]
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let _shareKey = wx.getStorageSync('shareActive') || '';
    let _opt = _shareKey ? `?shareKey=${_shareKey}`: '';
    const _this = this;
    return {
      title: _this.data.pageDesc || app.globalData.shareTitle,
      desc: _this.data.pageDesc || app.globalData.shareTitle,
      path: `/pages/active/active${_opt}`,
      imageUrl: _this.data.shareImage
    };
  },
  /** 获取页面楼层数据 */
  getPageData: function () {
    const { province, city } = app.globalData.locationData;
    const _position = city || province;
    API_Floor.getDiscoverData('WXS',  _position).then(response => {
      this.setData({
        pageBack: response.background,
        floorData: response.page_data ? JSON.parse(response.page_data) : [],
        pageDesc: response.page_desc,
        shareImage: response.share_image
      });
    });
  }
})