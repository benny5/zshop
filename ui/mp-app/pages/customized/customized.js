// pages/customized/customized.js
import * as API_Floor from '../../api/floor.js'
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageBack: '#f2f2f2',
    page_id: '',
    floorData: [],
    pageDesc:'',
    shareImage: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getPageData(options.page_id);
    this.setData({
      page_id: options.page_id
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.getShareKey('pages/customized/customized', 'shareCustomized');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let _shareKey = wx.getStorageSync('shareCustomized') || '';
    const { page_id, pageDesc, shareImage } = this.data;
    let _opt = _shareKey ? `?page_id=${page_id}&shareKey=${_shareKey}` : `?page_id=${page_id}`;
    return {
      title: pageDesc || app.globalData.shareTitle,
      desc: pageDesc || app.globalData.shareTitle,
      path: `/pages/customized/customized${_opt}`,
      imageUrl: shareImage
    };
  },
  /** 获取页面定位 */
  getLocation: function () {

  },
  /** 获取页面楼层数据 */
  getPageData: function (page_id) {
    API_Floor.getFloorData('WXS', '0', page_id).then(response => {
      this.setData({
        pageBack: response.background,
        floorData: response.page_data ? JSON.parse(response.page_data) : [],
        pageDesc: response.page_desc,
        shareImage: response.share_image
      });
    });
  }
})