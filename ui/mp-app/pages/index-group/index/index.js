// pages/index-group/index/index.js
import * as API_Floor from '../../../api/floor.js'
import * as API_Shop from '../../../api/shop.js'
const app = getApp();
let log = require('../../../log.js'); // 引用上面的log.js文件

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 页面颜色
    pageBack: '#f4f4f4',
    // 自定义头部高度
    navHeight: 0,
    // 获取位置权限信息
    getLocation: false,
    // 顶部地址
    locationData: {},
    // 滚动区域高度
    scrollHeight: 0,
    // 分享图片
    shareImage: '',
    // 楼层数据
    floorData: [],
    // 社区团购分组数据
    catsTypeData: [],
    // 团购购物车
    cartData: [],
    // 距离数据
    heightData: {},
    pageDesc: '',
    hideGoTop: true,
    isShowPoster: true,
    newcomer_coupon: true,
    posterImageUrl: 'http://zkq233.cn/upload/2020/11/counpon-f977073d92d048499316c2912793aef8.png',
    posterImageUrl2: 'http://zkq233.cn/upload/2020/11/counpon-receive-8bba04d765e5421d83d27c13008a225c.png',
    posterImageUrl3: 'http://zkq233.cn/upload/2020/11/底-a306e3402c07412e9dbb2557c51216d2.png',
    posterImageUrl4: 'http://zkq233.cn/upload/2020/11/开心收下-59a1a233053344c2a2261e811d540bcd.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      navHeight: app.globalData.navbar.height,
      scrollHeight: app.globalData.systemInfo.windowHeight - app.globalData.navbar.height + 'px'
    });
    this.setData({
      locationData: app.globalData.locationData,
      floorData: [],
      shareImage: ''  
    });
    // if (this.data.isShowPoster) {
    //   console.log(2333)
    // }
    this.getPageData();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: async function () {
    // 获取位置权限
    let _getLocation = wx.getStorageSync('getLocation');
    this.setData({
      getLocation: _getLocation
    });
    // 获取分享时需要绑定的key
    app.getShareKey('pages/index-group/index/index', 'shareGrouping');
    this.setData({
      locationData: app.globalData.locationData
    });
    // 获取团购购物车列表信息
    let shetuan_cart = await app.getTuanCart();
    this.setData({
      cartData: shetuan_cart
    });
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      locationData: app.globalData.locationData,
      floorData: [],
      shareImage: ''
    });
    this.getPageData('pull');
  },
  // 获取页面楼层数据
  getPageData: function (_type) {
    const { province, city } = this.data.locationData;
    let _position = city || province;
    // 2:团购首页
    API_Floor.getFloorCityData('WXS', '2', _position).then(async response => {
      if (_type === 'pull') {
        // 停止下拉[回弹]
        wx.stopPullDownRefresh();
      };
      let _pageData = response.page_data || '';
      let catsTypeList = [];
      if (_pageData.indexOf('"tpl_id":9000') > -1 || _pageData.indexOf('"tpl_id":9001') > -1) {
        await Promise.all([
          this.getCommunityCats()
        ]).then(res => {
          catsTypeList = res[0];
        });
      };
      this.setData({
        pageBack: response.background,
        floorData: response.page_data ? JSON.parse(response.page_data) : [],
        catsTypeData: catsTypeList,
        pageDesc: response.page_desc,
        shareImage: response.share_image
      });
    }).catch(error => {
      log.error(error);
    });
  },
  // 获取当前位置社区团购店铺分组列表数据
  getCommunityCats: async function() {
    const { locationData } = this.data;
    let _params = {
      'lat': locationData.latitude,
      'lng': locationData.longitude,
    };
    let catsTypeList = await API_Shop.getCommunityCats(_params);
    return catsTypeList;
  },
  // 页面滚动
  onPageScroll: function (e) {
    if (e.scrollTop > 200) {
      this.setData({hideGoTop: false})
    } else {
      this.setData({hideGoTop: true})
    };
    const _pageData = JSON.stringify(this.data.floorData);
    if (_pageData.indexOf('"tpl_id":9001') > -1) {
      let query = wx.createSelectorQuery();
      query.select('#tpl9001').boundingClientRect(rect => {
        const _sheTop = rect.top; // 社团组件距离顶部距离
        const { navHeight } = this.data;
        let _hight = {
          topHeight: navHeight
        };
        _hight.scrollY = _sheTop <= 0 || (_sheTop > 0 && _sheTop - navHeight <= 0);
        this.setData({
          heightData: _hight
        });
      }).exec();
    };
  },
  // 直播
  goLivePlay() {
    wx.navigateTo({ url: '/pages/live-list/live-list'})
  },
  // 返回顶部
  goTop: function () {
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 300
    });
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    const { floorData } = this.data;
    if (floorData.length && floorData[floorData.length - 1]) {
      /** 滚动区域团购模块置顶 */
      let myComponent = this.selectComponent('#tpl9001');
      if (myComponent) {
        myComponent.getNextList();
      };
    };
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let _shareKey = wx.getStorageSync('shareGrouping') || '';
    let _opt = _shareKey ? `?shareKey=${_shareKey}`: '';
    const { pageDesc, shareImage } = this.data;
    return {
      title: pageDesc || app.globalData.shareTitle,
      desc: pageDesc || app.globalData.shareTitle,
      path: `/pages/index-group/index/index${_opt}`,
      imageUrl: shareImage
    };
  },
  /**
    * @date 2020/11/03
    * @author kaiqiang
    * @description { 新人点击领取优惠卷 }
  */
  receiveCoupon() {
    this.setData({
      newcomer_coupon: false
    })
  }
})