import * as API_Goods from '../../api/goods'
import { Foundation } from '../../ui-utils/index.js'

Page({
  data: {
    goodsList: [], // 商品列表
    currentCategory: {},
    categoryFilter: false, // 分类筛选是否打开
    scrollLeft: 0,
    scrollTop: 0,
    scrollHeight: 0,
    tapData: [
      {
        'name': '综合排序',
        'type': 'def',
        'needSort': false,
        'id': 'defSort'
      },
      {
        'name': '价格',
        'type': 'price',
        'needSort': true,
        'id': 'priceSort'
      },
      {
        'name': '销量',
        'type': 'buynum',
        'needSort': true,
        'id': 'buynumSort'
      }
    ],
    currentTap: 'def',
    params:{
      page_no: 1,
      page_size: 20,
      sort: 'def_desc'
    },
    finished: false,
    showGoTop: false,
    pageCount: 0,
    msg: ''
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    let that = this
    if (options.category) {
      that.setData({'params.category': parseInt(options.category)});
    } else {
      that.setData({'params.keyword': options.keyword});
    };
    wx.getSystemInfo({
      success: function (res) {
        let query = wx.createSelectorQuery();
        query.select('.top').boundingClientRect(rect => {
          that.setData({
            scrollHeight: res.windowHeight - rect.height + "px"
          });
        }).exec();
      }
    });
    that.getGoodsList();
  },
  // 获取商品数据
  getGoodsList: function () {
    let that = this;
    let { currentTap } = this.data;
    let params = JSON.parse(JSON.stringify(this.data.params));
    if (params.sort_need) {
      params.sort = `${currentTap}_${params.sort_type}`;
    };
    delete params.sort_type;
    delete params.sort_need;
    API_Goods.getGoodsList(params).then((res)=> {
      let _pageCount = Math.ceil(res.data_total / this.data.params.page_size);
      this.setData({ pageCount: _pageCount })
      let data = res.data;
      if (data && data.length) {
        data.forEach(key => {
          key.price = Foundation.formatPrice(key.price)
        })
        this.setData({
          categoryFilter: false,
          goodsList: that.data.goodsList.concat(data)
        });
      } else {
        that.setData({ finished: true })
      };
    });
    wx.setNavigationBarTitle({ title: "商品分类"})
  },
  // 显示搜索页面
  showSearch () {
    let pages = getCurrentPages();
    if (pages[pages.length-2] && pages[pages.length-2].route === 'pages/search/search') {
      wx.navigateBack();
    } else {
      wx.navigateTo({
        url: 'pages/search/search',
      });
    }
  },
  loadMore: function (e) {
    this.setData({ 'params.page_no': this.data.params.page_no += 1});
    if (this.data.pageCount >= this.data.params.page_no) {
      this.getGoodsList();
    } else {
      this.setData({ msg: '已经到底了~' })
    }
  },
  scroll:function(e){
    if (e.detail.scrollTop > 200) {
      this.setData({showGoTop: true})
    } else {
      this.setData({showGoTop: false})
    }
  },
  goTop:function(){
    this.setData({scrollTop:0})
  },
  goseller(e){
    wx.navigateTo({
      url: '/pages/shop/shop_id/shop_id?id='+e.currentTarget.dataset.shop_id
    })
  },
  // 排序
  openSortFilter: function (e) {
    let { sort, type } = e.currentTarget.dataset;
    this.setData({
      categoryFilter: false,
      finished: false,
      goodsList: []
    });
    let { params } = this.data;
    params.page_no = 1;
    params.sort_need = sort;
    if (this.data.currentTap === type) {
      params.sort_type = params.sort_type === 'desc' ? 'asc' : 'desc';
      this.setData({
        params
      });
    } else {
      params.sort_type = 'desc';
      this.setData({
        params,
        currentTap: type
      });
    };
    this.getGoodsList();
  }
})