const beh = require('../../../../utils/behavior.js')
import * as API_Members from '../../../../api/members'
import { Foundation } from '../../../../ui-utils/index'
Component({
  properties: {
    goodsId:String,
    grade:String
  },
  data: {
    finished:false,
    params:{
      page_no:1,
      page_size:10,
      grade:''
    },
    comments:[],
    showPopup:false
  },
  behaviors: [beh],
  lifetimes: {
    attached() {
      
    },
    ready() {
      this.getGoodsComments()
    }
  },
  methods: {
    switchCommentsPop(){
      this.setData({ showPopup : true})
    },
    handleCommentsClose(){
      this.setData({showPopup:false})
    },
    //图片预览
    handleImagePreview(e){
      let imgArr = e.currentTarget.dataset.urls
      let img = e.currentTarget.dataset.img
      wx.previewImage({ urls: imgArr,current:img})
    },
    filterGrade(val) {
      switch (val) {
        case 'bad':
          return '差评'
        case 'neutral':
          return '中评'
        default:
          return '好评'
      }
    },
    onLoadmore(){
      this.setData({'params.page_no':this.data.params.page_no += 1})
      this.getGoodsComments()
    },
    getGoodsComments(){
      this.data.params.comment_type = 'INITIAL'
      API_Members.getGoodsComments(this.data.goodsId,this.data.params).then(response=>{
        const {data} = response
        if (!data && !data.length) {
          this.setData({ finished: true })
        }else{
          data.forEach(key=>{
            key.grade = this.filterGrade(key.grade)
            key.create_time = Foundation.unixToDate(key.create_time,'yyyy-MM-dd')
            if (key.additional_comment && key.additional_comment.audit_status === 'PASS_AUDIT') {
              key.additional_comment.create_time = Foundation.unixToDate(key.additional_comment.create_time,'yyyy-MM-dd')
            }
          })
          this.data.comments.push(...data)
          this.setData({ comments: this.data.comments})
        }
      })
    }
  }
})