import * as API_Members from '../../../../api/members'
Component({
  properties: {
    promotions: {
      type: Array,
      value: {},
    }
  },
  data: {
    showPromotion: true,
    showPopup: false
  },
  observers: {
    promotions: function (newVal) {
      let flag = false
      const proms = ['full_discount_vo', 'half_price_vo', 'minus_vo']
      newVal.forEach(item => {
        proms.forEach(porm => {
          if (item[porm]) flag = true
        })
      })
      this.setData({ showPromotion: flag })
    }
  },
  methods: {
    /** 显示弹窗 */
    popup() {
      this.selectComponent('#bottomFrame').showFrame();
    },
    handleToShow() {
      this.setData({ showPopup: true })
    },
    handleToClose() {
      this.setData({ showPopup: false })
    }
  }
})