import * as API_Trade from "../../../api/trade";
const util = require('../../../utils/util.js');
import regeneratorRuntime from '../../../lib/wxPromise.min.js'
const app = getApp();
import * as API_Connect from '../../../api/connect'
import * as API_Common from '../../../api/common'
import * as API_Member from '../../../api/members'
import * as API_Passport from '../../../api/passport'

Page({
  data: {
    uuid: '',
    code: '',
    // 默认不显示
    showWxAuth: false,
    projectName: '',
    _captcha_url: ''
  },
  onLoad(options) {
    const _projectName = app.globalData.projectName;
    this.setData({
      projectName: _projectName
    });
    // 如果未授权 进行授权
    let _wxauth = wx.getStorageSync('wxauth'); // 未微信授权
    this.setData({ showWxAuth: _wxauth });
    const uuid = wx.getStorageSync('uuid')
    const _captcha_url = API_Common.getValidateCodeUrl(uuid, 'LOGIN')
    this.setData({
      uuid: uuid,
      captcha_url: _captcha_url
    })
  },
  onShow() {
    if (wx.getStorageSync('refresh_token')) {
      let _pageList = getCurrentPages();
      let _length = getCurrentPages().length;
      if (_pageList[_length - 2].route === 'pages/ucenter/index/index') {
        wx.switchTab({ url: '/pages/ucenter/index/index' });
      } else {
        wx.navigateBack();
      };
    }
  },
  //微信授权登录
  wxLoginAuth(e) {
    var that = this;
    if (e.detail.userInfo) {
      wx.setStorageSync('wxauth', true);
      wx.setStorageSync('nickName', e.detail.userInfo.nickName);
      wx.login({
        success: res => {
          const _code = res.code;
          let _parmas = {
            uuid: this.data.uuid,
            code: _code
          };
          API_Passport.getUserInfo(_parmas).then(response => {
            if (response.auto_login === 'true') {
              app.afterLoginInfo(response);
            } else {
              app.globalData.openid = response.openid;
              wx.setStorageSync('openid', response.openid);
              _parmas.encrypted_data = e.detail.encryptedData;
              _parmas.iv = e.detail.iv;
              API_Passport.decryptWechat(_parmas).then(codeRes => {
                app.globalData.unionid = codeRes.unionId;
                wx.setStorageSync('unionid', codeRes.unionId);
                const _pages = getCurrentPages();
                const _currentPage = _pages[_pages.length - 1];
                if (_currentPage.route !== 'pages/ucenter/index/index') {
                  that.setData({
                    showWxAuth: true
                  });
                } else {
                  wx.navigateBack();
                };
              });
            };
          }).catch(err => {
            console.log(err);
          });
        }
      });
    };
  },
  //登录
  async accountLogin() {
    if (this.data.password.length < 1 || this.data.username.length < 1) {
      wx.showModal({
        title: '提示',
        content: '请输入用户名或密码',
        showCancel: false,
        confirmColor:'#f42424'
      });
      return false;
    }
    // 登录绑定事件
    // const params = {
    //   captcha: this.data.code
    // }
    // 存储access_token refresh_token user
    wx.setStorageSync('access_token', access_token)
    wx.setStorageSync('refresh_token', refresh_token)
    wx.setStorageSync('uid', uid)
    // 获取用户信息
    await app.getUserAllInfo();
    // const user = await API_Member.getUserInfo();
    // wx.setStorageSync('user', user)
    // 获取购物车信息
    // const cart_list = await API_Trade.getCarts()
    // wx.setStorageSync('shoplist', cart_list)
    wx.navigateBack({ delta: 1 })
  }
})