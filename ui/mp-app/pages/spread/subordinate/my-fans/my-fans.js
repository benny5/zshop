import * as API_Distribution from '../../../../api/distribution.js'

Component({

  /**
   * 组件的属性列表
   */
  properties: {
    scrollH: {
      type: Number,
      value: 0
    }
  },

  /*
    开启全局样式
  */
  options: {
    addGlobalClass: true
  },

  /**
   * 组件的初始数据
   */
  data: {
    dataList: [],
    params: {
      page_no: 1,
      page_size: 10,
      member_id: ''
    },
    page_count: 0 // 总页
  },
  // 生命周期
  lifetimes: {
    ready() {
      this.getPageData();
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
      * @date 2020/08/27
      * @author kaiqiang
      * @description { 下拉刷新时重新请求数据 }
    */
    getDataList() {
      this.setData({
        'params.page_no': 1
      });
      this.getPageData();
    },

    /**
     * @date 2020/08/24
     * @author kaiqiang
     * @description { 删除顶部标签 }
     */
    hideTip() {
      this.setData({
        hideTip: true
      });
    },

    /**
      * @date 2020/08/24
      * @author kaiqiang
      * @description { 获取我的粉丝列表 }
    */
   getPageData() {
    let { params, dataList } = this.data;
    dataList = params.page_no === 1 ? [] : dataList;
    this.setData({
      'params.member_id' : wx.getStorageSync('user').member_id
    });
    API_Distribution.getMyFansList(this.data.params).then(response => {
      const { data, data_total, page_size } = response;
      let _pageCount = Math.ceil(data_total / page_size);
      let _dataList = dataList.concat(data);
      this.setData({
        dataList: _dataList,
        page_count: _pageCount
      });
    })
   },
   /**
     * @date 2020/08/24
     * @author wolfMan
     * @description {加载更多}
   */
   loadMore: function () {
     let { params, page_count } = this.data;
     let _pageNo = params.page_no + 1;
     this.setData({ 
       'params.page_no':_pageNo
     });
     if (page_count >= _pageNo) {
       this.getPageData();
     };
   }
  }
})