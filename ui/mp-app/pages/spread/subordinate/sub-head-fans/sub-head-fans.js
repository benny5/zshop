import * as API_Distribution from '../../../../api/distribution.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 可视窗口的高度
    scrollH: 0,
    dataList: [],
    // 分页
    params: {
      page_no: 1,
      page_size: 10,
      member_id: ''
    },
    // 总页
    page_count: 0 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取页面高度
    wx.getSystemInfo({
      success: (response) => {
        this.setData({
          scrollH: response.windowHeight
        });
      }
    });
    // 将下级团长的id传过来并赋值
    this.setData({
      'params.member_id' : options.id
    });
    // 调用获取粉丝列表函数
    this.getPageData();
  },
  /**
    * @date 2020/08/25
    * @author kaiqiang
    * @description { 获取下级团长粉丝列表 }
  */
  getPageData() {
    API_Distribution.getSubHeadFansList(this.data.params).then(response => {
      const { data, data_total, page_size } = response;
        let _pageCount = Math.ceil(data_total / page_size);
        let _dataList = this.data.dataList.concat(data);
        this.setData({
          dataList: _dataList,
          page_count: _pageCount
        });
    })
  },

  /**
    * @date 2020/08/25
    * @author kaiqiang
    * @description { 下拉加载更多多 }
  */
  loadMore: function () {
    let { params, page_count } = this.data;
    let _pageNo = params.page_no + 1;
    this.setData({ 
      'params.page_no': _pageNo
    });
    if (page_count >= _pageNo) {
      this.getPageData();
    };
  }
})