import * as API_Distribution from '../../../api/distribution.js'
const app = getApp();
let that;

Page({
  data: {
    scrollH: 0, // 页面滚动距离
    // 顶部导航栏标签
    tabList: [
      {
        dataIndex: 1,
        type: 'my_fans',
        name: '我的粉丝',
        number: 0,
      },
      {
        dataIndex: 2,
        type: 'sub_head',
        name: '下级团长',
        number: 0,
      },
      {
        dataIndex: 0,
        type: 'reg_invite',
        name: '邀请注册',
        number: 0,
      }
    ],
    tabActive: 'my_fans',
    currentTab: 1,
  },

  /**
   * @date 2020/08/22
   * @author kaiqiang
   * @description { 我的下级首页顶部导航栏数据 }
   */
  onLoad: function (options) {
    that = this;
    wx.getSystemInfo({
      success: function (res) {
        let query = wx.createSelectorQuery();
        query.select('.tab-switch').boundingClientRect();
        query.exec(response => {
          that.setData({
            scrollH: res.windowHeight - response[0].height
          });
        });
      }
    });
  },
  onShow () {
    this.getPageData();
  },
  // 获取首页导航栏数据
  getPageData() {
    let { tabActive, tabList } = this.data;
    API_Distribution.getFansIndexList().then(response => {
      let { fansNum, leaderNum, inviterNum } = response;
      tabList[0].number = fansNum;
      tabList[1].number = leaderNum;
      tabList[2].number = inviterNum;
      this.setData({
        tabList
      });
    });
    this.selectComponent(`#${tabActive}`).getDataList();
  },

  /**
   * @date 2020/08/22
   * @author kaiqiang
   * @description { 切换这些标签  同时还要再加载一次数据 }
   */
  switchTab(e) {
    const { type } = e.currentTarget.dataset;
    this.setData({
      tabActive: type
    });
  },

  /**
   * @date 2020/08/26
   * @author kaiqiang
   * @description  { 下拉刷新? 首先先要判断当前currentbar的索引值 }
   */
  onPullDownRefresh: function () {
    // 刷新导航栏数据
    this.getPageData();
    wx.stopPullDownRefresh();
  }
})