// pages/spread/index/index.js
import * as API_Members from '../../../api/members.js'
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 推广收益
    profitCount: {
      dayOrder: '¥0.0',
      dayCommission: '¥0.0',
      monthOrder: '¥0.0',
      monthCommission: '¥0.0',
    },
    inviteCode: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: async function () {
    const _distributor = wx.getStorageSync('distributor');
    this.setData({
      isDistributor: _distributor && _distributor.audit_status === 2,
      inviteCode: _distributor ? _distributor.invite_code : ''
    });
    this.getMyView();
  },
  // 获取汇总数据
  getMyView(_leader) {
    API_Members.getMyView().then(async res => {
      let { dayStatistic, monthStatistic } = res;
      this.setData({
        profitCount: {
          dayOrder: dayStatistic.show_order_price_sum || '¥0.0',
          dayCommission: dayStatistic.show_commission_sum || '¥0.0',
          monthOrder: monthStatistic.show_order_price_sum || '¥0.0',
          monthCommission: monthStatistic.show_commission_sum || '¥0.0',
        }
      });
    });
  },
  // 团长收益
  goProfit() {
    app.sendNotice('PROFITE_NOTICE');
    wx.navigateTo({ url: "/pages/spread/profit/profit" })
  },
  // 复制邀请码
  codeCopy() {
    const { isDistributor, inviteCode } = this.data;
    if (!isDistributor || !inviteCode) {
      return;
    };
    wx.setClipboardData({
      data: inviteCode,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showToast({
              title: '邀请码复制成功'
            });
          }
        });
      }
    });
  },
  // 显示招募页面
  showRecruit() {
    const { isDistributor, inviteCode } = this.data;
    if (isDistributor && !!inviteCode) {
      return;
    };
    wx.navigateTo({
      url: '/pages/leader/recruit-show/recruit-show',
    });
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})