// pages/spread/profit-order/profit-order.js
const app = getApp();
let util = require('../../../utils/util.js')
import * as API_Order from '../../../api/order'
import * as API_ORDER from '../../../api/order'
import { Foundation } from '../../../ui-utils/index.js'

Page({
  data: {
    order_sn: '', // 订单编号
    order: '', // 订单信息
    skuList:'', // 规格列表
    num: 0, // 计算当前订单中的商品件数
    // 倒计时
    timer: null,
    color: '#ff6f10',
    day: 0,
    hours: '00',
    minutes: '00',
    seconds: '00',
  },
  onLoad: function (options) {
    // options为页面跳转所带来的参数页面初始化 
    this.setData({order_sn: options.order_sn})
  },
  onShow: function () {
    this.setData({
      order: '',
      skuList: '',
      num: 0,
    });
    this.getOrderDetail();
  },
  // 获取订单详情
  getOrderDetail: function () {
    API_Order.getOrderDetail(this.data.order_sn).then(data => {
      data.payment_time = Foundation.unixToDate(data.payment_time);
      let commissionUseMoney = data.need_pay_money - data.shipping_price;
      data.commission_use_money = Foundation.formatPrice(commissionUseMoney);
      data.need_pay_money = Foundation.formatPrice(data.need_pay_money);
      data.goods_price = Foundation.formatPrice(data.goods_price);
      // data.discount_price = Foundation.formatPrice(data.discount_price)

      if (data.coupon_price !== 0) {
        data.coupon_price = Foundation.formatPrice(data.coupon_price);
      };
      const skuList = data['order_sku_list']
      if(skuList && skuList.length){
        skuList.forEach(key=>{
          key.purchase_price = Foundation.formatPrice(key.purchase_price)
          if (!key.skuName) key.skuName = this.formatterSkuSpec(key);
          if(key && key.num){
            this.setData({
              num : this.data.num += key.num
            });
          };
        });
      };
      this.setData({
        order: data,
        skuList: skuList
      })
    })
  },
  /** 规格格式化显示 */
  formatterSkuSpec(sku) {
    if (!sku.spec_list || !sku.spec_list.length) return ''
    return sku.spec_list.map(spec => spec.spec_value).join(' - ')
  },
  // 复制订单号
  orderCopy(e) {
    const orderSn = e.currentTarget.dataset.sn;
    wx.setClipboardData({
      data: orderSn,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showToast({
              title: '订单号复制成功'
            });
          }
        });
      }
    });
  },
  // 显示发票弹框
  popup() { this.selectComponent('#bottomFrame').showFrame() },
  // “删除”点击效果
  deleteOrder: function () {
    let that = this;
    let orderInfo = that.data.orderInfo;

    wx.showModal({
      title: '',
      content: '确定要删除此订单？',
      success: function (res) {
        if (res.confirm) {
          util.request(api.OrderDelete, {
            orderId: orderInfo.id
          }, 'POST').then(function (res) {
            if (res.errno === 0) {
              wx.showToast({
                title: '删除订单成功'
              });
              util.redirect('/pages/ucenter/order/order');
            }
            else {
              util.showErrorToast(res.errmsg);
            }
          });
        }
      }
    });
  },  
  // “确认收货”点击效果
  rogOrder: function () {
    let that = this;
    let order_sn = that.data.order_sn;

    wx.showModal({
      title: '提示',
      content: '请确认是否收到货物，否则可能会钱财两空！',
      success: function (res) {
        if (res.confirm) {
          API_ORDER.confirmReceipt(order_sn).then(() => {
            wx.showToast({ title: '确认收货成功！' });
            this.getOrderDetail();
          });
        }
      }
    });
  },
  /** 倒计时 */
  contDown(time){
    let times = time
    this.setData({
      timer:setInterval(()=>{
        if (times <= 0) {
          clearInterval(this.data.timer);
          this.triggerEvent('count-end');
        } else {
          const endTime = Foundation.countTimeDown(times);
          this.setData({
            day: endTime.day,
            hours: endTime.hours,
            minutes: endTime.minutes,
            seconds: endTime.seconds
          })
          times --
        }
      },1000)
    })
  }
})