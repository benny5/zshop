// pages/leader/recruit-show/recruit-show.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    parentCode: '', // 邀请人编码
    urlTitle: 'https://localhost/images/leader-recruit/',
    recruitImgList: [
      'recruit-1.png', 'recruit-2.png', 'recruit-3.png', 'recruit-4.png',
      'recruit-5.png', 'recruit-6.png', 'recruit-7.png', 'recruit-8.png',
      'recruit-9.png'
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options) {
      this.setData({
        parentCode: options.inviteCode || ''
      });
    };
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 登录校验
    if (wx.getStorageSync('refresh_token')) {
      // 获取分享时需要绑定的key
      app.getShareKey('pages/leader/recruit-show/recruit-show', 'shareApplyShow');
    };
  },

  // 团长申请
  async showApply() {
    let _isLogin = await app.isLogin();
    if (!_isLogin) { return };
    wx.navigateTo({ url: `/pages/leader/apply/apply?inviteCode=${this.data.parentCode}`})
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const _distributData = wx.getStorageSync('distributor');
    let invite_code = _distributData ? _distributData.invite_code : '';
    let _shareKey = wx.getStorageSync('shareApplyShow') || '';
    let _opt = _shareKey ? `&shareKey=${_shareKey}`: '';
    return {
      title: '来门口淘做团长，一起赚丰厚佣金，大家都来了，你还等什么~',
      desc: '来门口淘做团长，一起赚丰厚佣金，大家都来了，你还等什么~',
      path: `/pages/leader/recruit-show/recruit-show?inviteCode=${invite_code}${_opt}`
    };
  }
})
