import { RegExp } from '../../../ui-utils/index.js';
import * as API_distribution from '../../../api/distribution.js';
import { formatTimeTwo } from '../../../utils/util.js'

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 我的上级团长
    pageData: {
      member: {
        face: '',
        uname: '',
        mobile: '',
        real_name: '',
        lv1_create_time: ''
      }
    },
    isHidden: true,
    memberBollen: true,
    value: '',
    monitor: '',
    // 默认显示更改团长按钮
    showUpdate: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getPageData();
  },
  /**
    * @date 2020/10/28
    * @author kaiqiang
    * @description { 展示我的上级团长 }
  */
  getPageData() {
    API_distribution.getMySuperiorDistribution().then(response => {
      if (response.distribution.audit_status === 2) {
        this.setData({
          showUpdate: false
        });
      }
      if (response.member) {
        this.setData({
          memberBollen: false,
          pageData: {
            member:{
              face: response.member.face,
              uname: response.member.uname,
              mobile: response.member.mobile,
              real_name: response.member.real_name,
              lv1_create_time: formatTimeTwo(response.distribution.lv1_create_time, 'Y-M-D h:m:s'),
            }
          }
        })
      }
    })
  },
  /**
    * @date 2020/10/28
    * @author kaiqiang
    * @description { 修改团长点击事件 }
  */
  uploadLeader() {
    this.setData({
      isHidden: false,
      monitor: '修改'
    })
  },
  /**
    * @date 2020/10/28
    * @author kaiqiang
    * @description { 确定按钮的点击事件 }
  */
  submit() {
    let value = this.data.value
    if (!value) {
      wx.showToast({ title: '请输入手机号或者验证码！', icon: 'none' });
      return 
    }
    if (value.length < 6) {
      wx.showToast({ title: '验证码最少6位数！', icon: 'none' });
      return
    }
    if (!RegExp.mobile.test(value) && value.length >= 7) {
      wx.showToast({ title: '手机号或者邀请码格式错误！', icon: 'none' });
      return
    }
    let parmas = {
      'mobile_or_invitation_code': value
    }
    API_distribution.updateSuperiorDistribution(parmas)
      .then(response => { 
        wx.showToast({  
          title: response.message,  
          icon: 'success',  
        });
        this.setData({
          isHidden: true,
          value: '',
        });
        this.getPageData();
      })
      .catch(error => {
        console.log(error);
      })
  },
  /**
    * @date 2020/10/28
    * @author kaiqiang
    * @description { 取消按钮点击事件 }
  */
  cancel() {
    this.setData({
      isHidden: true,
      value: ''
    })
  },
  /**
    * @date 2020/10/28
    * @author kaiqiang
    * @description { 监听input框输入的值 }
  */
  changeValue(e) {
    this.setData({
      'value': e.detail.value
    })
  },
  /**
    * @date 2020/10/28
    * @author kaiqiang
    * @description { 绑定上级团长 }
  */
  bind_Leader() {
    this.setData({
      isHidden: false,
      monitor: '绑定'
    })
  }
})