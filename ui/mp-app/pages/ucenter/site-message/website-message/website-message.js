const app = getApp();
let util = require('../../../../utils/util.js')
import * as API_Message from '../../../../api/message'
import { Foundation } from '../../../../ui-utils/index.js'
import regeneratorRuntime from '../../../../lib/wxPromise.min.js'

Page({
  data: {
    showType: 0,
    id: 0,
    messageList: [],
    params: {
      page_no: 1,
      page_size: 20,
      read:0
    },
    showGoTop: false,
    finished: false,
    scrollTop: 0,//滚动高度
    scrollHeight: '',
    delBtnWidth: 120
  },
  onLoad: function (options) {
    this.initEleWidth();
    this.getMessageList();
    this.setData({
      scrollHeight: wx.getSystemInfoSync().windowHeight - 42,
      'params.read': options.read
    })
  },
  switchTab(event) {
    let index = parseInt(event.currentTarget.dataset.index)
    if (index !== this.data.showType) {
      this.setData({
        showType: index,
        'params.page_no': 1,
        'params.read': index,
        messageList: []
      })
      this.getMessageList(true);
    }
  },
  getMessageList: function (reset = false) {
    if (reset) {
      this.setData({
        "params.page_no": 1,
        finished: false,
        messageList: []
      })
    }
    const params = JSON.parse(JSON.stringify(this.data.params))
    API_Message.getMessages(params).then(async response => {
      const data = response.data
      if (data && data.length) {
        data.forEach(key => {
          key.send_time = Foundation.unixToDate(key.send_time)
        })
        if (params.read === 0) {
          const ids = data.map(item => item.id).join(',')
          await API_Message.messageMarkAsRead(ids)
        }
        this.data.messageList.push(...data)
        this.setData({messageList: this.data.messageList})
      } else {
        this.setData({ finished: true })
      }
    })
  },
  //删除消息
  handleDeleteMessage(e) {
    var that = this
    const message_id = e.currentTarget.dataset.message_id
    this.data.messageList.forEach(key => { key.txtStyle = '' })
    this.setData({ messageList: this.data.messageList })
    wx.showModal({
      title: '提示',
      content: '确定要删除这条消息吗?',
      confirmColor: '#f42424',
      success: function (res) {
        if (res.confirm) {
          API_Message.deleteMessage(message_id).then(() => {
            that.getMessageList(true)
            wx.showToast({ title: '删除成功' })
          })
        }
      }
    })
  },
  loadMore () {
    if (!this.data.finshed) {
      this.setData({ "params.page_no": this.data.params.page_no + 1 })
      this.getMessageList()
    } else {
      return
    }
  },
  scroll(e) {
    let that = this
    if (e.detail.scrollTop > 200) {
      that.setData({showGoTop: true})
    } else {
      that.setData({showGoTop: false})
    }
  },
  //返回顶部
  goTop() {this.setData({ scrollTop: 0 })},


  // 开始滑动事件
  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置 
        startX: e.touches[0].clientX
      });
    }
  },
  touchM: function (e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置 
      var moveX = e.touches[0].clientX;
      //手指起始点位置与移动期间的差值 
      var disX = this.data.startX - moveX;
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) { //如果移动距离小于等于0，文本层位置不变 
        txtStyle = "left:0rpx";
      } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离 
        txtStyle = "left:-" + disX + "rpx";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度 
          txtStyle = "left:-" + delBtnWidth + "rpx";
          //获取手指触摸的是哪一项 
          var index = e.currentTarget.dataset.index;
          var list = this.data.messageList;
          list.forEach(key => { key.txtStyle = '' })
          list[index].txtStyle = txtStyle;
          //更新列表的状态 
          this.setData({
            messageList: list
          });
        }
      }
    }
  },
  // 滑动中事件
  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置 
      var endX = e.changedTouches[0].clientX;
      //触摸开始与结束，手指移动的距离 
      var disX = this.data.startX - endX;
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮 
      var txtStyle = "";
      txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0rpx";

      //获取手指触摸的是哪一项 
      var index = e.currentTarget.dataset.index;
      var list = this.data.messageList;
      list.forEach(key => { key.txtStyle = '' })
      list[index].txtStyle = txtStyle;
      //更新列表的状态 
      this.setData({
        messageList: list
      });
    }
  },
  //获取元素自适应后的实际宽度 
  getEleWidth: function (w) {
    var real = 0;
    try {
      var res = wx.getSystemInfoSync().windowWidth;
      var scale = (750 / 2) / (w / 2);
      real = Math.floor(res / scale);
      return real;
    } catch (e) {
      return false;
    }
  },
  initEleWidth: function () {
    var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
    this.setData({
      delBtnWidth: delBtnWidth
    });
  }
})