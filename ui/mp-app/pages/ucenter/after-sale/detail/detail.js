import * as API_AfterSale from '../../../../api/after-sale.js'
import { Foundation } from '../../../../ui-utils/index.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    sn:'',
    detail:'',
    skuList:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({sn:options.sn})
    this.getAfterSaleDetail()
  },
  getAfterSaleDetail(){
    API_AfterSale.getAfterSaleDetail(this.data.sn).then(response=>{
      response.refund.refund_price = Foundation.formatPrice(response.refund.refund_price)
      response.refund.create_time = Foundation.unixToDate(response.refund.create_time)
      response.refund_goods.forEach(key=>{
        key.price = Foundation.formatPrice(key.price)
        if(key && !key.skuName){
          key.skuName = this.formatterSkuSpec(key)
        }
      })
      this.setData({
        detail: response.refund,
        skuList: response.refund_goods
      })
    })
  },
  /** 规格格式化显示 */
  formatterSkuSpec(sku) {
    if (!sku.spec_list || !sku.spec_list.length) return ''
    return sku.spec_list.map(spec => spec.spec_value).join(' - ')
  }
})