page {
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  width: 100%;
  height: 100%;
  background: #f4f4f4;
}
view,
text {
  color: inherit;
  font-size: inherit;
}
.tab-switch {
  box-sizing: border-box;
  background: #fff;
}
.tab-switch .tab-list {
  display: flex;
  height: 100rpx;
}
.tab-switch .switch-item {
  flex: 1;
  display: flex;
  justify-content: center;
  padding: 0 10rpx;
  line-height: 0;
  text-align: center;
  color: #000;
}
.tab-switch .item-box {
  position: relative;
  padding: 30rpx 0;
  width: 160rpx;
  border-top: 6rpx solid transparent;
  border-bottom: 6rpx solid transparent;
  text-align: center;
  color: #a4a4a4;
}
.tab-switch .switch-txt {
  display: inline-block;
  line-height: 32rpx;
  text-align: center;
  font-size: 32rpx;
  font-weight: 700;
}
.tab-switch .tip-num {
  position: absolute;
  top: 22rpx;
  left: 50%;
  padding: 0 5rpx;
  margin-left: 32rpx;
  height: 24rpx;
  line-height: 24rpx;
  min-width: 24rpx;
  border-radius: 12rpx;
  box-sizing: border-box;
  background: #e83227;
  color: #fff;
  font-size: 20rpx;
}
.tab-switch .switch-item.active .item-box {
  border-bottom-color: #e83227;
  color: #e83227;
}
.coupon-box {
  padding: 20rpx 30rpx;
}
.coupon-cell {
  display: flex;
  align-items: center;
  padding: 30rpx 0;
  border-radius: 24rpx;
  background: #fff;
}
.coupon-cell + .coupon-cell {
    margin-top: 20rpx;
}
.coupon-cell .coupon-money {
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 20rpx;
  width: 170rpx;
  box-sizing: border-box;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.coupon-cell .money-box {
  text-align: center;
}
.coupon-cell .coupon-money .money {
  margin-right: 4rpx;
  color: #e83227;
  font-size: 24rpx;
}
.coupon-cell .money .big {
  font-size: 36rpx;
}
.coupon-cell .coupon-money .coupon-limit {
  margin-top: 10rpx;
  line-height: 24rpx;
  color: #999;
  font-size: 24rpx;
}

.coupon-cell .coupon-info {
  flex: 1;
  width: 0;
  position: relative;
  padding: 0 20rpx;
  box-sizing: border-box;
}
.coupon-cell .coupon-info::after {
  content: '';
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  width: 1px;
  height: 200%;
  background: #c6c6c6;
  transform-origin: 0 0;
  transform: scale(.5, .5);
}
.coupon-cell .coupon-info .info-text {
  display: flex;
  align-items: center;
}
.coupon-cell .coupon-info .show-text {
  flex: 1;
  width: 0;
}
.coupon-cell .show-text .name {
  line-height: 28rpx;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #000;
  font-size: 28rpx;
  font-weight: 700;
}
.coupon-cell .show-text .tip {
  margin-top: 20rpx;
  line-height: 24rpx;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #000;
  font-size: 24rpx;
}
.coupon-cell .info-text .iconfontimg {
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin-right: 36rpx;
    width: 90rpx;
    height: 68rpx;
    line-height: 68rpx;
    color: #999;
    font-size: 98rpx;
}
.coupon-cell .coupon-info .use-now {
  position: relative;
  width: 164rpx;
  height: 68rpx;
  line-height: 68rpx;
  border-radius: 24rpx;
  box-sizing: border-box;
  text-align: center;
  color: #e83227;
  font-size: 30rpx;
}
.coupon-cell .coupon-info .use-now::before {
  content: '';
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  width: 200%;
  height: 200%;
  border: solid 1px #e83227;
  border-radius: 68rpx;
  box-sizing: border-box;
  transform-origin: 0 0;
  transform: scale(.5, .5);
}
.coupon-cell .coupon-info .use-shop {
  margin-top: 18rpx;
  line-height: 24rpx;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #000;
  font-size: 24rpx;
}
.coupon-cell .coupon-info .time {
  margin-top: 18rpx;
  line-height: 18rpx;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #999;
  font-size: 20rpx;
}
.mlr4 {
  margin-left: 4rpx;
  margin-right: 4rpx;
}