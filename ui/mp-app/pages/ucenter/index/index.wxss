page {
  height: 100%;
  width: 100%;
  background: #ff3444;
}

.container {
  padding-bottom: 30rpx;
  height: auto;
  width: 100%;
  min-height: 100%;
  overflow: unset;
  box-sizing: border-box;
  background: #ededed;
}

.profile-info {
  display: flex;
  margin-bottom: -110rpx;
  padding-top: 128rpx;
  padding-bottom: 110rpx;
  width: 100%;
  height: 430rpx;
  box-sizing: border-box;
  background: linear-gradient(#ff3444, #fd7f26);
}

.profile-box {
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: flex-start;
  padding: 0 30.25rpx;
  width: 100%;
  color: #fff;
}

.profile-info .avatar {
  height: 100rpx;
  width: 100rpx;
  border-radius: 50%;
}

.profile-info .info {
  flex: 1;
  height: 85rpx;
  margin-left: 30rpx;
  display: flex;
  flex-direction: column;
  justify-content: center;
}

.profile-info .name {
  height: 45rpx;
  line-height: 45rpx;
  color: #fff;
  font-size: 30rpx;
  font-weight: 600;
}

.profile-info .identity {
  line-height: 30rpx;
  color: #fff;
  font-size: 20rpx;
}

.profile-info .level {
  height: 30rpx;
  line-height: 30rpx;
  margin-bottom: 10rpx;
  color: #7f7f7f;
  font-size: 30rpx;
}

.profile-info .iconimg-right {
  font-size: 40rpx;
}

.member-menu-box {
  margin-top: 30rpx;
  margin-left: 35rpx;
  margin-right: 35rpx;
  border-radius: 30rpx;
  background: #fff;
}

.member-menu-box.order-menu {
  margin-top: 0;
}

.big-title {
  display: flex;
  align-items: center;
  padding: 29.5rpx 0;
  margin-left: 40rpx;
  margin-right: 40rpx;
  line-height: 50rpx;
  border-bottom: solid 2rpx #d4d4d4;
}

.big-title .name {
  flex: 1;
  color: #000;
  font-size: 30rpx;
  font-weight: 600;
}

.big-title .menu-right {
  color: #9d9d9d;
  font-size: 24rpx;
}

.big-title .iconimg-right {
  display: inline-flex;
  justify-content: center;
  margin-left: 20rpx;
  width: 16rpx;
  font-size: 28rpx;
}

.menu-list {
  display: flex;
  flex-wrap: wrap;
  padding: 30rpx 0;
}

.menu-cell {
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 30rpx 0;
  width: 25%;
}

.menu-cell.menu-cell-btn {
  padding: 0;
}

.menu-cell .menu-btn {
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 30rpx 0;
  width: 100%;
  height: 100%;
  line-height: 0;
  box-shadow: border-box;
  background: inherit;
}

.menu-cell .img-box {
  display: flex;
  position: relative;
  text-align: center;
}

.menu-cell .img-box icon {
  display: inline-flex;
  justify-content: center;
  align-items: center;
  height: 40rpx;
  line-height: 40rpx;
  font-size: 46rpx;
}

.menu-cell .img-box .img-tip {
  display: inline-flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: -10rpx;
  right: -14rpx;
  padding-left: 5rpx;
  padding-right: 5rpx;
  height: 28rpx;
  min-width: 28rpx;
  line-height: 30rpx;
  border-radius: 16rpx;
  box-sizing: border-box;
  background: #ff3444;
  color: #fff;
  font-size: 20rpx;
}

.menu-cell text {
  margin-top: 18rpx;
  line-height: 24rpx;
  color: #535353;
  font-size: 24rpx;
}

.order-menu .menu-list {
  margin-left: 40rpx;
  margin-right: 40rpx;
}

.order-menu .menu-cell {
  width: 20%;
}

.order-menu .img-box icon {
  font-size: 44rpx;
}

.order-deliver .img-box icon {
  font-size: 48rpx;
}

.order-receipt .img-box icon {
  font-size: 38rpx;
}

.profit-menu .count-row {
  display: flex;
  align-items: center;
  padding: 0 40rpx;
}
.profit-menu .count-cell {
  flex: 1;
  width: 0;
  display: block;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  padding: 40rpx 10rpx;
  text-align: center;
}
.profit-menu .money {
  display: inline-flex;
  align-items: flex-end;
  line-height: 20rpx;
  color: #ff3444;
  font-size: 20rpx;
  font-weight: 600;
}
.profit-menu .money .big {
  line-height: 24rpx;
  font-size: 30rpx;
}
.profit-menu text {
  display: block;
  margin-top: 30rpx;
  line-height: 20rpx;
  color: #9d9d9d;
  font-size: 20rpx;
}

.member-nav,
.member-menu {
  margin-bottom: 20rpx;
  background-color: #fff;
}

.member-nav-head,
.item {
  display: flex;
  padding: 20rpx;
}

.member-nav-head view,
.item view {
  display: inline-block;
}

.member-nav-head .left,
.item .left {
  flex: 1;
  font-size: 28rpx;
}

.member-nav-head .right {
  margin-top: -20rpx;
}

.member-nav-head .right icon {
  font-size: 25rpx;
}

.nember-nav-items {
  width: 100%;
  height: auto;
  overflow: hidden;
}

.nember-nav-items .item {
  float: left;
  width: 20%;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  justify-content: center;
}

.nember-nav-items .item icon {
  margin-bottom: 10rpx;
  font-size: 40rpx;
}

.member-menu .item {
  border-bottom: 2rpx solid #f5f5f5;
}

.member-menu .item .left icon {
  margin: -8rpx 30rpx 0 10rpx;
  vertical-align: -4rpx;
  font-size: 34rpx;
}

.member-menu .item .right icon {
  font-size: 28rpx;
  color: #999;
  margin-top: -12rpx;
}

.logout {
  margin: 20rpx;
  line-height: 100rpx;
  text-align: center;
  border-radius: 10rpx;
  background-color: #f42424;
  color: #fff;
  font-size: 30rpx;
}

.modal {
  display: flex;
  justify-content: center;
  align-items: center;
  
}
.share-poster {
  position: relative;
  margin-bottom: -60rpx;
  width: 640rpx;
  box-sizing: border-box;
}
.share-poster .info {
  height: 900rpx;
  line-height: 0;
  border-radius: 30rpx;
  overflow: hidden;
  background: #fff;
}
.share-poster .info .share-img {
  width: 100%;
}
.share-poster .save-btn {
  margin: 30rpx auto 0 auto;
  height: 80rpx;
  line-height: 80rpx;
  border-radius: 40rpx;
  background: linear-gradient(to right, #fd7f26, #ff3444);
  color: #fff;
  font-size: 30rpx;
}
.share-poster .poster-close {
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 30rpx auto 0 auto;
  width: 80rpx;
  height: 80rpx;
  border: solid 4rpx #b9aca8;
  border-radius: 40rpx;
  box-sizing: border-box;
  color: #b9aca8;
}
.poster-close icon {
  display: inline-flex;
  width: 36rpx;
  height: 36rpx;
  line-height: 36rpx;
  font-size: 36rpx;
}