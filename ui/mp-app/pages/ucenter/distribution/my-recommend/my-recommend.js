import * as API_Distribution from '../../../../api/distribution.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    myRefereer: '',
    referee: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getMyReferee()
    this.getReferee()
  },
  getMyReferee(){
    API_Distribution.getMyRefereer().then(response=>{
      this.setData({myRefereer:response.message})
    })
  },
  getReferee(){
    API_Distribution.getRefereeList().then(response=>{
      this.setData({referee:response})
    })
  }
})