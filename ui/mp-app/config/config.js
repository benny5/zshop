const env = 'deveploment' // 开发环境 deveploment  生产环境 production
/**
 * base    : 基础业务API
 * buyer   : 买家API
 * seller  : 商家中心API
 * admin   : 后台管理API
 */
const api = {
  // 开发环境
  dev: {
    base: 'https://api.base.bluexigo.cn',
    buyer: 'https://api.buyer.bluexigo.cn',
    seller: 'https://api.seller.bluexigo.cn',
    admin: 'https://api.admin.bluexigo.cn'
  },
  // 生产环境
  pro: {
    base: 'https://api.base.bluexigo.cn',
    buyer: 'https://api.buyer.bluexigo.cn',
    seller: 'https://api.seller.bluexigo.cn',
    admin: 'https://api.admin.bluexigo.cn'
  }
}

module.exports = {
  api: env === 'deveploment' ? api.dev : api.pro,
  env
}
