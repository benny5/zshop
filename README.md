# zshop介绍
一个基于spring boot、spring security、mybatis、redis的轻量级、前后端分离、防范xss攻击、拥有分布式锁，数据库为b2b2c设计，拥有完整sku和下单流程的完全开源商城

### 开源说明
* 系统100%开源
* 基于[Apache2.0]开源协议，请尊重开源精神不要去掉代码中的原注释和版权信息

### 面向对象
* 企 业：帮助创立初期的公司或团队快速搭建产品的技术平台，加快公司项目开发进度；
* 开发者：帮助开发者快速完成承接外包的项目，避免从零搭建系统；
* 学习者：初学微服务的同学可以下载源代码来进行学习交流；

### 软件架构说明
1. 使用SpringCloud框架Greenwich.RELEASE版本
2. 使用SpringBoot框架2.1.0.RELEASE版本

### 技术选型
| 技术 | 版本 | 说明 |
| --- | --- | --- |
|Spring Boot|2.1.0|MVC核心框架|
|Spring Security|2.1.0|认证和授权框架|
|MyBatis|3.5.0|ORM框架|
|MyBatisPlus|3.1.0|基于mybatis，使用lambda表达式的|
|redisson|3.5.5|对redis进行封装、集成分布式锁等|
|Elasticsearch|5.5.0|强大的站内搜索工具|
|Swagger-UI|2.9.2|文档生产工具|
|Hibernator-Validator|6.0.17|验证框架|
|lombok|1.18.8|简化对象封装工具|
|modelmapper|3.1.0|更快的bean复制工具|
|fastjson|1.2.38|更快的json处理工具|
|easyexcel|2.2.3|高效的poi处理|
|...|...|...|

### 安装教程
1. 安装lombok插件
2. maven install framework和javashop-core
3. 帮助文档地址：
https://gitee.com/zhiyi-shanghai/zshop/wikis


### 参考文档
#### 演示环境：
后台管理地址：
http://admin.shop.bluexigo.com
superadmin
hm111111

卖家管理地址：
http://seller.shop.bluexigo.com
平台自营
hm111111

### 联系我们
- QQ群：469244116
- 微信号：hlgramay、sj527503637、z377569566、InspireFox
- 手机号：13916226937


### 功能清单
![img.png](doc/img/list1.png)
![img.png](doc/img/list2.png)
![img.png](doc/img/list3.png)

### 相关截图
##### 1、管理后台截图
![img.png](doc/img/manage1.png)
![img.png](doc/img/manage2.png)

##### 2、商家后台截图
![img.png](doc/img/seller1.png)
![img.png](doc/img/seller2.png)

##### 3、移动端截图
![img_1.png](doc/img/mini1.png)
![img_2.png](doc/img/mini2.png)
![img_3.png](doc/img/mini3.png)

### 提交反馈
砥砺前行，欢迎交流

![img.png](doc/img/zhop_communicate_qq.jpg)



