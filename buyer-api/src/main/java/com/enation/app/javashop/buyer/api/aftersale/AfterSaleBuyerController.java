package com.enation.app.javashop.buyer.api.aftersale;

import com.enation.app.javashop.core.aftersale.AftersaleErrorCode;
import com.enation.app.javashop.core.aftersale.model.dto.RefundDTO;
import com.enation.app.javashop.core.aftersale.model.dto.RefundDetailDTO;
import com.enation.app.javashop.core.aftersale.model.vo.*;
import com.enation.app.javashop.core.aftersale.model.vo.SpreadRefundQueryParamVO;
import com.enation.app.javashop.core.aftersale.model.enums.RefundStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.RefundTypeEnum;
import com.enation.app.javashop.core.aftersale.service.AfterSaleManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.redis.transactional.RedisTransactional;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author zjp
 * @version v7.0
 * @Description 售后相关API
 * @ClassName AfterSaleController
 * @since v7.0 下午8:10 2018/5/9
 */
@Api(description="售后相关API")
@RestController
@RequestMapping("/after-sales")
@Validated
public class AfterSaleBuyerController {

    @Autowired
    private AfterSaleManager afterSaleManager;

    @ApiOperation(value = "退款申请数据获取",response = RefundApplyVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn" , value = "订单号" , dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "sku_id" , value = "货品id" , required = false , dataType = "int", paramType = "query")
    })
    @GetMapping(value = "/refunds/apply/{order_sn}")
    public RefundApplyVO refundApply(@PathVariable("order_sn") String orderSn,@ApiIgnore Integer skuId){
        return afterSaleManager.refundApply(orderSn,skuId);
    }

    //商品申请售后--》退款
    @ApiOperation(value = "买家申请退款",response = BuyerRefundApplyVO.class)
    @PostMapping(value = "/refunds/apply")
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "refund_",value = "#refundApply.orderSn")
    public BuyerRefundApplyVO refund(@Valid BuyerRefundApplyVO refundApply) {
        afterSaleManager.applyRefund(refundApply);
        return refundApply;
    }
    //商品申请售后--》退货，确认收货后才有
    @ApiOperation(value = "买家申请退货",response = BuyerRefundApplyVO.class)
    @PostMapping(value = "/return-goods/apply")
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "refund_",value = "#refundApply.orderSn")
    public BuyerRefundApplyVO returnGoods(@Valid BuyerRefundApplyVO refundApply) {
        afterSaleManager.applyGoodsReturn(refundApply);
        return refundApply;
    }


    @ApiOperation(value = "买家对已付款的订单取消操作")
    @PostMapping(value = "/refunds/cancel-order")
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "refund_",value = "#buyerCancelOrderVO.orderSn")
    public String cancelOrder(@Valid BuyerCancelOrderVO buyerCancelOrderVO){
        afterSaleManager.cancelOrder(buyerCancelOrderVO);
        return "";
    }

    @ApiOperation(value = "买家查看退款(货)列表", response = RefundDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no" , value = "页码" , required = true , dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size" , value = "分页数" , required = true , dataType = "int", paramType = "query")
    })
    @GetMapping(value = "/refunds")
    public Page refundDetail(@ApiIgnore @NotNull(message = "页码不能为空") Integer pageNo, @ApiIgnore @NotNull(message = "每页数量不能为空") Integer pageSize ) {

        Buyer buyer = UserContext.getBuyer();
        RefundQueryParamVO queryParam = new RefundQueryParamVO();
        queryParam.setPageNo(Integer.valueOf(pageNo));
        queryParam.setPageSize(pageSize);
        queryParam.setMemberId( buyer.getUid());
        Page<RefundDTO> webPage = this.afterSaleManager.query(queryParam);
        return webPage;
    }

    @ApiOperation(value = "买家查看退款(货)详细", response = RefundDetailDTO.class)
    @ApiImplicitParam(name = "sn", value = "退款(货)编号", required =true, dataType = "String" ,paramType="path")
    @GetMapping(value = "/refund/{sn}")
    public RefundDetailDTO sellerDetail(@PathVariable("sn")  String sn  ) {
        Buyer buyer = UserContext.getBuyer();
        RefundDetailDTO detail  = this.afterSaleManager.getDetail(sn);
        if(!detail.getRefund().getMemberId().equals(buyer.getUid())){
            throw new ServiceException(AftersaleErrorCode.E603.name(),"退款单不存在");
        }
        return detail;
    }

    @ApiOperation(value = "买家分页查看多退少补列表", response = SpreadRefundQueryParamVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no" , value = "页码" , required = true , dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size" , value = "分页数" , required = true , dataType = "int", paramType = "query")
    })
    @GetMapping(value = "/refund/spread")
    public Page<SpreadRefundQueryParamVO> querySpreadRefund(@ApiIgnore @NotNull(message = "页码不能为空") Integer pageNo,
                                                            @ApiIgnore @NotNull(message = "每页数量不能为空") Integer pageSize ) {

        Buyer buyer = UserContext.getBuyer();
        SpreadRefundQueryParamVO spreadRefundQueryParamVO = new SpreadRefundQueryParamVO();
        spreadRefundQueryParamVO.setPageNo(Integer.valueOf(pageNo));
        spreadRefundQueryParamVO.setPageSize(pageSize);
        spreadRefundQueryParamVO.setMemberId( buyer.getUid());
        spreadRefundQueryParamVO.setRefundStatus(RefundStatusEnum.COMPLETED.value());
        spreadRefundQueryParamVO.setRefundType(RefundTypeEnum.SPREAD_REFUND.value());
        return this.afterSaleManager.querySpreadRefund(spreadRefundQueryParamVO);
    }

}
