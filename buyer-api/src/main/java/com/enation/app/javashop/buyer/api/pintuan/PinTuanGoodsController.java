package com.enation.app.javashop.buyer.api.pintuan;

import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goodssearch.model.GoodsSearchDTO;
import com.enation.app.javashop.core.goodssearch.model.GoodsSearchLine;
import com.enation.app.javashop.core.goodssearch.service.GoodsSearchManager;
import com.enation.app.javashop.core.promotion.pintuan.model.PinTuanGoodsVO;
import com.enation.app.javashop.core.promotion.pintuan.model.PintuanOrder;
import com.enation.app.javashop.core.promotion.pintuan.model.PintuanOrderDetailVo;
import com.enation.app.javashop.core.promotion.pintuan.service.PintuanGoodsManager;
import com.enation.app.javashop.core.promotion.pintuan.service.PintuanOrderManager;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kingapex on 2019-01-22.
 * 拼团商品API
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2019-01-22
 */
@Api(description = "拼团商品API")
@RestController
@RequestMapping("/pintuan/goods")
public class PinTuanGoodsController {

    @Autowired
    private PintuanGoodsManager pintuanGoodsManager;

    @Autowired
    private PintuanOrderManager pintuanOrderManager;

    @Autowired
    private GoodsSearchManager goodsSearchManager;

    @Autowired
    private GoodsClient goodsClient;

    @GetMapping("/skus/{sku_id}")
    @ApiOperation(value = "获取某个拼团商品sku的详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sku_id", value = "skuid", required = true, dataType = "int", paramType = "path")
    })
    public PinTuanGoodsVO detail(@ApiIgnore @PathVariable(name = "sku_id")  Integer skuId) {
        PinTuanGoodsVO pinTuanGoodsVO = pintuanGoodsManager.getDetail(skuId);
        return pinTuanGoodsVO;
    }


    @PostMapping("/skus/details")
    @ApiOperation(value = "获取多个拼团商品sku的详细")
    public Map<Integer, PinTuanGoodsVO> details(@RequestBody List<Integer> skuIds) {
        Map<Integer, PinTuanGoodsVO> pinTuanGoodsMap =  new HashMap<>();
        if(skuIds != null && !skuIds.isEmpty()){
            for (Integer skuId : skuIds) {
                PinTuanGoodsVO pinTuanGoodsVO = pintuanGoodsManager.getDetail(skuId);
                if(pinTuanGoodsVO != null){
                    pinTuanGoodsMap.put(skuId, pinTuanGoodsVO);
                }
            }

            Collection<PinTuanGoodsVO> pinTuanGoodsList = pinTuanGoodsMap.values();
            if(pinTuanGoodsList != null && pinTuanGoodsList.size() > 0){
                Object[] goodIdsArray = pinTuanGoodsList.stream().map(PinTuanGoodsVO::getGoodsId).toArray();

                // 搜集数据
                GoodsSearchDTO goodsSearch = new GoodsSearchDTO();
                goodsSearch.setPageNo(1);
                goodsSearch.setPageSize(1000);
                goodsSearch.setGoodsIds(StringUtils.join(goodIdsArray, ","));
                Page page = goodsSearchManager.search(goodsSearch);
                List<GoodsSearchLine> goodsList = page.getData();
                Map<Integer, GoodsSearchLine> indexMap = new HashMap<>();
                for (GoodsSearchLine goodsSearchLine : goodsList) {
                    indexMap.put(goodsSearchLine.getGoodsId(), goodsSearchLine);
                }

                // 封装
                for (PinTuanGoodsVO pinTuanGoodsVO : pinTuanGoodsList) {
                    pinTuanGoodsVO.setGoodsDetail(indexMap.get(pinTuanGoodsVO.getGoodsId()));

                    Integer skuId = pinTuanGoodsVO.getSkuId();
                    GoodsSkuVO goodsSkuVO = goodsClient.getSkuFromCache(skuId);
                    Integer quantity = 0;
                    if (goodsSkuVO != null) {
                        quantity = goodsSkuVO.getEnableQuantity();
                    }
                    pinTuanGoodsVO.setEnableQuantity(quantity);
                }
            }
        }
        return pinTuanGoodsMap;
    }

    @ApiOperation(value = "获取此商品拼团的所有参与的sku信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goods_id", value = "商品id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "pintuan_id", value = "拼团id", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/{goods_id}/skus")
    public List<GoodsSkuVO> skus(@ApiIgnore @PathVariable(name = "goods_id")  Integer goodsId,@ApiIgnore Integer pintuanId) {

        return pintuanGoodsManager.skus(goodsId,pintuanId);
    }


    @ApiOperation(value = "获取此商品待成团的订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goods_id", value = "商品id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "sku_id", value = "skuid", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/{goods_id}/orders")
    public List<PintuanOrderDetailVo> orders(@ApiIgnore @PathVariable(name = "goods_id")Integer goodsId, @ApiIgnore Integer skuId) {

        return pintuanOrderManager.getWaitOrder(goodsId,skuId);
    }


}
