package com.enation.app.javashop.buyer.api.map;

import com.enation.app.javashop.core.geo.model.*;
import com.enation.app.javashop.core.geo.service.GaodeManager;
import com.enation.app.javashop.core.geo.service.TencentManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会员足迹控制器
 *
 * @author zh
 * @version v7.1.4
 * @since vv7.1
 * 2019-06-18 15:18:56
 */
@RestController
@RequestMapping("/map")
@Api(description = "地图接口")
public class GaodeMapController {

    @Autowired
    private GaodeManager gaodeManager;


    @Autowired
    private TencentManager tencentManager;

    @ApiOperation(value = "坐标解析", response = ParseLatLngResponse.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lng", value = "经度", required = true, dataType = "double", paramType = "path"),
            @ApiImplicitParam(name = "lat", value = "纬度", required = true, dataType = "double", paramType = "path")
    })
    @GetMapping("/gaode/{lng}/{lat}")
    public RegeoCode parseLatLng(@PathVariable("lng") Double lng, @PathVariable("lat") Double lat) {
        return gaodeManager.parseLatLng(lng,lat);
    }

    @ApiOperation(value = "地址输入提示", response = Object.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", required = true, dataType = "String", paramType = "query")
    })
    @GetMapping("/gaode/inputtips")
    public List<Pois>  inputtips(@RequestParam(value = "keyword") String keyword) {
        return gaodeManager.inputtips(keyword);
    }


    @ApiOperation(value = "坐标解析", response = ParseLatLngResponse.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lng", value = "经度", required = true, dataType = "double", paramType = "path"),
            @ApiImplicitParam(name = "lat", value = "纬度", required = true, dataType = "double", paramType = "path")
    })
    @GetMapping("/tencent/{lng}/{lat}")
    public TencentLocation tencentParseLatLng(@PathVariable("lng") Double lng, @PathVariable("lat") Double lat) {
        return tencentManager.parseLatLng(lng,lat);
    }


}