package com.enation.app.javashop.buyer.api.promotion;

import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.promotion.newcomer.service.NewcomerGoodsManager;
import com.enation.app.javashop.core.promotion.newcomer.service.NewcomerManager;
import com.enation.app.javashop.framework.database.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

/**
 * 新人购
 */
@RestController
@RequestMapping("/promotions/newcomer")
@Api(description = "新人购相关API")
@Validated
public class NewcomerBuyerController {
    @Autowired
    private MemberManager memberManager;

    @Autowired
    private NewcomerGoodsManager newcomerGoodsManager;

    /**
     * 判断是不是新人  不需要token
     */
    @ApiOperation(value = "查看用户是不是新人")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "open_id", value = "openId", dataType = "String", paramType = "query")
    })
    @GetMapping("/new-user-flag")
    public boolean newUserFlag (@ApiIgnore String openId){
      return memberManager.selNewUserByOpenId(openId);
    }
    @ApiOperation(value = "查询新人购首页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "page_no", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "page_size", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "city", value = "city", dataType = "String", paramType = "query")
    })
    @GetMapping("/sel-new-comer-goods")
    public Map<String,Object> selNewComerGoods(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize, @ApiIgnore String city){
        return newcomerGoodsManager.selNewcomerGoods(pageNo,pageSize,city);
    }
}
