package com.enation.app.javashop.buyer.api.distribution;

import com.enation.app.javashop.core.base.model.vo.SuccessMessage;
import com.enation.app.javashop.core.distribution.exception.DistributionErrorCode;
import com.enation.app.javashop.core.distribution.exception.DistributionException;
import com.enation.app.javashop.core.distribution.model.dos.ShortUrlDO;
import com.enation.app.javashop.core.distribution.model.dto.QueryShareInfoDTO;
import com.enation.app.javashop.core.distribution.model.dto.ShareDTO;
import com.enation.app.javashop.core.distribution.service.DistributionManager;
import com.enation.app.javashop.core.distribution.service.ShortUrlManager;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 短链接识别
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/23 上午8:35
 */
@RestController
@RequestMapping("/distribution/su")
@Api(description = "短链接api")
public class ShortUrlBuyerController {


    @Resource
    private ShortUrlManager shortUrlManager;
    @Autowired
    private DistributionManager distributionManager;
    @Autowired
    private Cache cache;

    protected final Log logger = LogFactory.getLog(this.getClass());

    /**
     * 访问短链接 把会员id加入session中，并跳转页面
     *
     * @return
     */
    @GetMapping(value = "/visit")
    @ApiOperation("访问短链接 把会员id加入session中，并跳转页面")
    @ApiImplicitParam(name = "su", value = "短链接", required = true, paramType = "query", dataType = "String")
    public SuccessMessage visit(String su, @RequestHeader(required = false) String uuid) throws Exception {
        try {
            ShortUrlDO shortUrlDO = shortUrlManager.getLongUrl(su);
            if (shortUrlDO == null) {
                return null;
            }
            if (uuid != null) {
                cache.put(ShortUrlManager.PREFIX+uuid, getMemberId(shortUrlDO.getUrl()));
            }
            return new SuccessMessage(shortUrlDO.getUrl());
        } catch (Exception e) {
            logger.error("短连接验证出错", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());

        }

    }


    /**
     * url中提取member id
     *
     * @param url
     * @return
     */
    private Integer getMemberId(String url) {
        String pattern = "(member_id=)(\\d+)";
        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(url);
        if (m.find()) {
            return new Integer(m.group(2));
        }
        return 0;
    }


    @ApiOperation("生成短链接， 必须登录")
    @PostMapping(value = "/get-short-url")
    @ApiImplicitParam(name = "goods_id", value = "商品id", required = false, paramType = "query", dataType = "int")
    public SuccessMessage getShortUrl(@ApiIgnore Integer goodsId) {

        Buyer buyer = UserContext.getBuyer();
        // 没登录不能生成短链接
        if (buyer == null) {
            throw new DistributionException(DistributionErrorCode.E1001.code(), DistributionErrorCode.E1001.des());
        }
        try {
            int memberId = buyer.getUid();
            ShortUrlDO shortUrlDO = this.shortUrlManager.createShortUrl(memberId, goodsId);
            SuccessMessage successMessage = new SuccessMessage();
            successMessage.setMessage("/distribution/su/visit?su=" + shortUrlDO.getSu());
            return successMessage;
        } catch (Exception e) {
            logger.error("生成短连接出错", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


    /**
     * 根据类型查询分享内容
     */
    @ApiOperation("根据类型查询分享内容")
    @GetMapping(value = "/getShareInfo")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "share_type", value = "分享类型", required = true, paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "channel_type", value = "分享类型", required = true, paramType = "query", dataType = "Integer")
    })
    public ShareDTO getShareInfo(@ApiIgnore @NotEmpty(message = "主键必填") Integer id,
                                 @ApiIgnore @NotEmpty(message = "分享类型必填") Integer shareType,
                                 @ApiIgnore @NotEmpty(message = "分享渠道必填") Integer channelType){
        QueryShareInfoDTO queryShareInfoDTO = new QueryShareInfoDTO();
        queryShareInfoDTO.setId(id);
        queryShareInfoDTO.setShareType(shareType);
        queryShareInfoDTO.setChannelType(channelType);
        return distributionManager.getShareInfo(queryShareInfoDTO);
    }
}
