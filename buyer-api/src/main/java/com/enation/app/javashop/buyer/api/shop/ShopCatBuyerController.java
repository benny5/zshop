package com.enation.app.javashop.buyer.api.shop;

import com.enation.app.javashop.core.geo.model.AddressComponent;
import com.enation.app.javashop.core.geo.service.GaodeManager;
import com.enation.app.javashop.core.shop.model.dos.ShopCatDO;
import com.enation.app.javashop.core.shop.model.dos.ShopDO;
import com.enation.app.javashop.core.shop.service.ShopCatManager;
import com.enation.app.javashop.core.shop.service.ShopManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 店铺分组控制器
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-24 11:18:37
 */
@RestController
@RequestMapping("/shops/cats")
@Api(description = "店铺分组相关API")
public class ShopCatBuyerController {
	protected final Log logger = LogFactory.getLog(this.getClass());


	@Autowired
	private ShopCatManager shopCatManager;

	@Autowired
	private GaodeManager gaodeManager;

	@Autowired
	private ShopManager shopManager;


	@ApiOperation(value	= "查询店铺分组列表", response = ShopCatDO.class)
	@GetMapping("/{shop_id}")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "shop_id", value = "店铺id", required = true, dataType = "int", paramType = "path"),
			@ApiImplicitParam(name = "display", value = "是否展示,根据分类的显示状态查询：ALL(全部),SHOW(显示),HIDE(隐藏)", required = false, dataType = "string", paramType = "query",
					allowableValues = "ALL,SHOW,HIDE")
	})
	public List list(@PathVariable("shop_id") Integer shopId,@ApiIgnore  String display)	{
		return	this.shopCatManager.list(shopId,display);
	}


	/**
	 * 根据坐标查询出当前位置对应的社区团购店铺的分组列表
	 */
	@GetMapping("/community/cats")
	@ApiOperation(value = "根据客户当前的坐标位置，查询出对应的社区团购店铺的店铺分组列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "lng", value = "经度", required = false, dataType = "number"),
			@ApiImplicitParam(name = "lat", value = "纬度", required = false, dataType = "number")
	})
	public List list(@RequestParam(name = "lng") Double lng,@RequestParam(name="lat") Double lat){
		if(lng ==null || lat==null){
			return null;
		}

		//获取当前地址对应的县级社区团购点(高德)
		AddressComponent address = this.gaodeManager.parseLatLng(lng,lat).getAddressComponent();
		 //查询出位置对应的社区团购店铺ID
		ShopDO shopDO = this.shopManager.getShopByDistrict(address);
		if(shopDO!=null && shopDO.getShopId()!=null){
			return this.shopCatManager.list(shopDO.getShopId(),"SHOW");
		}else{
			return null;
		}
	}

}
