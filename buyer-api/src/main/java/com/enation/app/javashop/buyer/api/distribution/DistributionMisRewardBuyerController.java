package com.enation.app.javashop.buyer.api.distribution;

import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionRewardDO;
import com.enation.app.javashop.core.distribution.model.dto.QueryDistributionMissionRewardDTO;
import com.enation.app.javashop.core.distribution.service.DistributionMissionRewardManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zhou
 * @Date: 2021/1/26
 * @Description:  团长任务奖励相关API
 */
@RestController
@RequestMapping("/distribution/reward")
@Api(description = "团长任务奖励API")
public class DistributionMisRewardBuyerController {
    @Autowired
    private DistributionMissionRewardManager distributionMissionRewardManager;

    @ApiOperation(value = "查询团长任务奖励明细")
    @GetMapping
    public Page<DistributionMissionRewardDO> list(QueryDistributionMissionRewardDTO queryDTO) {
        Buyer buyer = UserContext.getBuyer();
        queryDTO.setMemberId(buyer.getUid());
        return distributionMissionRewardManager.queryDisMisRewList(queryDTO);
    }
}
