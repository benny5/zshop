package com.enation.app.javashop.buyer.api.goods;

import com.enation.app.javashop.core.client.trade.ShetuanClient;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsGroupVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsSearchDTO;
import com.enation.app.javashop.core.goodssearch.service.ShetuanSearchManager;
import com.enation.app.javashop.core.trade.order.model.dto.ShetuanOrderQueryParam;
import com.enation.app.javashop.core.trade.order.model.vo.OrderLineVO;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Buyer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 *社区团购商品
 */
@Api(description = "社区团购商品API")
@RestController
@RequestMapping("/shetuan/goods")
public class SheTuanGoodsController {

    @Autowired
    private ShetuanSearchManager sheTuanSearchManager;

    @Autowired
    private ShetuanClient shetuanClient;

    @ApiOperation(value = "搜索社区团购商品列表")
    @GetMapping
    public Page searchGoods(@ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize, ShetuanGoodsSearchDTO searchDTO) {

        searchDTO.setPageNo(pageNo);
        searchDTO.setPageSize(pageSize);
        //  根据用户订单地址选择 社区团购店铺 从用户checkParams中获取
        Page search= this.sheTuanSearchManager.search(searchDTO);
        return search;
    }

    @ApiOperation(value = "根据店铺一级分组查询所有商品，按照二级分组进行归类")
    @GetMapping("/{shop_cat_id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shop_cat_id", value = "店铺一级分组", required = true, dataType = "int", paramType = "path")
    })
    @Deprecated
    public List<ShetuanGoodsGroupVO> searchByShopCat(@ApiIgnore @PathVariable(value = "shop_cat_id")  Integer  shopCatId) {
       return this.sheTuanSearchManager.searchByShopCat(shopCatId);
    }

    @ApiOperation(value = "查询团长订单列表")
    @PostMapping("/queryOrderPage")
    public Page<OrderLineVO> list(@Valid @RequestBody ShetuanOrderQueryParam shetuanOrderQueryParam) {

        Buyer buyer = UserContext.getBuyer();
        shetuanOrderQueryParam.setLeaderMemberId(buyer.getUid());
        Page page = this.shetuanClient.queryShetuanOrders(shetuanOrderQueryParam);

        return page;
    }

}
