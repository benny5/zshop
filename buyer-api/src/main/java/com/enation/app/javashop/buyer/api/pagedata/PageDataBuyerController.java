package com.enation.app.javashop.buyer.api.pagedata;

import com.enation.app.javashop.core.pagedata.constraint.annotation.ClientAppType;
import com.enation.app.javashop.core.pagedata.constraint.annotation.PageType;
import com.enation.app.javashop.core.pagedata.model.PageData;
import com.enation.app.javashop.core.pagedata.model.enums.PageStateEnum;
import com.enation.app.javashop.core.pagedata.service.PageDataManager;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.framework.util.DictUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 楼层控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@RestController
@RequestMapping("/pages")
@Api(description = "楼层相关API")
@Validated
public class PageDataBuyerController {

    @Autowired
    private PageDataManager pageManager;

    @Autowired
    private ShopManager shopManager;

    @GetMapping(value = "/{client_type}/{page_type}")
    @ApiOperation(value = "查询楼层数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "要查询的客户端类型 APP/WAP/PC", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "page_type", value = "要查询的页面类型 INDEX 首页/SPECIAL 专题", required = true, dataType = "string", paramType = "path")
    })
    public PageData get(@ClientAppType @PathVariable("client_type") String clientType,@PageType @PathVariable("page_type") String pageType) {
        PageData pageData = new PageData();
        pageData.setClientType(clientType);
        pageData.setPageType(pageType);
        pageData.setState(PageStateEnum.ACTIVE.getValue());
        PageData page = this.pageManager.getOne(pageData);

        return page;
    }

    /**
     * 查询发现页数据
     * @param clientType
     * @param city
     * @return
     */


    @GetMapping(value = "/get_discover/{client_type}/{city}")
    @ApiOperation(value = "查询楼层数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "要查询的客户端类型 APP/WAP/PC", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "city", value = "城市", required = true, dataType = "string", paramType = "path")
    })
    public PageData getDiscover(@ClientAppType @PathVariable("client_type") String clientType,
            @PathVariable(value = "city",required = true) String city) {
        //设置开关，如果小程序审核通过了，就把此开关打开
        boolean onOff = true;

        //开关关闭，则走APP的配置页面
        if(onOff && clientType.equals("WXS")){
                clientType = "APP";
        }

        PageData page = this.pageManager.getDiscover(clientType,city);

        return page;
    }

    /**
     * john 根据页面id 查询页面数据
     * @param clientType
     * @param pageId
     * @return
     */
    @GetMapping(value = "/v2/{client_type}/{page_type}")
    @ApiOperation(value = "查询楼层数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "要查询的客户端类型 APP/WAP/PC", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "page_type", value = "0 微页面 1/商城首页 2团购首页 3 附近首页", required = true, dataType = "string", paramType = "path"),
            @ApiImplicitParam(name = "page_id", value = "页面id", required = false, dataType = "integer", paramType = "query")
    })
    public PageData getV2(@ClientAppType @PathVariable("client_type") String clientType,
                           @PathVariable("page_type") String pageType,
                          @RequestParam(value = "page_id",required = false) Integer pageId,
                          @RequestParam(value = "city",required = false) String city,
                          @RequestParam(value = "lng",required = false) Double lng,
                          @RequestParam(value = "lat",required = false) Double lat) {
        PageData page = null;
        // 除去微页面(自定义页面)
        if(!"0".equals(pageType)){
            //查询首页
            //设置开关，如果小程序审核通过了，就把此开关打开
            boolean onOff = true;

            //开关关闭，则走APP的配置页面
            if(onOff && clientType.equals("WXS")){
                clientType = "APP";
            }

            page=pageManager.getIndexPage(clientType,pageType,city,lng,lat);
        }else if(pageId!=null){
            page = pageManager.getModel(pageId);
        }else{
            //即不是查询首页，也没有传页面id进来， 说明传参有问题
            throw new RuntimeException("数据不合法");
        }

        return page;
    }
    @GetMapping(value = "/isShetuan")
    @ApiOperation(value = "查询是否开通社区团购")
    public Boolean isShetuan(@RequestParam(value = "city",required = false) String city) {
        Integer shopId = shopManager.getShopByCity(city);
        if(shopId!=null && shopId!=0){
           return true;
        }
        return false;
    }


}
