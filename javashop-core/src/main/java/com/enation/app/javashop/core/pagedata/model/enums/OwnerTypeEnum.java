package com.enation.app.javashop.core.pagedata.model.enums;

/**
 * @author john
 * @Description 页面归属
 * @ClassName PageTypeEnum
 */
public enum OwnerTypeEnum {
    //平台
    DEFINED(0,"无"),
    //平台
    PLATFORM(1,"平台"),
    //代理
    AGENT(2,"代理"),
    //代理
    SELLER(3,"商户");
    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    private String description;

    OwnerTypeEnum(int value ,String des) {
        this.value = value;
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public Integer value() {
        return this.getValue();
    }
}
