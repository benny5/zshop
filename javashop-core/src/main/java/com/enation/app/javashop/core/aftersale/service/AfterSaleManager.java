package com.enation.app.javashop.core.aftersale.service;

import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.aftersale.model.dos.RefundGoodsDO;
import com.enation.app.javashop.core.aftersale.model.dto.RefundDTO;
import com.enation.app.javashop.core.aftersale.model.dto.RefundDetailDTO;
import com.enation.app.javashop.core.aftersale.model.dto.SpreadRefundDTO;
import com.enation.app.javashop.core.aftersale.model.vo.SpreadRefundQueryParamVO;
import com.enation.app.javashop.core.aftersale.model.vo.*;
import com.enation.app.javashop.core.goods.model.enums.Permission;
import com.enation.app.javashop.core.trade.order.model.dto.RefundOrderDTO;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;

import java.util.List;

/**
 * 售后管理接口
 * @author zjp
 * @version v7.0
 * @since v7.0 下午3:07 2018/5/2
 */
public interface AfterSaleManager {

    /**
     * 申请退款
     * @param refundApply 退款申请
     */
    void applyRefund(BuyerRefundApplyVO refundApply);

    /**
     * 系统退款
     * @param refundApply 退款申请
     */
    void systemRefund(BuyerRefundApplyVO refundApply);

    /**
     * 退货申请
     * @param refundApply 退款申请VO
     */
    void applyGoodsReturn(BuyerRefundApplyVO refundApply);

    /**
     * 买家对已付款的订单执行取消操作
     * @param buyerCancelOrderVO 退款申请VO
     */
    void cancelOrder(BuyerCancelOrderVO buyerCancelOrderVO);

    /**
     * 系统对已付款的订单执行取消操作
     * @param buyerCancelOrderVO 退款申请VO
     */
    void sysCancelOrder(BuyerCancelOrderVO buyerCancelOrderVO);

    /**
     *
     * @param refundApproval 批准 vo
     * @param permission 审核权限枚举
     * @return 退款批准 vo
     */
    RefundApprovalVO approval(RefundApprovalVO refundApproval, Permission permission);

    /**
     * 卖家或平台入库
     * @param sn 退款单
     * @param remark 备注
     * @param permission 权限枚举
     */
    void sellerStockIn(String sn, String remark, Permission permission) ;

    /**
     * 财务审核/执行一个退款
     * @param refundApproval
     * 2020年11月28日 16:07:00 停用
     */
    FinanceRefundApprovalVO approval(FinanceRefundApprovalVO refundApproval);

    /**
     * 根据参数查询退款（货）单
     * @param param 查询参数
     */
    Page<RefundDTO> query(RefundQueryParamVO param);

    /**
     * 根据编号获取详细
     * @param sn 单据编号
     */
    RefundDetailDTO getDetail(String sn);

    /**
     * 买家取消退款或退货
     * @param sn 退款单sn
     */
    RefundDO cancelRefund(String sn);

    /**
     * 获取退货单的商品列表
     * @param sn 退款单号
     * @return 退货商品列表
     */
    List<RefundGoodsDO> getRefundGoods(String sn);

    /**
     * 查询退款方式为原路退回且状态为退款中的退款单
     */
    List<RefundDO> queryNoReturnOrder();

    /**
     * 更新退款单的状态
     * @param list 退款单列表
     */
    void update(List<RefundDO> list);

    /**
     * 卖家退款
     * @param sn 退款单号
     * @param remark 退款备注
     */
    void sellerRefund(String sn,String remark);

    /**
     * 退款单导出excel
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return 字节流
     */
    List<ExportRefundExcelVO> exportExcel(long startTime,long endTime);

    /**
     * 获取退款申请信息
     * @param orderSn 订单号
     * @param skuId skuId
     */
    RefundApplyVO refundApply(String orderSn,Integer skuId);
    /**
     * 获取未完成售后订单数量
     * @param memberId 会员ID
     * @param shopId 店铺ID
     * @return 未完成售后订单数量
     */
    Integer getAfterSaleCount(Integer memberId,Integer shopId);

    /**
     * 查询退款单状态
     */
    List<RefundDO> queryRefundStatus();

    /**
     * 修改退款单的店铺名称
     * @param shopId 店铺ID
     * @param shopName 店铺名称
     */
    void editRefundShopName(Integer shopId, String shopName);

    Page<RefundOrderDTO> queryRefundOrders(RefundQueryParamVO queryParam);

    RefundDetailDTO getDetailByOrderSn(String orderSn);

    /**
     * 查询支付订单数
     */
    Integer getAfterSaleCount();

    /**
     * 根据订单号数组和退补差价数据实现批量退补差价
     * @param orderSnList 订单号数组
     * @param spreadRefundDTO 多退少补退款VO
     */
    List<String> spreadRefund(String[] orderSnList, SpreadRefundDTO spreadRefundDTO, Seller seller);

    /**
     * 分页查询退补差价数据
     * @param spreadRefundQueryParamVO 多退少补查询VO
     */
    Page<SpreadRefundQueryParamVO> querySpreadRefund(SpreadRefundQueryParamVO spreadRefundQueryParamVO);
}
