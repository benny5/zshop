package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.PayStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipStatusEnum;
import com.enation.app.javashop.core.trade.order.support.OrderSpecialStatus;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import lombok.Data;
import org.apache.commons.lang.StringUtils;


@Data
public class ExcelGroupVO {


    @ExcelProperty("订单编号")
    private String sn;

    @ExcelProperty("下单时间")
    private String createTimeShow;

    @ExcelProperty("所属店铺")
    private String sellerName;

    @ExcelProperty("站长姓名")
    private String leaderName;

    @ExcelProperty("站长手机号")
    private String leaderMobile;

    @ExcelProperty("站点名称")
    private String siteName;

    @ExcelProperty("站点地址")
    private String pickAddress;

    @ExcelProperty("团长姓名")
    private String realName;

    @ExcelProperty("团长手机号")
    private String realMobile;


    @ExcelProperty("下单会员")
    private String memberName;

    @ExcelProperty("订单总额")
    private Double orderPrice;

    @ExcelProperty("订单运费")
    private Double shippingPrice;

    @ExcelProperty("订单优惠")
    private Double discountPrice;

    @ExcelProperty("订单状态")
    private String orderStatusText;

    @ExcelProperty("付款状态")
    private String payStatusText;

    @ExcelProperty("发货状态")
    private String shipStatusText;

    @ExcelProperty("支付类型")
    private String paymentType;

    @ExcelProperty("付款时间")
    private String paymentTimeShow;

    @ExcelProperty("付款金额")
    private String payMoney;

    @ExcelProperty("收货人")
    private String shipName;

    @ExcelProperty("收货人联系方式")
    private String shipMobile;

    @ExcelProperty("收货地址")
    private String shipAddr;

    //==========================================================================

    @ExcelProperty("商品id")
    private Integer goodsId;

    @ExcelProperty("商品名称")
    private String name;

    @ExcelProperty("商品数量")
    private Integer goodsNum;

    @ExcelProperty("sku名称")
    private String skuName;

    @ExcelProperty("商品售价")
    private Double purchasePrice;

    @ExcelProperty("商品进价")
    private Double originalPrice;

    @ExcelProperty("SKU_ID")
    private Integer skuId;

    @ExcelProperty("供应商")
    private String supplierName;

    @ExcelProperty("外部商品ID")
    private String upGoodsId;

    @ExcelProperty("外部SKU_ID")
    private String upSkuId;

    @ExcelProperty("商品货号")
    private String skuSn;


    //=====================================
    @ExcelProperty("物流公司")
    private String logiName;

    @ExcelProperty("快递单号")
    private String shipNo;

    @ExcelProperty("买家备注")
    private String remark;


    public void transStatus(OrderDO orderDO){
        //先从特殊的流程-状态显示 定义中读取，如果为空说明不是特殊的状态，直接显示为 状态对应的提示词
        this.orderStatusText = OrderSpecialStatus.getStatusText(orderDO.getOrderType(), orderDO.getPaymentType(), orderDO.getOrderStatus());
        // 售后中
        String serviceStatus = OrderSpecialStatus.getStatusText(orderDO.getOrderType(), orderDO.getServiceStatus(),null);
        this. orderStatusText = StringUtils.isNotEmpty(serviceStatus) ?serviceStatus:orderStatusText;
        // 常规状态
        if (StringUtil.isEmpty(orderStatusText)) {
            this.orderStatusText = OrderStatusEnum.valueOf(orderDO.getOrderStatus()).description();
        }

        this. payStatusText = PayStatusEnum.valueOf(orderDO.getPayStatus()).description();
        this. shipStatusText = ShipStatusEnum.valueOf(orderDO.getShipStatus()).description();
        this.createTimeShow= DateUtil.toString(orderDO.getCreateTime(), "yyyy-MM-dd HH:mm:ss");
        this.paymentTimeShow= DateUtil.toString(orderDO.getCreateTime(), "yyyy-MM-dd HH:mm:ss");
    }
}
