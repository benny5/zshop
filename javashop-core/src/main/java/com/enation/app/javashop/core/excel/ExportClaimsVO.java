package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2020/9/24
 * @Description: 理赔单导出实体类
 */
@Data
public class ExportClaimsVO {

    @ExcelProperty(value = "理赔单号", index = 0)
    private String claimSn;

    @ExcelProperty(value = "异常单单号", index = 1)
    private String exceptionSn;

    @ExcelProperty(value = "订单号", index = 2)
    private String orderSn;

    @ExcelProperty(value = "理赔状态", index = 3)
    String claimStatusText;

    @ExcelProperty(value = "理赔类型", index = 4)
    String claimTypeText;

    @ExcelProperty(value = "多退少补原因", index = 5)
    private String refundReasonText;

    @ExcelProperty(value = "理赔商品信息json", index = 6)
    private String claimSkus;

    @ExcelProperty(value = "理赔金额", index = 7)
    private Double claimPrice;

    @ExcelProperty(value = "理赔说明", index = 8)
    private String claimDescribe;

    @ExcelProperty(value = "配送方式", index = 9)
    String shippingTypeText;

    @ExcelProperty(value = "买家昵称", index = 10)
    String memberNikename;

    @ExcelProperty(value = "收货人", index = 11)
    private String shipName;

    @ExcelProperty(value = "收货电话", index = 12)
    private String shipMobile;

    @ExcelProperty(value = "收货地址", index = 13)
    private String shipAddr;

    @ExcelProperty(value = "团长姓名", index = 14)
    private String memberRealnameLv1;

    @ExcelProperty(value = "团长电话", index = 15)
    private String memberMobileLv1;

    @ExcelProperty(value = "自提站点", index = 16)
    private String siteName;

    @ExcelProperty(value = "站长姓名", index = 17)
    private String siteLeaderName;

    @ExcelProperty(value = "站长电话", index = 18)
    private String siteLeaderMobile;

    @ExcelProperty(value = "审核时间", index = 19)
    private String auditTime;

    @ExcelProperty(value = "审核人姓名", index = 20)
    @Column(name = "audit_member_name")
    private String auditMemberName;

    @ExcelProperty(value = "审核备注", index = 21)
    private String auditDescribe;

    @ExcelProperty(value = "申请时间", index = 22)
    private String applyTime;

    @ExcelProperty(value = "申请人姓名", index = 23)
    private String applyMemberName;

    @ExcelProperty(value = "理赔时间", index = 24)
    private String claimTime;

    @ExcelProperty(value = "理赔人姓名", index = 25)
    private String claimMemberName;

}
