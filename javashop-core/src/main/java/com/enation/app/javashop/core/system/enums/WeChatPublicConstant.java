package com.enation.app.javashop.core.system.enums;

/**
 * @author 王志杨
 * @since 2020/11/10 10:12
 * 微信公众号常量
 */
public class WeChatPublicConstant {

    //APPID
    public static final String appID = "wx51fa9f001084a3a9";
    //appsecret
    public static final String appsecret = "097158fe887a4384911b6c4d00b0fb15";
    //接入公众号的Token
    public static final String TOKEN = "zhiyi_gongzhonghao_token";
    //回复消息的类型-text
    public static final String RESP_MESSAGE_TYPE_TEXT = "text";
    public static final Object REQ_MESSAGE_TYPE_TEXT = "text";
    public static final Object REQ_MESSAGE_TYPE_IMAGE = "image";
    public static final Object REQ_MESSAGE_TYPE_VOICE = "voice";
    public static final Object REQ_MESSAGE_TYPE_VIDEO = "video";
    public static final Object REQ_MESSAGE_TYPE_LOCATION = "location";
    public static final Object REQ_MESSAGE_TYPE_LINK = "link";
    //消息类型，event
    public static final Object REQ_MESSAGE_TYPE_EVENT = "event";
    //订阅事件
    public static final Object EVENT_TYPE_SUBSCRIBE = "subscribe";
    //取消订阅事件
    public static final Object EVENT_TYPE_UNSUBSCRIBE = "unsubscribe";
    public static final Object EVENT_TYPE_SCAN = "SCAN";
    public static final Object EVENT_TYPE_LOCATION = "LOCATION";
    public static final Object EVENT_TYPE_CLICK = "CLICK";

    //发送方帐号（一个OpenID）---订阅人的openID/发信人的openID
    public static final String FromUserName = "FromUserName";
    //开发者微信号
    public static final String ToUserName = "ToUserName";
    //消息类型，event
    public static final String MsgType = "MsgType";
    public static final String Content = "Content";
    //事件类型，subscribe(订阅)、unsubscribe(取消订阅)
    public static final String Event = "Event";
    //微信公众号的access_token存入缓存的key---公众平台以access_token为接口调用凭据，来调用接口，所有接口的调用需要先获取access_token
    public static final String WEIXIN_PUBLIC_TOKEN = "WEIXIN_PUBLIC_TOKEN";
    //微信公众号发送模板消息的topcolor参数标题颜色（红色）
    public static final String TOP_COLOR_RED = "#FF0000";
    //微信公众号发送模板消息的topcolor参数标题颜色（红色）
    public static final String TOP_COLOR_BLUE = "#173177";
    //微信公众号发送模板消息的topcolor参数标题颜色（红色）
    public static final String WELCOME_TEXT = "您好，欢迎关注智溢商城\uD83D\uDE0B\uD83D\uDE0B智溢商城本地生活服务平台，致力于解决您日常生活的购物所需\uD83D\uDE09\uD83D\uDE09让您足不出户，淘尽全城好物";



}
