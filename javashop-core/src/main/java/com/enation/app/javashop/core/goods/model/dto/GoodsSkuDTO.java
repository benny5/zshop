package com.enation.app.javashop.core.goods.model.dto;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/7/21 10:38
 */

@Data
public class GoodsSkuDTO {
    private Integer skuId;
    private String goodsName;
    private String sn;
    private Double cost;
    private Integer enableQuantity;
    private String upSkuId;
    private String  supplierName;
    private String upGoodsId;
    private String specs;






}
