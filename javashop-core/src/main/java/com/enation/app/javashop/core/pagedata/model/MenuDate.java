package com.enation.app.javashop.core.pagedata.model;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 菜单配置-实体类
 */
@Table(name = "es_page")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public  class MenuDate implements Serializable {

    @Id(name = "page_id")
    @ApiModelProperty(hidden = true)
    private Integer pageId;

    @Column(name = "page_name")
    @ApiModelProperty(name = "page_name", value = "配置名称", required = true)
    @NotEmpty(message = "名称不能为空")
    private String pageName;

    @Column(name = "page_data")
    @ApiModelProperty(name = "page_data", value = "菜单设置数据", required = true)
    @NotEmpty(message = "菜单设置不能为空")
    private String pageData;


    @Column(name = "owner_type")
    @ApiModelProperty(name = "owner_type", value = "配置类型", required = true)
    @NotNull(message = "请选中类型")
    private Integer ownerType;

    @Column(name = "province")
    @ApiModelProperty(name = "province", value = "省")
    private String province;

    @Column(name = "city")
    @ApiModelProperty(name = "city", value = "市")
    private String city;

    @Column(name = "county")
    @ApiModelProperty(name = "county", value = "区/县")
    private String county;

    // 0 删除 1线上
    @Column(name = "delete_flag")
    @ApiModelProperty(name = "delete_flag", value = "删除标识",hidden = true)
    private Integer deleteFlag;
}
