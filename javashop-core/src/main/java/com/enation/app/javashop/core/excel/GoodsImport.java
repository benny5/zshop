package com.enation.app.javashop.core.excel;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/7/11 17:31
 */

@Data
public class GoodsImport {
    private Integer goodsId;

    private String upGoodsId;

    private String goodsName;

    private String categoryName;

    private String shopCatName;


}
