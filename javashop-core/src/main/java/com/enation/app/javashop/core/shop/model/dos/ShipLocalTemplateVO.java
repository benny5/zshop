package com.enation.app.javashop.core.shop.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


/**
 * 同城配送运费模版实体
 *
 * @author JFENG
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 21:44:49
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShipLocalTemplateVO extends  ShipTemplateDO implements Serializable {

    private static final long serialVersionUID = 6348162955890093L;
    // ================JFENG================

    @ApiParam("配送费用")
    @Column(name = "fee_setting_vo")
    private TemplateFeeSetting  feeSettingVO;

    @ApiParam("配送时间")
    @Column(name = "time_setting_vo")
    private TemplateTimeSetting  timeSettingVO;
}




