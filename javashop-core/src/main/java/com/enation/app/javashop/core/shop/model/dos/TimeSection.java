package com.enation.app.javashop.core.shop.model.dos;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
public class TimeSection  implements Serializable {
    private static final long serialVersionUID = 6348162955890093L;

    @ApiParam("派送时间")
    private String shipTime;
    @ApiParam("到达类型 0 当日 1 次日")
    private Integer  shipTimeType;
    @ApiParam("到达时间")
    private String  arriveTime;
}
