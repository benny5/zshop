package com.enation.app.javashop.core.shop.model.dos;

import java.io.Serializable;
import java.util.List;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * 店铺分组实体
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-24 11:18:37
 */
@Table(name="es_shop_cat")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShopCatDO implements Serializable {

    private static final long serialVersionUID = 9888143360348481L;

    /**店铺分组id*/
    @Id(name = "shop_cat_id")
    @ApiModelProperty(name = "shop_cat_id",value = "店铺分组id")
    private Integer shopCatId;
    /**店铺分组父ID*/
    @Column(name = "shop_cat_pid")
    @ApiModelProperty(name="shop_cat_pid",value="店铺分组父ID",required=false)
    private Integer shopCatPid;
    /**店铺id*/
    @Column(name = "shop_id")
    @ApiModelProperty(name="shop_id",value="店铺id",required=false)
    private Integer shopId;
    /**店铺分组名称*/
    @Column(name = "shop_cat_name")
    @ApiModelProperty(name="shop_cat_name",value="店铺分组名称",required=true)
    @NotEmpty(message = "店铺分组名称必填")
    private String shopCatName;

    /**店铺分组描述*/
    @Column(name = "shop_cat_desc")
    @ApiModelProperty(name="shop_cat_desc",value="店铺分组描述",required=false)
    private String shopCatDesc;

    /**店铺分组描述*/
    @Column(name = "shop_cat_pic")
    @ApiModelProperty(name="shop_cat_pic",value="店铺分组图片",required=false)
    private String shopCatPic;

    /**店铺分组显示状态:1显示 0不显示*/
    @Column(name = "disable")
    @ApiModelProperty(name="disable",value="店铺分组显示状态:1显示 0不显示",required=true)
    @NotNull(message = "店铺分组显示状态必填")
    private Integer disable;
    /**排序*/
    @Column(name = "sort")
    @ApiModelProperty(name="sort",value="排序",required=true)
    @NotNull(message = "排序必填")
    private Integer sort;
    /**分组路径*/
    @Column(name = "cat_path")
    @ApiModelProperty(name="cat_path",value="分组路径",required=false)
    private String catPath;
    /**分组路径*/
    @Column(name = "cat_path_name")
    @ApiModelProperty(name="cat_path_name",value="分组路径名称",required=false)
    private String catPathName;

    /**子分组*/
    @ApiModelProperty(name="children",value="分组路径",required=false)
    private List<ShopCatDO> children;

}
