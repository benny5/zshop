package com.enation.app.javashop.core.geo.model;

import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2019/10/22 10:43
 */
@Data
public class ParseKeywordResponse {
    private String status;
    private String info;
    private String infocode;
    private List<Pois> tips;
}
