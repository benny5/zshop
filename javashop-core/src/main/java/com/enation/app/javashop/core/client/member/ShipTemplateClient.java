package com.enation.app.javashop.core.client.member;

import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.shop.model.dos.ShipLocalTemplateVO;
import com.enation.app.javashop.core.shop.model.vo.ShipTemplateVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartVO;

/**
 * @version v7.0
 * @Description: 店铺运费模版Client默认实现
 * @Author: zjp
 * @Date: 2018/7/25 16:20
 */
public interface ShipTemplateClient {
    /**
     * 获取运费模版
     * @param id 运费模版主键
     * @return ShipTemplate  运费模版
     */
    ShipTemplateVO get(Integer id);

    ShipLocalTemplateVO getLocalTemplate(Integer localTemplateId);

    void checkLocalCart(CartVO cartVO, MemberAddress address);
}
