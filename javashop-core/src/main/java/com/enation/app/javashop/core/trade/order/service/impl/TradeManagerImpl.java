package com.enation.app.javashop.core.trade.order.service.impl;

import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.member.MemberAddressClient;
import com.enation.app.javashop.core.client.member.MemberClient;
import com.enation.app.javashop.core.goods.service.GoodsManager;
import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.payment.model.enums.ClientType;
import com.enation.app.javashop.core.trade.cart.model.enums.CartSourceType;
import com.enation.app.javashop.core.trade.cart.model.enums.CheckedWay;
import com.enation.app.javashop.core.trade.cart.model.vo.CartVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartView;
import com.enation.app.javashop.core.trade.cart.model.vo.ShipWayVO;
import com.enation.app.javashop.core.trade.cart.service.CartReadManager;
import com.enation.app.javashop.core.trade.order.model.vo.CheckoutParamVO;
import com.enation.app.javashop.core.trade.order.model.vo.TradeVO;
import com.enation.app.javashop.core.trade.order.service.*;
import com.enation.app.javashop.framework.redis.transactional.RedisTransactional;
import com.enation.app.javashop.framework.util.StringUtil;
import net.bytebuddy.implementation.MethodCall;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 交易业务类
 *
 * @author Snow create in 2018/4/8
 * @version v2.0
 * @since v7.0.0
 *
 * @version 2.1
 * 使用TradeCreator来创建交易
 */
@Service
@Primary
public class TradeManagerImpl implements TradeManager {
    protected final Log logger = LogFactory.getLog(this.getClass());


    @Autowired
    protected CheckoutParamManager checkoutParamManager;

    @Autowired
    protected CartReadManager cartReadManager;

    @Autowired
    protected ShippingManager shippingManager;

    @Autowired
    protected GoodsClient goodsClient;


    @Autowired
    protected TradeSnCreator tradeSnCreator;

    @Autowired
    protected MemberAddressClient memberAddressClient;

    @Autowired
    protected MemberClient memberClient;

    @Autowired
    protected TradeIntodbManager tradeIntodbManager;

    @Autowired
    private GoodsManager goodsManager;


    @Override
    public TradeVO createTrade(String client, CheckedWay way) {

        this.setClientType(client);
        // 1 用户结算选择
        CheckoutParamVO param = checkoutParamManager.getParam();
        // 2 查看购物车
        CartView cartView = this.cartReadManager.getCheckedItems(way);
        // 3 用户默认地址
        MemberAddress memberAddress = this.memberAddressClient.getModel(param.getAddressId());
        // 4 组合1/2/3的参数
        TradeCreator tradeCreator = new DefaultTradeCreator(param,cartView,memberAddress).setTradeSnCreator( tradeSnCreator).setGoodsClient(goodsClient).setMemberClient(memberClient).setShippingManager(shippingManager);

        //判断是否是虚拟商品，如果是虚拟商品则不用计算运费
        boolean isVirtual=goodsManager.checkVirtualGoods(cartView);
        TradeVO tradeVO =new TradeVO();
        if(isVirtual){
            tradeVO = tradeCreator
                    .checkGoods()
                    .checkPromotion()
                    .createTrade();
        }else {
            //检测配置范围-> 检测商品合法性 -> 检测促销活动合法性 -> 创建交易
            tradeVO = tradeCreator
                    .checkShipRange()
                    .checkGoods()
                    .checkPromotion()
                    .createTrade();
        }

        tradeVO.setCartSourceType(CartSourceType.COMMON_CART.name());

        //订单入库
        this.tradeIntodbManager.intoDB(tradeVO);

        return tradeVO;
    }

    /**
     * 普通商品_下单
     * @param client
     * @param way
     * @param sellerId
     * @param buyerId
     * @return
     */
    @Override
    @RedisTransactional(acquireTimeout = 1,lockTimeout = 30,lockName = "createTrade_",value = "#buyerId")
    public TradeVO createTrade(String client, CheckedWay way, int sellerId, Integer buyerId, double walletPayPrice) {

        this.setClientType(client);
        // 1 用户结算选择
        CheckoutParamVO param = checkoutParamManager.getParam();
        // 2 查看购物车详情
        CartView cartView = this.cartReadManager.getCheckedItems(way,sellerId);
        // 3 用户默认选中收货地址
        MemberAddress memberAddress = this.memberAddressClient.getModel(param.getAddressId());
        // 4 封装1/2/3的参数
        TradeCreator tradeCreator = new DefaultTradeCreator(param, cartView, memberAddress)
                .setTradeSnCreator(tradeSnCreator)
                .setGoodsClient(goodsClient)
                .setMemberClient(memberClient)
                .setShippingManager(shippingManager);


        //判断是否是虚拟商品，如果是虚拟商品则不用计算运费
        boolean isVirtual = goodsManager.checkVirtualGoods(cartView);
        TradeVO tradeVO;
        if (isVirtual) {
            tradeVO = tradeCreator
                    .checkGoods()
                    .checkPromotion()
                    .createTrade();
        } else {
            //检测配置范围-> 检测商品合法性 -> 检测促销活动合法性 -> 创建交易
            tradeVO = tradeCreator
                    .checkShipRange()
                    .checkGoods()
                    .checkPromotion()
                    .createTrade();
        }

        tradeVO.setCartSourceType(CartSourceType.COMMON_CART.name());
        tradeVO.setWalletPayPrice(walletPayPrice);
        //订单入库
        this.tradeIntodbManager.intoDB(tradeVO);

        return tradeVO;
    }


    /**
     * 设置client type
     * @param client
     */
    protected void setClientType(String client) {

        String clientType=null;
        if (StringUtil.isWap()) {
            clientType = ClientType.WAP.name();
        } else if (ClientType.REACT.getClient().equals(client)) {

            clientType = ClientType.REACT.getClient();

        }else{
            clientType = ClientType.PC.name();
        }

        this.checkoutParamManager.setClientType(clientType);
    }




}
