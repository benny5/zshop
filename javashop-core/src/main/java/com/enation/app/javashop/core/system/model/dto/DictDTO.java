package com.enation.app.javashop.core.system.model.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author JFeng
 * @date 2020/8/13 14:53
 */
@Data
public class DictDTO {
    private String key;
    private  List<Map<String, List<String>>> value;
}

