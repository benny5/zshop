package com.enation.app.javashop.core.client.member.impl;

import com.enation.app.javashop.core.client.member.ShipTemplateClient;
import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.shop.model.dos.ShipLocalTemplateVO;
import com.enation.app.javashop.core.shop.model.dos.ShipTemplateDO;
import com.enation.app.javashop.core.shop.model.vo.ShipTemplateVO;
import com.enation.app.javashop.core.shop.service.ShipTemplateLocalManager;
import com.enation.app.javashop.core.shop.service.ShipTemplateManager;
import com.enation.app.javashop.core.trade.cart.model.vo.CartVO;
import com.enation.app.javashop.framework.util.GeoUtils;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @version v7.0
 * @Description:
 * @Author: zjp
 * @Date: 2018/7/25 16:24
 */
@Service
@ConditionalOnProperty(value="javashop.product", havingValue="stand")
public class ShipTemplateClientDefaultImpl implements ShipTemplateClient {

    @Autowired
    private ShipTemplateManager shipTemplateManager;

    @Autowired
    private ShipTemplateLocalManager  shipTemplateLocalManager;

    @Override
    public ShipTemplateVO get(Integer id) {
        return shipTemplateManager.getFromCache(id);
    }

    @Override
    public ShipLocalTemplateVO getLocalTemplate(Integer localTemplateId) {
        return shipTemplateLocalManager.getFromCache(localTemplateId);
    }

    @Override
    public void checkLocalCart(CartVO cartVO  ,MemberAddress address ) {
        if (address == null) {
            return;
        }
        StringBuilder errorMsgBuilder = new StringBuilder();
        // 查询同城配送模板
        ShipLocalTemplateVO localTemplate = shipTemplateLocalManager.getFromCache(cartVO.getSkuList().get(0).getLocalTemplateId());
        // 计算收货地址和店铺地址距离
        double distanceMeter =( address.getShipLng()==null || cartVO.getShopLng()==null )?0:GeoUtils.getDistanceMeter(new GlobalCoordinates(address.getShipLat(), address.getShipLng()), new GlobalCoordinates(cartVO.getShopLat(), cartVO.getShopLng()));
        // 距离单位换算
        BigDecimal distanceKiloMeter = BigDecimal.valueOf(distanceMeter / 1000).setScale(2, RoundingMode.HALF_UP);

        //1.起送费 totalGoodsPrice<BaseShipPrice
        if(cartVO.getTotalGoodsPrice().compareTo(localTemplate.getBaseShipPrice().doubleValue())==-1){
            // if (totalGoodsPrice.compareTo(0.01) == -1) {
            errorMsgBuilder.append("订单商品金额低于订单起送费" + localTemplate.getBaseShipPrice() + "元;");
            cartVO.setAllowLocal(0);
        }
        //2.起送距离 计算距离  distanceKiloMeter >= shipRange
        if(distanceKiloMeter.compareTo(BigDecimal.valueOf(localTemplate.getShipRange()))>-1){
            // if (distanceKiloMeter.compareTo(BigDecimal.valueOf(5000)) > -1) {
            errorMsgBuilder.append("商品超出店铺同城可配送距离" + localTemplate.getShipRange() + "公里;");
            cartVO.setAllowLocal(0);
        }

        cartVO.setShipLocalTemplateVO(localTemplate);
        cartVO.setDistance(distanceKiloMeter);
        cartVO.setErrorMsg(errorMsgBuilder.toString());
    }
}
