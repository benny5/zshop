package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常类型
 */
public enum ExceptionTypeEnum {

    ALL_MISSED("未收到货"),
    GOODS_WRONG("商品错发"),
    PART_MISSED("商品少发"),
    GOODS_DAMAGE("商品破损"),
    BAD_QUALITY("质量不好"),
    BAD_TASTE("口味不好"),
    SHIP_PROBLEM("配送问题"),
    DELIVERY_DELAY("送达延迟"),
    SYSTEM_ERROR("系统异常"),
    NO_STOCK("采购缺货"),
    NEW_TASTE("新品试吃"),
    SHORT_WEIGHT("缺斤少两"),
    CUSTOMER_REQUIRED("客户要求"),
    RETURN_SHIP_PRICE("运费返还"),
    OUT_STORAGE_ERROR("出库异常"),
    OTHER("其他");

    private String description;

    ExceptionTypeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String value(){
        return this.name();
    }
}
