package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.core.goods.model.dos.TagsDO;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/4/14
 * @Description: 附近店铺详情页的商品列表
 */
@Data
public class NearbyShopCatGoodsVO {

    @ApiModelProperty(name = "one_cat_id", value = "一级分组ID")
    private Integer oneCatId;

    @ApiModelProperty(name = "one_cat_name", value = "一级分组名")
    private String oneCatName;

    @ApiModelProperty(name = "two_cat_id", value = "二级分组ID")
    private Integer twoCatId;

    @ApiModelProperty(name = "two_cat_name", value = "二级分组名")
    private String twoCatName;

    @ApiModelProperty(name = "small", value = "商品小图")
    private String small;

    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Integer goodsId;

    @ApiModelProperty(name = "goods_name", value = "商品名称")
    private String goodsName;

    @ApiModelProperty(name = "cart_sku_num", value = "店铺购物车加购sku总数")
    private Integer cartSkuNum;

    @ApiModelProperty(name = "cart_sku_list", value = "店铺购物车加购sku")
    private List<CartSkuVO> cartSkuList;

    @ApiModelProperty(name = "price", value = "商品价格")
    @Max(value = 99999999, message = "价格不能超过99999999")
    private Double price;

    @ApiModelProperty(name = "sku_list", value = "sku列表")
    List<GoodsSkuVO> skuList;

    @ApiModelProperty(name = "tag_list", value = "商品标签列表")
    List<TagsDO> tagList;
}
