package com.enation.app.javashop.core.member.model.dto;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;

@Data
public class NearLeaderDTO {

    @Id(name = "leader_id")
    @ApiModelProperty(name = "leader_id",value = "团长ID")
    private Integer leaderId;


    @Column(name="leader_name")
    @ApiModelProperty(name = "leader_name",value = "团长名")
    private String leaderName;

    @Column(name="facade_pic_url")
    @ApiModelProperty(name = "facade_pic_url",value = "门面图片")
    private String facadePicUrl;

    @Column(name="site_name")
    @ApiModelProperty(name = "site_name",value = "站点名")
    private String siteName;

    @Column(name = "leader_mobile")
    @ApiModelProperty(name = "leader_mobile",value = "团长手机")
    @NotEmpty(message = "团长手机不能为空")
    private String leaderMobile;

    @Column(name="fans_count")
    @ApiModelProperty(name = "fans_count",value = "粉丝数")
    private Long fansCount;


    @Column(name="distance")
    @ApiModelProperty(name="distance",value="距离")
    private Double distance;

    @Column(name="op_status")
    @ApiModelProperty(name = "op_status",value = "营业状态")
    private Integer opStatus;

    @ApiModelProperty(name="address",value = "全地址")
    private String address;

    @Column(name="province")
    @ApiModelProperty(hidden = true)
    private String province;

    @Column(name="city")
    @ApiModelProperty(hidden = true)
    private String city;

    @Column(name="county")
    @ApiModelProperty(hidden = true)
    private String county;

    @ApiModelProperty(name="recommend",value = "是否推荐团长")
    private Integer recommend;
    @JsonIgnore
    private Double lat;
    @JsonIgnore
    @ApiModelProperty(name = "lng", value = "经度")
    private Double lng;

}
