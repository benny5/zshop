package com.enation.app.javashop.core.promotion.shetuan.service.impl;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.base.message.ShetuanChangeMsg;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.client.member.ShopCatClient;
import com.enation.app.javashop.core.distribution.model.dos.DistributionGoods;
import com.enation.app.javashop.core.distribution.service.DistributionGoodsManager;
import com.enation.app.javashop.core.goods.model.dos.GoodsDO;
import com.enation.app.javashop.core.goods.model.dos.GoodsSkuDO;
import com.enation.app.javashop.core.goods.model.dto.GoodsQuantityRefreshDTO;
import com.enation.app.javashop.core.goods.model.enums.Permission;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goods.service.GoodsManager;
import com.enation.app.javashop.core.goods.service.GoodsQuantityManager;
import com.enation.app.javashop.core.goods.service.GoodsQueryManager;
import com.enation.app.javashop.core.goods.service.GoodsSkuManager;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.enums.ShetuanGoodsStatusEnum;
import com.enation.app.javashop.core.promotion.shetuan.model.enums.ShetuanStatusEnum;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanGoodsManager;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanGoodsOperateManager;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanbuyQuantityLogManager;
import com.enation.app.javashop.core.promotion.tool.service.PromotionGoodsManager;
import com.enation.app.javashop.core.promotion.tool.support.SkuNameUtil;
import com.enation.app.javashop.core.shop.model.dos.ShopCatDO;
import com.enation.app.javashop.core.shop.model.vo.ShopCatItem;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 拼团业务类
 *
 * @author admin
 * @version vv1.0.0
 * @since vv7.1.0
 * 2019-01-21 15:17:57
 */
@Service
public class ShetuanGoodsOperateManagerImpl implements ShetuanGoodsOperateManager {
	private static Logger logger = LoggerFactory.getLogger(ShetuanGoodsOperateManagerImpl.class);
	@Autowired
	@Qualifier("tradeDaoSupport")
	private DaoSupport tradeDaoSupport;

	@Autowired
	private GoodsQueryManager goodsQueryManager;

	@Autowired
	private GoodsSkuManager goodsSkuManager;

	@Autowired
	private AmqpTemplate amqpTemplate;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ShetuanGoodsManager shetuanGoodsManager;

	@Autowired
	private GoodsManager goodsManager;

	@Autowired
	private GoodsQuantityManager goodsQuantityManager;

	@Autowired
	private DistributionGoodsManager distributionGoodsManager;

	/**
	 * 社区团购活动，新增商品
	 *
	 * @param shetuanGoodsDO
	 */
	@Override
	public ShetuanGoodsDO composeAddStGoods(GoodsSkuVO skuVO, ShetuanGoodsDO shetuanGoodsDO, GoodsDO goodsDO, Seller seller) {

		checkAddData(shetuanGoodsDO);

		if (skuVO.getIsGlobal() == null) {
			throw new ServiceException("1000", skuVO.getGoodsName() + ",不支持快递到家");
		}
		if (goodsDO == null) {
			goodsDO = this.goodsQueryManager.getModel(skuVO.getGoodsId());
		}

		DistributionGoods distributionGoodVo = distributionGoodsManager.getModel(skuVO.getGoodsId());
		if (distributionGoodVo != null) {
			shetuanGoodsDO.setFirstRate(distributionGoodVo.getGrade1Rebate());
			shetuanGoodsDO.setSecondRate(distributionGoodVo.getGrade2Rebate());
			shetuanGoodsDO.setSelfRaisingRate(distributionGoodVo.getInviterRate());
		}

		shetuanGoodsDO.setSellerId(seller.getSellerId());

		shetuanGoodsDO.setSellerName(seller.getSellerName());

		shetuanGoodsDO.setGoodsNum(skuVO.getEnableQuantity());

		shetuanGoodsDO.setGoodsId(skuVO.getGoodsId());
		shetuanGoodsDO.setSn(skuVO.getSn());
		shetuanGoodsDO.setThumbnail(skuVO.getThumbnail());
		shetuanGoodsDO.setGoodsName(goodsDO.getGoodsName() + " " + SkuNameUtil.createSkuName(skuVO.getSpecs()));
		shetuanGoodsDO.setVideoUrl(goodsDO.getVideoUrl());
		shetuanGoodsDO.setShopCatId(goodsDO.getShopCatId());

		shetuanGoodsDO.setOriginPrice(goodsDO.getMktprice());
		shetuanGoodsDO.setSalesPrice(skuVO.getPrice());
		shetuanGoodsDO.setCost(skuVO.getCost());

		shetuanGoodsDO.setShetuanPrice(skuVO.getPrice());

		shetuanGoodsDO.setShopCatItems(goodsDO.getShopCatItems());

		// 默认状态是已下架
		shetuanGoodsDO.setStatus(
				shetuanGoodsDO.getStatus() == null ? ShetuanGoodsStatusEnum.OFF_LINE.getIndex() : shetuanGoodsDO.getStatus());
		shetuanGoodsDO.setPriority(50);

		// 新增社区团购商品
		shetuanGoodsDO.setCreateTime(DateUtil.getDateline());
		return shetuanGoodsDO;

	}

	private void checkAddData(ShetuanGoodsDO shetuanGoodsDO) {

		/**
		 * 查询出此商品是否已在,已经存在则更新
		 */
		String sqlExist = "select * from es_shetuan_goods  where sku_id = ?  and shetuan_id = ? and status != "
				+ ShetuanGoodsStatusEnum.DELETE.getIndex();

		ShetuanGoodsDO existDO = this.tradeDaoSupport.queryForObject(sqlExist, ShetuanGoodsDO.class, shetuanGoodsDO.getSkuId(),
				shetuanGoodsDO.getShetuanId());

		if (existDO != null) {
			throw new ServiceException("10000", existDO.getGoodsName() + "社区团购商品已存在，不能重复添加");
		}

	}

	@Override
	public void updateStGoods(ShetuanGoodsVO shetuanGoodsVO) {
		ShetuanGoodsDO shetuanGoodsDO = new ShetuanGoodsDO();
		BeanUtil.copyProperties(shetuanGoodsVO, shetuanGoodsDO);
		// 单个商品保存更新库存
		ShetuanGoodsVO shetuanGoodsDetail = this.shetuanGoodsManager.getShetuanGoodsDetail(shetuanGoodsDO.getId());
		if (shetuanGoodsDetail.getShetuanStatus().equals(ShetuanStatusEnum.ON_LINE.getIndex())
				&& !shetuanGoodsDetail.getGoodsNum().equals(shetuanGoodsVO.getGoodsNum())) {
			// 更新商品库存
			// this.syncGoodsNumToGoodsQuantity(Arrays.asList(shetuanGoodsDO));
			shetuanGoodsDO.setGoodsNum(shetuanGoodsVO.getGoodsNum());
		}
		if (CollectionUtils.isEmpty(shetuanGoodsVO.getShopCatItemsList())) {
			shetuanGoodsDO.setShopCatItems(null);
		} else {
			shetuanGoodsDO.setShopCatItems(JSON.toJSON(shetuanGoodsVO.getShopCatItemsList()).toString());
		}
		// 校验社区团购商品参数
		checkData(shetuanGoodsDO);

		this.tradeDaoSupport.update(shetuanGoodsDO, shetuanGoodsDO.getId());

		// 更新索引
		ShetuanChangeMsg shetuanChangeMsg = new ShetuanChangeMsg();
		shetuanChangeMsg.setShetuanId(shetuanGoodsDO.getShetuanId());
		shetuanChangeMsg.setSkuIds(Arrays.asList(shetuanGoodsDO.getSkuId()));
		this.amqpTemplate.convertAndSend(AmqpExchange.SHETUAN_CHANGE, AmqpExchange.SHETUAN_CHANGE + "_ROUTING", shetuanChangeMsg);
	}

	@Override
	@Transactional
	public boolean batchUpdateStGoods(List<ShetuanGoodsDO> goodsList) {

		// 要形成的指更新sql
		List<String> sqlList = new ArrayList<String>();
		List<Integer> shetuanGoodsIds = new ArrayList<Integer>();
		for (ShetuanGoodsDO shetuanGoodsDO : goodsList) {
			checkData(shetuanGoodsDO);

			StringBuilder sql = new StringBuilder("update es_shetuan_goods set ");
			if (shetuanGoodsDO.getShetuanPrice() != null) {
				sql.append(" shetuan_price=").append(shetuanGoodsDO.getShetuanPrice()).append(",");
			}
			if (shetuanGoodsDO.getVisualNum() != null) {
				sql.append(" visual_num=").append(shetuanGoodsDO.getVisualNum()).append(",");
			}
			if (shetuanGoodsDO.getGoodsNum() != null) {
				sql.append(" goods_num=").append(shetuanGoodsDO.getGoodsNum()).append(",");
			}
			if (shetuanGoodsDO.getLimitNum() != null) {
				sql.append(" limit_num=").append(shetuanGoodsDO.getLimitNum()).append(",");
			}
			if (shetuanGoodsDO.getFirstRate() != null) {
				sql.append(" first_rate=").append(shetuanGoodsDO.getFirstRate()).append(",");
			}
			if (shetuanGoodsDO.getSecondRate() != null) {
				sql.append(" second_rate=").append(shetuanGoodsDO.getSecondRate()).append(",");
			}
			if (shetuanGoodsDO.getThirdRate() != null) {
				sql.append(" third_rate=").append(shetuanGoodsDO.getThirdRate()).append(",");
			}
			if (shetuanGoodsDO.getInviteRate() != null) {
				sql.append(" invite_rate=").append(shetuanGoodsDO.getInviteRate()).append(",");
			}
			if (shetuanGoodsDO.getSelfRaisingRate() != null) {
				sql.append(" self_raising_rate=").append(shetuanGoodsDO.getSelfRaisingRate()).append(",");
			}
			if (shetuanGoodsDO.getSubsidyRate() != null) {
				sql.append(" subsidy_rate=").append(shetuanGoodsDO.getSubsidyRate()).append(",");
			}
			if (shetuanGoodsDO.getPriority() != null) {
				sql.append(" priority=").append(shetuanGoodsDO.getPriority()).append(",");
			}
			if (shetuanGoodsDO.getCost() != null) {
				sql.append(" cost=").append(shetuanGoodsDO.getCost()).append(",");
			}
			String sqlStr = sql.toString();
			if (sqlStr.endsWith(",")) {
				sqlStr = sqlStr.substring(0, sqlStr.length() - 1);
			}
			sqlStr = sqlStr + " where sku_id=" + shetuanGoodsDO.getSkuId() + " and shetuan_id=" + shetuanGoodsDO.getShetuanId()
					+ " and status != " + ShetuanGoodsStatusEnum.DELETE.getIndex();
			sqlList.add(sqlStr);
			shetuanGoodsIds.add(shetuanGoodsDO.getSkuId());
		}
		// 更新mysql
		this.tradeDaoSupport.batchUpdate(sqlList.toArray(new String[] {}));
		// 更新索引
		this.amqpTemplate.convertAndSend(AmqpExchange.SHETUAN_CHANGE, AmqpExchange.SHETUAN_CHANGE + "_ROUTING",
				new ShetuanChangeMsg(goodsList.get(0).getShetuanId(), shetuanGoodsIds));
		return true;
	}

	private void checkData(ShetuanGoodsDO shetuanGoodsDO) {
		// 防止虚拟团购数量超过商品总数
		if (shetuanGoodsDO.getVisualNum() != null && shetuanGoodsDO.getVisualNum() > shetuanGoodsDO.getGoodsNum()) {
			shetuanGoodsDO.setVisualNum(shetuanGoodsDO.getGoodsNum() / 2);
		}
		if (shetuanGoodsDO.getGoodsNum() < 1) {
			throw new ServiceException("500", "参团数量不能小于1");
		}

		Assert.notNull(shetuanGoodsDO.getShopCatId(), "必须设置店铺分组");
		Assert.notNull(shetuanGoodsDO.getShetuanPrice(), "必须设置社区团购价");
		Assert.notNull(shetuanGoodsDO.getFirstRate(), "必须设置团长分佣比例");
	}

	@Override
	public void batchAddShetuanGoods(List<ShetuanGoodsDO> shetuanGoodsDOList) {
		String sql = "INSERT INTO `es_shetuan_goods`("
				+ "`shetuan_id`, `sku_id`,  `goods_id`,`seller_id`, `seller_name`, `goods_name`, `settle_price`, `origin_price`, `sales_price`, `shetuan_price`,`sn`, `specs`,`thumbnail`, `video_url`, "
				+ " `status`,`first_rate`, `second_rate`, `third_rate`, `invite_rate`, `self_raising_rate`,`subsidy_rate`,`shop_cat_id`,  `goods_num`, `visual_num`,  `limit_num`,  `shop_cat_name`,"
				+ "`create_time`,  `priority`,`cost`, `shop_cat_items`) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ShetuanGoodsDO shetuanGoodsDO = shetuanGoodsDOList.get(i);
				ps.setInt(1, shetuanGoodsDO.getShetuanId());
				ps.setInt(2, shetuanGoodsDO.getSkuId());
				ps.setInt(3, shetuanGoodsDO.getGoodsId());
				ps.setInt(4, shetuanGoodsDO.getSellerId());
				ps.setString(5, shetuanGoodsDO.getSellerName());
				ps.setString(6, shetuanGoodsDO.getGoodsName());
				ps.setDouble(7, shetuanGoodsDO.getSettlePrice() == null ? 0.00 : shetuanGoodsDO.getSettlePrice());
				ps.setDouble(8, shetuanGoodsDO.getOriginPrice() == null ? 0.00 : shetuanGoodsDO.getOriginPrice());
				ps.setDouble(9, shetuanGoodsDO.getSalesPrice() == null ? 0.00 : shetuanGoodsDO.getSalesPrice());
				ps.setDouble(10, shetuanGoodsDO.getShetuanPrice() == null ? 0.00 : shetuanGoodsDO.getShetuanPrice());
				ps.setString(11, shetuanGoodsDO.getSn());
				ps.setString(12, shetuanGoodsDO.getSpecs());
				ps.setString(13, shetuanGoodsDO.getThumbnail());
				ps.setString(14, shetuanGoodsDO.getVideoUrl());
				ps.setInt(15, shetuanGoodsDO.getStatus());
				ps.setDouble(16, shetuanGoodsDO.getFirstRate() == null ? 8.00 : shetuanGoodsDO.getFirstRate());
				ps.setDouble(17, shetuanGoodsDO.getSecondRate() == null ? 2.00 : shetuanGoodsDO.getSecondRate());
				ps.setDouble(18, shetuanGoodsDO.getThirdRate() == null ? 0.00 : shetuanGoodsDO.getThirdRate());
				ps.setDouble(19, shetuanGoodsDO.getInviteRate() == null ? 0.00 : shetuanGoodsDO.getInviteRate());
				ps.setDouble(20, shetuanGoodsDO.getSelfRaisingRate() == null ? 2.00 : shetuanGoodsDO.getSelfRaisingRate());
				ps.setDouble(21, shetuanGoodsDO.getSubsidyRate() == null ? 0.00 : shetuanGoodsDO.getSubsidyRate());
				ps.setInt(22, shetuanGoodsDO.getShopCatId() == null ? 0 : shetuanGoodsDO.getShopCatId());
				ps.setInt(23, shetuanGoodsDO.getGoodsNum() == null ? 0 : shetuanGoodsDO.getGoodsNum());
				ps.setInt(24, shetuanGoodsDO.getVisualNum() == null ? 0 : shetuanGoodsDO.getVisualNum());
				ps.setInt(25, shetuanGoodsDO.getLimitNum() == null ? 0 : shetuanGoodsDO.getLimitNum());
				ps.setString(26, shetuanGoodsDO.getShopCatName());
				ps.setLong(27, shetuanGoodsDO.getCreateTime());
				ps.setInt(28, shetuanGoodsDO.getPriority());
				ps.setDouble(29, shetuanGoodsDO.getCost() == null ? 0.00 : shetuanGoodsDO.getCost());
				ps.setString(30, shetuanGoodsDO.getShopCatItems());
			}

			@Override
			public int getBatchSize() {
				return shetuanGoodsDOList.size();
			}
		});
	}

	@Override
	public void updateGoodsCat(Integer goodsId, List<ShopCatItem> shopCatList) {
		this.tradeDaoSupport.execute("update es_shetuan_goods set shop_cat_items= ?  where goods_id = ? ",
				JSON.toJSONString(shopCatList), goodsId);
	}

	/**
	 * 社区团购商品 -->>>> 上架 下架  删除
	 * @param shetuanGoodsId
	 * @param newStatus
	 */
	@Override
	public void operateStGoodsStatus(Integer shetuanGoodsId, int newStatus) {
		ShetuanGoodsVO shetuanGoodsVO = this.shetuanGoodsManager.getShetuanGoodsDetail(shetuanGoodsId);
		GoodsDO goodsDO = this.goodsQueryManager.getModel(shetuanGoodsVO.getGoodsId());

		// 商品上架条件
		if (newStatus == ShetuanGoodsStatusEnum.ON_LINE.getIndex()) {
			GoodsSkuDO skuDO = goodsSkuManager.getModel(shetuanGoodsVO.getSkuId());
			if (skuDO == null) {
				throw new ServiceException("10001", skuDO.getGoodsName() + "sku已经被删除，请重新上架修改后sku");
			}
			if (shetuanGoodsVO.getGoodsNum() < 1) {
				throw new ServiceException("10002", skuDO.getGoodsName() + "商品参团数量为0");
			}
			if (goodsDO.getDisabled() == 0) {
				throw new ServiceException("10003", skuDO.getGoodsName() + "商品已删除,请至回收站查找!");
			}
		}

		// 联动商城商品上架或者下架
		updateGoodsUpOrDown(newStatus, shetuanGoodsVO, goodsDO);

		// 更新mysql
		this.tradeDaoSupport.execute("update es_shetuan_goods set status = ?,update_time=? , audit_name=? where id = ? ",
				newStatus, DateUtil.getDateline(), UserContext.getSeller().getSellerName(), shetuanGoodsId);

		// 更新ES
		this.amqpTemplate.convertAndSend(AmqpExchange.SHETUAN_CHANGE, AmqpExchange.SHETUAN_CHANGE + "_ROUTING",
				new ShetuanChangeMsg(shetuanGoodsVO.getShetuanId(), Arrays.asList(shetuanGoodsVO.getSkuId())));
	}

	private void updateGoodsUpOrDown(int newStatus, ShetuanGoodsVO shetuanGoodsVO, GoodsDO goodsDO) {
		// 联动只针对当日上线社区团购活动
		if (!shetuanGoodsVO.getShetuanStatus().equals(ShetuanStatusEnum.ON_LINE.getIndex())) {
			return;
		}

		// 上架商城商品
		if (shetuanGoodsVO.getShetuanStatus().equals(ShetuanStatusEnum.ON_LINE.getIndex()) && goodsDO.getMarketEnable() == 0) {
			goodsManager.up(goodsDO.getGoodsId());
		}

		// 下架商城商品（下架商品和删除商品都要进行商城下架）
		if ((newStatus == ShetuanGoodsStatusEnum.OFF_LINE.getIndex() || newStatus == ShetuanGoodsStatusEnum.DELETE.getIndex())
				&& goodsDO.getMarketEnable() == 1) {
			goodsManager.under(new Integer[] { goodsDO.getGoodsId() }, "社区团购下架", Permission.SELLER);
		}
	}

	/**
	 * 库存只同步 待上线和已上架的商品;
	 * 这个方法是为了把团购限购数量+已售数量更新为商品的库存
	 * @param shetuanGoodsDOS
	 */
	@Override
	public void syncGoodsNumToGoodsQuantity(List<ShetuanGoodsDO> shetuanGoodsDOS) {
		// 查询待发货商品数量
		String sql = "SELECT oi.product_id sku_id,oi.goods_id goods_id,oi.name,SUM(oi.num) ship_num FROM es_order_items oi "
				+ "left join es_order oo ON oi.order_sn=oo.sn "
				+ "WHERE oo.seller_id=?  and (oo.order_status='PAID_OFF' or oo.order_status='FORMED')  GROUP BY product_id ";
		List<GoodsQuantityRefreshDTO> shipGoodsNum = this.tradeDaoSupport.queryForList(sql, GoodsQuantityRefreshDTO.class,
				shetuanGoodsDOS.get(0).getSellerId());
		Map<Integer, GoodsQuantityRefreshDTO> shipGoodsIndex = shipGoodsNum.stream()
				.collect(Collectors.toMap(GoodsQuantityRefreshDTO::getSkuId, Function.identity()));
		List<GoodsQuantityRefreshDTO> needRefreshSkuList = new ArrayList<>();
		for (ShetuanGoodsDO shetuanGoodsDO : shetuanGoodsDOS) {
			GoodsQuantityRefreshDTO goodsQuantityRefreshDTO = shipGoodsIndex.get(shetuanGoodsDO.getSkuId());
			if (goodsQuantityRefreshDTO == null) {
				goodsQuantityRefreshDTO = new GoodsQuantityRefreshDTO();
				goodsQuantityRefreshDTO.setGoodsId(shetuanGoodsDO.getGoodsId());
				goodsQuantityRefreshDTO.setSkuId(shetuanGoodsDO.getSkuId());
				goodsQuantityRefreshDTO.setShipNum(0);
			}
			goodsQuantityRefreshDTO.setGoodsName(shetuanGoodsDO.getGoodsName());
			goodsQuantityRefreshDTO.setEnableQuantity(shetuanGoodsDO.getGoodsNum());
			goodsQuantityRefreshDTO
					.setQuantity(goodsQuantityRefreshDTO.getEnableQuantity() + goodsQuantityRefreshDTO.getShipNum());

			needRefreshSkuList.add(goodsQuantityRefreshDTO);
		}
		// 执行库存数据更新
		goodsQuantityManager.batchUpdateQuantity(needRefreshSkuList);

	}

	/**
	 * 把商品的可用库存更新为社团商品的活动商品总数
	 * @param shetuanId 社团活动的ID
	 */
	@Override
	public void syncGoodsQuantityToGoodsNum(Integer shetuanId) {

		// 1、查询所有团购商品
		List<ShetuanGoodsDO> shetuanGoodsDOS = this.shetuanGoodsManager.getByShetuanAndStatus(shetuanId,
				new Integer[] { ShetuanGoodsStatusEnum.ON_LINE.getIndex(), ShetuanStatusEnum.NEW.getIndex() });

		// 2、查询所有社团商品的库存
		List<Integer> shetuanGoodsIdList = shetuanGoodsDOS.stream().map(ShetuanGoodsDO::getGoodsId).collect(Collectors.toList());
		String sql = "SELECT goods_id,enable_quantity FROM es_goods WHERE goods_id IN ("
				+ StringUtil.listToString(shetuanGoodsIdList, ",") + ")";
		List<GoodsQuantityRefreshDTO> goodsQuantityList = this.tradeDaoSupport.queryForList(sql, GoodsQuantityRefreshDTO.class);
		Map<Integer, Integer> shipGoodsIndex = goodsQuantityList.stream()
				.collect(Collectors.toMap(GoodsQuantityRefreshDTO::getGoodsId, GoodsQuantityRefreshDTO::getEnableQuantity));

		// 3、执行库存数据更新，把商品的可用库存更新为社团商品的活动商品总数
		List<Object[]> shetuanGoodsBatchArgs = new ArrayList<>();
		for (ShetuanGoodsDO shetuanGoodsDO : shetuanGoodsDOS) {
			Integer goodsEnableQuantity = shipGoodsIndex.get(shetuanGoodsDO.getGoodsId());
			if (goodsEnableQuantity == null) {
				goodsEnableQuantity = 0;
			}

			shetuanGoodsBatchArgs.add(new Object[] { goodsEnableQuantity, shetuanGoodsDO.getId() });
		}

		this.jdbcTemplate.batchUpdate("UPDATE es_shetuan_goods SET goods_num=?  WHERE id = ? ", shetuanGoodsBatchArgs);
	}
}
