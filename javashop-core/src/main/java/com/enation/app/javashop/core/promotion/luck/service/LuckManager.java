package com.enation.app.javashop.core.promotion.luck.service;

import com.enation.app.javashop.core.promotion.luck.model.dos.LuckDO;
import com.enation.app.javashop.core.promotion.luck.model.vo.QueryLuckVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.Map;

/**
 * @author 孙建
 */
public interface LuckManager {

    Page<LuckDO> queryLuckList(QueryLuckVO queryLuckVO);

    LuckDO getByLuckId(Integer luckId);

    void onOffLine(Integer luckId, Integer luckStatus);

    Map queryLuckIndex(Integer memberId);

    LuckDO queryLatestLuck();

    Map startLuck(Integer luckId, Integer memberId);

    Map confirmLuck(Integer luckId, String exchangeId, Integer memberId);

    void addOrUpdateLuck(LuckDO luckDO);

    void deleteLuck(Integer luckId);

    // 根据单号判断是否显示抽奖按钮
    boolean checkButtonStatus(String orderSn);

    void expireLuck();

}
