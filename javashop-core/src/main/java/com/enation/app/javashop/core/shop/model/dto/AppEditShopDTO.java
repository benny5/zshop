package com.enation.app.javashop.core.shop.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFeng
 * @date 2020/8/20 11:20
 */
@Data
public class AppEditShopDTO {

    /**联系人姓名*/
    @ApiModelProperty(name="link_name",value="联系人姓名",required=false)
    private String linkName;

    /**联系人电话*/
    @ApiModelProperty(name="link_phone",value="联系人电话",required=false)
    private String linkPhone;

    /**法人身份证照片*/
    @ApiModelProperty(name="legal_img",value="法人身份证照片",required=false)
    private String legalImg;

    @ApiModelProperty(name="shop_desc",value="店铺简介",required=false)
    private String shopDesc;

    @ApiModelProperty(name="shop_logo",value="店铺logo",required=false)
    private String shopLogo;

    @ApiModelProperty(name="shop_name",value="店铺名称",required=false)
    private String shopName;

    @ApiModelProperty(name="business_status",value="店铺营业状态 1营业 0不营业",required=false)
    private Integer businessStatus;

    @ApiModelProperty(name="open_time_type",value="门店营业时间类型 1.全天 2.多时间段",required=false)
    private Integer openTimeType;

    @ApiModelProperty(name="open_time",value="店铺营业时间json",required=false)
    private String openTime;
}
