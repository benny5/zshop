package com.enation.app.javashop.core.shop.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author 王志杨
 * @Date 2020/8/26 14:20
 * @Descroption 店铺服务范围实体
 */
@Table(name="es_shop_gis")
@ApiModel
@Data
public class ShopGisDO implements Serializable {


    /**店铺服务范围id*/
    @Id(name = "gis_id")
    @ApiModelProperty(hidden=true)
    private Integer gisId;

    /**店铺Id*/
    @Column(name = "shop_id")
    @ApiModelProperty(name="shop_id",value="店铺id",required=false)
    private Integer shopId;

    /**多边形Id*/
    @Column(name = "polygon_id")
    @ApiModelProperty(name="polygon_id",value="多边形Id",required=false)
    private Integer polygonId;

    /**店铺服务范围多边形各节点坐标组成的json串*/
    @Column(name = "polygon_json")
    @ApiModelProperty(name="polygon_json",value="店铺服务范围各节点坐标组成的字符串",required=false)
    private String polygonJson;

//    /**店铺服务范围多多边形各节点坐标*/
//    @Column(name = "multi_polygon")
//    @ApiModelProperty(name="multi_polygon",value="店铺服务范围多多边形各节点坐标",required=false)
//    private Object multiPolygon;

    /**店铺服务范围多边形各节点坐标*/
    @Column(name = "polygon_coords")
    @ApiModelProperty(name="polygon_coords",value="店铺服务范围单个多边形各节点坐标",required=false)
    private Object polygonCoords;

    /**店铺服务范围多边形中点*/
    @Column(name = "center_point_str")
    @ApiModelProperty(name="center_point_str",value="店铺服务范围多边形中点",required=false)
    private String centerPointStr;

    /**外边框线的颜色*/
    @Column(name = "line_color")
    @ApiModelProperty(name="line_color",value="外边框线的颜色",required=false)
    private String lineColor;

    /**外边框线的透明度*/
    @Column(name = "line_opacity")
    @ApiModelProperty(name="line_opacity",value="外边框线的透明度",required=false)
    private Double lineOpacity;

    /**外边框线的宽度*/
    @Column(name = "line_weight")
    @ApiModelProperty(name="line_weight",value="外边框线的宽度",required=false)
    private Integer lineWeight;

    /**多边形填充色*/
    @Column(name = "fill_color")
    @ApiModelProperty(name="fill_color",value="多边形填充色",required=false)
    private String fillColor;

    /**多边形填充透明度*/
    @Column(name = "fill_opacity")
    @ApiModelProperty(name="fill_opacity",value="多边形填充透明度",required=false)
    private Double fillOpacity;

    /**多边形绘制人*/
    @Column(name = "create_name")
    @ApiModelProperty(name="create_name",value="多边形绘制人",required=false)
    private String createName;

    /**多边形绘制时间*/
    @Column(name = "create_time")
    @ApiModelProperty(name="create_time",value="多边形绘制时间",required=false)
    private Long createTime;

    /**最后一次修改人*/
    @Column(name = "update_name")
    @ApiModelProperty(name="update_name",value="最后一次修改人",required=false)
    private String updateName;

    /**多边形修改时间*/
    @Column(name = "update_time")
    @ApiModelProperty(name="update_time",value="多边形修改时间",required=false)
    private Long updateTime;

}
