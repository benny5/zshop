package com.enation.app.javashop.core.aftersale.model.vo;

import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.core.trade.order.model.vo.OrderDetailVO;
import com.enation.app.javashop.core.trade.sdk.model.OrderSkuDTO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author 王志杨
 * @since  2020/9/22 11:45
 * 异常处理展示组合类
 */
@ApiModel("异常处理展示组合类")
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionStructVO implements Serializable {

    /*异常单数据*/
    private ExceptionOrderVO exceptionOrder;
    /*订单数据*/
    private OrderDetailVO orderDetail;
    /*购买商品数据*/
    private List<OrderSkuDTO> orderSkuDTOList;
    /*可退金额*/
    private Double allowRefundPrice;
    //优惠券列表
    private List<CouponDO> couponDOList;
}
