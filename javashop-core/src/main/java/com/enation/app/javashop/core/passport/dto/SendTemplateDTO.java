package com.enation.app.javashop.core.passport.dto;

import lombok.Data;

/**
 * @author ChienSun
 * @date 2019/2/19
 */
@Data
public class SendTemplateDTO {

    // 	接收者openid
    private String touser;

    // 公众号模板消息相关的信息
    private MpTemplateMsgDTO mp_template_msg;

}
