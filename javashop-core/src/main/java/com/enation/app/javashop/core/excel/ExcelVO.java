package com.enation.app.javashop.core.excel;

import lombok.Data;

/**
 * @author JFeng
 * @date 2019/10/26 17:18
 */

@Data
public class ExcelVO {
    private String fileName;

    private Integer count;

    private String fileUrl;


}
