package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class GroupPurchaseVO {

    @ApiModelProperty(name = "page_no", value = "页码", required = false)
    private Integer pageNo;

    @ApiModelProperty(name = "page_size", value = "分页数", required = false)
    private Integer pageSize;

    @ApiModelProperty(name = "buyer_name", value = "收货人姓名")
    private String buyerName;

    @ApiModelProperty(name = "buyer_mobile", value = "收货人手机号")
    private String buyerMobile;

    @ApiModelProperty(name = "order_sn", value = "订单编号")
    private String orderSn;

    @ApiModelProperty(value = "订单状态",
            example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                    "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论")
    private String orderStatus;

}
