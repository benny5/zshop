package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.aftersale.model.dto.ClaimSkusDTO;
import com.enation.app.javashop.core.trade.order.model.enums.ServiceStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.core.trade.sdk.model.OrderSkuDTO;
import com.enation.app.javashop.core.wms.model.dos.WmsOrderItemsDO;
import com.enation.app.javashop.core.wms.model.enums.WmsOrderStatusEnums;
import com.enation.app.javashop.core.wms.model.enums.WmsOrderTypeEnums;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.enation.app.javashop.framework.util.JsonUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author JFeng
 * @date 2020/9/21 17:42
 */
@Data
public class WmsOrderVO {
    /** 主键 */
    @ApiModelProperty(hidden = true)
    private Integer deliveryId;

    @ApiModelProperty(name = "delivery_sn", value = "出库编号", required = false)
    private String deliverySn;

    /**
     * {@link WmsOrderStatusEnums}
     */
    @ApiModelProperty(name = "delivery_status", value = "出库状态", required = false)
    private String deliveryStatus;

    @ApiModelProperty(name = "delivery_status_text", value = "出库状态说明", required = false)
    private String deliveryStatusText;

    @ApiModelProperty(name = "claims_id", value = "理赔单id", required = false)
    private Integer claimsId;

    @ApiModelProperty(name = "order_id", value = "订单id", required = false)
    private Integer orderId;

    @ApiModelProperty(name = "order_sn", value = "订单编号", required = false)
    private String orderSn;

    @ApiModelProperty(name = "ship_name", value = "收货人", required = false)
    private String shipName;

    @ApiModelProperty(name = "ship_mobile", value = "收货人手机号", required = false)
    private String shipMobile;

    @ApiModelProperty(name = "ship_addr", value = "收货地址", required = false)
    private String shipAddr;

    @ApiModelProperty(name = "site_addr", value = "站点地址", required = false)
    private String siteAddr;

    @ApiModelProperty(name = "site_name", value = "站点联系人", required = false)
    private String siteName;

    @ApiModelProperty(name = "site_short_name", value = "站点联系人", required = false)
    private String siteShortName;

    @ApiModelProperty(name = "site_mobile", value = "站点手机号", required = false)
    private String siteMobile;

    @ApiModelProperty(name = "seller_id", value = "店铺id", required = false)
    private Integer sellerId;

    @ApiModelProperty(name = "site_id", value = "站点id", required = false)
    private Integer siteId;

    /**
     * {@link  ShipTypeEnum}
     */
    @ApiModelProperty(name = "shipping_type", value = "配送类型", required = false)
    private String shippingType;

    /**
     * {@link  WmsOrderTypeEnums}
     */
    @ApiModelProperty(name = "wms_order_type", value = "上游来源类型 NORMAL 商城 SHETUAN 社团 EXCEPTION 异常单", required = false)
    private String wmsOrderType;

    @ApiModelProperty(name = "order_type", value = "订单类型", required = false)
    private String orderType;

    @ApiModelProperty(name = "items_json", value = "货物列表json", required = false)
    private String itemsJson;

    @ApiModelProperty(name = "delivery_date", value = "出库日期", required = false)
    private String deliveryDate;

    @ApiModelProperty(name = "delivery_time", value = "实际出库时间", required = false)
    private String deliveryTime;

    @ApiModelProperty(name = "warehouse_code", value = "仓库分拣单号", required = false)
    private String warehouseCode;

    @ApiModelProperty(name = "print_times", value = "打印次数", required = false)
    private Integer printTimes;

    @ApiModelProperty(name = "remarks", value = "出库备注", required = false)
    private String remarks;

    @ApiModelProperty(name = "order_flag",value = "订单类型" , required = false)
    private String orderFlag;

    private Long receiveTime;

    private Integer receiveTimeType;

    private Integer sortingSeq;

    private Integer sortingBatch;

    private String operatorName;

    private Long orderCreateTime;

    private Double shippingPrice;

    private Double orderPrice;

    private Integer orderNum;

    private Integer orderSeq;

    private String sortingSeqList;

    private String claimsSn;


    private List<OrderSkuVO>  skuList;

    private List<WmsOrderItemsDO>  itemsList;

    public void transverse(){
        List<OrderSkuVO> skuList = new ArrayList<>();
        // 正常订单
        if(this.wmsOrderType.equals(WmsOrderTypeEnums.ORDER.value())){
            List<OrderSkuVO>  skuListOrigin =JSON.parseArray(this.itemsJson, OrderSkuVO.class);
            for (OrderSkuVO orderSkuVO : skuListOrigin) {
                if(orderSkuVO.getServiceStatus()!=null && orderSkuVO.getServiceStatus().equals(ServiceStatusEnum.APPLY.value())){
                    orderPrice=orderPrice-orderSkuVO.getActualPayTotal();
                }else{
                    skuList.add(orderSkuVO);
                }
            }
        }
        // 异常订单
        if(this.wmsOrderType.equals(WmsOrderTypeEnums.CLAIMS.value())){
            List<ClaimSkusDTO> claimSkus = JSON.parseArray(this.itemsJson, ClaimSkusDTO.class);
            if(!CollectionUtils.isEmpty(claimSkus)){
                this.orderPrice=0.0;
                for (ClaimSkusDTO skus : claimSkus) {
                    OrderSkuVO orderSkuVO = new OrderSkuVO();
                    BeanUtil.copyProperties(skus,orderSkuVO);
                    orderSkuVO.setName(skus.getGoodsName());
                    orderSkuVO.setNum(skus.getCompensationNum());
                    orderSkuVO.setGoodsImage(skus.getThumbnail());
                    double price = skus.getPrice() == null ? 0.0 : skus.getPrice();
                    orderSkuVO.setPurchasePrice(price);
                    orderPrice+=price;
                    skuList.add(orderSkuVO);
                }
            }
        }
       this.skuList=skuList;
       this.wmsOrderType=WmsOrderTypeEnums.valueOf(this.wmsOrderType).description();
       this.shippingType=ShipTypeEnum.valueOf(this.shippingType).getDesc();
       // 出库状态
       this.deliveryStatusText=WmsOrderStatusEnums.getTextByIndex(this.deliveryStatus);
    }
}
