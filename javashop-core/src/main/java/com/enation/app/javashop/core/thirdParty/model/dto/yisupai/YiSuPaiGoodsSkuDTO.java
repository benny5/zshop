package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/5/14
 * @Description: 商品SKU实体类
 */
@Data
public class YiSuPaiGoodsSkuDTO {
    private String skuId;
    private String skuPicUrl;
    private String skuTitle;
    // 销售价格（分）
    private Integer priceCent;
    // 是否可售卖
    private Boolean canSell;
    // 规格名和规格值
    private String skuPropertiesJson;
}
