package com.enation.app.javashop.core.shop.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/3/10
 * @Description: 附近店铺列表数据
 */
@Data
public class NearbyShopDTO {

    @ApiModelProperty(value = "商品二级分类名")
    private String twoCategoryName;

    @ApiModelProperty(value = "商品三级分类名")
    private String threeCategoryName;

    @ApiModelProperty(value = "商品二级分类ID")
    private Integer threeParentId;

    @ApiModelProperty(value = "活动ID")
    private Integer activityId;

    @ApiModelProperty(value = "拼团活动拼成人数，如果不是拼团活动则默认是0")
    private Integer requiredNum;

    @ApiModelProperty(value = "活动类型")
    private String promotionType;

    @ApiModelProperty(value = "活动名称")
    private String title;

    @ApiModelProperty(value = "skuID")
    private Integer skuId;

    @ApiModelProperty(value = "商品ID")
    private Integer goodsId;

    @ApiModelProperty(value = "商品名")
    private String goodsName;

    @ApiModelProperty(value = "商品售价")
    private Double price;

    @ApiModelProperty(value = "商品划线价")
    private Double mktprice;

    @ApiModelProperty(value = "商品头图")
    private String thumbnail;

    @ApiModelProperty(value = "店铺ID")
    private Integer sellerId;

    @ApiModelProperty(value = "店铺名")
    private String sellerName;

    @ApiModelProperty(value = "店铺logo")
    private String shopLogo;

    @ApiModelProperty(value = "店铺服务态度分数")
    private Double shopServiceCredit;

    @ApiModelProperty(value = "店铺经度")
    private Double shopLat;

    @ApiModelProperty(value = "店铺纬度")
    private Double shopLng;

    @ApiModelProperty(value = "店铺经验类目ID")
    private String goodsManagementCategory;

    @ApiModelProperty(value = "店铺营业时间")
    private String openTime;

    @ApiModelProperty(value = "店铺营业状态 1营业 0不营业")
    private Integer businessStatus;

    @ApiModelProperty(value = "店铺地址")
    private String shopAddress;

    @ApiModelProperty(value = "起送价格")
    private Double baseShipPrice;

    @ApiModelProperty(value = "最低配送价格")
    private Double minShipPrice;

}
