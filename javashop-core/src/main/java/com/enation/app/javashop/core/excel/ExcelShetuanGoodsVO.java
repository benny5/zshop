package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2020/10/20
 * @Description: 导出社团商品
 */
@Data
public class ExcelShetuanGoodsVO {
    @ExcelProperty(value = "活动ID", index = 0)
    private Integer shetuanId;

    @ExcelProperty(value = "活动名称", index = 1)
    private String shetuanName;

    @ExcelProperty(value = "活动状态名称",index = 2)
    private String shetuanStatusText;

    @ExcelProperty(value = "商品上下架名称",index = 3)
    private String shetuanGoodsStatusText;

    @ExcelProperty(value = "参团商家",index = 4)
    private String shopName;

    @ExcelProperty(value = "skuID", index = 5)
    private Integer skuId;

    @ExcelProperty(value = "商品名称", index = 6)
    private String goodsName;

    @ExcelProperty(value = "商品分组",index = 7)
    private String shopCatName;

    @ExcelProperty(value = "优先级", index = 8)
    private Integer priority;

    @ExcelProperty(value = "成本价格", index = 9)
    private Double cost;

    @ExcelProperty(value = "市场价",index = 10)
    private Double originPrice;

    @ExcelProperty(value = "店铺价",index = 11)
    private Double salesPrice;

    @ExcelProperty(value = "团购价", index = 12)
    private Double shetuanPrice;

    @ExcelProperty(value = "参团数量", index = 13)
    private Integer goodsNum;

    @ExcelProperty(value = "虚拟数量", index = 14)
    private Integer visualNum;

    @ExcelProperty(value = "限购数量", index = 15)
    private Integer limitNum;

    @ExcelProperty(value = "团长佣金比例", index = 16)
    private Double firstRate;

    @ExcelProperty(value = "二级团长佣金", index = 17)
    private Double secondRate;

    @ExcelProperty(value = "自提站点佣金", index = 18)
    private Double selfRaisingRate;

    @ExcelProperty(value = "邀请佣金比例", index = 19)
    private Double inviteRate;
}
