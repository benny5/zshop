package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ShetuanGoodsListVO {
    @ApiModelProperty(name = "shetuan_id",value = "团购活动id")
    private Integer shetuanId;

    @ApiModelProperty(name = "goods_list",value = "商品goodsList")
    private List<ShetuanGoodsDO>  goodsList;

    @ApiModelProperty(name = "delete_ids",value = "删除的商品ids")
    private Integer[]  deleteIds;

    @ApiModelProperty(name = "goods_ids",value = "商品ids")
    private Integer[]  goodsIds;
}
