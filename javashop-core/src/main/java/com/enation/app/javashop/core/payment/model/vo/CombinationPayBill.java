package com.enation.app.javashop.core.payment.model.vo;

import com.enation.app.javashop.core.payment.model.vo.PayBill;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;


/**
 * 组合支付帐单实体
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-04-16 17:28:07
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CombinationPayBill extends PayBill implements Serializable {

    private static final long serialVersionUID = 1505285960529233095L;

    private List<PayBill> payBillList;

    public List<PayBill> getPayBillList() {
        return payBillList;
    }

    public void setPayBillList(List<PayBill> payBillList) {
        this.payBillList = payBillList;
    }

    public CombinationPayBill(List<PayBill> payBillList) {
        this.payBillList = payBillList;
    }

    public CombinationPayBill() {
    }

    @Override
    public String toString() {
        return "CombinationPayBill{" +
                "payBillList=" + payBillList +
                '}';
    }
}