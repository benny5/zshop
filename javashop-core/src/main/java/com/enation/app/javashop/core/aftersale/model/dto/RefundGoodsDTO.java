package com.enation.app.javashop.core.aftersale.model.dto;

import com.enation.app.javashop.core.goods.model.dos.SpecValuesDO;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/7/20 17:04
 */

@Data
public class RefundGoodsDTO {
    /**退货表id*/
    @ApiModelProperty(hidden=true)
    private Integer id;
    /**退货(款)编号*/
    @ApiModelProperty(name="refund_sn",value="退货(款)编号",required=false)
    private String refundSn;
    /**商品id*/
    @ApiModelProperty(name="goods_id",value="商品id",required=false)
    private Integer goodsId;
    /**产品id*/
    @ApiModelProperty(name="sku_id",value="产品id",required=false)
    private Integer skuId;
    /**发货数量*/
    @ApiModelProperty(name="ship_num",value="发货数量",required=false)
    private Integer shipNum;
    /**商品价格*/
    @ApiModelProperty(name="price",value="商品价格",required=false)
    private Double price;
    /**退货数量*/
    @ApiModelProperty(name="return_num",value="退货数量",required=false)
    private Integer returnNum;
    /**入库数量*/
    @ApiModelProperty(name="storage_num",value="入库数量",required=false)
    private Integer storageNum;
    /**商品编号*/
    @ApiModelProperty(name="goods_sn",value="商品编号",required=false)
    private String goodsSn;
    /**商品名称*/
    @ApiModelProperty(name="goods_name",value="商品名称",required=false)
    private String goodsName;
    /**商品图片*/
    @ApiModelProperty(name="goods_image",value="商品图片",required=false)
    private String goodsImage;

    /**规格数据 {\"specType\":0,\"specImage\":\"\",\"specValueId\":188,\"specId\":1,\"specValue\":\"黄色\",\"specName\":\"颜色\"}*/
    @ApiModelProperty(name="goods_spec",value="规格数据",required=false)
    private List<SpecValuesDO> goodsSpec;
}
