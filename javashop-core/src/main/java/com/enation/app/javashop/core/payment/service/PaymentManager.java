package com.enation.app.javashop.core.payment.service;

import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.dos.PaymentMethodDO;
import com.enation.app.javashop.core.payment.model.dto.PayParam;
import com.enation.app.javashop.core.payment.model.enums.ClientType;
import com.enation.app.javashop.core.payment.model.enums.TradeType;
import com.enation.app.javashop.core.payment.model.vo.PayBill;

/**
 * 全局统一支付接口
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-04-10
 */
public interface PaymentManager {

    /**
     * 保存 payment_bill，给第三方的支付账单中设置out_trade_no
     */
    PaymentBillDO createPaymentBill(PayBill payBill, PaymentMethodDO paymentMethod);

    /**
     * 同步回调方法
     *
     * @param tradeType
     * @param paymentPluginId
     */
    void payReturn(TradeType tradeType, String paymentPluginId);


    /**
     * 支付异步回调
     *
     * @param tradeType
     * @param paymentPluginId
     * @param clientType
     * @return
     */
    String payCallback(TradeType tradeType, String paymentPluginId, ClientType clientType);

    /**
     * 查询支付结果并更新订单状态
     *
     * @param param
     * @return
     */
    String queryResult(PayParam param);

    /*
     * @param pluginId 根据pluginId判断使用的插件
     */
    PaymentPluginManager findPlugin(String pluginId);
}
