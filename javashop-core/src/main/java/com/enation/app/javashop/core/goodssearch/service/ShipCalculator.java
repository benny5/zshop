package com.enation.app.javashop.core.goodssearch.service;

import com.enation.app.javashop.core.goods.model.dto.ShipCalculateDTO;

public interface ShipCalculator {

    void calculate(ShipCalculateDTO shipCalculateDTO);


}
