package com.enation.app.javashop.core.orderbill.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 结算单项表实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-04-26 15:39:57
 */
@Data
@Table(name = "es_bill_item")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BillItem implements Serializable {

    private static final long serialVersionUID = 8456961440202335L;

    /**
     * 主键id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;
    /**
     * 订单编号
     */
    @Column(name = "order_sn")
    @ApiModelProperty(name = "order_sn", value = "订单编号", required = false)
    private String orderSn;
    /**
     * 订单价格
     */
    @Column(name = "price")
    @ApiModelProperty(name = "price", value = "金额，退款金额或者收款金额", required = false)
    private Double price;
    /**
     * 优惠价格
     */
    @Column(name = "discount_price")
    @ApiModelProperty(name = "discount_price", value = "优惠价格", required = false)
    private Double discountPrice;
    /**
     * 单项类型 收款/退款
     */
    @Column(name = "item_type")
    @ApiModelProperty(name = "item_type", value = "单项类型 收款/退款", required = false)
    private String itemType;
    /**
     * 加入时间
     */
    @Column(name = "add_time")
    @ApiModelProperty(name = "add_time", value = "加入时间", required = false)
    private Long addTime;
    /**
     * 所属账单id
     */
    @Column(name = "bill_id")
    @ApiModelProperty(name = "bill_id", value = "所属账单id", required = false)
    private Integer billId;
    /**
     * 状态
     */
    @Column(name = "status")
    @ApiModelProperty(name = "status", value = "状态", required = false)
    private Integer status;
    /**
     * 店铺id
     */
    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "店铺id", required = false)
    private Integer sellerId;
    /**
     * 下单时间
     */
    @Column(name = "order_time")
    @ApiModelProperty(name = "order_time", value = "下单时间", required = false)
    private Long orderTime;
    /**
     * 退款单号
     */
    @Column(name = "refund_sn")
    @ApiModelProperty(name = "refund_sn", value = "退款单号", required = false)
    private String refundSn;
    /**
     * 会员id
     */
    @Column(name = "member_id")
    @ApiModelProperty(name = "member_id", value = "会员id", required = false)
    private Integer memberId;
    /**
     * 会员名称
     */
    @Column(name = "member_name")
    @ApiModelProperty(name = "member_name", value = "会员名称", required = false)
    private String memberName;
    /**
     * 收货人
     */
    @Column(name = "ship_name")
    @ApiModelProperty(name = "ship_name", value = "收货人", required = false)
    private String shipName;
    /**
     * 支付方式
     */
    @Column(name = "payment_type")
    @ApiModelProperty(name = "payment_type", value = "支付方式", required = false)
    private String paymentType;
    /**
     * 退货时间
     */
    @Column(name = "refund_time")
    @ApiModelProperty(name = "refund_time", value = "退货时间", required = false)
    private Long refundTime;

    /**
     * 平台收益
     */
    @Column(name = "platform_money")
    @ApiModelProperty(name = "platform_money", value = "平台收益", required = false)
    private BigDecimal platformMoney;
    /**
     * 卖家收益
     */
    @Column(name = "seller_money")
    @ApiModelProperty(name = "seller_money", value = "卖家收益", required = false)
    private BigDecimal sellerMoney;
    /**
     * 团长收益
     */
    @Column(name = "leader_money")
    @ApiModelProperty(name = "leader_money", value = "团长收益", required = false)
    private BigDecimal leaderMoney;
    /**
     * 分销收益
     */
    @Column(name = "distribution_money")
    @ApiModelProperty(name = "distribution_money", value = "分销收益", required = false)
    private BigDecimal distributionMoney;
    /**
     * 分销收益
     */
    @Column(name = "lv1_money")
    @ApiModelProperty(name = "lv1_money", value = "一级分销收益", required = false)
    private BigDecimal lv1Money;
    /**
     * 分销收益
     */
    @Column(name = "lv2_money")
    @ApiModelProperty(name = "lv2_money", value = "二级分销收益", required = false)
    private BigDecimal lv2Money;
    /**
     * 分销收益
     */
    @Column(name = "invite_money")
    @ApiModelProperty(name = "invite_money", value = "邀请收益", required = false)
    private BigDecimal inviteMoney;
    /**
     * 补贴金额
     */
    @Column(name = "subsidy_money")
    @ApiModelProperty(name = "subsidy_money", value = "补贴金额", required = false)
    private BigDecimal subsidyMoney;

}