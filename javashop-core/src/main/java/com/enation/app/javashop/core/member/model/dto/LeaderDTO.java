package com.enation.app.javashop.core.member.model.dto;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class LeaderDTO implements Serializable {

    @Id(name = "leader_id")
    @ApiModelProperty(name = "leader_id",value = "团长ID")
    private Integer leaderId;

    @ApiModelProperty(name = "leader_code",value = "团长编码")
    private String leaderCode;

    @ApiModelProperty(name ="member_id",value = "会员ID")
    private Integer memberId;

    @ApiModelProperty(name="seller_id",value = "ID")
    private Integer sellerId;

    @ApiModelProperty(name = "leader_name",value = "团长名")
    private String leaderName;

    @ApiModelProperty(name = "leader_mobile",value = "团长手机")
    private String leaderMobile;

    @ApiModelProperty(name = "leader_type",value = "团长类型 1自提点 2配送点")
    private Integer leaderType;

    @ApiModelProperty(name = "op_status",value = "营业状态")
    private Integer opStatus;

    @ApiModelProperty(name = "status",value = "状态")
    private Integer status;

    @ApiModelProperty(name = "audit_status",value = "审核状态")
    private Integer auditStatus;

    @ApiModelProperty(name = "audit_time",value = "审核时间")
    private Long auditTime;

    @ApiModelProperty(name = "audit_remark",value = "审核备注")
    private String auditRemark;

    @ApiModelProperty(name = "facade_pic_url",value = "门面图片")
    private String facadePicUrl;

    @ApiModelProperty(name = "fans_count",value = "粉丝数")
    private Long fansCount;

    @Column(name="street")
    @ApiModelProperty(name = "street",value = "街道")
    private String street;

    @ApiModelProperty(name = "address",value = "地址")
    private String address;

    @ApiModelProperty(name = "doorplate",value = "门牌号")
    private String doorplate;

    @ApiModelProperty(name = "cell_name",value = "小区名称")
    private String cellName;

    @ApiModelProperty(name = "site_name",value = "站点名称")
    private String siteName;

    @Column(name="origin_site_name")
    @ApiModelProperty(name = "origin_site_name",value = "原站点名称")
    private String originSiteName;

    @ApiModelProperty(name = "site_type",value = "站点类型")
    private Integer siteType;

    @ApiModelProperty(name="join_reason",value = "加入原因")
    private String joinReason;

    @ApiModelProperty(name="source_from",value = "注册来源")
    private Integer sourceFrom;

    /**所在省id*/
    @ApiModelProperty(name="province_id",value="所在省id",required=false,hidden = true)
    private Integer provinceId;
    /**所在市id*/
    @ApiModelProperty(name="city_id",value="所在市id",required=false,hidden = true)
    private Integer cityId;
    /**所在县id*/
    @ApiModelProperty(name="county_id",value="所在县id",required=false,hidden = true)
    private Integer countyId;
    /**所在镇id*/
    @ApiModelProperty(name="town_id",value="所在镇id",required=false,hidden = true)
    private Integer townId;
    /**所在省*/
    @Column(name = "province")
    @ApiModelProperty(name="province",value="所在省",required=false,hidden = true)
    private String province;
    /**所在市*/
    @Column(name = "city")
    @ApiModelProperty(name="city",value="所在市",required=false,hidden = true)
    private String city;
    /**所在县*/
    @ApiModelProperty(name="county",value="所在县",required=false,hidden = true)
    private String county;
    /**所在镇*/
    @ApiModelProperty(name="town",value="所在镇",required=false,hidden = true)
    private String town;

    @ApiModelProperty(name = "lat", value = "经度")
    private Double lat;

    @Column(name = "lng")
    @ApiModelProperty(name = "lng", value = "经度")
    private Double lng;

    @ApiModelProperty(name = "work_time", value = "营业时间")
    private String workTime;

    @ApiModelProperty(name = "update_time",value = "更新时间")
    private Long updateTime;

    @ApiModelProperty(name ="add_time",value = "新增时间")
    private Long addTime;

    @ApiModelProperty(name ="creater_name",value = "创建人")
    private String createrName;

    @ApiModelProperty(name ="remark",value = "备注")
    private String remark;

    @ApiModelProperty(name ="operateAreas",value = "运营区域")
    private String operateAreas;

    /**
     * 身份证号
     */
    @ApiModelProperty(name = "midentity", value = "身份证号", required = false)
    private String midentity;
    /**
     * 昵称(前端所属用户)
     */
    @ApiModelProperty(name = "nick_name", value = "昵称", required = false)
    private String nickname;

    @ApiModelProperty(name = "ship_priority", value = "配送优先级", required = false)
    private Integer shipPriority;

    @ApiModelProperty(name = "dispatch_type", value = "分拣配送方式", required = false)
    private String dispatchType;
}
