package com.enation.app.javashop.core.shop.model.vo;

import javax.validation.constraints.NotEmpty;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 店铺设置VO
 *
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午7:58:55
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShopSettingVO {
    /**
     * 店铺Id
     */
    @Column(name = "shop_id")
    @ApiModelProperty(name = "shop_id", value = "店铺Id", required = false)
    private Integer shopId;
    /**
     * 店铺所在省id
     */
    @Column(name = "shop_province_id")
    @ApiModelProperty(name = "shop_province_id", value = "店铺所在省id", required = false, hidden = true)
    private Integer shopProvinceId;
    /**
     * 店铺所在市id
     */
    @Column(name = "shop_city_id")
    @ApiModelProperty(name = "shop_city_id", value = "店铺所在市id", required = false, hidden = true)
    private Integer shopCityId;
    /**
     * 店铺所在县id
     */
    @Column(name = "shop_county_id")
    @ApiModelProperty(name = "shop_county_id", value = "店铺所在县id", required = false, hidden = true)
    private Integer shopCountyId;
    /**
     * 店铺所在镇id
     */
    @Column(name = "shop_town_id")
    @ApiModelProperty(name = "shop_town_id", value = "店铺所在镇id", required = false, hidden = true)
    private Integer shopTownId;
    /**
     * 店铺所在省
     */
    @Column(name = "shop_province")
    @ApiModelProperty(name = "shop_province", value = "店铺所在省", required = false, hidden = true)
    private String shopProvince;
    /**
     * 店铺所在市
     */
    @Column(name = "shop_city")
    @ApiModelProperty(name = "shop_city", value = "店铺所在市", required = false, hidden = true)
    private String shopCity;
    /**
     * 店铺所在县
     */
    @Column(name = "shop_county")
    @ApiModelProperty(name = "shop_county", value = "店铺所在县", required = false, hidden = true)
    private String shopCounty;
    /**
     * 店铺所在镇
     */
    @Column(name = "shop_town", allowNullUpdate = true)
    @ApiModelProperty(name = "shop_town", value = "店铺所在镇", required = false, hidden = true)
    private String shopTown;
    /**
     * 店铺详细地址
     */
    @Column(name = "shop_add")
    @ApiModelProperty(name = "shop_add", value = "店铺详细地址", required = true)
    private String shopAdd;
    /**
     * 联系人电话
     */
    @Column(name = "link_phone")
    @ApiModelProperty(name = "link_phone", value = "联系人电话", required = true)
    @NotEmpty(message = "联系人电话必填")
    private String linkPhone;
    /**
     * 店铺logo
     */
    @Column(name = "shop_logo")
    @ApiModelProperty(name = "shop_logo", value = "店铺logo", required = false)
    private String shopLogo;
    /**
     * 店铺横幅
     */
    @Column(name = "shop_banner")
    @ApiModelProperty(name = "shop_banner", value = "店铺横幅", required = false)
    private String shopBanner;
    /**
     * 店铺简介
     */
    @Column(name = "shop_desc")
    @ApiModelProperty(name = "shop_desc", value = "店铺简介", required = false)
    private String shopDesc;
    /**
     * 店铺客服qq
     */
    @Column(name = "shop_qq")
    @ApiModelProperty(name = "shop_qq", value = "店铺客服qq", required = false)
    private String shopQq;

    /**
     * 店铺名称
     */
    @Column(name = "shop_name")
    @ApiModelProperty(name = "shop_name", value = "店铺名称", required = false)
    private String shopName;



    @Column(name = "shop_lat")
    @ApiModelProperty(name="shopLat",value="纬度",required=false)
    private Double shopLat;
    @Column(name = "shop_lng")
    @ApiModelProperty(name="shopLng",value="经度",required=false)
    private Double shopLng;
    @Column(name = "ship_type")
    @ApiModelProperty(name="shipType",value="支持配送方式",required=false)
    private String shipType;

    @Column(name = "open_time_type")
    @ApiModelProperty(name="open_time_type",value="门店营业时间类型 0.全天 1.自定义",required=true)
    private Integer openTimeType;

    @Column(name = "open_start_time")
    @ApiModelProperty(name="open_start_time",value="门店开始营业时间 HH:mm:ss",required=false)
    private String openStartTime;

    @Column(name = "open_end_time")
    @ApiModelProperty(name="open_end_time",value="门店结束营业时间 HH:mm:ss",required=false)
    private String openEndTime;

    @Column(name = "open_time")
    @ApiModelProperty(name="open_time",value="店铺营业时间 HH:mm:ss",required=false)
    private String openTime;

    @Column(name = "business_status")
    @ApiModelProperty(name="business_status",value="店铺营业状态 1营业 0不营业",required=false)
    private Integer businessStatus;

}

