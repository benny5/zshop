package com.enation.app.javashop.core.goods.listener;

import com.enation.app.javashop.core.base.CachePrefix;
import com.enation.app.javashop.core.goods.service.GoodsQuantityManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.framework.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author JFeng
 * @date 2021/1/13 9:39
 */
@Component
public class QuantityEventsListener implements ApplicationListener<GoodsQuantityEvent> {
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void onApplicationEvent(GoodsQuantityEvent event) {
        if (!(event instanceof GoodsQuantityEvent)) {
            return;
        }

        OrderDO orderDO = event.getOrderDO();
        // 社区团购扣减库存
        if(orderDO.getOrderStatus().equals(OrderStatusEnum.PAID_OFF.name()) || orderDO.getOrderType().equals(OrderTypeEnum.shetuan.name())){
            cutQuantity(orderDO);
        }
        // 拼团订单扣减库存
        if(orderDO.getOrderStatus().equals(OrderStatusEnum.FORMED.name()) || orderDO.getOrderType().equals(OrderTypeEnum.pintuan.name())){
            cutQuantity(orderDO);
        }
        // 秒杀扣减库存
        if(orderDO.getOrderStatus().equals(OrderStatusEnum.FORMED.name()) || orderDO.getOrderType().equals(OrderTypeEnum.pintuan.name())){
            //this.seckillApplyManager.addSoldNum(promotionDTOSekillList);
            //seckillApplyManager.rollbackStock(promotionDTOSekillList);
            //groupbuyGoodsManager.rollbackStock(promotionDTOGroupBuyList, order.getSn());
            //shetuanGoodsManager.rollbackStock(promotionDTOShetuanList);
        }

        System.out.println(event.getOrderDO());
        System.out.println(event.getMsg());
        System.out.println(event.getQuantityType().name());
    }

    /**
     * 扣减库存： 顶大
     * @param orderDO
     */
    private void cutQuantity(OrderDO orderDO) {
        if(orderDO.getOrderType().equals(OrderTypeEnum.shetuan.name())){
            List<OrderSkuVO> skuVOList = JsonUtil.jsonToList(orderDO.getItemsJson(), OrderSkuVO.class);
            // 更新REDIS 中社区团购商品购买数量 【每天晚上定时清理失效key】
            for (OrderSkuVO orderSkuVO : skuVOList) {
                Integer shetuanGoodsId = orderSkuVO.getShetuanGoodsId();
                if(shetuanGoodsId!=null){
                    redisTemplate.opsForValue().increment(CachePrefix.ST_BUY_NUM.getPrefix()+shetuanGoodsId,orderSkuVO.getNum());
                }
            }
        }
    }

    /**
     * 回滚库存：
     * @param orderDO
     */
    private void rollBackQuantity(OrderDO orderDO) {

    }
}
