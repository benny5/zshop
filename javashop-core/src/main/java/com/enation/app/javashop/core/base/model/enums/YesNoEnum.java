package com.enation.app.javashop.core.base.model.enums;

import com.enation.app.javashop.framework.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

public enum YesNoEnum {
    NO(0, "否"),
    YES(1, "是");

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (YesNoEnum item : YesNoEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    private int index;
    private String text;

    private YesNoEnum(int index, String text) {
        this.index = index;
        this.text = text;
    }

    public int getIndex() {
        return this.index;
    }

    public String getText() {
        return this.text;
    }


    public static String getTextByIndex(Integer index){
        return index == null ? "" : enumMap.get(index);
    }

}
