package com.enation.app.javashop.core.promotion.newcomer.model.vos;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: zhou
 * @Date: 2021/2/25
 * @Description: 新人购活动查询
 */
@Data
public class QueryNewcomerVO {

    @ApiModelProperty(value = "页码", name = "page_no", required = true)
    @NotNull(message = "请输入当前页码")
    private Integer pageNo;

    @ApiModelProperty(value = "分页大小", name = "page_size", required = true)
    @NotNull(message = "请输入分页大小")
    private Integer pageSize;

    /**
     * 新人专享活动名
     */
    @ApiModelProperty(value = "新人专享活动名", name = "newcomer_name")
    private String newcomerName;

    /**
     * 活动状态
     */
    @ApiModelProperty(value = "活动状态", name = "status")
    private Integer status;
    /**
     * 活动开始时间
     */
    @ApiModelProperty(value = "活动开始时间", name = "start_time")
    private Long startTime;

    /**
     * 活动结束时间
     */
    @ApiModelProperty(value = "活动结束时间", name = "end_time")
    private Long endTime;
}
