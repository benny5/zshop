package com.enation.app.javashop.core.system.service.impl;

import javax.validation.Valid;

import com.enation.app.javashop.framework.util.SqlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.StringUtil;
import com.enation.app.javashop.core.shop.ShopErrorCode;
import com.enation.app.javashop.core.system.model.dos.LogisticsCompanyDO;
import com.enation.app.javashop.core.system.service.LogisticsCompanyManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 物流公司业务类
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-29 15:10:38
 */
@Service
public class LogisticsCompanyManagerImpl implements LogisticsCompanyManager {

	@Autowired
	@Qualifier("systemDaoSupport")
	private	DaoSupport	daoSupport;
	
	@Override
	public Page list(int page,int pageSize){
		
		String sql = "select * from es_logistics_company  ";
		Page  webPage = this.daoSupport.queryForPage(sql,page, pageSize ,LogisticsCompanyDO.class );
		
		return webPage;
	}
	
	@Override
	@Transactional(value = "systemTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public LogisticsCompanyDO add(LogisticsCompanyDO logi)	{
		LogisticsCompanyDO logicode = this.getLogiByCode(logi.getCode());
		LogisticsCompanyDO logikdcode = this.getLogiBykdCode(logi.getKdcode());
		LogisticsCompanyDO loginame = this.getLogiByName(logi.getName());
		if(loginame != null){
			throw new ServiceException(ShopErrorCode.E211.name(), "物流公司名称重复");
		}
		if(logicode != null){
			throw new ServiceException(ShopErrorCode.E213.name(), "物流公司代码重复");
		}
		if(logikdcode != null){
			throw new ServiceException(ShopErrorCode.E212.name(), "快递鸟公司代码重复");
		}
		
		this.daoSupport.insert(logi);
		int lastId = this.daoSupport.getLastId("es_logistics_company");
		logi.setId(lastId);
		
		return logi;
	}
	
	@Override
	@Transactional(value = "systemTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public LogisticsCompanyDO edit(@Valid LogisticsCompanyDO logi, Integer id){
		LogisticsCompanyDO model = this.getModel(id);
		if(model==null) {
			throw new ServiceException(ShopErrorCode.E214.name(), "物流公司不存在");
		}
		//当支持电子面单时，需要填写快递鸟物流公司code
		if(logi.getIsWaybill() == 1 && StringUtil.isEmpty(logi.getKdcode())){
			throw new ServiceException(ShopErrorCode.E212.name(), "快递鸟公司代码必填");
		}

		LogisticsCompanyDO logicode = this.getLogiByCode(logi.getCode());
		LogisticsCompanyDO logikdcode = this.getLogiBykdCode(logi.getKdcode());
		LogisticsCompanyDO loginame = this.getLogiByName(logi.getName());
		if(logikdcode != null && !logikdcode.getId().equals(logi.getId())){
			throw new ServiceException(ShopErrorCode.E212.name(), "快递鸟公司代码重复");
		}
		if(loginame != null && !loginame.getId() .equals(logi.getId())){
			throw new ServiceException(ShopErrorCode.E211.name(), "物流公司名称重复");
		}
		if(logicode != null && !logicode.getId().equals(logi.getId())){
			throw new ServiceException(ShopErrorCode.E213.name(), "物流公司代码重复");
		}
		this.daoSupport.update(logi, id);
		return logi;
	}
	
	@Override
	@Transactional(value = "systemTransactionManager",propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public	void delete(Integer[] logiId)	{
		for (Integer logi : logiId) {
			LogisticsCompanyDO model = this.getModel(logi);
			if(model==null) {
				throw new ServiceException(ShopErrorCode.E214.name(), "物流公司不存在");
			}
		}
		String id =StringUtil.implode(",", logiId);
		if(id==null || "".equals(id)){
			return ;
		}
		List term = new ArrayList<>();
		String idsStr = SqlUtil.getInSql(logiId, term);
		String sql = "delete from es_logistics_company where id in (" + idsStr + ")";
		this.daoSupport.execute(sql,term.toArray());
	}
	
	@Override
	public LogisticsCompanyDO getModel(Integer id)	{
		return this.daoSupport.queryForObject(LogisticsCompanyDO.class, id);
	}

	@Override
	public LogisticsCompanyDO getLogiByCode(String code) {
		String sql  = "select * from es_logistics_company where code=?";
		LogisticsCompanyDO logiCompany =  this.daoSupport.queryForObject(sql, LogisticsCompanyDO.class, code);
		return logiCompany;
	}

	@Override
	public LogisticsCompanyDO getLogiBykdCode(String kdcode) {
		String sql  = "select * from es_logistics_company where kdcode=?";
		LogisticsCompanyDO logiCompany =  this.daoSupport.queryForObject(sql, LogisticsCompanyDO.class, kdcode);
		return logiCompany;
	}

	@Override
	public LogisticsCompanyDO getLogiByName(String name) {
		String sql  = "select * from es_logistics_company where name=?";
		LogisticsCompanyDO logiCompany =  this.daoSupport.queryForObject(sql, LogisticsCompanyDO.class, name);
		return logiCompany;
	}

	@Override
	public List<LogisticsCompanyDO> list() {
		String sql = "select * from es_logistics_company  ";
		return this.daoSupport.queryForList(sql,LogisticsCompanyDO.class);
	}
}
