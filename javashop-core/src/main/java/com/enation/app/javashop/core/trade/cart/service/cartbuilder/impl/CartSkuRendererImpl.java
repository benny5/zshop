package com.enation.app.javashop.core.trade.cart.service.cartbuilder.impl;

import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.core.trade.cart.model.enums.CartSourceType;
import com.enation.app.javashop.core.trade.cart.model.enums.CartType;
import com.enation.app.javashop.core.trade.cart.model.enums.CheckedWay;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuOriginVo;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartVO;
import com.enation.app.javashop.core.trade.cart.service.CartOriginDataManager;
import com.enation.app.javashop.core.trade.cart.service.cartbuilder.CartSkuRenderer;
import com.enation.app.javashop.core.trade.cart.util.CartUtil;
import com.enation.app.javashop.core.trade.converter.CartSkuVOConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商城
 * 购物车sku渲染实现<br>
 * 文档请参考：<br>
 * <a href="http://doc.javamall.com.cn/current/achitecture/jia-gou/ding-dan/cart-and-checkout.html#购物车显示" >购物车显示</a>
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/12/11
 */
@Service
@Primary
public class CartSkuRendererImpl implements CartSkuRenderer {


    @Autowired
    private CartOriginDataManager cartOriginDataManager;

    @Autowired
    private ShopManager shopManager;

    @Override
    public void renderSku(List<CartVO> cartList, CartType cartType, CheckedWay way) {

        //获取原始数据
        List<CartSkuOriginVo> originList = cartOriginDataManager.read(way, CartSourceType.COMMON_CART);

        //用原始数据渲染购物车 （按照店铺单独核算）
        for (CartSkuOriginVo originVo : originList) {

            //转换为购物车skuvo
            CartSkuVO skuVO = CartSkuVOConvert.toSkuVo(originVo);

            innerRunder(skuVO,cartList,cartType);

        }
    }

    /**
     * @param cartList
     * @param cartSkuFilter
     * @param cartType
     * @param way
     */
    @Override
    public void renderSku(List<CartVO> cartList, CartSkuFilter cartSkuFilter,CartType cartType, CheckedWay way) {
        //获取原始数据
        List<CartSkuOriginVo> originList = cartOriginDataManager.read(way);

        //用原始数据渲染购物车
        for (CartSkuOriginVo originVo : originList) {

            //转换为购物车skuvo
            CartSkuVO skuVO = CartSkuVOConvert.toSkuVo(originVo);

            //如果过滤成功才继续
            if( !cartSkuFilter.accept(skuVO)){
                continue;
            }

            innerRunder(skuVO,cartList,cartType);

        }
    }



    /**
     * 内部化用的渲染方法
     * @param skuVO
     * @param cartList
     * @param cartType
     */
    private void innerRunder(CartSkuVO skuVO, List<CartVO> cartList, CartType cartType) {
        Integer sellerId = skuVO.getSellerId();
        String sellerName = skuVO.getSellerName();

        CartVO cartVO = CartUtil.findCart(sellerId,cartList);
        if (cartVO == null) {
            // 查询店铺信息（店铺名称修改）
            ShopVO shopVO = this.shopManager.getShop(sellerId);

            cartVO = new CartVO(sellerId, sellerName,shopVO.getShopLogo(),cartType);
            // 购物车类型
            cartVO.setCartSourceType(CartSourceType.COMMON_CART.name());
            cartVO.setCommunityShop(shopVO.getCommunityShop());
            // 店铺坐标
            cartVO.setShopLat(shopVO.getShopLat());
            cartVO.setShopLng(shopVO.getShopLng());
            // 店铺是否自营
            cartVO.setSelfOperated(shopVO.getSelfOperated());
            // 检查店铺是否营业
            cartVO.setShopIsOpen(true);

            cartList.add(cartVO);
        }


        //压入到当前店铺的sku列表中
        cartVO.getSkuList().add(skuVO);
    }



}
