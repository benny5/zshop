package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * DistributionGoods
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-11 上午7:39
 */
@Data
@Table(name = "es_distribution_goods")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DistributionGoods {

    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    @Column(name = "goods_id")
    @ApiModelProperty(name = "goods_id", value = "商品id", required = true)
    private Integer goodsId;

    @Column(name = "grade1_rebate")
    @ApiModelProperty(name = "grade1_rebate", value = "1级提成比例", required = true)
    private Double grade1Rebate;

    @Column(name = "grade2_rebate")
    @ApiModelProperty(name = "grade2_rebate", value = "2级提成比例", required = true)
    private Double grade2Rebate;

    @Column(name = "inviter_rate")
    @ApiModelProperty(name = "inviter_rate", value = "邀请佣金比例", required = true)
    private Double inviterRate;

    @Column(name = "subsidy_rate")
    @ApiModelProperty(name = "subsidy_rate", value = "补贴比例", required = true)
    private Double subsidyRate;

    public DistributionGoods(){}

    public DistributionGoods(Integer goodsId, double grade1Rebate, double grade2Rebate, double inviterRate) {
        this.goodsId = goodsId;
        this.grade1Rebate = grade1Rebate;
        this.grade2Rebate = grade2Rebate;
        this.inviterRate = inviterRate;
    }

}
