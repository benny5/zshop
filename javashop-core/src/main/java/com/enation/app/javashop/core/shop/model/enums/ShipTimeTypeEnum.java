package com.enation.app.javashop.core.shop.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 同城配送费 计价方式
 */
public enum ShipTimeTypeEnum {
    BASE_TIME(1, "基础时间"),

    SECTION_TIME(2, "时间段配送");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (ShipTimeTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    ShipTimeTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }


}
