package com.enation.app.javashop.core.promotion.luck.service;

import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.promotion.luck.model.dos.LuckDO;
import com.enation.app.javashop.core.promotion.luck.model.dos.LuckPrizeDO;
import com.enation.app.javashop.core.promotion.luck.model.dos.LuckRecordDO;
import com.enation.app.javashop.core.promotion.luck.model.vo.QueryLuckRecordVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

/**
 * @author 孙建
 */
public interface LuckRecordManager {

    Page<LuckRecordDO> queryLuckRecordList(QueryLuckRecordVO queryLuckRecordVO);

    LuckRecordDO saveLuckRecord(Member member, LuckDO luckDO, LuckPrizeDO luckPrize);

    void luckRecordExport(QueryLuckRecordVO queryLuckRecordVO);

    void exchangePrize(String luckRecordIds);

    void LuckRefund(List<RefundDO>  refundDO);

}
