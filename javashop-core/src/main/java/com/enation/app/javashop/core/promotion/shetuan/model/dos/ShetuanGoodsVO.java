package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.goods.model.dto.BuyRecord;
import com.enation.app.javashop.core.goods.model.dto.TagsDTO;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.shop.model.vo.ShopCatItem;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kingapex on 2019-01-22.
 * 拼团商品信息VO
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2019-01-22
 */

@Data
public class ShetuanGoodsVO implements Serializable {

    private static final long serialVersionUID = 4796645552318671313L;

    @ApiModelProperty(name = "start_time_left", value = "开始剩余时间，秒数")
    private Long startTimeLeft;

    @ApiModelProperty(name = "time_left", value = "结束剩余时间，秒数")
    private Long timeLeft;

    @ApiModelProperty(name = "limit_num", value = "限购数量")
    private Integer limitNum;

    @ApiModelProperty(hidden = true, value = "开始时间戳")
    private Long startTime;

    @ApiModelProperty(hidden = true, value = "结束时间戳")
    private Long endTime;

    @ApiModelProperty(name = "enable_quantity", value = "可用库存")
    private Integer enableQuantity;

    private String address;

    private Long pickTime;


    private Integer shopCatId;

    private String shopCatPath;

    private String shopCatName;

    private String videoUrl;

    private String original;

    private String  selling;

    private Integer todaySoldQuantity;

    private Integer cartNum;

    private String endTimeShow;

    private String pickTimeShow;

    // 活动状态代码
    private Integer shetuanStatus;

    private Long distanceEndTime;

    private Long distanceStartTime;

    private Integer isStart;

    private List<BuyRecord> buyRecordList;

    private Boolean isEnableSale;

    // 活动状态名称
    private String shetuanStatusText;

    //商品上下架名称
    private String shetuanGoodsStatusText;

    private String shetuanName;

    private Integer viewCount;

    private Integer buyCount;

    private Integer priority;

    private Boolean isPreSale;

    private Integer id;

    private Integer shetuanId;
    private Integer skuId;

    private Integer goodsId;

    private Integer sellerId;

    private String sellerName;

    private String goodsName;
    private Double settlePrice;
    private Double originPrice;
    private Double salesPrice;
    private Double shetuanPrice;
    private String sn;
    private String specs;
    private String thumbnail;
    // 商品上下架代码
    private Integer status;
    private Double firstRate;
    private Double secondRate;
    private Double thirdRate;
    private Double inviteRate;
    private Double selfRaisingRate;
    private Double subsidyRate;
    private Integer goodsNum;
    private Integer visualNum;
    private Integer mockNum;

    private Integer buyNum;

    private  Double grade;
    private  Long createTime;

    private String shopCatItems;

    private Integer shetuanGoodsId;

    // 成本价格
    private Double cost;

    private List<ShopCatItem> shopCatItemsList;

    private List<TagsDTO> shopHomeTags;

    private List<TagsDTO> goodsListTags;

    private List<TagsDTO> goodsDetailsTags;

    public ShetuanGoodsVO() {
    }

    public ShetuanGoodsVO(ShetuanDO shetuan, ShetuanGoodsDO good) {
        BeanUtil.copyProperties(good,this);
        this.startTime=shetuan.getStartTime();
        this.endTime=shetuan.getEndTime();
        this.pickTime=shetuan.getPickTime();
        this.shetuanStatus=shetuan.getStatus();
        this.shetuanId=shetuan.getShetuanId();
        if(StringUtils.isNotEmpty(good.getShopCatItems())){
           this.shopCatItemsList = JSON.parseArray(good.getShopCatItems(), ShopCatItem.class);
        }
    }


    /**
     * 计算社区团购活动时间
     */
    public void countTime() {

        //当前时间
        long currentTime = DateUtil.getDateline();


        long startTime = this.startTime;
        long endTime =  this.endTime;

        //距离活动结束的时间
        long distanceEndTime = 0;

        //距离活动开始的时间
        long distanceStartTime = 0;


        //是否已经开始，1为开始
        int isStart = 0;

        //当前时间大于活动开始的时刻，说明已经开始，计算距离结束的时间
        if (currentTime >= startTime) {
            //当前时间
            distanceEndTime = endTime - currentTime;
            isStart = 1;
        } else {
            //活动的开始时间
            distanceStartTime = currentTime - startTime;
        }
        this.distanceEndTime=distanceEndTime;
        this.distanceStartTime=distanceStartTime;
        this.isStart=isStart;
    }


    public void calTodaySold(Integer todayBuyNum) {

        visualNum=visualNum == null ? 0 :visualNum;

        int todaySold = todayBuyNum < visualNum ? visualNum : todayBuyNum;

        // 今日累计已售
        this.todaySoldQuantity = todaySold < 0 ? 0 : todaySold;
        this.buyNum= this.buyNum == null ? 0 : this.buyNum;
        this.buyNum+=this.todaySoldQuantity;
    }

    public void calGoodsQuantity(GoodsSkuVO skuVO) {

        if (skuVO != null && skuVO.getEnableQuantity() > 0) {
            // 商品剩余数量  参团数量-实际下单数量=参团剩余库存  总商品剩余库存
            if(goodsNum<=todaySoldQuantity){
                this.enableQuantity =skuVO.getEnableQuantity();
                this.goodsNum =skuVO.getQuantity();
            }else {
                this.enableQuantity =goodsNum-todaySoldQuantity;
            }

            this.isEnableSale = true;
        } else {
            this.enableQuantity = 0;
            this.todaySoldQuantity = this.goodsNum;
            this.isEnableSale = false;
        }
    }

    public void setBuyRecordList(List<BuyRecord> buyRecordList) {
        if (CollectionUtils.isEmpty(buyRecordList)) {
            return;
        }
        this.buyRecordList = buyRecordList;
        // 历史累计数量 不能超过头像数量 【头像不足的场景】
        this.buyNum=buyRecordList.size()<5 && buyRecordList.size()<this.buyNum ?buyRecordList.size():this.buyNum;
    }
}
