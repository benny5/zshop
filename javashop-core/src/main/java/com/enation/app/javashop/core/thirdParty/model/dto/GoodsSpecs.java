package com.enation.app.javashop.core.thirdParty.model.dto;

import lombok.Data;

@Data
public class GoodsSpecs {

    //规格id
    private Integer specsId;
    //规格值
    private String specsName;
    //规格名
    private String specsTitle;
}
