package com.enation.app.javashop.core.wms.model.enums;

/**
 * @author JFeng
 * @date 2020/10/19 14:18
 */
public enum LeaderDispatchTypeEnums {

    /**
     * B+C 分拣模式 自提点接收已分拣订单 第三方快递公司接收C端订单 城市仓统一分拣
     */
    SELF_GLOBAL,

    /**
     * B模式 自提点接收未分拣商品 由自提站点进行订单分拣
     */
    SELF_BATCH;
}
