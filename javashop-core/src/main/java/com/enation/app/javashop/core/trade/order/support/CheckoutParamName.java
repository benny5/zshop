package com.enation.app.javashop.core.trade.order.support;

/**
 * 注释
 *
 * @author Snow create in 2018/4/8
 * @version v2.0
 * @since v7.0.0
 */
public class CheckoutParamName {

    /** 配送时间*/
    public static  String RECEIVE_TIME = "receiveTime";


    /** 收货地址ID*/
    public static  String ADDRESS_ID = "addressId";

    /** 设置自提地址id*/
    public static  String SITE_ID = "siteId";

    /** 支付类型*/
    public static  String PAYMENT_TYPE = "paymentType";


    /** 发票*/
    public static  String RECEIPT = "receipt";


    /** 备注*/
    public static  String REMARK = "remark";

    /** 客户端类型*/
    public static  String CLIENT_TYPE = "clientType";

    /** 店铺配送类型*/
    public static  String SHOP_SHIP_WAY = "shipWay";

    /** 客户收货姓名*/
    public static  String SHIP_NAME = "shipName";
    /** 客货收货电话*/
    public static  String SHIP_MOBILE = "shipMobile";
    /** 客户当前定位城市*/
    public static  String CITY = "city";

}
