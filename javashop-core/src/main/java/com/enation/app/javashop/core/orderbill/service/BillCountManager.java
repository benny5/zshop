package com.enation.app.javashop.core.orderbill.service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 结算单统计类
 * @author 孙建
 */
public interface BillCountManager {

 	Map<String, Object> countAccount(Integer sellerId);

	BigDecimal countPassPrice(Integer sellerId);

}