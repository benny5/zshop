package com.enation.app.javashop.core.system.model.dto;

import lombok.Data;

@Data
public class UpgradeRespDTO {

    // 是否强制升级
    private boolean castUpdate;
    // 是否需要升级
    private boolean needUpgrade;
    // 最新版本
    private String lastVersion;
    // 应用名称
    private String appName;
    // 更新内容
    private String upgradeContent;
    // 下载地址
    private String downloadUrl;

}
