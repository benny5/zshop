package com.enation.app.javashop.core.promotion.shetuan.service;

import com.enation.app.javashop.core.promotion.shetuan.model.ShetuanVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsSearchDTO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.shop.model.dos.ShopDO;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Seller;

import java.util.List;


public interface ShetuanManager {


    List<ShetuanDO> get(String status);

    Page<ShetuanDO> getPageBySellerId(Integer pageSize , Integer pageNo, Integer sellerId);

    ShetuanDO add(ShetuanDO shetuanDO);

    void edit(ShetuanDO shetuanDO);

    ShetuanVO getShetuanDetailFromDB(Integer shetuanId);

    void startSignUp(Integer shetuanId);

    void delete(Integer shetuanId);

    ShetuanDO getShetuanById(Integer shetuanId);

    List<ShetuanDO>  getOverlapShetuan( Integer shetuanId );

    /**
     * 社区团购待上线活动删除
     * @param shetuanId
     */
    void deleteNewStatus(Integer shetuanId);

    /**
     * 查询该店铺中 能发消息的活动
     */
    List<ShetuanDO>  getShetuanIdBySellerId(Integer sellerId);
}
