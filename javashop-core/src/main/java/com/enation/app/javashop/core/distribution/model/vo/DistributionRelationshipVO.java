package com.enation.app.javashop.core.distribution.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class DistributionRelationshipVO implements Serializable{
    /**
     * 主键id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    /**
     * 会员id
     */
    @ApiModelProperty(value = "会员id", name = "member_id")
    private Integer memberId;

    /**
     * 上级团长id
     */
    @ApiModelProperty(value = "上级团长id", name = "member_lv1_id")
    private Integer memberLv1Id;

    /**
     * 上级团长手机号
     */
    @ApiModelProperty(value = "上级团长手机号", name = "member_lv1_mobile")
    private String memberLv1Mobile;

    /**
     * 操作类型
     */
    @ApiModelProperty(value = "操作类型", name = "operation_type")
    private String operationType;

    /**
     * 操作方式
     */
    @ApiModelProperty(value = "操作方式", name = "operation_mode")
    private String operationMode;

    /**
     * 操作描述
     */
    @ApiModelProperty(value = "操作描述", name = "operation_describe")
    private String operationDescribe;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", name = "create_time")
    private Long createTime;

    /**
     * 来源
     */
    @ApiModelProperty(value = "数据来源 1 后台修改 2 用户更改 3 系统自动", name = "source")
    private Integer source;

    /**
     * 团长名称
     */
    @ApiModelProperty(value = "团长名称", name = "distribution_name")
    private String distributionName;
}
