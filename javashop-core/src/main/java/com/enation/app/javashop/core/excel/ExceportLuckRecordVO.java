package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author: xlg
 * @Date: 2021/02/02
 * @Description: 抽奖记录导出实体
 */
@Data
public class ExceportLuckRecordVO {

    @ExcelProperty("抽奖时间")
    private String createTime;

    @ExcelProperty("抽奖用户昵称")
    private String memberNickname;

    @ExcelProperty("抽奖用户真实姓名")
    private String memberRealName;

    @ExcelProperty("抽中奖品")
    private String prizeName;

    @ExcelProperty("奖品类型")
    private String prizeType;

    @ExcelProperty("奖品个数")
    private Integer winPrizeNum;

    @ExcelProperty("兑换方式")
    private String exchangeType;

    @ExcelProperty("兑换状态")
    private String exchangeStatus;

    @ExcelProperty("兑换时间")
    private String exchangeTime;
}
