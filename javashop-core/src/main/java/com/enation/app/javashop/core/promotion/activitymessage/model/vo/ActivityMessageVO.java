package com.enation.app.javashop.core.promotion.activitymessage.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 活动消息表
 * @author xlg
 * 2020/11/05 17:51
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ActivityMessageVO {
    /**
     * id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;
    /**
     * 活动名称
     */
    @Column(name = "activity_name")
    @ApiModelProperty(name = "activity_name", value = "活动名称", required = true)
    private String activityName;

    /**
     * 活动内容
     */
    @Column(name = "activity_content")
    @ApiModelProperty(name = "activity_content", value = "活动内容", required = true)
    private String activityContent;

    /**
     * 活动时间
     */
    @Column(name = "activity_time")
    @ApiModelProperty(name = "activity_time", value = "活动时间", required = true)
    private Long activityTime;

    /**
     * 温馨提示
     */
    @Column(name = "reminder")
    @ApiModelProperty(name = "reminder", value = "温馨提示", required = true)
    private String reminder;

    /**
     * 店铺id
     */
    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "店铺id", required = true)
    private Integer sellerId;

    /**
     * 活动id
     */
    @Column(name = "activity_id")
    @ApiModelProperty(name = "activity_id", value = "活动Id", required = true)
    private Integer activityId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;
}
