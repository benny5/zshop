package com.enation.app.javashop.core.client.trade.impl;

import com.enation.app.javashop.core.client.trade.BillClient;
import com.enation.app.javashop.core.client.trade.OrderClient;
import com.enation.app.javashop.core.orderbill.model.dos.BillItem;
import com.enation.app.javashop.core.orderbill.model.enums.BillType;
import com.enation.app.javashop.core.orderbill.service.BillItemManager;
import com.enation.app.javashop.core.orderbill.service.BillManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v1.0
 * @Description: 结算单对外接口实现
 * @date 2018/7/26 11:22
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="javashop.product", havingValue="stand")
public class BillClientDefaultImpl implements BillClient {

    @Autowired
    private BillManager billManager;
    @Autowired
    private OrderClient orderClient;
    @Autowired
    private BillItemManager billItemManager;

    @Override
    public void createBills(Long startTime,Long endTime) {

        this.billManager.createBills(startTime,endTime);
    }

    @Override
    public BillItem add(BillItem billItem) {

        return this.billItemManager.add(billItem);
    }

    @Override
    public BillItem buildBillItem(String orderSn, BillType billType) {
        OrderDetailDTO orderDetail = orderClient.getModel(orderSn);

        Double discountPrice = orderDetail.getDiscountPrice();

        BillItem item = new BillItem();
        item.setAddTime(DateUtil.getDateline());
        item.setItemType(billType.name());
        //未出账
        item.setStatus(0);
        item.setOrderSn(orderSn);
        item.setPrice(orderDetail.getOrderPrice());
        item.setDiscountPrice(discountPrice);
        item.setSellerId(orderDetail.getSellerId());
        item.setMemberId(orderDetail.getMemberId());
        item.setMemberName(orderDetail.getMemberName());
        item.setOrderTime(orderDetail.getCreateTime());
        item.setPaymentType(orderDetail.getPaymentType());
        item.setShipName(orderDetail.getShipName());
        return item;
    }


    public static void main(String[] args) {
        Long[] time = DateUtil.getLastMonth();
        System.out.println(time[0]);
        System.out.println(time[1]);
    }

}
