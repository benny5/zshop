package com.enation.app.javashop.core.distribution.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ApplyDistributionDTO {

    @ApiModelProperty(name = "real_name", value = "会员真实姓名")
    private String realName;

    @ApiModelProperty(name = "midentity", value = "身份证号", required = true)
    private String midentity;

    @ApiModelProperty(name = "apply_reason", value = "申请原因", required = true)
    private String applyReason;

    @ApiModelProperty(name = "lv1_invite_code", value = "上级邀请码", required = false)
    private String lv1InviteCode;

    @ApiModelProperty(name = "business_type", value = "业务类型", required = true)
    private Integer businessType;

}
