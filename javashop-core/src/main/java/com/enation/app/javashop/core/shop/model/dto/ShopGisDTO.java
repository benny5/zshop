package com.enation.app.javashop.core.shop.model.dto;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author 王志杨
 * @Date 2020/8/26 14:26
 * @Descroption 店铺服务范围DTO
 */
@ApiModel
@Data
public class ShopGisDTO implements Serializable {

    /**店铺Id*/
    @ApiModelProperty(name="shop_id",value="店铺id",required=false)
    private Integer shopId;

    /**多边形Id*/
    @ApiModelProperty(name="polygon_id",value="多边形Id",required=false)
    private Integer polygonId;

    /**店铺服务范围多边形各节点坐标组成的集合*/
    @ApiModelProperty(name="polygon_coords",value="店铺服务范围各节点坐标组成的集合List<List<Double>>",required=false)
    //private List<String> polygons;
    private List<List<Double>> polygonCoords;

    /**外边框线的颜色*/
    @ApiModelProperty(name="line_color",value="外边框线的颜色",required=false)
    private String lineColor;

    /**外边框线的透明度*/
    @ApiModelProperty(name="line_opacity",value="外边框线的透明度",required=false)
    private Double lineOpacity;

    /**外边框线的宽度*/
    @ApiModelProperty(name="line_weight",value="外边框线的宽度",required=false)
    private Integer lineWeight;

    /**多边形填充色*/
    @ApiModelProperty(name="fill_color",value="多边形填充色",required=false)
    private String fillColor;

    /**多边形填充透明度*/
    @ApiModelProperty(name="fill_opacity",value="多边形填充透明度",required=false)
    private Double fillOpacity;

    /**多边形绘制人*/
    @ApiModelProperty(name="create_name",value="多边形绘制人",required=false)
    private String createName;




}
