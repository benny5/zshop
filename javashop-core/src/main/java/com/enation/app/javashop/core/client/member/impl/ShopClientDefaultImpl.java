package com.enation.app.javashop.core.client.member.impl;

import com.enation.app.javashop.core.client.member.ShopClient;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.shop.model.dos.ShopDO;
import com.enation.app.javashop.core.shop.model.dos.ShopDetailDO;
import com.enation.app.javashop.core.shop.model.dto.ShopBankDTO;
import com.enation.app.javashop.core.shop.model.enums.ShopStatusEnum;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.security.model.Buyer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zjp
 * @version v7.0
 * @Description
 * @ClassName ShopClientDefaultImpl
 * @since v7.0 上午10:02 2018/7/13
 */
@Service
@ConditionalOnProperty(value="javashop.product", havingValue="stand")
public class ShopClientDefaultImpl implements ShopClient{

    @Autowired
    private ShopManager shopManager;

    @Autowired
    @Qualifier("memberDaoSupport")
    private DaoSupport daoSupport;

    @Override
    public ShopVO getShop(Integer shopId) {
        return shopManager.getShop(shopId);
    }

    @Override
    public List<ShopBankDTO> listShopBankInfo() {
        return shopManager.listShopBankInfo();
    }

    @Override
    public void addCollectNum(Integer shopId) {
        shopManager.addcollectNum(shopId);
    }

    @Override
    public void reduceCollectNum(Integer shopId) {
        shopManager.reduceCollectNum(shopId);
    }

    @Override
    public void calculateShopScore() {
        shopManager.calculateShopScore();
    }

    @Override
    public void updateShopGoodsNum(Integer sellerId, Integer sellerGoodsCount) {
        String sql = "update es_shop_detail set goods_num  = ? where shop_id = ? ";
        this.daoSupport.execute(sql,sellerGoodsCount,sellerId);
    }

    @Override
    public ShopDO getShopById(Integer sellerId) {
        return shopManager.getShopById(sellerId);
    }

    @Override
    public List<ShopDetailDO> getShopDetailByIds(List<Integer> shopIds) {
        return   shopManager.getShopDetailByIds(shopIds);
    }

    @Override
    public ShopVO getShopByMemberId(Integer memberId) {
        return shopManager.getShopByMemberId(memberId);
    }

    @Override
    public void initShopByMember(Member member) {
        Buyer buyer = new Buyer();
        buyer.setUid(member.getMemberId());
        buyer.setUsername(member.getUname());
        shopManager.initShop(buyer);
    }

    @Override
    public void editStatus(ShopStatusEnum applying, Integer shopId) {
        shopManager.editStatus(applying,shopId);
    }

    @Override
    public void checkShopIsApply(Member member) {
        // 1.查询会员店铺信息
        ShopVO shopVO = getShopByMemberId(member.getMemberId());
        // 2.被拒绝的再次申请修改状态
        if (shopVO != null && shopVO.getShopDisable().equals(ShopStatusEnum.REFUSED.value())) {
            editStatus(ShopStatusEnum.APPLYING, shopVO.getShopId());
        }
        // 3.初始化店铺 等待后台审核
        if (shopVO == null) {
            initShopByMember(member);
            ShopVO shopRegist= getShopByMemberId(member.getMemberId());
            //被拒绝的再次申请修改状态
            editStatus(ShopStatusEnum.APPLYING, shopRegist.getShopId());
        }
    }
}
