package com.enation.app.javashop.core.member.model.dos;

import java.io.Serializable;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * 咨询实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-07 16:13:32
 */
@Table(name = "es_member_ask")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberAsk implements Serializable {

    private static final long serialVersionUID = 1642694855238993L;

    /**
     * 主键
     */
    @Id(name = "ask_id")
    @ApiModelProperty(hidden = true)
    private Integer askId;
    /**
     * 商品id
     */
    @Column(name = "goods_id")
    @ApiModelProperty(name = "goods_id", value = "商品id", required = false)
    private Integer goodsId;
    /**
     * 会员id
     */
    @Column(name = "member_id")
    @ApiModelProperty(name = "member_id", value = "会员id", required = false)
    private Integer memberId;
    /**
     * 咨询内容
     */
    @Column(name = "content")
    @ApiModelProperty(name = "content", value = "咨询内容", required = false)
    private String content;
    /**
     * 咨询时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "咨询时间", required = false)
    private Long createTime;
    /**
     * 卖家id
     */
    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "卖家id", required = false)
    private Integer sellerId;
    /**
     * 回复内容
     */
    @Column(name = "reply")
    @ApiModelProperty(name = "reply", value = "回复内容", required = false)
    private String reply;
    /**
     * 回复时间
     */
    @Column(name = "reply_time")
    @ApiModelProperty(name = "reply_time", value = "回复时间", required = false)
    private Long replyTime;
    /**
     * 回复状态 1 已回复 0未回复
     */
    @Column(name = "reply_status")
    @ApiModelProperty(name = "reply_status", value = "回复状态 1 已回复 0 未回复", required = false)
    private Integer replyStatus;
    /**
     * 状态
     */
    @Column(name = "status")
    @ApiModelProperty(name = "status", value = "状态,0已删除,1正常 ", required = false)
    private Integer status;
    /**
     * 卖家名称
     */
    @Column(name = "member_name")
    @ApiModelProperty(name = "member_name", value = "卖家名称", required = false)
    private String memberName;
    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    @ApiModelProperty(name = "goods_name", value = "商品名称", required = false)
    private String goodsName;

    /**
     * 会员头像
     */
    @Column(name = "member_face")
    @ApiModelProperty(name = "member_face", value = "会员头像", required = false)
    private String memberFace;

    /**
     * 审核状态
     */
    @Column(name = "auth_status")
    @ApiModelProperty(name = "auth_status", value = "审核状态", required = false)
    private String authStatus;

    @PrimaryKeyField
    public Integer getAskId() {
        return askId;
    }

    public void setAskId(Integer askId) {
        this.askId = askId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Long getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Long replyTime) {
        this.replyTime = replyTime;
    }

    public Integer getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(Integer replyStatus) {
        this.replyStatus = replyStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getMemberFace() {
        return memberFace;
    }

    public void setMemberFace(String memberFace) {
        this.memberFace = memberFace;
    }


    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MemberAsk memberAsk = (MemberAsk) o;

        return new EqualsBuilder()
                .append(askId, memberAsk.askId)
                .append(goodsId, memberAsk.goodsId)
                .append(memberId, memberAsk.memberId)
                .append(content, memberAsk.content)
                .append(createTime, memberAsk.createTime)
                .append(sellerId, memberAsk.sellerId)
                .append(reply, memberAsk.reply)
                .append(replyTime, memberAsk.replyTime)
                .append(replyStatus, memberAsk.replyStatus)
                .append(status, memberAsk.status)
                .append(memberName, memberAsk.memberName)
                .append(goodsName, memberAsk.goodsName)
                .append(memberFace, memberAsk.memberFace)
                .append(authStatus, memberAsk.authStatus)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(askId)
                .append(goodsId)
                .append(memberId)
                .append(content)
                .append(createTime)
                .append(sellerId)
                .append(reply)
                .append(replyTime)
                .append(replyStatus)
                .append(status)
                .append(memberName)
                .append(goodsName)
                .append(memberFace)
                .append(authStatus)
                .toHashCode();
    }


    @Override
    public String toString() {
        return "MemberAsk{" +
                "askId=" + askId +
                ", goodsId=" + goodsId +
                ", memberId=" + memberId +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                ", sellerId=" + sellerId +
                ", reply='" + reply + '\'' +
                ", replyTime=" + replyTime +
                ", replyStatus=" + replyStatus +
                ", status=" + status +
                ", memberName='" + memberName + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", memberFace='" + memberFace + '\'' +
                ", authStatus='" + authStatus + '\'' +
                '}';
    }


}