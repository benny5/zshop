package com.enation.app.javashop.core.trade.order.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 订单查询参数DTO
 * @author Snow create in 2018/5/14
 * @version v2.0
 * @since v7.0.0
 */
@ApiIgnore
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class OrderQueryParam {

    @ApiModelProperty(value = "第几页")
    private Integer pageNo;

    @ApiModelProperty(value = "每页条数")
    private Integer pageSize;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @ApiModelProperty(value = "交易编号")
    private String tradeSn;

    /** 例如：所有订单，待付款，待发货 等等
     *  @link OrderTagEnum
     */
    @ApiModelProperty(value = "页面标签",
            example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                    "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中")
    private String tag;

    @ApiModelProperty(value = "商家ID")
    private Integer sellerId;

    @ApiModelProperty(value = "会员ID")
    private Integer memberId;

    @ApiModelProperty(value = "收货人")
    private String shipName;

    @ApiModelProperty(value = "收货人手机号")
    private String shipMobile;

    @ApiModelProperty(value = "开始时间")
    private Long startTime;

    @ApiModelProperty(value = "起止时间")
    private Long endTime;

    @ApiModelProperty(value = "买家昵称")
    private String buyerName;

    @ApiModelProperty(value = "订单状态")
    private String orderStatus;

    @ApiModelProperty(value = "付款状态")
    private String payStatus;

    /**
     * 可以查询 订单编号 买家 商品名称
     */
    @ApiModelProperty(value = "关键字")
    private String keywords;

    /**
     * 订单付款方式
     */
    @ApiModelProperty(value = "付款方式")
    private String paymentType;
    /**
     * 订单来源
     */
    @ApiModelProperty(value = "订单来源")
    private String clientType;

    @ApiModelProperty(value = "订单类型")
    private String orderType;

    @ApiModelProperty(value = "会员昵称")
    private String nickName;

    @ApiModelProperty(value = "团长名称")
    private String lv1MemberNickName;

    @ApiModelProperty(value = "团长手机号")
    private String lv1MemberMobile;

    @ApiModelProperty(value = "skuId")
    private Integer skuId;

    @ApiModelProperty(value = "非目标订单状态")
    private String antiOrderStatus;

    @ApiModelProperty(value = "售后订单状态")
    private String serviceStatus;

    @ApiModelProperty(value = "订单总额")
    private Double orderPrice;

}
