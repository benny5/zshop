package com.enation.app.javashop.core.distribution.model.dto;

import com.enation.app.javashop.core.distribution.model.vo.DistributionMissionDetailVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CreateMissionDetailsDTO {

    @ApiModelProperty(name = "day_mission_detail", value = "日团长任务详情")
    private DistributionMissionDetailVO dayMissionDetail;

    @ApiModelProperty(name = "month_mission_detail", value = "月团长任务详情")
    private DistributionMissionDetailVO monthMissionDetail;

}
