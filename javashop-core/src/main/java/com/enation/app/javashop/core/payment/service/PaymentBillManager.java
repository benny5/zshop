package com.enation.app.javashop.core.payment.service;

import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.enums.TradeType;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

/**
 * 支付帐单业务层
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-04-16 17:28:07
 */
public interface PaymentBillManager	{

	/**
	 * 查询支付帐单列表
	 * @param page 页码
	 * @param pageSize 每页数量
	 * @return Page
	 */
	Page list(int page, int pageSize);
	/**
	 * 添加支付帐单
	 * @param paymentBill 支付帐单
	 * @return PaymentBill 支付帐单
	 */
	PaymentBillDO add(PaymentBillDO paymentBill);

	/**
	 * 支付成功调用
	 * @param billSn 支付账单号
	 * @param returnTradeNo 第三方平台回传单号（第三方平台的支付单号）
	 * @param tradeType 交易类型
	 * @param payPrice 支付金额
	 */
    void paySuccess(String billSn, String returnTradeNo, TradeType tradeType, double payPrice);


	/**
	 * 使用单号和交易类型查询对应的支付流水，最后一条
	 * @param sn 单号
	 * @param tradeType 交易类型
	 * @return PaymentBillDO
	 */
	PaymentBillDO getBillBySnAndTradeType(String sn, String tradeType);

	/**
	 * 使用单号和交易类型查询对应的支付流水集合
	 * @param sn 单号
	 * @param tradeType 交易类型
	 * @return List<PaymentBillDO>
	 */
	List<PaymentBillDO> getBillListBySnAndTradeType(String sn, String tradeType);

	/**
	 * 使用第三方单号查询流水
	 * @param returnTradeNo 第三方单号
	 * @return PaymentBillDO
	 */
	PaymentBillDO getBillByReturnTradeNo(String returnTradeNo);

	/**
	 *
	 * @param sn 单号
	 * @param tradeType 交易类型 trade/order
	 * @param paymentPluginId 支付插件ID
	 */
	PaymentBillDO getBillBySnAndTradeTypeAndPaymentPluginID(String sn, String tradeType, String paymentPluginId);

	/**
	 * 根据主键拼接字符串查询交易单
	 * @param billIds 交易单主键拼接字符串
	 */
	List<PaymentBillDO> getBillByBillIds(String billIds);

	/**
	 * 查询添加的主键
	 */
	int selectLastInsertId();

    void updatePaymentBill(PaymentBillDO paymentBill);
}