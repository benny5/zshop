package com.enation.app.javashop.core.client.member.impl;

import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.member.MemberClient;
import com.enation.app.javashop.core.client.member.MemberHistoryClient;
import com.enation.app.javashop.core.goods.model.vo.CacheGoods;
import com.enation.app.javashop.core.member.model.dos.HistoryDO;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.dto.HistoryDTO;
import com.enation.app.javashop.core.member.service.HistoryManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 会员历史
 *
 * @author zh
 * @version v7.0
 * @date 18/7/27 下午2:57
 * @since v7.0
 */

@Service
@ConditionalOnProperty(value = "javashop.product", havingValue = "stand")
public class MemberHistoryDefaultImpl implements MemberHistoryClient {

    @Autowired
    private HistoryManager historyManager;

    @Autowired
    @Qualifier("memberDaoSupport")
    private DaoSupport daoSupport;


    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private MemberClient memberClient;

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void addMemberHistory(HistoryDTO historyDTO) {
        //会员id
        Integer memberId = historyDTO.getMemberId();
        //校验此商品是否存在并且上架状态
        CacheGoods cacheGoods = goodsClient.getFromCache(historyDTO.getGoodsId());
        //如果商品为下架状态则不记录足迹
        if (!cacheGoods.getMarketEnable().equals(1)) {
            return;
        }
        //检测此商品是否已经存在浏览记录
        HistoryDO historyDO = historyManager.getHistoryByGoods(historyDTO.getGoodsId(), historyDTO.getMemberId());
        //如果为空则是添加反之为修改
        if (historyDO != null) {
            historyDO.setCreateTime(DateUtil.getDateline());
            historyDO.setUpdateTime(getDateDay());
            historyManager.edit(historyDO, historyDO.getId());
            return;
        }
        //校验如果从会员足迹已经超过一百个，需要删除历史最早的
        String sql = "select count(1) from es_history where member_id = ?";
        Integer count = this.daoSupport.queryForInt(sql, memberId);
        if (count >= 100) {
            this.historyManager.delete(historyDTO.getMemberId());
        }
        //获取当前会员
        Member member = memberClient.getModel(historyDTO.getMemberId());
        //如果当前会员不存在，则不记录信息
        if (member == null) {
            return;
        }
        historyDO = new HistoryDO();
        historyDO.setGoodsImg(cacheGoods.getThumbnail());
        historyDO.setGoodsName(cacheGoods.getGoodsName());
        historyDO.setGoodsId(historyDTO.getGoodsId());
        historyDO.setGoodsPrice(cacheGoods.getPrice());
        historyDO.setMemberId(memberId);
        historyDO.setMemberName(member.getUname());
        historyDO.setCreateTime(DateUtil.getDateline());
        historyDO.setUpdateTime(getDateDay());
        this.daoSupport.insert(historyDO);
    }

    /**
     * 获取当前天的时间戳
     *
     * @return 当前天的时间戳
     */
    private static long getDateDay() {
        String res = DateUtil.toString(new Date(), "yyyy-MM-dd");
        return DateUtil.getDateline(res);
    }
}
