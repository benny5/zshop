package com.enation.app.javashop.core.goods.model.dos;

import com.enation.app.javashop.core.goods.model.dto.GoodsDTO;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.enation.app.javashop.framework.util.StringUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 商品实体
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-21 11:23:10
 */
@Table(name = "es_goods")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class GoodsDO implements Serializable {

	private static final long serialVersionUID = 9115135430405642L;

	/** 主键 */
	@Id(name = "goods_id")
	@ApiModelProperty(hidden = true)
	private Integer goodsId;
	/** 商品名称 */
	@Column(name = "goods_name")
	@ApiModelProperty(name = "goods_name", value = "商品名称", required = false)
	private String goodsName;
	/** 商品编号 */
	@Column(name = "sn")
	@ApiModelProperty(name = "sn", value = "商品编号", required = false)
	private String sn;
	/** 品牌id */
	@Column(name = "brand_id")
	@ApiModelProperty(name = "brand_id", value = "品牌id", required = false)
	private Integer brandId;
	/** 分类id */
	@Column(name = "category_id")
	@ApiModelProperty(name = "category_id", value = "分类id", required = false)
	private Integer categoryId;
	/** 商品类型normal普通point积分 */
	@Column(name = "goods_type")
	@ApiModelProperty(name = "goods_type", value = "商品类型NORMAL普通,POINT积分,VIRTUAL虚拟商品", required = false)
	private String goodsType;
	/** 重量 */
	@Column(name = "weight")
	@ApiModelProperty(name = "weight", value = "重量", required = false)
	private Double weight;
	/** 上架状态 1上架 0下架 */
	@Column(name = "market_enable")
	@ApiModelProperty(name = "market_enable", value = "上架状态 1上架  0下架", required = false)
	private Integer marketEnable;
	/** 详情 */
	@Column(name = "intro")
	@ApiModelProperty(name = "intro", value = "详情", required = false)
	private String intro;
	/** 商品价格 */
	@Column(name = "price")
	@ApiModelProperty(name = "price", value = "商品价格", required = false)
	private Double price;
	/** 成本价格 */
	@Column(name = "cost")
	@ApiModelProperty(name = "cost", value = "成本价格", required = false)
	private Double cost;
	/** 市场价格 */
	@Column(name = "mktprice")
	@ApiModelProperty(name = "mktprice", value = "市场价格", required = false)
	private Double mktprice;
	/** 商品卖点 */
	@Column(name = "selling")
	@ApiModelProperty(name = "selling", value = "商品卖点", required = false)
	private String selling;


	/** 是否有规格0没有 1有 */
	@Column(name = "have_spec")
	@ApiModelProperty(name = "have_spec", value = "是否有规格0没有 1有", required = false)
	private Integer haveSpec;
	/** 创建时间 */
	@Column(name = "create_time")
	@ApiModelProperty(name = "create_time", value = "创建时间", required = false)
	private Long createTime;
	/** 最后修改时间 */
	@Column(name = "last_modify")
	@ApiModelProperty(name = "last_modify", value = "最后修改时间", required = false)
	private Long lastModify;
	/** 浏览数量 */
	@Column(name = "view_count")
	@ApiModelProperty(name = "view_count", value = "浏览数量", required = false)
	private Integer viewCount;
	/** 购买数量 */
	@Column(name = "buy_count")
	@ApiModelProperty(name = "buy_count", value = "购买数量", required = false)
	private Integer buyCount;
	/** 是否被删除0 删除 1未删除 */
	@Column(name = "disabled")
	@ApiModelProperty(name = "disabled", value = "是否被删除0 删除 1未删除", required = false)
	private Integer disabled;
	/** 库存 */
	@Column(name = "quantity")
	@ApiModelProperty(name = "quantity", value = "库存", required = false)
	private Integer quantity;
	/** 可用库存 */
	@Column(name = "enable_quantity")
	@ApiModelProperty(name = "enable_quantity", value = "可用库存", required = false)
	private Integer enableQuantity;
	/** 如果是积分商品需要使用的积分 */
	@Column(name = "point")
	@ApiModelProperty(name = "point", value = "如果是积分商品需要使用的积分", required = false)
	private Integer point;
	/** seo标题 */
	@Column(name = "page_title")
	@ApiModelProperty(name = "page_title", value = "seo标题", required = false)
	private String pageTitle;
	/** seo关键字 */
	@Column(name = "meta_keywords")
	@ApiModelProperty(name = "meta_keywords", value = "seo关键字", required = false)
	private String metaKeywords;
	/** seo描述 */
	@Column(name = "meta_description")
	@ApiModelProperty(name = "meta_description", value = "seo描述", required = false)
	private String metaDescription;
	/** 商品好评率 */
	@Column(name = "grade")
	@ApiModelProperty(name = "grade", value = "商品好评率", required = false)
	private Double grade;
	/** 缩略图路径 */
	@Column(name = "thumbnail")
	@ApiModelProperty(name = "thumbnail", value = "缩略图路径", required = false)
	private String thumbnail;
	/** 大图路径 */
	@Column(name = "big")
	@ApiModelProperty(name = "big", value = "大图路径", required = false)
	private String big;
	/** 小图路径 */
	@Column(name = "small")
	@ApiModelProperty(name = "small", value = "小图路径", required = false)
	private String small;
	/** 原图路径 */
	@Column(name = "original")
	@ApiModelProperty(name = "original", value = "原图路径", required = false)
	private String original;
	/** 卖家id */
	@Column(name = "seller_id")
	@ApiModelProperty(name = "seller_id", value = "卖家id", required = false)
	private Integer sellerId;
	/** 店铺分类id */
	@Column(name = "shop_cat_id")
	@ApiModelProperty(name = "shop_cat_id", value = "店铺分类id", required = false)
	private Integer shopCatId;
	/** 店铺多分组 */
	@Column(name = "shop_cat_items")
	@ApiModelProperty(name = "shop_cat_items", value = "店铺多分组", required = false)
	private String shopCatItems;
	/** 评论数量 */
	@Column(name = "comment_num")
	@ApiModelProperty(name = "comment_num", value = "评论数量", required = false)
	private Integer commentNum;
	/** 运费模板id */
	@Column(name = "template_id")
	@ApiModelProperty(name = "template_id", value = "运费模板id", required = false)
	private Integer templateId;
	/** 谁承担运费0：买家承担，1：卖家承担 */
	@Column(name = "goods_transfee_charge")
	@ApiModelProperty(name = "goods_transfee_charge", value = "谁承担运费0：买家承担，1：卖家承担", required = false)
	private Integer goodsTransfeeCharge;
	/** 卖家名字 */
	@Column(name = "seller_name")
	@ApiModelProperty(name = "seller_name", value = "卖家名字", required = false)
	private String sellerName;
	/** 0 需要审核 并且待审核，1 不需要审核 2需要审核 且审核通过 3 需要审核 且审核未通过 */
	@Column(name = "is_auth")
	@ApiModelProperty(name = "is_auth", value = "0 待审核，1 审核通过 2 未通过", required = false)
	private Integer isAuth;
	/** 审核信息 */
	@Column(name = "auth_message")
	@ApiModelProperty(name = "auth_message", value = "审核信息", required = false)
	private String authMessage;

	/** 下架原因 */
	@Column(name = "under_message")
	@ApiModelProperty(name = "under_message", value = "下架原因", required = false)
	private String underMessage;

	@Column(name = "self_operated")
	@ApiModelProperty(name = "self_operated", value = "是否自营0否 1是", required = false)
	private Integer selfOperated;
	/**店铺纬度*/
	@Column(name = "shop_lat")
	@ApiModelProperty(name="shop_lat",value="店铺纬度",required=false)
	private Double shopLat;
	/**店铺经度*/
	@Column(name = "shop_lng")
	@ApiModelProperty(name="shop_lng",value="店铺经度",required=false)
	private Double shopLng;

	@Column(name = "is_local")
	@ApiModelProperty(value = "是否本地商品")
	private Integer isLocal;

	@Column(name = "is_global")
	@ApiModelProperty(value = "是否商城商品")
	private Integer isGlobal;


	@Column(name = "local_template_id")
	@ApiModelProperty(value = "同城配送模板id")
	private Integer localTemplateId;

	@Column(name = "is_self_take")
	@ApiModelProperty(name = "is_self_take", value = "是否自提", required = false)
	private Integer isSelfTake;

	@Column(name = "freight_pricing_way")
	@ApiModelProperty(name = "freight_pricing_way", value = "运费定价方式 1统一价 2模板", required = false)
	private Integer freightPricingWay;

	/** 运费一口价 */
	@Column(name = "freight_unified_price")
	@ApiModelProperty(name = "freight_unified_price", value = "运费统一价", required = false)
	private Double freightUnifiedPrice;

	@Column(name = "video_url")
	@ApiModelProperty(name = "video_url", value = "主图视频", required = false)
	private String videoUrl;

	@Column(name = "supplier_name")
	@ApiModelProperty(name = "supplier_name", value = "供应商姓名")
	private String  supplierName;

	@Column(name = "up_goods_id")
	@ApiModelProperty(name = "up_goods_id", value = "上游商品id")
	private String upGoodsId;

	@Column(name = "store")
	@ApiModelProperty(name = "store",value = "贮藏要求")
	private Integer store;

	@Column(name = "pre_sort")
	@ApiModelProperty(name = "pre_sort",value = "是否预分拣")
	private Integer preSort;


	@Column(name = "expiry_day")
	@ApiModelProperty(name = "expiry_day",value = "虚拟商品的使用截止日期")
	private Integer expiryDay;

	@Column(name = "available_date")
	@ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
	private String availableDate;


	public GoodsDO(GoodsDTO goodsVO) {

		this.goodsId = goodsVO.getGoodsId();
		this.categoryId = goodsVO.getCategoryId();
		this.shopCatId = goodsVO.getShopCatId();
		this.brandId = goodsVO.getBrandId();
		this.goodsName = goodsVO.getGoodsName();
		this.sn = goodsVO.getSn();
		this.price = goodsVO.getPrice();
		this.cost = goodsVO.getCost();
		this.mktprice = goodsVO.getMktprice();
		this.weight = goodsVO.getWeight();
		this.goodsTransfeeCharge = goodsVO.getGoodsTransfeeCharge();
		this.intro = goodsVO.getIntro();
		this.haveSpec = goodsVO.getHaveSpec();
		this.templateId = goodsVO.getTemplateId()==null || goodsVO.getTemplateId()==0?null:goodsVO.getTemplateId();
		this.pageTitle = goodsVO.getPageTitle();
		this.metaKeywords = goodsVO.getMetaKeywords();
		this.metaDescription = goodsVO.getMetaDescription();
		this.marketEnable = goodsVO.getMarketEnable() != 1 ? 0 : 1;
		this.isLocal = goodsVO.getIsLocal()?1:0;
		this.isGlobal = goodsVO.getIsGlobal()?1:0;
		this.isSelfTake = goodsVO.getIsSelfTake()?1:0;
		this.freightPricingWay = goodsVO.getFreightPricingWay();
		this.freightUnifiedPrice = goodsVO.getFreightUnifiedPrice();
		this.store = goodsVO.getStore();
		this.preSort = goodsVO.getPreSort();
		this.goodsType = goodsVO.getGoodsType();
		this.expiryDay = goodsVO.getExpiryDay();
		this.availableDate = goodsVO.getAvailableDate();

		Integer localTemplateId = goodsVO.getLocalTemplateId();
		if(localTemplateId!=null && localTemplateId==0){
			localTemplateId=null;
		}

		this.localTemplateId = localTemplateId;
		this.videoUrl = goodsVO.getVideoUrl();
		this.selling = goodsVO.getSelling();
		this.supplierName = goodsVO.getSupplierName();
		this.upGoodsId = goodsVO.getUpGoodsId();

		if (StringUtil.isEmpty(this.pageTitle)) {
			this.pageTitle = goodsVO.getGoodsName();
		}
		if (StringUtil.isEmpty(this.metaKeywords)) {
			this.metaKeywords = goodsVO.getGoodsName();
		}
		if (StringUtil.isEmpty(this.metaDescription)) {
			this.metaDescription = goodsVO.getGoodsName();
		}
	}

	public GoodsDO() {
	}

	public static void main(String[] args) {
		Integer i =null;
		if(i==0){
			System.out.println("ceshi");
		}
	}
}
