package com.enation.app.javashop.core.trade.order.model.dos;

import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 订单序列号
 *
 * @author xlg
 * 2020-09-11 10:10:37
 */
@Table(name = "es_sorting_serial")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class SortingSerial implements Serializable {

    @Id(name = "order_sn")
    @ApiModelProperty(name = "order_sn", value = "订单号", required = false)
    private String orderSn;

    @Id(name = "sn")
    @ApiModelProperty(name = "sn", value = "分拣序列号", required = false)
    private Integer sn;

    @Id(name = "status")
    @ApiModelProperty(name = "status", value = "10-代分拣,20-已分拣", required = false)
    private Integer status;

    @Id(name = "bat_no")
    @ApiModelProperty(name = "bat_no", value = "批次", required = false)
    private Integer batNo;

}
