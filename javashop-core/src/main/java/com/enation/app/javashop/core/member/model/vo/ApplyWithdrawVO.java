package com.enation.app.javashop.core.member.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;


/**
 * 申请提现
 * @author 孙建
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApplyWithdrawVO {

    /**提现金额**/
    @ApiModelProperty(name="apply_money",value="提现金额",required=true)
    @NotEmpty(message="提现金额必填")
    private Double applyMoney;

    /**申请备注**/
    @ApiModelProperty(name="apply_remark",value="申请备注",required=false)
    private String applyRemark;

    /**开户银行支行名称*/
    @ApiModelProperty(name="bank_name",value="开户银行支行名称",required=true)
    @NotEmpty(message="开户银行支行名称必填")
    private String bankName;

    /**银行开户名*/
    @ApiModelProperty(name="bank_account_name",value="银行开户名",required=true)
    @NotEmpty(message="银行开户名必填")
    private String bankAccountName;

    /**银行开户账号*/
    @ApiModelProperty(name="bank_number",value="银行开户账号",required=true)
    @NotEmpty(message="银行开户账号必填")
    private String bankNumber;

    /** 验证码 */
    @ApiModelProperty(name="captcha",value="银行开户账号", required=true)
    @NotEmpty(message="验证码必填")
    private String captcha;

    public Double getApplyMoney() {
        return applyMoney;
    }

    public void setApplyMoney(Double applyMoney) {
        this.applyMoney = applyMoney;
    }

    public String getApplyRemark() {
        return applyRemark;
    }

    public void setApplyRemark(String applyRemark) {
        this.applyRemark = applyRemark;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}