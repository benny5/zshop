package com.enation.app.javashop.core.promotion.shetuan.service;

import com.enation.app.javashop.core.promotion.shetuan.model.ShetuanVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsDO;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;


public interface ShetuanOperateManager {


    ShetuanDO  offLine(Integer shetuanId);

    ShetuanDO  onLine(Integer shetuanId);

    List<ShetuanGoodsDO> updateShetuanGoodsIndex(Integer shetuanId);

    void updateBuyRecord(List<OrderSkuVO> list, Integer memberId);
}
