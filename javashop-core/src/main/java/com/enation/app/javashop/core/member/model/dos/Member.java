
package com.enation.app.javashop.core.member.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.enation.app.javashop.framework.validation.annotation.Mobile;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * 会员表实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 14:27:48
 */
@Data
@Table(name = "es_member")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Member implements Serializable {

    private static final long serialVersionUID = 3227466962489949L;
    /**
     * 会员ID
     */
    @Id(name = "member_id")
    @ApiModelProperty(hidden = true)
    private Integer memberId;
    /**
     * 会员真实姓名
     */
    @Column(name = "real_name")
    @ApiModelProperty(name = "real_name", value = "会员真实姓名")
    private String realName;
    /**
     * 会员登录用户名
     */
    @Column(name = "uname")
    @NotEmpty(message = "会员登录用户名不能为空")
    @ApiModelProperty(name = "uname", value = "会员登录用户名", required = true)
    private String uname;
    /**
     * 邮箱
     */
    @Column(name = "email")
    @Email(message = "格式不正确")
    @ApiModelProperty(name = "email", value = "邮箱", required = false)
    private String email;
    /**
     * 会员登录密码
     */
    @Column(name = "password")
    @ApiModelProperty(name = "password", value = "会员登录密码", required = false)
    private String password;
    /**
     * 会员注册时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(name = "createTime", value = "会员注册时间", required = false)
    private Long createTime;
    /**
     * 会员性别
     */
    @Column(name = "sex")
    @Min(message = "必须为数字,1", value = 0)
    @ApiModelProperty(name = "sex", value = "会员性别,1为男，0为女", required = false)
    private Integer sex;
    /**
     * 会员生日
     */
    @Column(name = "birthday")
    @ApiModelProperty(name = "birthday", value = "会员生日", required = false)
    private Long birthday;
    /**
     * 所属省份ID
     */
    @Column(name = "province_id")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "province_id", value = "所属省份ID", required = false)
    private Integer provinceId;
    /**
     * 所属城市ID
     */
    @Column(name = "city_id")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "city_id", value = "所属城市ID", required = false)
    private Integer cityId;
    /**
     * 所属县(区)ID
     */
    @Column(name = "county_id")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "county_id", value = "所属县(区)ID", required = false)
    private Integer countyId;
    /**
     * 所属城镇ID
     */
    @Column(name = "town_id")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "town_id", value = "所属城镇ID", required = false)
    private Integer townId;
    /**
     * 所属省份名称
     */
    @Column(name = "province")
    @ApiModelProperty(name = "province", value = "所属省份名称", required = false)
    private String province;
    /**
     * 所属城市名称
     */
    @Column(name = "city")
    @ApiModelProperty(name = "city", value = "所属城市名称", required = false)
    private String city;
    /**
     * 所属县(区)名称
     */
    @Column(name = "county")
    @ApiModelProperty(name = "county", value = "所属县(区)名称", required = false)
    private String county;
    /**
     * 所属城镇名称
     */
    @Column(name = "town")
    @ApiModelProperty(name = "town", value = "所属城镇名称", required = false)
    private String town;
    /**
     * 详细地址
     */
    @Column(name = "address")
    @ApiModelProperty(name = "address", value = "详细地址", required = false)
    private String address;
    /**
     * 纬度
     */
    @Column(name = "lat")
    @ApiModelProperty(name = "lat", value = "纬度", required = false)
    private Double lat;
    /**
     * 经度
     */
    @Column(name = "lng")
    @ApiModelProperty(name = "lng", value = "经度", required = false)
    private Double lng;
    /**
     * 手机号码
     */
    @Column(name = "mobile")
    @NotEmpty(message = "手机号码不能为空")
    @Mobile
    @ApiModelProperty(name = "mobile", value = "手机号码", required = true)
    private String mobile;
    /**
     * 座机号码
     */
    @Column(name = "tel")
    @ApiModelProperty(name = "tel", value = "座机号码", required = false)
    private String tel;
    /**
     * 等级积分
     */
    @Column(name = "grade_point")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "grade_point", value = "等级积分", required = false)
    private Integer gradePoint;

    /**
     * 消费积分
     */
    @Column(name = "consum_point")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "consum_point", value = "消费积分", required = false)
    private Integer consumPoint;
    /**
     * 会员MSN
     */
    @Column(name = "msn")
    @ApiModelProperty(name = "msn", value = "会员MSN", required = false)
    private String msn;
    /**
     * 会员备注
     */
    @Column(name = "remark")
    @ApiModelProperty(name = "remark", value = "会员备注", required = false)
    private String remark;
    /**
     * 上次登录时间
     */
    @Column(name = "last_login")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "last_login", value = "上次登录时间", required = false)
    private Long lastLogin;
    /**
     * 登录次数
     */
    @Column(name = "login_count")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "login_count", value = "登录次数", required = false)
    private Integer loginCount;
    /**
     * 邮件是否已验证
     */
    @Column(name = "is_cheked")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "is_cheked", value = "邮件是否已验证", required = false)
    private Integer isCheked;
    /**
     * 注册IP地址
     */
    @Column(name = "register_ip")
    @ApiModelProperty(name = "register_ip", value = "注册IP地址", required = false)
    private String registerIp;
    /**
     * 是否已经完成了推荐积分
     */
    @Column(name = "recommend_point_state")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "recommend_point_state", value = "是否已经完成了推荐积分", required = false)
    private Integer recommendPointState;
    /**
     * 会员信息是否完善
     */
    @Column(name = "info_full")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "info_full", value = "会员信息是否完善，0为未完善", required = false)
    private Integer infoFull;
    /**
     * find_code
     */
    @Column(name = "find_code")
    @ApiModelProperty(name = "find_code", value = "find_code", required = false)
    private String findCode;
    /**
     * 会员头像
     */
    @Column(name = "face")
    @ApiModelProperty(name = "face", value = "会员头像", required = false)
    private String face;
    /**
     * 身份证号
     */
    @Column(name = "midentity")
    @ApiModelProperty(name = "midentity", value = "身份证号", required = false)
    private String midentity;
    /**
     * 会员状态
     */
    @Column(name = "disabled")
    @ApiModelProperty(name = "disabled", value = "会员状态,0为正常 -1为在回收站中", required = false)
    private Integer disabled;
    /**
     * 店铺ID
     */
    @Column(name = "shop_id")
    @ApiModelProperty(name = "shop_id", value = "店铺ID", required = false)
    private Integer shopId;
    /**
     * 是否开通店铺
     */
    @Column(name = "have_shop")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "have_shop", value = "是否开通店铺,0为没有开通，1为开通", required = false)
    private Integer haveShop;

    /**
     * 昵称
     */
    @Column(name = "nickname")
    @ApiModelProperty(name = "nickname", value = "昵称", required = false)
    private String nickname;

    /**
     * 账户会员id
     */
    @Column(name = "account_member_id")
    @ApiModelProperty(name = "accountMemberId", value = "账户会员id", required = false)
    private String accountMemberId;

    /**
     * 会员上级团长姓名
     */
    @ApiModelProperty(name = "memberRealNameLv1", value = "会员上级团长姓名", required = false)
    private String memberRealNameLv1;
}