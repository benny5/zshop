package com.enation.app.javashop.core.geo.service;


import com.enation.app.javashop.core.geo.model.GeoCode;
import com.enation.app.javashop.core.geo.model.ParseLatLngResponse;
import com.enation.app.javashop.core.geo.model.Pois;
import com.enation.app.javashop.core.geo.model.RegeoCode;
import com.enation.app.javashop.core.goods.model.vo.BuyCountVO;

import java.util.List;

/**
 * @author 96556
 * @since 2019-06-30
 */
public interface GaodeManager {
    GeoCode parseAddress(String address, String cityName);

    RegeoCode parseLatLng(Double lng, Double lat);

    List<Pois> inputtips(String keyword);

    double countDrivingDistance(String origin, String destination);
}
