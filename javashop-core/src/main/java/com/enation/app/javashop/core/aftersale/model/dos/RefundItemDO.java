package com.enation.app.javashop.core.aftersale.model.dos;

import com.enation.app.javashop.core.aftersale.model.enums.RefundItemStatusEnum;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 退款单详情实体
 * @author 王志杨
 * @since 2020年10月23日 18:53:43
 */
@Table(name="es_refund_item")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RefundItemDO implements Serializable {

    private static final long serialVersionUID = 7198738238019890963L;

    /**退款单详情id*/
    @Id(name = "item_id")
    @ApiModelProperty(hidden=true)
    private Integer itemId;

    /**退(货)款单id*/
    @Column(name = "refund_id")
    @ApiModelProperty(name="refund_id",value="退(货)款单id",required=false)
    private Integer refundId;

    /**退货(款)单编号*/
    @Column(name = "refund_sn")
    @ApiModelProperty(name="refund_sn",value="退货(款)单编号",required=false)
    private String refundSn;

    /**会员id*/
    @Column(name = "member_id")
    @ApiModelProperty(name="member_id",value="会员id",required=false)
    private Integer memberId;

    /**会员名称*/
    @Column(name = "member_name")
    @ApiModelProperty(name="member_name",value = "会员名称",required=false)
    private String memberName;

    /**交易编号*/
    @Column(name = "trade_sn")
    @ApiModelProperty(name="trade_sn",value="交易编号",required=false)
    private String tradeSn;

    /**订单编号*/
    @Column(name = "order_sn")
    @ApiModelProperty(name="order_sn",value="订单编号",required=false)
    private String orderSn;

    /**退款详情单状态*/
    @Column(name = "item_status")
    @ApiModelProperty(name="item_status",value="退款详情单状态",required=false)
    private String itemStatus;

    /**创建时间*/
    @Column(name = "create_time")
    @ApiModelProperty(name="create_time",value="创建时间",required=false)
    private Long createTime;

    /**退款金额*/
    @Column(name = "refund_price")
    @ApiModelProperty(name="refund_price",value="退款金额",required=false)
    private Double refundPrice;

    /**支付结果交易号*/
    @Column(name = "pay_order_no")
    @ApiModelProperty(name="pay_order_no",value="支付结果交易号",required=false)
    private String payOrderNo;

    /**退款（支付）插件ID*/
    @Column(name = "payment_plugin_id")
    @ApiModelProperty(name="payment_plugin_id",value="退款（支付）插件ID",required=false)
    private String paymentPluginId;

    @Column(name = "refund_time")
    @ApiModelProperty(name = "refund_time",value = "退款时间",hidden = true)
    private Long refundTime;

    @Column(name = "refund_fail_reason")
    @ApiModelProperty(name = "refund_fail_reason",value = "退款失败原因",required = false)
    private String refundFailReason;

    @PrimaryKeyField
    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getRefundId() {
        return refundId;
    }

    public void setRefundId(Integer refundId) {
        this.refundId = refundId;
    }

    public String getRefundSn() {
        return refundSn;
    }

    public void setRefundSn(String refundSn) {
        this.refundSn = refundSn;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getTradeSn() {
        return tradeSn;
    }

    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Double getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Double refundPrice) {
        this.refundPrice = refundPrice;
    }

    public String getPayOrderNo() {
        return payOrderNo;
    }

    public void setPayOrderNo(String payOrderNo) {
        this.payOrderNo = payOrderNo;
    }

    public String getPaymentPluginId() {
        return paymentPluginId;
    }

    public void setPaymentPluginId(String paymentPluginId) {
        this.paymentPluginId = paymentPluginId;
    }

    public Long getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Long refundTime) {
        this.refundTime = refundTime;
    }

    public String getRefundFailReason() {
        return refundFailReason;
    }

    public void setRefundFailReason(String refundFailReason) {
        this.refundFailReason = refundFailReason;
    }

    public RefundItemDO() {
    }

    public RefundItemDO(RefundDO refundDO) {
        this.refundId = refundDO.getId();
        this.refundSn = refundDO.getSn();
        this.memberId = refundDO.getMemberId();
        this.memberName = refundDO.getMemberName();
        this.tradeSn = refundDO.getTradeSn();
        this.orderSn = refundDO.getOrderSn();
        this.itemStatus = RefundItemStatusEnum.APPLY.value();
        this.createTime = DateUtil.getDateline();
    }
}