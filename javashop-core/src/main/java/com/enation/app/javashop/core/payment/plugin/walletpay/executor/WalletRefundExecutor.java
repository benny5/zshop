package com.enation.app.javashop.core.payment.plugin.walletpay.executor;

import com.dag.eagleshop.core.account.model.dto.refund.UnifiedRefundReqDTO;
import com.dag.eagleshop.core.account.model.dto.refund.UnifiedRefundRespDTO;
import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;
import com.dag.eagleshop.core.account.model.enums.TradeSubjectEnum;
import com.dag.eagleshop.core.account.service.AccountRefundManager;
import com.enation.app.javashop.core.aftersale.model.dos.RefundItemDO;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.enums.PaymentPluginEnum;
import com.enation.app.javashop.core.payment.model.vo.RefundBill;
import com.enation.app.javashop.core.payment.service.AbstractPaymentPlugin;
import com.enation.app.javashop.core.payment.service.PaymentBillManager;
import com.enation.app.javashop.core.payment.service.RefundItemManager;
import com.enation.app.javashop.core.trade.order.service.OrderQueryManager;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.security.model.Buyer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;


/**
 * @author 王志杨
 * @since 2020/10/22 15:54
 * 钱包退款执行器
 */
@Service
public class WalletRefundExecutor extends AbstractPaymentPlugin {
    
    @Autowired
    private Cache cache;
    @Autowired
    private AccountRefundManager accountRefundManager;
    @Autowired
    private RefundItemManager refundItemManager;
    @Autowired
    private MemberManager memberManager;
    /**
     * 原路退回
     *
     * @param bill 原路退款使用vo
     */
    public boolean returnPay(RefundBill bill) {

        try {
            RefundItemDO refundItem = refundItemManager.queryRefundItemByPayOrderNo(bill.getReturnTradeNo(), bill.getRefundSn());
            if (refundItem == null) {
                throw new RuntimeException("钱包退款失败：returnTradeNo=" +bill.getReturnTradeNo()+ "，未查询到退款单明细");
            }
            Member member = memberManager.getModel(refundItem.getMemberId());
            if (member == null) {
                throw new RuntimeException("钱包退款失败：returnTradeNo=" +bill.getReturnTradeNo()+ "，未查询到用户信息");
            }
            UnifiedRefundReqDTO unifiedRefundReqDTO = this.buildUnifiedRefundReqDTO(bill, member);

            UnifiedRefundRespDTO unifiedRefundRespDTO = accountRefundManager.unifiedRefund(unifiedRefundReqDTO);
            boolean isSuccess = unifiedRefundRespDTO.isSuccess();
            if(!isSuccess){
                //退款流水号
                String serialNo = unifiedRefundRespDTO.getSerialNo();
                //退款错误描述
                String message = unifiedRefundRespDTO.getMessage();
                String failReason = "请联系管理员并提供退款错误信息：退款流水号：" + serialNo+"," + message;

                this.cache.put(REFUND_ERROR_MESSAGE+"_"+bill.getRefundSn(), failReason, 60);
            }
            return isSuccess;
        } catch (RuntimeException e) {
            this.logger.error("钱包退款失败", e);
            this.cache.put(REFUND_ERROR_MESSAGE+"_"+bill.getRefundSn(), "钱包退款异常", 60);
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 查询退款状态
     *
     * @param bill 原路退款使用vo
     */
    public String queryRefundStatus(RefundBill bill) {
        return "";
    }

    private UnifiedRefundReqDTO buildUnifiedRefundReqDTO(RefundBill bill,  Member member){
        UnifiedRefundReqDTO unifiedRefundReqDTO = new UnifiedRefundReqDTO();
        unifiedRefundReqDTO.setSerialNo(bill.getReturnTradeNo());
        unifiedRefundReqDTO.setTradeVoucherNo(bill.getRefundSn());
        unifiedRefundReqDTO.setRefundMemberId(member.getAccountMemberId());
        unifiedRefundReqDTO.setAccountType(AccountTypeEnum.MEMBER_MASTER.getIndex());
        unifiedRefundReqDTO.setAmount(BigDecimal.valueOf(bill.getRefundPrice()));
        unifiedRefundReqDTO.setOperatorId(member.getMemberId().toString());
        unifiedRefundReqDTO.setOperatorName(member.getNickname());
        unifiedRefundReqDTO.setTradeSubject(TradeSubjectEnum.ORDER_REFUND_WALLET.description());
        return unifiedRefundReqDTO;
    }

    @Override
    protected String getPluginId() {
        return PaymentPluginEnum.walletPayPlugin.toString();
    }
}
