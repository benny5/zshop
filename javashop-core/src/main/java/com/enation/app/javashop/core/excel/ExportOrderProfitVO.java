package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.enation.app.javashop.framework.database.annotation.Column;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 佣金明细导出
 * @author xlg
 * 2020/10/28 10:59
 */
@Data
public class ExportOrderProfitVO {

    @ExcelProperty(value = "佣金单号" )
    private String profitNo;

    @ExcelProperty(value = "佣金状态" )
    private String orderStatusName;

    @ExcelProperty(value = "订单类型" )
    private String orderTypeName;

    @ExcelProperty(value = "佣金类型" )
    String commissionTypeName;

    @ExcelProperty(value = "推广类型" )
    String spreadWay;

    @ExcelProperty(value = "收益用户" )
    private String memberName;

    @ExcelProperty(value = "订单号" )
    private String orderSn;

    @ExcelProperty(value = "销售金额" )
    private BigDecimal orderPrice;

    @ExcelProperty(value = "提成比例" )
    private String commissionRate;

    @ExcelProperty(value = "提成金额" )
    private BigDecimal commissionMoney;

    @ExcelProperty(value = "提成时间" )
    private String createTime;

    @ExcelProperty(value = "发放时间" )
    private String payTime;

    @ExcelProperty(value = "作废时间" )
    private String cancleTime;

    @ExcelProperty(value = "所属商户" )
    private String sellerName;

    @ExcelProperty(value = "备注" )
    private String remark;

}
