package com.enation.app.javashop.core.trade.order.model.vo;

import com.enation.app.javashop.core.system.model.vo.ExpressDetailVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderItemsDO;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/7/22 18:09
 */

@Data
public class OrderPackageVO {
    // 包裹商品
    private List<OrderItemsDO> orderItems;
    // 物流信息
    private ExpressDetailVO expressDetail;

}
