package com.enation.app.javashop.core.shop.service;

import com.enation.app.javashop.core.shop.model.dto.QueryNearbyShopDTO;
import com.enation.app.javashop.core.shop.model.dto.NearbyShopCatGoodsDTO;
import com.enation.app.javashop.core.shop.model.vo.NearbyShopCatGoodsVO;
import com.enation.app.javashop.core.shop.model.vo.NearbyShopDetailsVO;
import com.enation.app.javashop.core.shop.model.vo.NearbyShopVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/3/10
 * @Description: 附近店铺列表
 */
public interface NearbyShopManager {

    /**
     * 返回附近店铺列表信息
     */
    Page nearbyShopList(QueryNearbyShopDTO queryNearbyShopDTO);

    // 新的店铺详情信息
    NearbyShopDetailsVO nearbyShopDetails(QueryNearbyShopDTO queryNearbyShopDTO);

    // 返回分组商品信息
    List<NearbyShopCatGoodsVO> getGoodsList(Integer sellerId, Integer shopCatId);
}
