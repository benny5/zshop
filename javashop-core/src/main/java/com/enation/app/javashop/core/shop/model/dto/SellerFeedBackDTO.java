package com.enation.app.javashop.core.shop.model.dto;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/8/4 14:33
 */
@Data
public class SellerFeedBackDTO {
    // 建议类型
    private Integer adviceType;

    // 建议内容
    private String content;

    /**
     * 软件问题
     */
    public final static int APP_BUG = 1;

    /**
     * 需求建议
     */
    public final static int BIZ_REQUIRE = 2;

    /**
     * 业务问题
     */
    public final static int BIZ_PROBLEM = 3;

    /**
     * 其他问题
     */
    public final static int OTHERS = 4;
}
