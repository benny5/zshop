package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Data
public class GoodsPriceVO {
    @ApiModelProperty(name="id",value = "商品id")
    @NotEmpty
    private Integer id;

    @ApiModelProperty(name = "price",value = "价格")
    @Min(0)
    private Double price;

    @ApiModelProperty(name = "price_name",value = "价格名称")
    @NotEmpty(message = "价格名称不能为空")
    private String priceName;
}
