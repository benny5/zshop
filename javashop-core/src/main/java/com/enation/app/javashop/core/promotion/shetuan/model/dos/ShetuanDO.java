package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;


/**
 * 社区团购活动
 */
@Table(name = "es_shetuan")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShetuanDO {


    /**
     * id
     */
    @Id(name = "shetuan_id")
    @ApiModelProperty(name = "shetuan_id", value = "社区团购id")
    private Integer shetuanId;
    /**
     * 活动名称
     */
    @Column(name = "shetuan_name")
    @NotEmpty(message = "社区团购名称不能为空")
    @ApiModelProperty(name = "shetuan_name", value = "社区团购名称", required = true)
    private String shetuanName;
    @Column(name = "shetuan_no")
    @ApiModelProperty(name = "shetuan_no", value = "社团编码", required = true)
    private String shetuanNo;
    /**
     * 提货时间
     */
    @Column(name = "pick_time")
    private Long pickTime;
    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Long startTime;

    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Long endTime;

    /**
     * 报名截止时间
     */
    @Column(name = "join_stop_time")
    private Long joinStopTime;

    /**
     * 计划上线时间
     */
    @Column(name = "plan_on_time")
    private Long planOnTime;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 店铺id
     */
    @Column(name = "seller_id")
    private Integer sellerId;


    @Column(name = "province_id")
    private Integer provinceId;
    @Column(name = "city_id")
    private Integer cityId;
    @Column(name = "county_id")
    private Integer countyId;

    @Column(name = "province")
    private String province;
    @Column(name = "city")
    private String city;
    @Column(name = "county")
    private String county;

    @Column(name = "address")
    private String address;

    @Column(name = "description")
    private String description;

    @Column(name="pre")
    private Integer pre;

    @Column(name = "status")
    private Integer status;

    @Column(name = "create_time")
    private Long createTime;

    @Column(name = "update_time")
    private Long updateTime;
    /**
     * 首页是否弹框
     * 1 弹框
     * 0 不弹
     */
    @Column(name = "page_popup")
    private Integer pagePopup;
}
