package com.enation.app.javashop.core.promotion.coupon.service;

import com.enation.app.javashop.core.base.message.MemberRegisterMsg;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.core.promotion.coupon.model.dos.SendCouponDTO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

/**
 * 优惠券业务层
 *
 * @author Snow
 * @version v2.0
 * @since v7.0.0
 * 2018-04-17 23:19:39
 */
public interface CouponManager {

    /**
     * 查询当前商家优惠券列表
     *
     * @param page      页码
     * @param pageSize  每页数量
     * @param startTime 起始时间
     * @param endTime   截止时间
     * @param keyword   搜索关键字
     * @return Page
     */
    Page list(int page, int pageSize, Long startTime, Long endTime, String keyword, String couponType);

    /**
     * 读取商家优惠券，正在进行中的
     *
     * @param sellerId
     * @return
     */
    List<CouponDO> getList(Integer sellerId, String couponType);

    /**
     * 添加优惠券
     *
     * @param coupon 优惠券
     * @return Coupon 优惠券
     */
    CouponDO add(CouponDO coupon);

    /**
     * 修改优惠券
     *
     * @param coupon 优惠券
     * @param id     优惠券主键
     * @return Coupon 优惠券
     */
    CouponDO edit(CouponDO coupon, Integer id);

    /**
     * 删除优惠券
     *
     * @param id 优惠券主键
     */
    void delete(Integer id);

    /**
     * 获取优惠券
     *
     * @param id 优惠券主键
     * @return Coupon  优惠券
     */
    CouponDO getModel(Integer id);

    /**
     * 验证操作权限<br/>
     * 如有问题直接抛出权限异常
     *
     * @param id
     */
    void verifyAuth(Integer id);

    /**
     * 增加优惠券使用数量
     *
     * @param id
     */
    void addUsedNum(Integer id);


    /**
     * 增加被领取数量
     *
     * @param couponId
     */
    void addReceivedNum(Integer couponId);


    /**
     * 查询所有商家优惠券列表
     *
     * @param page     页码
     * @param pageSize 每页数量
     * @param sellerId 商家id
     * @return Page
     */
    Page all(int page, int pageSize, Integer sellerId, String couponType);

    /**
     * 修改店铺名称改变优惠券中的店铺名称
     *
     * @param shopId   店铺id
     * @param shopName 店铺名称
     */
    void editCouponShopName(Integer shopId, String shopName);

    /**
     * 根据失效状态获取优惠券数据集合
     * @param status 失效状态 0：全部，1：有效，2：失效
     * @return
     */
    List<CouponDO> getByStatus(Integer status);


    /**
     * 根据ids查询优惠券
     * @param couponIds
     * @return
     */
    List<CouponDO> getByIds(List<Integer> couponIds);

    void sendCoupon(SendCouponDTO sendCouponDTO);

    /**
     * 给注册的用户发放优惠卷
     */
    void memberRegister(MemberRegisterMsg memberRegisterMsg,Integer userType,String city);

    /**
     * 根据手机号给用户推送优惠卷
     */
    void batchPush(String[] couponIds, String content, String[] mobiles);

    /**
     * 给用户发券
     */
    void  issueVouchersToUsers(Member member, Integer couponId);

    /**
     * 结束优惠券活动
     * @param id 满优惠活动主键
     */
    void endCoupon(Integer id);

    /**
     * 根据优惠券IDs和优惠券类型查询正在进行中优惠券
     * @param couponIds 优惠券IDs
     * @param sellerId 店铺ID
     * @param couponType 优惠券类型
     * @return 优惠券集合
     */
    List<CouponDO> getMarketingCouponsByIds(String couponIds, Integer sellerId, String couponType);
}
