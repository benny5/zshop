package com.enation.app.javashop.core.trade.order.service;

import com.enation.app.javashop.core.aftersale.model.vo.RefundQueryParamVO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderItemsDO;
import com.enation.app.javashop.core.trade.order.model.dos.SortingSerial;
import com.enation.app.javashop.core.trade.order.model.dto.OrderDetailQueryParam;
import com.enation.app.javashop.core.trade.order.model.dto.OrderQueryParam;
import com.enation.app.javashop.core.trade.order.model.dto.ShetuanOrderQueryParam;
import com.enation.app.javashop.core.trade.order.model.enums.ShipStatusEnum;
import com.enation.app.javashop.core.trade.order.model.vo.*;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import com.enation.app.javashop.core.trade.sdk.model.OrderVerificationDTO;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.security.model.Buyer;

import java.util.List;

/**
 * 订单相关
 *
 * @author Snow create in 2018/5/14
 * @version v2.0
 * @since v7.0.0
 */
public interface OrderQueryManager {


    /**
     * 查询订单表列表
     *
     * @param paramDTO 参数对象
     * @return Page
     */
    Page list(OrderQueryParam paramDTO);


    /**
     * 读取一个订单详细<br/>
     * @param orderSn  订单编号 必传
     * @param queryParam  参数
     * @return
     */
    OrderDetailVO getModel(String orderSn,OrderDetailQueryParam queryParam);

    /**
     * 读取一个订单详细
     * @param orderId
     * @return
     */
        OrderDO getModel(Integer orderId);

    /**
     * 读取订单状态的订单数
     *
     * @param memberId
     * @param sellerId
     * @return
     */
    OrderStatusNumVO getOrderStatusNum(Integer memberId, Integer sellerId);


    /**
     * 获取某订单的订单项
     *
     * @param orderSn
     * @return
     */
    List<OrderItemsDO> orderItems(String orderSn);

    OrderItemsDO queryOrderItem(String orderSn, Integer goodsId);

    /**
     * 根据订单sn读取，订单的流程
     *
     * @param orderSn 订单编号
     * @return
     */
    List<OrderFlowNode> getOrderFlow(String orderSn);


    /**
     * 读取会员所有的订单数量
     * @param memberId
     * @return
     */
    Integer getOrderNumByMemberId(Integer memberId);


    /**
     * 读取会员(评论状态)订单数量
     * @param memberId
     * @param commentStatus 评论状态
     * @return
     */
    Integer getOrderCommentNumByMemberId(Integer memberId, String commentStatus);


    /**
     * 读取订单列表根据交易编号——系统内部使用 OrderClient
     * @param tradeSn
     * @return
     */
    List<OrderDetailDTO> getOrderByTradeSn(String tradeSn);


    /**
     * 读取订单列表根据交易编号
     *
     * @param tradeSn
     * @param memberId
     * @return
     */
    List<OrderDetailVO> getOrderByTradeSn(String tradeSn, Integer memberId);

    /**
     * 查询一个订单的详细
     * @param orderSn
     * @return
     */
    OrderDetailDTO getModel(String orderSn);

    /**
     * 获取订单可退款总额
     * @param orderSn 订单编号
     * @return
     */
    double getOrderRefundPrice(String orderSn);

    Page queryRefundOrders(RefundQueryParamVO queryParam);

    Page listOrderDO(OrderQueryParam orderQueryParam);

    List<OrderItemsDO> getOrderItems(List<String> orderSns);

    List<OrderItemsDO> getOrderItems(String orderSn);

    /**
     * @param orderSn 订单号
     * @param skuIds skuIds拼接的字符串
     * @return 订单详情集合
     */
    List<OrderItemsDO> getOrderItems(String orderSn, String skuIds);

    // 查询订单总表
    List<OrderDO> loadOrderList(List<String> orderSns);

    /**
     * 获取今日营业额度
     */
    Double getPayMoney();

    /**
     * 分销订单查询
     * @param paramDTO
     * @return
     */
    Page listDistributionOrder(OrderQueryParam paramDTO);

    List<OrderItemsDO> listOrderSku(OrderQueryParam orderQueryParam);

    /**
     * 订单序列号 对象
     * @param orderSn 订单号
     */
    SortingSerial getSortingSerial(String orderSn);

    /**
     * 订单序列号 对象
     * @param orderSn 订单号
     */
    void updatePrintTimes(String orderSn,Integer printTimes);

    /**
     * 查询订单表联合会员表列表，展示用户昵称
     * @param paramDTO 查询参数对象
     * @return Page
     */
    Page<OrderLineVO> queryOrderAndNicknameList(OrderQueryParam paramDTO);

    /**
     *  读取会员是否下过单
     */
    Integer getOrderNumberByMemberId(Integer memberId,Integer sellerId);

    /**
     * 修改标记首单
     */
    void updateOrderFlag(String orderSn,String orderFlag);

    /**
     * 根据订单号和物流状态查询订单详情（包含商品的物流信息）
     * @param orderSn 订单号
     * @param shipStatus 物流状态
     * @return 订单详情
     */
    List<OrderItemsDO> getOrderItemsByShipStatus(String orderSn, String shipStatus);

    OrderVerificationDTO queryVirtualMessage(String orderSn, Buyer buyer);

    Integer queryVerificationStatus(OrderDO orderDO);

    /**
     * 根据虚拟订单的核销码查询订单详情
     * @param verCode 虚拟订单的核销码
     * @return 订单详情
     */
    OrderDetailVO getOrderDetailByVerCode(String verCode,Integer sellerId);

    //  根据会员集合查询订单
    List<OrderDO> getOrderList(List<String> memberId);
}