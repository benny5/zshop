package com.enation.app.javashop.core.system.model.dto;

/**
 * @author 王志杨
 * @since 2020/11/10 11:53
 * 微信公众号Token实体
 */
public class WeChatPublicToken {

    // 接口访问凭证
    private String accessToken;
    // 凭证有效期，单位：秒
    private int expiresIn;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

}
