package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import io.swagger.models.auth.In;
import lombok.Data;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/5/26
 * @Description:
 */
@Data
public class YiSuPaiOrderDetailDTO {
    // 交易关闭时间
    private String closeTime;
    // 交易完成时间
    private String endTime;
    // 发货时间
    private String sendTime;
    // 支付时间
    private String paymentTime;
    private String orderNo;
    // 货物状态(1=未发货 -> 等待卖家发货 2=已发货 -> 等待买家确认收货 3=已 收货 -> 交易成功 4=已经退货 -> 交易失败
    // 5=部分收货 -> 交易成功 6=部分发货中 8=还未创建 物流订单）
    private Integer logisticsStatus;
    // 实际付款金额,单位是分
    private Integer payment;
    // 运费,单位是分
    private Integer postage;
    // 订单状态:0-已取消，10-未付款，20-已付款，40-已发货，50-交易成功，60-交易关闭
    private Integer status;
    // 子订单列表
    private List<YiSuPaiSubOrder> subOrderList;

}
