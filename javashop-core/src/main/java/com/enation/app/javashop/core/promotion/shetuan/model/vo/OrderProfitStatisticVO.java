package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 收益统计
 */

@Data
public class OrderProfitStatisticVO {
    // 统计时间
    private String ssTime;
    // 订单成交总额
    private Double orderPriceSum=0.0;
    // 佣金总额
    private Double commissionSum=0.0;
    // 订单数量
    private Integer orderNum=0;

    // 订单成交总额展示字段
    private String showOrderPriceSum;
    // 佣金总额展示字段
    private String showCommissionSum;

    public void show() {
        orderPriceSum = orderPriceSum == null ? 0.00 : orderPriceSum;
        commissionSum = commissionSum == null ? 0.00 : commissionSum;

        if(orderPriceSum >= 10000){
            showOrderPriceSum = "￥" + (BigDecimal.valueOf(orderPriceSum).divide(BigDecimal.valueOf(10000)).setScale(2, BigDecimal.ROUND_HALF_DOWN)) + "W";
        }else{
            showOrderPriceSum = "￥" + orderPriceSum;
        }

        if(commissionSum >= 10000){
            showCommissionSum = "￥" + (BigDecimal.valueOf(commissionSum).divide(BigDecimal.valueOf(10000)).setScale(2, BigDecimal.ROUND_HALF_DOWN)) + "W";
        }else{
            showCommissionSum = "￥" + commissionSum;
        }
    }
}
