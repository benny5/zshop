package com.enation.app.javashop.core.distribution.model.vo;

import com.enation.app.javashop.core.distribution.model.dos.DistributionDO;
import com.enation.app.javashop.core.distribution.model.dto.DistributionDTO;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 分销商显示vo
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-05-23 上午9:57
 */
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DistributionVO {

    @ApiModelProperty(value = "当前会员id", name = "id")
    private Integer id;
    @ApiModelProperty(value = "当前会员id", name = "member_id")
    private Integer memberId;
    @ApiModelProperty(value = "lv1id", name = "lv1_id")
    private Integer lv1Id;
    @ApiModelProperty(value = "lv2id", name = "lv2_id")
    private Integer lv2Id;
    @ApiModelProperty(value = "名字")
    private String name;
    @ApiModelProperty(value = "模版名称", name = "current_tpl_name")
    private String currentTplName;
    @ApiModelProperty(value = "模版id", name = "current_tpl_id")
    private Integer currentTplId;
    @ApiModelProperty(value = "下线人数", name = "downline")
    private Integer downline;
    @ApiModelProperty(value = "返利金额")
    private Double rebateTotal;

    @Column(name = "order_num")
    @ApiModelProperty(name = "order_num", value = "提成相关订单数", required = true)
    private Integer orderNum = 0;

    @Column(name = "turnover_price")
    @ApiModelProperty(name = "turnover_price", value = "营业额总额", required = true)
    private Double turnoverPrice = 0D;

    @ApiModelProperty(value = "下线", name = "item")
    private List<DistributionVO> item;

    @ApiModelProperty(value = "结算单", name = "bill_member_vo")
    private BillMemberVO billMemberVO;

    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;

    @ApiModelProperty(name = "audit_status", value = "状态", required = false)
    private Integer auditStatus;

    @ApiModelProperty(name = "status_name", value = "状态名称", required = false)
    private String statusName;

    @ApiModelProperty(name = "apply_reason", value = "申请原因", required = false)
    private String applyReason;

    @ApiModelProperty(name = "real_name", value = "会员真实姓名")
    private String realName;

    @ApiModelProperty(name = "face", value = "会员头像")
    private String face;

    /**
     * 身份证号
     */
    @ApiModelProperty(name = "midentity", value = "身份证号", required = false)
    private String midentity;

    @ApiModelProperty(name = "audit_remark", value = "审核备注", required = false)
    private String auditRemark;

    @ApiModelProperty(name = "distributor_grade", value = "分销等级", required = false)
    private Integer distributorGrade;

    @ApiModelProperty(name = "distributor_title", value = "分销头衔", required = false)
    private String distributorTitle;

    @ApiModelProperty(name = "status", value = "启用/禁用", required = false)
    private Integer status;

    @ApiModelProperty(name = "mobile", value = "手机号", required = false)
    private String mobile;
    /**
     * 昵称(前端所属用户)
     */
    @ApiModelProperty(name="nickname", value="会员昵称",required = false)
    private String nickName;

    @ApiModelProperty(name="auditTime", value="审核时间",required = false)
    private  Long auditTime;

    @ApiModelProperty(name="lv1CreateTime", value="1级关系创建时间",required = false)
    private Long lv1CreateTime;

    @ApiModelProperty(name="lv1ExpireTime", value="1级关系失效时间",required = false)
    private Long lv1ExpireTime;

    @ApiModelProperty(name="lv2ExpireTime", value="2级关系失效时间",required = false)
    private Long lv2ExpireTime;

    @ApiModelProperty(name="lastOrderTime", value="最新下单时间",required = false)
    private Long lastOrderTime;

    @ApiModelProperty(name="keyWords", value="邀请码,姓名,电话",required = false)
    private String keyWords;

    @ApiModelProperty(name="inviteCode", value="分销邀请码",required = false)
    private String inviteCode;

    @ApiModelProperty(name="lv1InviteCode", value="上级邀请码",required = false)
    private String lv1InviteCode;

    @ApiModelProperty(name="lv1RealName", value="上级团长名称",required = false)
    private String lv1RealName;

    @ApiModelProperty(name = "team_name", value = "别名 如 第8团", required = false)
    private String teamName;

    @ApiModelProperty(name = "province_id", value = "省Id", required = false)
    private Integer provinceId;

    @ApiModelProperty(name = "province", value = "省", required = false)
    private String province;

    @ApiModelProperty(name = "city_id", value = "市Id", required = false)
    private Integer cityId;

    @ApiModelProperty(name = "city", value = "市", required = false)
    private String city;

    @ApiModelProperty(name = "county_id", value = "区/县Id", required = false)
    private Integer countyId;

    @ApiModelProperty(name = "county", value = "区/县", required = false)
    private String county;

    @ApiModelProperty(name = "address", value = "详细地址", required = false)
    private String address;

    @ApiModelProperty(name = "settle_mode", value = "结算模式", required = false)
    private Integer settleMode;

    @ApiModelProperty(name = "business_type", value = "业务类型", required = false)
    private Integer businessType;

    @ApiModelProperty(name = "visit_channel", value = "访问渠道", required = false)
    private Integer visitChannel;

    // 描述一
    private String describe1;

    // 描述二
    private String describe2;

    public DistributionVO(){}

    public DistributionVO(DistributionDO ddo) {
        this.id = ddo.getMemberId();
        this.lv1Id = ddo.getMemberIdLv1();
        this.lv2Id = ddo.getMemberIdLv2();
        this.rebateTotal = ddo.getRebateTotal();
        this.downline = ddo.getDownline();
        this.turnoverPrice = ddo.getTurnoverPrice();
        this.name = ddo.getMemberName();
        this.currentTplName = ddo.getCurrentTplName();
        this.orderNum = ddo.getOrderNum();
        this.currentTplId = ddo.getCurrentTplId();
        this.createTime = ddo.getCreateTime();
        this.auditStatus = ddo.getAuditStatus();
        this.applyReason = ddo.getApplyReason();
        this.auditRemark = ddo.getAuditRemark();
        this.settleMode = ddo.getSettleMode();
        this.businessType = ddo.getBusinessType();
        this.visitChannel = ddo.getVisitChannel();
    }


    public DistributionVO(DistributionDTO ddo) {
        this.id = ddo.getMemberId();
        this.lv1Id = ddo.getMemberIdLv1();
        this.lv2Id = ddo.getMemberIdLv2();
        this.rebateTotal = ddo.getRebateTotal();
        this.downline = ddo.getDownline();
        this.turnoverPrice = ddo.getTurnoverPrice();
        this.name = ddo.getMemberName();
        this.currentTplName = ddo.getCurrentTplName();
        this.orderNum = ddo.getOrderNum();
        this.currentTplId = ddo.getCurrentTplId();
        this.createTime = ddo.getCreateTime();
        this.auditStatus = ddo.getAuditStatus();
        this.applyReason = ddo.getApplyReason();
        this.auditRemark = ddo.getAuditRemark();
        this.midentity = ddo.getMidentity();
        this.realName = ddo.getRealName();
        this.distributorGrade = ddo.getDistributorGrade();
        this.distributorTitle = ddo.getDistributorTitle();
        this.status = ddo.getStatus();
        this.mobile = ddo.getMobile();
        this.nickName = ddo.getNickname();
        this.auditTime = ddo.getAuditTime();
        this.lv1CreateTime = ddo.getLv1CreateTime();
        this.lv1ExpireTime = ddo.getLv1ExpireTime();
        this.lv2ExpireTime = ddo.getLv2ExpireTime();
        this.lastOrderTime = ddo.getLastOrderTime();
        this.inviteCode = ddo.getInviteCode();
        this.lv1InviteCode = ddo.getLv1InviteCode();
        this.lv1RealName = ddo.getLv1RealName();
        this.teamName = ddo.getTeamName();
        this.provinceId = ddo.getProvinceId();
        this.province = ddo.getProvince();
        this.cityId = ddo.getCityId();
        this.city = ddo.getCity();
        this.countyId = ddo.getCountyId();
        this.county = ddo.getCounty();
        this.address = ddo.getAddress();
        this.settleMode = ddo.getSettleMode();
        this.businessType = ddo.getBusinessType();
        this.visitChannel = ddo.getVisitChannel();
    }

}
