package com.enation.app.javashop.core.promotion.luck.service.impl;

import com.enation.app.javashop.core.promotion.luck.model.dos.LuckPrizeDO;
import com.enation.app.javashop.core.promotion.luck.service.LuckPrizeManager;
import com.enation.app.javashop.core.promotion.luck.utils.ProbabilityUtils;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.core.trade.order.model.enums.YesOrNoEnums;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.security.model.Seller;
import com.enation.app.javashop.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 孙建
 */
@Service
public class LuckPrizeManagerImpl implements LuckPrizeManager {

    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport daoSupport;

    @Override
    public List<LuckPrizeDO> queryListByLuckId(Integer luckId) {
        return daoSupport.queryForList("select * from es_luck_prize where luck_id = ? and is_delete = 0 order by create_time desc", LuckPrizeDO.class, luckId);
    }

    @Override
    public void addAndUpdateLuckPrize(List<LuckPrizeDO> prizeDOList) {
        if (CollectionUtils.isEmpty(prizeDOList)) {
            return;
        }

        // 校验获奖概率
        List<Double> probabilityList = prizeDOList.stream().map(prizeDO -> prizeDO.getProbability().doubleValue()).collect(Collectors.toList());
        ProbabilityUtils.validate(probabilityList);

        List<LuckPrizeDO> insertList = new ArrayList<>();

        Seller seller = UserContext.getSeller();
        for (LuckPrizeDO luckPrizeDO : prizeDOList) {
            luckPrizeDO.setSellerId(seller.getSellerId());
            luckPrizeDO.setSellerName(seller.getSellerName());

            if (luckPrizeDO.getLuckPrizeId() != null && luckPrizeDO.getLuckPrizeId() != 0) {
                this.updateLuckPrize(luckPrizeDO);
            } else {
                luckPrizeDO.setCreateTime(DateUtil.getDateline());
                luckPrizeDO.setIsDelete(YesOrNoEnums.NO.getIndex());
                insertList.add(luckPrizeDO);
            }
        }

        if (!CollectionUtils.isEmpty(insertList)) {
            this.daoSupport.batchInsert("es_luck_prize", insertList);
        }
    }

    @Override
    public void updateLuckPrize(LuckPrizeDO luckPrizeDO) {
        StringBuilder updateSql = new StringBuilder();

        if (!StringUtil.isEmpty(luckPrizeDO.getPrizeName())) {
            updateSql.append(" prize_name = '" + luckPrizeDO.getPrizeName() + "',");
        }

        if (luckPrizeDO.getProbability() != null) {
            updateSql.append(" probability = " + luckPrizeDO.getProbability() + ",");
        }

        if (luckPrizeDO.getPrizeNum() != null) {
            updateSql.append(" prize_num = " + luckPrizeDO.getPrizeNum() + ",");
        }

        if (luckPrizeDO.getPrizeType() != null && luckPrizeDO.getPrizeType() != 0) {
            updateSql.append(" prize_type = " + luckPrizeDO.getPrizeType() + ",");
        }

        if (!StringUtil.isEmpty(luckPrizeDO.getPrizeImg())) {
            updateSql.append(" prize_img = '" + luckPrizeDO.getPrizeImg() + "',");
        }

        if (luckPrizeDO.getDeliveryType() != null && luckPrizeDO.getDeliveryType() != 0) {
            updateSql.append(" delivery_type = " + luckPrizeDO.getDeliveryType() + ",");
        }

        if (luckPrizeDO.getCouponId() != null && luckPrizeDO.getCouponId() != 0) {
            updateSql.append(" coupon_id = " + luckPrizeDO.getCouponId() + ",");
        }

        if (updateSql.length() > 0) {
            updateSql.insert(0, "update es_luck_prize set ");
            updateSql.deleteCharAt(updateSql.length() - 1);
            updateSql.append(" where luck_prize_id = " + luckPrizeDO.getLuckPrizeId() + ";");
            this.daoSupport.execute(updateSql.toString());
        }
    }

}
