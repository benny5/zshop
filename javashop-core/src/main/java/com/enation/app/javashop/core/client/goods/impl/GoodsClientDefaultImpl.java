package com.enation.app.javashop.core.client.goods.impl;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.base.message.GoodsChangeMsg;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.client.trade.ExchangeGoodsClient;
import com.enation.app.javashop.core.goods.model.dos.*;
import com.enation.app.javashop.core.goods.model.dto.BuyRecord;
import com.enation.app.javashop.core.goods.model.dto.GoodsQueryParam;
import com.enation.app.javashop.core.goods.model.enums.GoodsType;
import com.enation.app.javashop.core.goods.model.vo.*;
import com.enation.app.javashop.core.goods.service.*;
import com.enation.app.javashop.core.goodssearch.model.GoodsTagIndex;
import com.enation.app.javashop.core.member.model.dos.Member;
import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.member.service.MemberManager;
import com.enation.app.javashop.core.promotion.exchange.model.dos.ExchangeDO;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import com.jcraft.jsch.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author fk
 * @version v2.0
 * @Description: 商品对外的接口实现
 * @date 2018/7/26 10:43
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value = "javashop.product", havingValue = "stand")
public class GoodsClientDefaultImpl implements GoodsClient {

    @Autowired
    private GoodsManager goodsManager;
    @Autowired
    private GoodsQueryManager goodsQueryManager;
    @Autowired
    private GoodsSkuManager goodsSkuManager;
    @Autowired
    @Qualifier("goodsDaoSupport")
    private DaoSupport daoSupport;
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private GoodsParamsManager goodsParamsManager;
    @Autowired
    private CategoryManager categoryManager;
    @Autowired
    private BrandManager brandManager;
    @Autowired
    private GoodsGalleryManager goodsGalleryManager;
    @Autowired
    private ExchangeGoodsClient exchangeGoodsClient;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<BackendGoodsVO> newGoods(Integer length) {

        return this.goodsManager.newGoods(length);
    }

    @Override
    public void underShopGoods(Integer sellerId) {

        this.goodsManager.underShopGoods(sellerId);
    }

    @Override
    public void updateGoodsGrade() {

        this.goodsManager.updateGoodsGrade();
    }

    @Override
    public CacheGoods getFromCache(Integer goodsId) {

        return this.goodsQueryManager.getFromCache(goodsId);
    }

    @Override
    public List<GoodsSelectLine> query(Integer[] goodsIds) {

        return this.goodsQueryManager.query(goodsIds, null);
    }

    @Override
    public List<GoodsDO> queryDo(Integer[] goodsIds) {

        return this.goodsQueryManager.queryDo(goodsIds);
    }

    @Override
    public void checkSellerGoodsCount(Integer[] goodsIds) {

        this.goodsQueryManager.checkSellerGoodsCount(goodsIds);
    }

    @Override
    public List<Map<String, Object>> getGoodsAndParams(Integer[] goodsIds) {

        return this.goodsQueryManager.getGoodsAndParams(goodsIds);
    }

    @Override
    public Map<String, String> getGoodsTags(Integer goodsId) {
        Map<String, String> map=new HashMap<>();
        String tagSql = "SELECT t.* FROM es_tag_goods tg LEFT JOIN es_tags t ON tg.tag_id=t.tag_id WHERE t.`status` = 0 and tg.goods_id ="+goodsId;
        List<GoodsTagIndex> listTags = this.daoSupport.queryForList(tagSql,GoodsTagIndex.class);
        if(!CollectionUtils.isEmpty(listTags)){
            try {
                List<GoodsTagIndex> shopTagList=listTags.stream().filter(tagsDO -> (tagsDO.getShopHomePage()==null?false:tagsDO.getShopHomePage()==0)).collect(Collectors.toList());
                map.put("shopHomeTags", JSON.toJSONString(shopTagList));
                List<GoodsTagIndex> goodsListTags=listTags.stream().filter(tagsDO -> (tagsDO.getGoodsListPage()==null?false:tagsDO.getGoodsListPage()==0)).collect(Collectors.toList());
                map.put("goodsListTags",JSON.toJSONString(goodsListTags));
                List<GoodsTagIndex> goodsDetailsTags=listTags.stream().filter(tagsDO -> (tagsDO.getGoodsDetailsPage()==null?false:tagsDO.getGoodsDetailsPage()==0)).collect(Collectors.toList());
                map.put("goodsDetailsTags",JSON.toJSONString(goodsDetailsTags));
            }catch (Exception e){
                System.out.println(e);
            }
        }
        return map;
    }

    @Override
    public List<Map<String, Object>> getGoodsAndParams(Integer sellerId) {
        return this.goodsQueryManager.getGoodsAndParams(sellerId);
    }

    @Override
    public List<GoodsDO> listGoods(Integer sellerId) {

        return this.goodsQueryManager.listGoods(sellerId);
    }

    @Override
    public List<GoodsDO> listGoodsByTag(Integer sellerId, String mark, int limit) {

        return this.goodsQueryManager.listGoodsByTag(sellerId, mark, limit);
    }

    @Override
    public GoodsSkuVO getSkuFromCache(Integer skuId) {

        return this.goodsSkuManager.getSkuFromCache(skuId);
    }

    @Override
    public GoodsSkuVO getSku(Integer skuId) {
        return this.goodsSkuManager.getSku(skuId);
    }

    @Override
    public List<Map<String, Object>> getGoods(Integer[] goodsIds) {
        return goodsQueryManager.getGoods(goodsIds);
    }

    @Override
    public void updateCommentCount(Integer goodsId) {
        String updateSql = "update es_goods set comment_num=comment_num + 1 where goods_id=?";
        this.daoSupport.execute(updateSql, goodsId);
        // 发送商品消息变化消
        GoodsChangeMsg goodsChangeMsg = new GoodsChangeMsg(new Integer[]{goodsId},
                GoodsChangeMsg.UPDATE_OPERATION);
        this.amqpTemplate.convertAndSend(AmqpExchange.GOODS_CHANGE, AmqpExchange.GOODS_CHANGE + "_ROUTING", goodsChangeMsg);

    }

    @Override
    public void updateBuyCount(List<OrderSkuVO> list) {
        Set<Integer> set = new HashSet<>();
        for (OrderSkuVO sku : list) {
            String sql = "update es_goods set buy_count=buy_count+? where goods_id=?";
            this.daoSupport.execute(sql, sku.getNum(), sku.getGoodsId());
            set.add(sku.getGoodsId());
        }
        // 发送修改商品消息
        GoodsChangeMsg goodsChangeMsg = new GoodsChangeMsg(set.toArray(new Integer[set.size()]),
                GoodsChangeMsg.UPDATE_OPERATION);
        this.amqpTemplate.convertAndSend(AmqpExchange.GOODS_CHANGE, AmqpExchange.GOODS_CHANGE + "_ROUTING", goodsChangeMsg);
    }

    @Override
    public Integer queryGoodsCount() {
        return this.goodsQueryManager.getGoodsCountByParam(new GoodsDO());
    }

    @Override
    public Integer queryGoodsCountByParam(Integer status, Integer sellerId) {
        GoodsDO goodsDO = new GoodsDO();
        goodsDO.setMarketEnable(status);
        goodsDO.setSellerId(sellerId);
        goodsDO.setDisabled(1);
        return this.goodsQueryManager.getGoodsCountByParam(goodsDO);
    }
    @Override
    public Integer queryGoodsCountByParam(Integer status, Integer sellerId,Integer authStatus) {
        GoodsDO goodsDO = new GoodsDO();
        goodsDO.setMarketEnable(status);
        goodsDO.setIsAuth(authStatus);
        goodsDO.setSellerId(sellerId);
        goodsDO.setDisabled(1);
        return this.goodsQueryManager.getGoodsCountByParam(goodsDO);
    }


    @Override
    public List<Map> queryGoodsByRange(Integer pageNo, Integer pageSize) {

        StringBuffer sqlBuffer = new StringBuffer("select g.* from es_goods  g order by seller_id desc");
        List<Map> goodsList = this.daoSupport.queryForListPage(sqlBuffer.toString(), pageNo, pageSize);
        return goodsList;
    }


    @Override
    public List<Map> queryGoodsBySellerId(Integer pageNo, Integer pageSize, Integer sellerId) {

        String sql = "select * from es_goods  where  seller_id =?";
        List<Object> params = new ArrayList<>();
        params.add(sellerId);
        List<Map> goodsList = this.daoSupport.queryForListPage(sql, pageNo, pageSize, params.toArray());
        return goodsList;
    }

    @Override
    public GoodsSnapshotVO queryGoodsSnapShotInfo(Integer goodsId) {

        //商品
        GoodsDO goods = this.goodsQueryManager.getModel(goodsId);

        //判断是否为积分商品
        if (GoodsType.POINT.name().equals(goods.getGoodsType())) {
            //积分兑换信息
            ExchangeDO exchangeDO = this.exchangeGoodsClient.getModelByGoods(goodsId);
            goods.setPoint(exchangeDO.getExchangePoint());
        }


        //参数
        List<GoodsParamsGroupVO> paramList = goodsParamsManager.queryGoodsParams(goods.getCategoryId(), goodsId);
        //品牌
        BrandDO brand = brandManager.getModel(goods.getBrandId());
        //分类
        CategoryDO category = categoryManager.getModel(goods.getCategoryId());
        //相册
        List<GoodsGalleryDO> galleryList = goodsGalleryManager.list(goodsId);

        return new GoodsSnapshotVO(goods, paramList, brand, category, galleryList);
    }

    @Override
    public Page list(GoodsQueryParam goodsQueryParam) {

        return goodsQueryManager.list(goodsQueryParam);
    }

    @Override
    public CategoryDO getCategory(Integer id) {

        return categoryManager.getModel(id);
    }

    /**
     * 校验商品模版是否使用
     *
     * @param templateId
     * @return 商品名称
     */
    @Override
    public GoodsDO checkShipTemplate(Integer templateId) {
        return goodsManager.checkShipTemplate(templateId);
    }

    @Override
    public Integer getSellerGoodsCount(Integer sellerId) {

        return goodsQueryManager.getSellerGoodsCount(sellerId);
    }

    /**
     * 修改某店铺商品店铺名称
     *
     * @param sellerId   商家id
     * @param sellerName 商家名称
     */
    @Override
    public void changeSellerName(Integer sellerId, String sellerName) {
        this.goodsManager.changeSellerName(sellerId, sellerName);
    }

    @Override
    public void updateGoodsType(Integer sellerId, String type) {
        goodsManager.updateGoodsType(sellerId, type);
    }

    @Override
    public List<GoodsDO> listPointGoods(Integer shopId) {
        String sql = "select * from es_goods where goods_type = '" + GoodsType.POINT.name() + "' and seller_id = ? ";

        return this.daoSupport.queryForList(sql, GoodsDO.class, shopId);
    }
}
