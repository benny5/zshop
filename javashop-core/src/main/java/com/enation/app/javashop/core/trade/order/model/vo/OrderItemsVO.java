package com.enation.app.javashop.core.trade.order.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 订单详情VO
 * @author 王志杨
 * @since 2020年12月1日 22:04:06
 */
@ApiModel(value = "订单详情VO")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderItemsVO implements Serializable {

    /**
     * 商品ID
     */
    @ApiModelProperty(name="goods_id", value="商品ID")
    private Integer goodsId;
    /**
     * skuID
     */
    @ApiModelProperty(name="product_id", value="货品ID")
    private Integer productId;
    /**
     * 销售量
     */
    @ApiModelProperty(name="num", value="销售量")
    private Integer num;
    /**
     * 发货量
     */
    @ApiModelProperty(name="ship_num", value="发货量")
    private Integer shipNum;
    /**
     * 交易编号
     */
    @ApiModelProperty(name="trade_sn", value="交易编号")
    private String tradeSn;
    /**
     * 订单编号
     */
    @ApiModelProperty(name="order_sn", value="订单编号")
    private String orderSn;
    /**
     * 图片
     */
    @ApiModelProperty(name="image", value="图片")
    private String image;
    /**
     * 商品名称
     */
    @ApiModelProperty(name="name", value="商品名称")
    private String name;
    /**
     * 销售金额
     */
    @ApiModelProperty(name="price", value="销售金额")
    private Double price;
    /**
     * 分类ID
     */
    @ApiModelProperty(name="cat_id", value="分类ID")
    private Integer catId;
    /**
     * 状态
     */
    @ApiModelProperty(name="state", value="状态")
    private Integer state;
    /**
     * 快照id
     */
    @ApiModelProperty(name="snapshot_id", value="快照id")
    private Integer snapshotId;
    /**
     * 规格json
     */
    @ApiModelProperty(name="spec_json", value="规格json")
    private String specJson;
    /**
     * 促销类型
     */
    @ApiModelProperty(name="promotion_type", value="促销类型")
    private String promotionType;
    /**
     * 促销id
     */
    @ApiModelProperty(name="promotion_id", value="促销id")
    private Integer promotionId;
    /*
     *可退款金额
     */
    @ApiModelProperty(name="refund_price", value="可退款金额")
    private Double refundPrice;
    /**
     * 社区团购商品id
     */
    @ApiModelProperty(name="shetuan_goods_id", value="社区团购商品id")
    private Integer shetuanGoodsId;
    /**
     * 评论状态
     */
    @ApiModelProperty(name="comment_status", value="评论状态:未评论(UNFINISHED),待追评(WAIT_CHASE),评论完成(FINISHED)，")
    private String commentStatus;
    /**
     * 货运状态
     */
    @ApiModelProperty(name="ship_status", value="货运状态")
    private String shipStatus;
    /**
     * 发货单号
     */
    @ApiModelProperty(name="ship_no", value="发货单号")
    private String shipNo;
    /**
     * 物流公司ID
     */
    @ApiModelProperty(name="logi_id", value="物流公司ID")
    private Integer logiId;
    /**
     * 物流公司名称
     */
    @ApiModelProperty(name="logi_name", value="物流公司名称")
    private String logiName;
    /**
     * 签收人
     */
    @ApiModelProperty(name="the_sign", value="签收人")
    private String theSign;
    /**
     * 送货时间
     */
    @ApiModelProperty(name="ship_time", value = "送货时间")
    private Long shipTime;
    /**
     * 售后状态
     */
    @ApiModelProperty(name="service_status", value="商品售后状态")
    private String serviceStatus;
    /**
     * 控制小程序默认展示
     */
    @ApiModelProperty(name="show", value="控制小程序默认展示", hidden = true)
    private Boolean show;

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getShipNum() {
        return shipNum;
    }

    public void setShipNum(Integer shipNum) {
        this.shipNum = shipNum;
    }

    public String getTradeSn() {
        return tradeSn;
    }

    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(Integer snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getSpecJson() {
        return specJson;
    }

    public void setSpecJson(String specJson) {
        this.specJson = specJson;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public Double getRefundPrice() {
        return refundPrice;
    }

    public void setRefundPrice(Double refundPrice) {
        this.refundPrice = refundPrice;
    }

    public Integer getShetuanGoodsId() {
        return shetuanGoodsId;
    }

    public void setShetuanGoodsId(Integer shetuanGoodsId) {
        this.shetuanGoodsId = shetuanGoodsId;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getShipStatus() {
        return shipStatus;
    }

    public void setShipStatus(String shipStatus) {
        this.shipStatus = shipStatus;
    }

    public String getShipNo() {
        return shipNo;
    }

    public void setShipNo(String shipNo) {
        this.shipNo = shipNo;
    }

    public Integer getLogiId() {
        return logiId;
    }

    public void setLogiId(Integer logiId) {
        this.logiId = logiId;
    }

    public String getLogiName() {
        return logiName;
    }

    public void setLogiName(String logiName) {
        this.logiName = logiName;
    }

    public String getTheSign() {
        return theSign;
    }

    public void setTheSign(String theSign) {
        this.theSign = theSign;
    }

    public Long getShipTime() {
        return shipTime;
    }

    public void setShipTime(Long shipTime) {
        this.shipTime = shipTime;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }
}
