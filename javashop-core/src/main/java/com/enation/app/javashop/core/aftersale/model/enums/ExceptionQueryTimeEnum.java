package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常单处理状态
 */
public enum ExceptionQueryTimeEnum {

    CREATE_TIME("上报时间"),
    RECIVE_TIME("接单时间"),
    PROCESS_TIME("处理时间"),
    COMPLETE_TIME("完成时间"),
    FINISH_TIME("结单时间");

    private String description;

    ExceptionQueryTimeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String value(){
        return this.name();
    }
}
