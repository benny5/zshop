package com.enation.app.javashop.core.system.service;

import com.enation.app.javashop.core.system.model.dto.MiniproSendMsgDTO;

/**
 * 小程序服务消息模板发送
 */
public interface WechatMiniproTemplateManager {

    /**
     * 发送消息
     */
    void send(MiniproSendMsgDTO miniproSendMsgDTO);

}