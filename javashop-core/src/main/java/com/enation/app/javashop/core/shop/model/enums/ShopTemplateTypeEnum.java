package com.enation.app.javashop.core.shop.model.enums;

/**
 * @author zjp
 * @version v7.0
 * @Description 店铺站内消息枚举类
 * @ClassName ShopNoticeTypeEnum
 * @since v7.0 下午2:21 2018/7/10
 */
public enum ShopTemplateTypeEnum {
    /**
     * 订单
     */
    KUAIDI("快递配送"),
    /**
     * 商品
     */
    TONGCHENG("同城配送");

    private String description;

    ShopTemplateTypeEnum(String des) {
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
