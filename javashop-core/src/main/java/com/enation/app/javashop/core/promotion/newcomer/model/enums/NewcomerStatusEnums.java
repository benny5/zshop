package com.enation.app.javashop.core.promotion.newcomer.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2021/2/25
 * @Description: 新人购活动上下线状态枚举
 */
public enum NewcomerStatusEnums {

    NEW(1, "待上线"),
    ONLINE(2, "已上线"),
    OFFLINE(3, "已下线");

    private int index;
    private String describe;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    NewcomerStatusEnums(int index, String describe) {
        this.index = index;
        this.describe = describe;
    }


    public int getIndex() {
        return index;
    }

    public String getDescribe() {
        return describe;
    }

    public static String getTextByIndex(Integer index) {
        return enumMap.get(index);
    }
}