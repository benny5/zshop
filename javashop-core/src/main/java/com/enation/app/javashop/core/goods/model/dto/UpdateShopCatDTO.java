package com.enation.app.javashop.core.goods.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/10/12 15:42
 */
@Data
public class UpdateShopCatDTO {
    @ApiModelProperty(name = "goods_id", value = "商品id", required = true)
    private Integer goodsId;

    @ApiModelProperty(name = "shop_cat_ids", value = "分组id", required = true)
    private List<Integer> shopCatIds;

    @ApiModelProperty(name = "update_type",value = "修改商品类型,1-社团商品，0-普通商品，",required = true)
    private Integer updateType;
}
