package com.enation.app.javashop.core.member.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@Table(name = "es_leader")
@ApiModel
public class LeaderDO implements Serializable {

    @Id(name = "leader_id")
    @ApiModelProperty(name = "leader_id",value = "团长ID")
    private Integer leaderId;

    @Column(name = "leader_code")
    @ApiModelProperty(name = "leader_code",value = "团长编码")
    private String leaderCode;

    @Column(name = "member_id")
    @ApiModelProperty(name ="member_id",value = "会员ID")
    private Integer memberId;

    @Column(name="seller_id")
    @ApiModelProperty(name="seller_id",value = "ID")
    private Integer sellerId;

    @Column(name="leader_name")
    @ApiModelProperty(name = "leader_name",value = "团长名")
    private String leaderName;

    @Column(name = "leader_mobile")
    @ApiModelProperty(name = "leader_mobile",value = "团长手机")
    private String leaderMobile;

    @Column(name="leader_type")
    @ApiModelProperty(name = "leader_type",value = "团长类型 1自提点 2配送点")
    private Integer leaderType;

    @Column(name="op_status")
    @ApiModelProperty(name = "op_status",value = "营业状态 0 休息中 1 营业中")
    private Integer opStatus;

    @Column(name="status")
    @ApiModelProperty(name = "status",value = "状态")
    private Integer status;

    @Column(name="audit_status")
    @ApiModelProperty(name = "audit_status",value = "审核状态")
    private Integer auditStatus;

    @Column(name="audit_time")
    @ApiModelProperty(name = "audit_time",value = "审核时间")
    private Long auditTime;

    @Column(name="audit_remark")
    @ApiModelProperty(name = "audit_remark",value = "审核备注")
    private String auditRemark;

    @Column(name="facade_pic_url")
    @ApiModelProperty(name = "facade_pic_url",value = "门面图片")
    private String facadePicUrl;

    @Column(name="fans_count")
    @ApiModelProperty(name = "fans_count",value = "粉丝数")
    private Long fansCount;

    @Column(name="street")
    @ApiModelProperty(name = "street",value = "街道")
    private String street;

    @Column(name = "address")
    @ApiModelProperty(name = "address",value = "地址")
    private String address;

    @Column(name="doorplate")
    @ApiModelProperty(name = "doorplate",value = "门牌号")
    private String doorplate;

    @Column(name="cell_name")
    @ApiModelProperty(name = "cell_name",value = "小区名称")
    private String cellName;

    @Column(name="site_name")
    @ApiModelProperty(name = "site_name",value = "站点名称")
    private String siteName;

    @Column(name="origin_site_name")
    @ApiModelProperty(name = "origin_site_name",value = "原站点名称")
    private String originSiteName;

    @Column(name="site_type")
    @ApiModelProperty(name = "site_type",value = "站点类型")
    private Integer siteType;

    @Column(name="join_reason")
    @ApiModelProperty(name="join_reason",value = "加入原因")
    private String joinReason;

    @Column(name="source_from")
    @ApiModelProperty(name="source_from",value = "注册来源")
    private Integer sourceFrom;

    /**所在省id*/
    @Column(name = "province_id")
    @ApiModelProperty(name="province_id",value="所在省id",required=false,hidden = true)
    private Integer provinceId;
    /**所在市id*/
    @Column(name = "city_id")
    @ApiModelProperty(name="city_id",value="所在市id",required=false,hidden = true)
    private Integer cityId;
    /**所在县id*/
    @Column(name = "county_id")
    @ApiModelProperty(name="county_id",value="所在县id",required=false,hidden = true)
    private Integer countyId;
    /**所在镇id*/
    @Column(name = "town_id")
    @ApiModelProperty(name="town_id",value="所在镇id",required=false,hidden = true)
    private Integer townId;
    /**所在省*/
    @Column(name = "province")
    @ApiModelProperty(name="province",value="所在省",required=false,hidden = true)
    private String province;
    /**所在市*/
    @Column(name = "city")
    @ApiModelProperty(name="city",value="所在市",required=false,hidden = true)
    private String city;
    /**所在县*/
    @Column(name = "county")
    @ApiModelProperty(name="county",value="所在县",required=false,hidden = true)
    private String county;
    /**所在镇*/
    @Column(name = "town", allowNullUpdate = true)
    @ApiModelProperty(name="town",value="所在镇",required=false,hidden = true)
    private String town;

    @Column(name = "lat")
    @ApiModelProperty(name = "lat", value = "经度")
    private Double lat;

    @Column(name = "lng")
    @ApiModelProperty(name = "lng", value = "经度")
    private Double lng;

    @Column(name = "work_time")
    @ApiModelProperty(name = "work_time", value = "营业时间")
    private String workTime;

    @Column(name="update_time")
    @ApiModelProperty(name = "update_time",value = "更新时间")
    private Long updateTime;

    @Column(name = "add_time")
    @ApiModelProperty(name ="add_time",value = "新增时间")
    private Long addTime;

    @Column(name = "creater_name")
    @ApiModelProperty(name ="creater_name",value = "创建人")
    private String createrName;

    @Column(name = "remark")
    @ApiModelProperty(name ="remark",value = "备注")
    private String remark;

    @Column(name = "operate_areas")
    @ApiModelProperty(name ="operateAreas",value = "运营区域")
    private String operateAreas;

    @Column(name = "ship_priority")
    @ApiModelProperty(name ="ship_priority",value = "配送优先级")
    private Integer shipPriority;

    @Column(name = "dispatch_type")
    @ApiModelProperty(name ="dispatch_type",value = "分拣配送类型")
    private String dispatchType;
}
