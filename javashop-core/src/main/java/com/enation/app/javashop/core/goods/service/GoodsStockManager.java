package com.enation.app.javashop.core.goods.service;

import com.enation.app.javashop.core.trade.order.model.dto.OrderDTO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;

/**
 * 商品库存管理
 * @author JFeng
 * @date 2020/12/26 18:05
 */
public interface GoodsStockManager {

    boolean updateTradeState(String tradeSn,  Integer times, OrderStatusEnum orderStatusEnum);

    boolean updateState(String sn, Integer times, OrderStatusEnum orderStatusEnum);

    void rollbackNormal(OrderDTO order);

    void rollbackPromotionStock(OrderDTO order);

    boolean lockPromotionStock(OrderDTO order);

    boolean lockNormalStock(OrderDTO order);
}
