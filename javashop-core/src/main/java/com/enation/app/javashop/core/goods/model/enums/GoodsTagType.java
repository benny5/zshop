package com.enation.app.javashop.core.goods.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/11/25
 * @Description: 商品标签类型
 */
public enum GoodsTagType {

    SYSTEM_DEFAULT(0,"系统默认"),
    CUSTOM(1,"自定义");

    private Integer  index;
    private String name;

    GoodsTagType(Integer index , String name){
        this.name = name;
        this.index= index;
    }

    public Integer  getIndex(){
        return this.index;
    }
    public String  getName(){
        return this.name;
    }
}
