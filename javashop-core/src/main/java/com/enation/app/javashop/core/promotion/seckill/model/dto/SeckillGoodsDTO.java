package com.enation.app.javashop.core.promotion.seckill.model.dto;

import java.io.Serializable;

import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillApplyDO;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillConvertGoodsVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author Snow
 * @version 1.0
 * @since v7.0.0
 * 2017年12月14日 16:58:55
 */
@SuppressWarnings("AlibabaPojoMustOverrideToString")
@ApiModel(description = "活动商品vo")
@Data
public class SeckillGoodsDTO implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 5327491947477140478L;


	@ApiModelProperty(value = "售空数量")
	private Integer applyId;

	@ApiModelProperty(value = "商品id")
	private Integer goodsId;

	@ApiModelProperty(value = "商品图片")
	private String goodsImage;

	@ApiModelProperty(value = "商品名称")
	private String goodsName;

	@ApiModelProperty(value = "商品普通价格")
	private Double originalPrice;

	@ApiModelProperty(value = "是否可销售")
	private Boolean salesEnable;

	@ApiModelProperty(value = "秒杀活动价格")
	private Double seckillPrice;

	@ApiModelProperty(value = "商品规格id")
	private Integer skuId;

	@ApiModelProperty(value = "已售数量")
	private Integer soldNum;

	@ApiModelProperty(value = "售空数量")
	private Integer soldQuantity;

	@ApiModelProperty(value = "不同规格下的页面url")
	private String url;


}
