package com.enation.app.javashop.core.promotion.luck.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 孙建
 * 奖品类型
 */
public enum PrizeTypeEnums {

    ACTUAL(1, "实物奖品"),
    COUPON(2, "优惠券"),
    VIRTUAL(3, "虚拟奖品"),
    THANKS(4, "谢谢参与");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (PrizeTypeEnums item : PrizeTypeEnums.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    PrizeTypeEnums(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }
}
