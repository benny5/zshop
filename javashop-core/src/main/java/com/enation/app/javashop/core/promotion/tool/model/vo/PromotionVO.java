package com.enation.app.javashop.core.promotion.tool.model.vo;


import com.enation.app.javashop.core.promotion.exchange.model.dos.ExchangeDO;
import com.enation.app.javashop.core.promotion.fulldiscount.model.dos.FullDiscountGiftDO;
import com.enation.app.javashop.core.promotion.fulldiscount.model.vo.FullDiscountVO;
import com.enation.app.javashop.core.promotion.groupbuy.model.vo.GroupbuyGoodsVO;
import com.enation.app.javashop.core.promotion.halfprice.model.vo.HalfPriceVO;
import com.enation.app.javashop.core.promotion.minus.model.vo.MinusVO;
import com.enation.app.javashop.core.promotion.newcomer.model.dos.NewcomerGoodsDO;
import com.enation.app.javashop.core.promotion.newcomer.model.vos.MiniNewcomerDataVO;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.tool.model.enums.PromotionTypeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 活动商品对照表
 *
 * @author Snow
 * @since v6.4
 * @version v1.0 2017年08月22日
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class PromotionVO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4796645552318671313L;

	@ApiModelProperty(value = "商品id")
	private Integer goodsId;

	@ApiModelProperty(value = "商品图片")
	private String thumbnail;

	@ApiModelProperty(value = "商品名称")
	private String name;

	@ApiModelProperty(value = "货品id")
	private Integer skuId;

	@ApiModelProperty(value = "活动开始时间")
	private Long startTime;

	@ApiModelProperty(value = "活动结束时间")
	private Long endTime;

	@ApiModelProperty(value = "活动id")
	private Integer activityId;

	/**
	 * 由此字段识别具体的活动类型
	 * 此字段对应的是一个枚举值
	 * @see PromotionTypeEnum
	 *
	 */
	@ApiModelProperty(value = "活动工具类型")
	private String promotionType;

	@ApiModelProperty(value = "活动名称")
	private String title;

	@ApiModelProperty(value = "积分兑换对象")
	private ExchangeDO exchange;

	@ApiModelProperty(value = "团购活动对象")
	private GroupbuyGoodsVO groupbuyGoodsVO;

	@ApiModelProperty(value = "满优惠活动")
	private FullDiscountVO fullDiscountVO;

	@ApiModelProperty(value = "满赠的赠品VO")
	private FullDiscountGiftDO fullDiscountGift;

	@ApiModelProperty(value = "单品立减活动")
	private MinusVO minusVO;

	@ApiModelProperty(value = "第二件半价活动")
	private HalfPriceVO halfPriceVO;

	@ApiModelProperty(value = "限时抢购活动")
	private SeckillGoodsVO seckillGoodsVO;

	@ApiModelProperty(value = "新人购活动")
	private MiniNewcomerDataVO miniNewcomerDataVO;

	@ApiModelProperty(value = "社区团购活动")
	private ShetuanGoodsVO shetuanGoodsVO;


	@ApiModelProperty(value = "售空数量")
	private Integer num;

	@ApiModelProperty(value = "活动价格")
	private Double price;

	@ApiModelProperty(value = "限购数量")
	private Integer limitNum;


}
