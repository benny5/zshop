package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFeng
 * @date 2020/6/2 10:34
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShetuanGoodsSearchDTO {
    @ApiModelProperty(name = "page_no", value = "页码")
    private Integer pageNo;
    @ApiModelProperty(name = "page_size", value = "每页数量")
    private Integer pageSize;
    @ApiModelProperty(name = "keyword", value = "关键字")
    private String keyword;
    @ApiModelProperty(name = "category", value = "分类")
    private Integer category;
    @ApiModelProperty(name = "category_path", value = "分类路径")
    private String categoryPath;
    @ApiModelProperty(name = "price", value = "价格",example = "10_30")
    private String price;

    @ApiModelProperty(name = "prop", value = "属性:参数名_参数值@参数名_参数值",example = "屏幕类型_LED@屏幕尺寸_15英寸")
    private String prop;
    @ApiModelProperty(name = "seller_id", value = "卖家id，搜索店铺商品的时候使用")
    private Integer sellerId;
    @ApiModelProperty(name = "shop_cat_id", value = "商家分组id，搜索店铺商品的时候使用")
    private Integer shopCatId;
    @ApiModelProperty(name = "goods_ids", value = "商品ids，搜索多个商品")
    private String goodsIds;
    @ApiModelProperty(name = "user_lat", value = "用户经度")
    private double userLat;
    @ApiModelProperty(name = "user_lng", value = "用户纬度")
    private double userLng;
    @ApiModelProperty(name = "sort", value = "排序:关键字_排序",allowableValues = "price_asc,price_desc,buynum_asc,buynum_desc,pick_asc,pick_desc")
    private String sort;

    @ApiModelProperty(name = "city", value = "商品城市")
    private String city;

}
