package com.enation.app.javashop.core.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.aftersale.model.dto.ExceptionOrderDTO;
import com.enation.app.javashop.core.aftersale.service.ExceptionOrderManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.security.model.Seller;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

 /**
 * @author 王志杨
 * @since 2020年9月21日 11:45:26
 * 异常订单导入模板的读取类
 */
@Slf4j
public class ExceptionOrderDataListener extends AnalysisEventListener<ExceptionOrderImport> {
    /**
     * 每隔20条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 300;
    private List<ExceptionOrderImport> importList = new ArrayList<>();
    //private List<ExceptionOrderImport> errorImportList = new ArrayList<>();
    private List<ExceptionOrderDTO> insertList = new ArrayList<>();


    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
    private ExceptionOrderManager exceptionOrderManager;

    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param exceptionOrderManager spring管理的类传进来
     */
    public ExceptionOrderDataListener(ExceptionOrderManager exceptionOrderManager) {
        this. exceptionOrderManager= exceptionOrderManager;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data
     *            one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context 分析上下文
     */
    @Override
    public void invoke(ExceptionOrderImport data, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(data));
        importList.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (importList.size() >= BATCH_COUNT) {
            insertData();
            // 存储完成清理 list
            importList.clear();
        }
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context 分析上下文
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        insertData();
        log.info("所有数据解析完成！");
    }

    /**
     * 加上存储数据库
     */
    private void insertData() {
        log.info("{}条异常单数据，开始存储数据库！", importList.size());
        for (ExceptionOrderImport importItem : importList) {
            ExceptionOrderDTO exceptionOrderDTO = new ExceptionOrderDTO();
            exceptionOrderDTO.setOrderSn(importItem.getOrderSn());
            exceptionOrderDTO.setExceptionSource(importItem.getExceptionSource());
            exceptionOrderDTO.setExceptionType(importItem.getExceptionType());
            exceptionOrderDTO.setExceptionDescription(importItem.getExceptionDescription());
            insertList.add(exceptionOrderDTO);
        }
        Seller seller = UserContext.getSeller();
        exceptionOrderManager.batchInsertException(insertList, seller);
        log.info("存储数据库成功！");
    }

}