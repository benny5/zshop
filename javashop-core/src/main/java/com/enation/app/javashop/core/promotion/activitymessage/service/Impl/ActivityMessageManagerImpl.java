package com.enation.app.javashop.core.promotion.activitymessage.service.Impl;

import com.enation.app.javashop.core.base.JobAmqpExchange;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.base.rabbitmq.TimeExecute;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageDO;
import com.enation.app.javashop.core.promotion.activitymessage.model.dos.ActivityMessageRecordDO;
import com.enation.app.javashop.core.promotion.activitymessage.model.vo.ActivityMessageRecordVO;
import com.enation.app.javashop.core.promotion.activitymessage.service.ActivityMessageManager;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanDO;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanManager;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.core.system.sendMessage.WechatSendMessage;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.security.model.Buyer;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityMessageManagerImpl implements ActivityMessageManager {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport daoSupport;
    @Autowired
    private ShetuanManager shetuanManager;
    @Autowired
    private WechatSendMessage wechatSendMessage;
    @Autowired
    private ShopManager shopManager;
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Override
    public void addActivityMessage(ActivityMessageDO activityMessageDO) {
        Integer sellerId = UserContext.getSeller().getSellerId();
        activityMessageDO.setSellerId(sellerId);
        activityMessageDO.setCreateTime(DateUtil.getDateline());
        daoSupport.insert(activityMessageDO);
        //推送活动 ACTIVITY_PUSH_TEMPLATE
        amqpTemplate.convertAndSend(AmqpExchange.CUSTOMER_PUSH_MESSAGE,
                AmqpExchange.CUSTOMER_PUSH_MESSAGE + "_ROUTING",
                activityMessageDO);
//        wechatSendMessage.sendActivityMessage(activityMessageDO);
    }

    @Override
    public void addActivityMessageRecordVO(ActivityMessageRecordVO activityMessageRecordVO) {
        Integer buyerId = UserContext.getBuyer().getUid();
        ActivityMessageRecordDO activityMessageRecordDO = new ActivityMessageRecordDO();
        BeanUtil.copyProperties(activityMessageRecordVO,activityMessageRecordDO);
        activityMessageRecordDO.setMemberId(buyerId);
        activityMessageRecordDO.setCreateTime(DateUtil.getDateline());

        // 查询最新的活动
        // 根据城市查询 店铺id
        Integer shopId = shopManager.getShopByCity(activityMessageRecordVO.getCity());
        activityMessageRecordDO.setSellerId(shopId);
        // 查询最新的活动
        List<ShetuanDO> shetuanDOList = shetuanManager.getShetuanIdBySellerId(shopId);
        for (ShetuanDO shetuanDO : shetuanDOList) {
            // 查询有没有订阅过当前活动
            String sql = "SELECT * from es_activity_message_record where activity_id = ? and member_id = ? ";
            List<ActivityMessageRecordDO> activityMessageRecord = daoSupport.queryForList(sql, ActivityMessageRecordDO.class, shetuanDO.getShetuanId(), buyerId);
            if(!StringUtil.isNotEmpty(activityMessageRecord)){
                activityMessageRecordDO.setActivityId(shetuanDO.getShetuanId());
                daoSupport.insert(activityMessageRecordDO);
                break;
            }
        }
    }

    @Override
    public List<ActivityMessageRecordDO> getActivityMessageRecordDO(Integer activityId) {
        String sql = "SELECT * from es_activity_message_record where activity_id = ? ";
        return daoSupport.queryForList(sql,ActivityMessageRecordDO.class,activityId);
    }

    @Override
    public List<ActivityMessageDO> getActivityMessageDO(Integer activityId) {
        String sql = "select * from es_activity_message where activity_id = ? order by create_time desc ";
        return daoSupport.queryForList(sql,ActivityMessageDO.class,activityId);
    }

    @Override
    public Integer getSubscribeActivity(Integer memberId,String city) {
        // 根据城市查询 店铺id
        Integer shopId = shopManager.getShopByCity(city);
        Integer subscribedNum = 0;
        // 查询最新的活动
        List<ShetuanDO> shetuanDOList = shetuanManager.getShetuanIdBySellerId(shopId);
        if(StringUtil.isNotEmpty(shetuanDOList) ){
            // 当天的开始时间
            long startOfTodDay = DateUtil.startOfTodDay();
            // 当天的结束时间
            long endOfTodDay = DateUtil.endOfTodDay();
            // 查询当前人 今天在该店铺中订阅了多少条消息
            String sql = "SELECT count(1) as num from es_activity_message_record where member_id = ? and seller_id = ? and create_time >= ? and  create_time <= ?";
            subscribedNum = daoSupport.queryForInt(sql, memberId,shopId, startOfTodDay,endOfTodDay);
            // 当活动条数 和 订阅不相等是 继续订阅
            if(shetuanDOList.size() != subscribedNum){
                subscribedNum = 0 ;
            }
        }
        return subscribedNum;
    }
}
