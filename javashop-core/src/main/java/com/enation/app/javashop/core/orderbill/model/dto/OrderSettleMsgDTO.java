package com.enation.app.javashop.core.orderbill.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 订单结算消息
 * @author 孙建
 */
@Data
public class OrderSettleMsgDTO implements Serializable{

    // 订单id
    private Integer orderId;

    // 1打款 2退款
    private Integer settleType;

}
