package com.enation.app.javashop.core.aftersale.model.dto;

import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goods.model.vo.SpecValueVO;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/21
 * @Description:
 */
@Data
public class ClaimSkusDTO {

    @ApiModelProperty(hidden = true)
    private Integer skuId;

    @ApiModelProperty(name = "goods_id", value = "商品id", hidden = true)
    private Integer goodsId;

    @ApiModelProperty(name = "goods_name", value = "商品名称", hidden = true)
    private String goodsName;

    //为了前端显示兼容，name和goodsName都是商品名称,因为OrderSkuDTO中商品名是name
    @ApiModelProperty(name = "name", value = "商品名称", hidden = true)
    private String name;

    @ApiModelProperty(name = "sn", value = "商品编号", required = false)
    private String sn;

    @ApiModelProperty(name = "quantity", value = "库存", required = false)
    private Integer quantity;

    @ApiModelProperty(name = "enable_quantity", value = "可用库存")
    private Integer enableQuantity;

    @ApiModelProperty(name = "price", value = "商品价格", required = false)
    private Double price;

    @ApiModelProperty(name = "cost", value = "成本价格", required = true)
    private Double cost;

    @ApiModelProperty(name = "mktprice", value = "市场价格")
    private Double mktprice;

    @ApiModelProperty(name = "thumbnail", value = "商品图片")
    private String thumbnail;

    @ApiModelProperty(name = "seller_id", value = "卖家id", hidden = true)
    private Integer sellerId;

    @ApiModelProperty(name = "seller_name", value = "卖家名称", hidden = true)
    private String sellerName;

    @ApiModelProperty(name = "category_id", value = "分类id", hidden = true)
    private Integer categoryId;

    @ApiModelProperty(name = "上游skuId")
    private String upSkuId;

    @ApiModelProperty(name = "specs", value = "规格信息json", hidden = true)
    @JsonIgnore
    private String specs;

    @ApiModelProperty(name = "num", value = "购买数量")
    private Integer num;

    @ApiModelProperty(name = "compensation_num", value = "补发数量")
    private Integer compensationNum;

}
