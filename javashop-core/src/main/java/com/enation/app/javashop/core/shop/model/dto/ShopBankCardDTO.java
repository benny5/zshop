package com.enation.app.javashop.core.shop.model.dto;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/4/9 16:03
 */

@Data
public class ShopBankCardDTO {

    /**银行开户名*/
    @Column(name = "bank_account_name")
    @ApiModelProperty(name="bank_account_name",value="银行开户名",required=true)
    private String bankAccountName;

    /**银行开户账号*/
    @Column(name = "bank_number")
    @ApiModelProperty(name="bank_number",value="银行开户账号",required=true)
    private String bankNumber;

    /**开户银行支行名称*/
    @Column(name = "bank_name")
    @ApiModelProperty(name="bank_name",value="开户银行支行名称",required=true)
    private String bankName;

    private Integer shopId;
    /**
     * 开户省份
     */
    @Column(name = "bank_province")
    @ApiModelProperty(name ="bank_province", value = "开户省份")
    private String bankProvince;
    /**
     * 开户城市
     */
    @Column(name = "bank_city")
    @ApiModelProperty(name ="bank_city", value = "开户城市")
    private String bankCity;
    /**
     * 开户省份ID
     */
    @Column(name = "bank_province_id")
    @ApiModelProperty(name ="bank_province_id", value = "开户省份ID")
    private Integer bankProvinceId;
    /**
     * 开户城市ID
     */
    @Column(name = "bank_city_id")
    @ApiModelProperty(name ="bank_city_id", value = "开户城市ID")
    private Integer bankCityId;

}

