package com.enation.app.javashop.core.member.model.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *  发送短信DTO
 */
@Data
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BatchSendMsgDTO {

    @ApiModelProperty(name = "message_code", value = "消息编码", required = true)
    private String messageCode;

    @ApiModelProperty(name = "send_mode", value = "发送模式 1指定手机号 99全部用户", required = false)
    private Integer sendMode;

    @ApiModelProperty(name = "mobile_array", value = "手机号数组", required = false)
    private String[] mobileArray;

    @ApiModelProperty(hidden = true)
    private String password;

}
