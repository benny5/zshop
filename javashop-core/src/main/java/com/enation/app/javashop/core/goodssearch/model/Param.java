package com.enation.app.javashop.core.goodssearch.model;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Created by kingapex on 2018/7/19.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/7/19
 */
public class Param {

    private String name;

    private String value;

    public Param(){
    }

    public Param(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Param{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
