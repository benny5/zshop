package com.enation.app.javashop.core.aftersale.model.dto;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常订单实体DTO
 */
@ApiModel("异常订单实体DTO")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ExceptionOrderDTO implements Serializable {

    private static final long serialVersionUID = 1967770547302576708L;

    /**异常订单ID*/
    @ApiModelProperty(hidden=true)
    private Integer exceptionId;
    /**异常单号*/
    @ApiModelProperty(name="exception_sn",value="异常单号",required=false)
    private String exceptionSn;
    /**订单号*/
    @ApiModelProperty(name="order_sn",value="订单号",required=true)
    @NotBlank(message = "订单号不得为空")
    private String orderSn;
    /**异常处理状态*/
    @ApiModelProperty(name="exception_status",value="异常处理状态",required=false)
    private String exceptionStatus;
    /**异常上报来源*/
    @ApiModelProperty(name="exception_source",value="异常上报来源",required=true)
    @NotBlank(message = "异常上报来源不得为空")
    private String exceptionSource;
    /**异常类型*/
    @ApiModelProperty(name="exception_type",value="异常类型",required=true)
    @NotBlank(message = "异常类型不得为空")
    private String exceptionType;
    /**异常类型*/
    @ApiModelProperty(name="force_inerst",value="是否强制上报异常",required=true)
    @NotNull(message = "是否强制上报异常不得为空")
    private Boolean forceInerst;
    /**解决说明*/
    @ApiModelProperty(name="solve_explain",value="解决说明",required=false)
    private String solveExplain;
    /**解决方式字符串*/
    @ApiModelProperty(name="solve_types",value="解决方式字符串",required=false)
    private String solveTypes;
    /**客户评分*/
    @ApiModelProperty(name="member_score",value="客户评分",required=false)
    private Double memberScore;
    /**客户评价*/
    @ApiModelProperty(name="member_comment",value="客户评价",required=false)
    private String memberComment;
    /**买家昵称*/
    @ApiModelProperty(name="member_nickname",value="买家昵称",required=false)
    private String memberNickname;
    /**买家电话*/
    @ApiModelProperty(name="member_mobile",value="买家电话",required=false)
    private String memberMobile;
    /**一级团长ID*/
    @ApiModelProperty(name="member_id_lv1",value="一级团长ID",required=false)
    private Integer memberIdLv1;
    /**一级团长真实姓名*/
    @ApiModelProperty(name="member_realname_lv1",value="一级团长真实姓名",required=false)
    private String memberRealnameLv1;
    /**一级团长手机号码*/
    @ApiModelProperty(name="member_mobile_lv1",value="一级团长手机号码",required=false)
    private String memberMobileLv1;
    /**站点名*/
    @ApiModelProperty(name="site_name",value="站点名",required=false)
    private String siteName;
    /**站长名称*/
    @ApiModelProperty(name="site_leader_name",value="站长名称",required=false)
    private String siteLeaderName;
    /**站长手机号码*/
    @ApiModelProperty(name="site_leader_mobile",value="站长手机号码",required=false)
    private String siteLeaderMobile;
    /**上报时间*/
    @ApiModelProperty(name="create_time",value="上报时间",required=false)
    private Long createTime;
    /**上报人ID*/
    @ApiModelProperty(name="create_id",value="上报人ID",required=false)
    private Integer createId;
    /**上报人*/
    @ApiModelProperty(name="create_name",value="上报人",required=false)
    private String createName;
    /**接单时间*/
    @ApiModelProperty(name="recive_time",value="接单时间",required=false)
    private Long reciveTime;
    /**接单人id*/
    @ApiModelProperty(name="recive_id",value="接单人id",required=false)
    private Integer reciveId;
    /**接单人*/
    @ApiModelProperty(name="recive_name",value="接单人",required=false)
    private String reciveName;
    /**处理时间*/
    @ApiModelProperty(name="process_time",value="处理时间",required=false)
    private Long processTime;
    /**处理人id*/
    @ApiModelProperty(name="process_id",value="处理人id",required=false)
    private Integer processId;
    /**处理人*/
    @ApiModelProperty(name="process_name",value="处理人",required=false)
    private String processName;
    /**完成时间*/
    @ApiModelProperty(name="complete_time",value="完成时间",required=false)
    private Long completeTime;
    /**结单时间*/
    @ApiModelProperty(name="finish_time",value="结单时间",required=false)
    private Long finishTime;
    /**结单人id*/
    @ApiModelProperty(name="finish_id",value="结单人id",required=false)
    private Integer finishId;
    /**结单人*/
    @ApiModelProperty(name="finish_name",value="结单人",required=false)
    private String finishName;
    /**异常描述*/
    @ApiModelProperty(name="exception_description",value="异常描述",required=true)
    @NotBlank(message = "异常描述不得为空")
    private String exceptionDescription;

    public Integer getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(Integer exceptionId) {
        this.exceptionId = exceptionId;
    }

    public String getExceptionSn() {
        return exceptionSn;
    }

    public void setExceptionSn(String exceptionSn) {
        this.exceptionSn = exceptionSn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getExceptionStatus() {
        return exceptionStatus;
    }

    public void setExceptionStatus(String exceptionStatus) {
        this.exceptionStatus = exceptionStatus;
    }

    public String getExceptionSource() {
        return exceptionSource;
    }

    public void setExceptionSource(String exceptionSource) {
        this.exceptionSource = exceptionSource;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public Boolean getForceInerst() {
        return forceInerst;
    }

    public void setForceInerst(Boolean forceInerst) {
        this.forceInerst = forceInerst;
    }

    public String getSolveExplain() {
        return solveExplain;
    }

    public void setSolveExplain(String solveExplain) {
        this.solveExplain = solveExplain;
    }

    public String getSolveTypes() {
        return solveTypes;
    }

    public void setSolveTypes(String solveTypes) {
        this.solveTypes = solveTypes;
    }

    public Double getMemberScore() {
        return memberScore;
    }

    public void setMemberScore(Double memberScore) {
        this.memberScore = memberScore;
    }

    public String getMemberComment() {
        return memberComment;
    }

    public void setMemberComment(String memberComment) {
        this.memberComment = memberComment;
    }

    public String getMemberNickname() {
        return memberNickname;
    }

    public void setMemberNickname(String memberNickname) {
        this.memberNickname = memberNickname;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public Integer getMemberIdLv1() {
        return memberIdLv1;
    }

    public void setMemberIdLv1(Integer memberIdLv1) {
        this.memberIdLv1 = memberIdLv1;
    }

    public String getMemberRealnameLv1() {
        return memberRealnameLv1;
    }

    public void setMemberRealnameLv1(String memberRealnameLv1) {
        this.memberRealnameLv1 = memberRealnameLv1;
    }

    public String getMemberMobileLv1() {
        return memberMobileLv1;
    }

    public void setMemberMobileLv1(String memberMobileLv1) {
        this.memberMobileLv1 = memberMobileLv1;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteLeaderName() {
        return siteLeaderName;
    }

    public void setSiteLeaderName(String siteLeaderName) {
        this.siteLeaderName = siteLeaderName;
    }

    public String getSiteLeaderMobile() {
        return siteLeaderMobile;
    }

    public void setSiteLeaderMobile(String siteLeaderMobile) {
        this.siteLeaderMobile = siteLeaderMobile;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateId() {
        return createId;
    }

    public void setCreateId(Integer createId) {
        this.createId = createId;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public Long getReciveTime() {
        return reciveTime;
    }

    public void setReciveTime(Long reciveTime) {
        this.reciveTime = reciveTime;
    }

    public Integer getReciveId() {
        return reciveId;
    }

    public void setReciveId(Integer reciveId) {
        this.reciveId = reciveId;
    }

    public String getReciveName() {
        return reciveName;
    }

    public void setReciveName(String reciveName) {
        this.reciveName = reciveName;
    }

    public Long getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Long processTime) {
        this.processTime = processTime;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    public Long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Long finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getFinishId() {
        return finishId;
    }

    public void setFinishId(Integer finishId) {
        this.finishId = finishId;
    }

    public String getFinishName() {
        return finishName;
    }

    public void setFinishName(String finishName) {
        this.finishName = finishName;
    }

    public String getExceptionDescription() {
        return exceptionDescription.trim();
    }

    public void setExceptionDescription(String exceptionDescription) {
        this.exceptionDescription = exceptionDescription;
    }

}
