package com.enation.app.javashop.core.shop.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: zhou
 * @Date: 2021/3/19
 * @Description: 附近店铺列表请求数据
 */
@Data
public class QueryNearbyShopDTO {

    // 店铺ID
    private String shopId;
    // 用户的openId
    private String openId;
    // 页码
    private Integer pageNo;
    // 每页数量
    private Integer pageSize;
    // 经度
    @NotNull(message = "经度不能为空")
    private Double locationLat;
    // 维度
    @NotNull(message = "维度不能为空")
    private Double locationLng;
}
