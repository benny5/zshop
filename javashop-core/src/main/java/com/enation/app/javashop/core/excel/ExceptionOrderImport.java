package com.enation.app.javashop.core.excel;

import lombok.Data;

/**
 * @author 王志杨
 * @date 2020年9月23日 16:35:51
 * @Description 异常单导入实体
 */

@Data
public class ExceptionOrderImport {

    /**订单号*/
    private String orderSn;
    /**异常上报来源*/
    private String exceptionSource;
    /**异常类型*/
    private String exceptionType;
    /**异常描述*/
    private String exceptionDescription;

}
