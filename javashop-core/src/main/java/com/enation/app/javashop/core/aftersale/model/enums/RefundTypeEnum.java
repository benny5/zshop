package com.enation.app.javashop.core.aftersale.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 申请售后类型枚举类
 * @author zjp
 * @version v7.0
 * @since v7.0 上午9:38 2018/5/3
 */
public enum RefundTypeEnum {
    //取消订单
    CANCEL_ORDER("取消订单"),
    //申请售后
    AFTER_SALE("申请售后"),
    //多付退款
    SPREAD_REFUND("多付退款");

    private static Map<String, String> enumMap = new HashMap<>();

    static {
        for (RefundTypeEnum item : RefundTypeEnum.values()) {
            enumMap.put(item.value(), item.description());
        }
    }

    private String description;

    RefundTypeEnum(String des){
        this.description=des;
    }

    public String description(){
        return this.description;
    }

    public String value(){
        return this.name();
    }

    public static String analysis(String name){
        return enumMap.get(name);
    }
}
