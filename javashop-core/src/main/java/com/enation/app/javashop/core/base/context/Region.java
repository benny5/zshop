package com.enation.app.javashop.core.base.context;


import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 地区对象
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/5/2
 */
@Data
public class Region {
    /**
     * 城市id
     */
    private Integer cityId;
    /**
     * 镇id
     */
    private Integer townId;
    /**
     * 县区id
     */
    private Integer countyId;
    /**
     * 省id
     */
    private Integer provinceId;
    /**
     * 省名称
     */
    private String province;
    /**
     * 县区名称
     */
    private String county;
    /**
     * 城市名称
     */
    private String city;
    /**
     * 镇名称
     */
    private String town;

    //=================================JFENG 20190214=========================================
    /**
     * 门店坐标-纬度
     */
    private Double shopLat;

    /**
     * 门店坐标-经度
     */
    private Double shopLng;

    /**
     * 门店联系电话
     */
    // private String shopContact;
    /**
     * 门店自提地址
     */
    private String shopAddress;
    /**
     * 门店营业时间类型
     */
    private Integer openTimeType;
    /**
     * 门店开始营业时间
     */
    private String openStartTime;
    /**
     * 门店结束营业时间
     */
    private String openEndTime;

    /**
     * 支持配送方式
     */
    private String shipType;

}
