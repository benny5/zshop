package com.enation.app.javashop.core.shop.model.vo;

import javax.validation.constraints.NotEmpty;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * 申请开店第四部VO
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 下午4:08:19
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ApplyStep4VO {
	/**店铺名称*/
	@Column(name = "shop_name")
	@ApiModelProperty(name="shop_name",value="店铺名称",required=true)
	@NotEmpty(message="店铺名称必填")
	private String shopName;

	/**店铺经营类目*/
	@Column(name = "goods_management_category")
	@ApiModelProperty(name="goods_management_category",value="店铺经营类目",required=true)
	@NotEmpty(message="店铺经营类目必填")
	private String goodsManagementCategory;
	/**店铺所在省id*/
	@Column(name = "shop_province_id")
	@ApiModelProperty(name="shop_province_id",value="店铺所在省id",required=false,hidden = true)
	private Integer shopProvinceId;
	/**店铺所在市id*/
	@Column(name = "shop_city_id")
	@ApiModelProperty(name="shop_city_id",value="店铺所在市id",required=false,hidden = true)
	private Integer shopCityId;
	/**店铺所在县id*/
	@Column(name = "shop_county_id")
	@ApiModelProperty(name="shop_county_id",value="店铺所在县id",required=false,hidden = true)
	private Integer shopCountyId;
	/**店铺所在镇id*/
	@Column(name = "shop_town_id")
	@ApiModelProperty(name="shop_town_id",value="店铺所在镇id",required=false,hidden = true)
	private Integer shopTownId;
	/**店铺所在省*/
	@Column(name = "shop_province")
	@ApiModelProperty(name="shop_province",value="店铺所在省",required=false,hidden = true)
	private String shopProvince;
	/**店铺所在市*/
	@Column(name = "shop_city")
	@ApiModelProperty(name="shop_city",value="店铺所在市",required=false,hidden = true)
	private String shopCity;
	/**店铺所在县*/
	@Column(name = "shop_county")
	@ApiModelProperty(name="shop_county",value="店铺所在县",required=false,hidden = true)
	private String shopCounty;
	/**店铺所在镇*/
	@Column(name = "shop_town",allowNullUpdate = true)
	@ApiModelProperty(name="shop_town",value="店铺所在镇",required=false,hidden = true)
	private String shopTown;
	/**申请开店进度*/
	@Column(name = "step")
	@ApiModelProperty(name="step",value="申请开店进度：1,2,3,4",required=false)
	private Integer step;
	@Column(name = "ship_type")
	@ApiModelProperty(name="shipType",value="支持配送方式",required=false)
	private String shipType;
}