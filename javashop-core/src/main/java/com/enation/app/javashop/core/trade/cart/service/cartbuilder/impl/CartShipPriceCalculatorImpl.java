package com.enation.app.javashop.core.trade.cart.service.cartbuilder.impl;

import com.enation.app.javashop.core.client.member.MemberAddressClient;
import com.enation.app.javashop.core.client.member.ShipTemplateClient;
import com.enation.app.javashop.core.goods.GoodsErrorCode;
import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.shop.model.dos.ShipLocalTemplateVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartVO;
import com.enation.app.javashop.core.trade.cart.service.cartbuilder.CartShipPriceCalculator;
import com.enation.app.javashop.core.trade.order.service.CheckoutParamManager;
import com.enation.app.javashop.core.trade.order.service.ShippingManager;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.security.model.Buyer;
import com.enation.app.javashop.framework.util.GeoUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;

/**
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/12/19
 */
@Service
public class CartShipPriceCalculatorImpl implements CartShipPriceCalculator {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private ShippingManager shippingManager;

    /**
     * 计算快递配送价格
     * @param cartList
     */
    @Override
    public void countShipPrice(List<CartVO> cartList) {
        shippingManager.setShippingPrice(cartList);

        if (logger.isDebugEnabled()) {
            logger.debug("购物车处理运费结果为：");
            logger.debug(cartList);
        }
    }


    /**
     * 计算同城配送费用
     * @param cartList
     */
    @Override
    public void countLocalShipPrice(List<CartVO> cartList) {
        shippingManager.setLocalShippingPrice(cartList);

        if (logger.isDebugEnabled()) {
            logger.debug("购物车处理同城运费结果为：");
            logger.debug(cartList);
        }
    }

    /**
     * 核算【可用】配送方式
     * @param cartList
     */
    @Override
    public void uniqueShipWay(List<CartVO> cartList) {
        shippingManager.checkShipWay(cartList);

        if (logger.isDebugEnabled()) {
            logger.debug("购物车汇总配送方式结果为：");
            logger.debug(cartList);
        }
    }

}
