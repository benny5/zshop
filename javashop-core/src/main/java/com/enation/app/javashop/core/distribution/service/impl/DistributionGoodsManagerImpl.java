package com.enation.app.javashop.core.distribution.service.impl;

import com.enation.app.javashop.core.distribution.model.dos.DistributionGoods;
import com.enation.app.javashop.core.distribution.service.DistributionGoodsManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.util.SqlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DistributionGoodsManagerImpl
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-14 上午12:39
 */
@Service
public class DistributionGoodsManagerImpl implements DistributionGoodsManager {


    @Autowired
    @Qualifier("distributionDaoSupport")
    private DaoSupport daoSupport;

    /**
     * 修改分销商品提现设置
     *
     * @param distributionGoods
     * @return
     */
    @Override
    @Transactional(value = "distributionTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public DistributionGoods edit(DistributionGoods distributionGoods) {
        DistributionGoods old = this.daoSupport.queryForObject("select * from es_distribution_goods where goods_id = ?", distributionGoods.getClass(), distributionGoods.getGoodsId());
        if (null == old) {
            daoSupport.insert("es_distribution_goods", distributionGoods);
            return distributionGoods;
        } else {
            Map<String, Object> map = new HashMap<>(16);
            map.put("id", old.getId());
            daoSupport.update("es_distribution_goods", distributionGoods, map);
            return distributionGoods;
        }
    }

    /**
     * 删除
     *
     * @param goodsId
     */
    @Override
    public void delete(Integer goodsId) {
        this.daoSupport.execute("delete from es_distribution_goods where goods_id = ?", goodsId);
    }

    /**
     * 获取
     *
     * @param goodsId
     */
    @Override
    public DistributionGoods getModel(Integer goodsId) {
        return this.daoSupport.queryForObject("select * from es_distribution_goods where goods_id = ?", DistributionGoods.class, goodsId);
    }

    /**
     * 批量编辑比例
     */
    @Override
    @Transactional(value = "distributionTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void batchEditRate(Integer[] goodsIds, DistributionGoods distributionGoods) {
        if(goodsIds == null || goodsIds.length == 0){
            return;
        }
        String sql = "update es_distribution_goods set ";

        Double grade1Rebate = distributionGoods.getGrade1Rebate();
        if(grade1Rebate != null){
            sql += "grade1_rebate = " + grade1Rebate + ",";
        }
        Double grade2Rebate = distributionGoods.getGrade2Rebate();
        if(grade2Rebate != null){
            sql += "grade2_rebate =" + grade2Rebate + ",";
        }
        Double inviterRate = distributionGoods.getInviterRate();
        if(inviterRate != null){
            sql += "inviter_rate = " + inviterRate + ",";
        }
        Double subsidyRate = distributionGoods.getSubsidyRate();
        if(subsidyRate != null){
            sql += "subsidy_rate = " + subsidyRate + ",";
        }

        // 去掉逗号
        sql = sql.substring(0, sql.length() - 1);

        // 再加上批量的ids
        List<Object> term = new ArrayList<>();
        String str = SqlUtil.getInSql(goodsIds, term);
        sql += " where goods_id in (" + str + ")";
        this.daoSupport.execute(sql, term.toArray());
    }
}
