package com.enation.app.javashop.core.base.plugin.sms;

import com.enation.app.javashop.framework.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SmsLczssUtil {

	public static final char[] array = { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h',
			'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Q',
			'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C',
			'V', 'B', 'N', 'M' };



	/**
	 * 使用httpclient发送httpPost请求，区别于urlconnection的post请求
	 * @Title Util.java
	 * @Package util
	 * @author liusiyuan
	 * @date 2018年12月21日
	 * @Description TODO
	 */
	public static String sendHttpClientPost(String url,Map<String,String> headers,Map<String,String> map) throws SocketTimeoutException,Exception{
		// 创建连接
		CloseableHttpClient client = HttpClients.createDefault();
		URIBuilder builder = new URIBuilder(url);
		RequestConfig config = RequestConfig.custom().setConnectTimeout(10000)
				.setConnectionRequestTimeout(10000)
				.setSocketTimeout(10000).build();
		// 从url中获取post连接
		HttpPost post = new HttpPost(builder.build());
		post.setConfig(config);
		// 获取头信息map
		if(headers != null) {
			Set<String> headerKeySet = headers.keySet();
			for (String key : headerKeySet) {
				post.addHeader(key, headers.get(key));
			}
		}
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		if(map != null) {
			for (String key : map.keySet()) {
				nvps.add(new BasicNameValuePair(key, map.get(key)));
			}
		}
		// 设置post消息实体(封装传输的参数)
		post.setEntity(new UrlEncodedFormEntity(nvps,"UTF-8"));
		// 获取返回值
		CloseableHttpResponse resp = client.execute(post);
		HttpEntity httpEntity = resp.getEntity();
		return EntityUtils.toString(httpEntity,"UTF-8");
	}
	/**
	 * 使用httpclient发送httpPost请求，区别于urlconnection的post请求
	 *
	 * @Title Util.java
	 * @Package util
	 * @author liusiyuan
	 * @date 2018年12月21日
	 * @Description TODO
	 */
	public static String sendHttpClientPostJSON(String url, Map<String, String> headers, String jsonParams)
			throws SocketTimeoutException, Exception {
		// 创建连接
		CloseableHttpClient client = HttpClients.createDefault();
		URIBuilder builder = new URIBuilder(url);
		RequestConfig config = RequestConfig.custom().setConnectTimeout(10000).setConnectionRequestTimeout(10000)
				.setSocketTimeout(10000).build();
		// 从url中获取post连接
		HttpPost post = new HttpPost(builder.build());
		post.setConfig(config);
		// 获取头信息map
		if (headers != null) {
			Set<String> headerKeySet = headers.keySet();
			for (String key : headerKeySet) {
				post.addHeader(key, headers.get(key));
			}
		}
		// 设置post消息实体(封装传输的参数)
		StringEntity entity = new StringEntity(jsonParams, Charset.forName("UTF-8"));
		entity.setContentEncoding("UTF-8");
		entity.setContentType("application/json;charset=utf-8");
		post.setEntity(entity);
		// 获取返回值
		CloseableHttpResponse resp = client.execute(post);
		HttpEntity httpEntity = resp.getEntity();
		return EntityUtils.toString(httpEntity, "UTF-8");
	}

	public static String sendGet(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + "?" + param;
			System.out.println(urlNameString);
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			/*
			 * for (String key : map.keySet()) { System.out.println(key + "--->" +
			 * map.get(key)); }
			 */
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 冒泡排序 int[] a={1,4,2,7,5,9,6};
	 *
	 * @param a
	 * @return
	 */
	public static int[] sort(int[] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = i; j < a.length; j++) {
				if (a[i] > a[j]) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
		return a;
	}

	/**
	 * 将json字符串转换为map
	 *
	 * @param data
	 * @return
	 */
	public static Map<String, Object> jsonmap(String data) {
		GsonBuilder gb = new GsonBuilder();
		Gson g = gb.create();
		Map<String, Object> map = g.fromJson(data, new TypeToken<Map<String, Object>>() {
		}.getType());
		return map;
	}

	/**
	 * 根据输入模板从13位时间戳转化时间 toDate(1468377253000L, "YYYY/MM/dd HH:mm:ss")
	 *
	 * @param time
	 *            13位时间戳
	 * @param formate
	 *            时间格式"yy/MM/dd h:m:s"
	 * @return
	 */
	public static String toDate(Long time, String formate) {
		SimpleDateFormat sdf = new SimpleDateFormat(formate);
		String date = sdf.format(new Date(time));
		return date;
	}

	/**
	 * 时间字符串转化时间戳
	 *
	 * @param time
	 *            时间字符串
	 * @param fromat
	 *            字符串格式
	 * @return 十三位时间戳
	 */
	public static Long dateTotimeStemp(String time, String fromat) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromat);
		Long timeStemp = 0L;
		try {
			Date date = simpleDateFormat.parse(time);
			timeStemp = (Long) date.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return timeStemp;
	}




	/**
	 * 把字节数组转成16进位制数
	 *
	 * @param bytes
	 * @return
	 */
	public static String bytesToHex(byte[] bytes) {
		StringBuffer md5str = new StringBuffer();
		// 把数组每一字节换成16进制连成md5字符串
		int digital;
		for (int i = 0; i < bytes.length; i++) {
			digital = bytes[i];
			if (digital < 0) {
				digital += 256;
			}
			if (digital < 16) {
				md5str.append("0");
			}
			md5str.append(Integer.toHexString(digital));
		}
		return md5str.toString();
	}

	/**
	 * 把字节数组转换成md5
	 *
	 * @param input
	 * @return
	 */
	public static String bytesToMD5(byte[] input) {
		String md5str = null;
		try {
			// 创建一个提供信息摘要算法的对象，初始化为md5算法对象
			MessageDigest md = MessageDigest.getInstance("MD5");
			// 计算后获得字节数组
			byte[] buff = md.digest(input);
			// 把数组每一字节换成16进制连成md5字符串
			md5str = bytesToHex(buff);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return md5str;
	}

	/**
	 * 把字符串转换成md5
	 *
	 * @param str
	 * @return
	 */
	public static String strToMD5(String str) {
		byte[] input = str.getBytes();
		return bytesToMD5(input);
	}

	/**
	 * 把文件转成md5字符串
	 *
	 * @param file
	 * @return
	 */
	public static String fileToMD5(File file) {
		if (file == null) {
			return null;
		}
		if (file.exists() == false) {
			return null;
		}
		if (file.isFile() == false) {
			return null;
		}
		FileInputStream fis = null;
		try {
			// 创建一个提供信息摘要算法的对象，初始化为md5算法对象
			MessageDigest md = MessageDigest.getInstance("MD5");
			fis = new FileInputStream(file);
			byte[] buff = new byte[1024];
			int len = 0;
			while (true) {
				len = fis.read(buff, 0, buff.length);
				if (len == -1) {
					break;
				}
				// 每次循环读取一定的字节都更新
				md.update(buff, 0, len);
			}
			// 关闭流
			fis.close();
			// 返回md5字符串
			return bytesToHex(md.digest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 向指定URL发送POST方法的请求
	 *
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是name1=value1&name2=value2的形式
	 * @return URL所代表远程资源的响应
	 * @throws SocketException
	 */
	public static String sendPost(String url, String param) throws SocketException {
		// String param="{\"jobID\":\"CR-22ee1cfb5d9a0e8112ca887e198548c5\"}";
		// String s1 =
		// sendPost("http://ctc.cloudcc.cc:5678/checkcdr.php","{\"jobID\":\"CR-22ee1cfb5d9a0e8112ca887e198548c5\");

		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestProperty("contentType", "UTF-8");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");

			conn.setDoOutput(true);// 发送POST请求必须设置如下两行
			conn.setDoInput(true);
			conn.setReadTimeout(20000);
			conn.setConnectTimeout(20000);
			out = new PrintWriter(conn.getOutputStream());// 获取URLConnection对象对应的输出流s
			out.print(param);// 发送请求参数
			out.flush();// flush输出流的缓冲
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));// 定义BufferedReader输入流来读取URL的响应
			String line;
			while ((line = in.readLine()) != null) {
				result += "\n" + line;
			}
		} catch (SocketException e1) {

			throw e1;

		} catch (Exception e) {
			System.out.println("发送POST请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 向指定URL发送POST方法的请求
	 *
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是name1=value1&name2=value2的形式
	 * @return URL所代表远程资源的响应
	 */
	public static String sendPost1(String url, String param) {
		// String param="{\"jobID\":\"CR-22ee1cfb5d9a0e8112ca887e198548c5\"}";
		// String s1 =
		// sendPost("http://ctc.cloudcc.cc:5678/checkcdr.php","{\"jobID\":\"CR-22ee1cfb5d9a0e8112ca887e198548c5\");

		OutputStreamWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestProperty("contentType", "UTF-8");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");

			conn.setDoOutput(true);// 发送POST请求必须设置如下两行
			conn.setDoInput(true);

			out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");// 获取URLConnection对象对应的输出流s
			out.write(param);// 发送请求参数
			out.flush();// flush输出流的缓冲
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));// 定义BufferedReader输入流来读取URL的响应
			String line;
			while ((line = in.readLine()) != null) {
				result += "\n" + line;
			}
		} catch (Exception e) {
			System.out.println("发送POST请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	public static boolean isNotNullString(String str) {
		return !isNullString(str);
	}

	public static boolean isNullString(String str) {
		return null == str || str.length() == 0 || ("null").equals(str);
	}

	/**
	 * 空值检测
	 *
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		return null == str || str.length() == 0;
	}

	/**
	 * 非空检测
	 *
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	/**
	 * 空格检测
	 *
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str) {
		if (StringUtil.isEmpty(str)) {
			return true;
		}
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 无空格检测
	 *
	 * @param str
	 * @return
	 */
	public static boolean isNotBlank(String str) {
		return !isNotBlank(str);
	}

	/**
	 * 将整数组成的字符串转换为整数列表
	 *
	 * 3,4,5 -> {3, 4, 5}
	 *
	 * @param str
	 *            字符串
	 * @return
	 */
	public static List<Integer> str2IntList(String str) throws Exception {
		String[] strArrays = str.split(",");
		List<Integer> intList = new ArrayList<Integer>();
		for (String strArray : strArrays) {
			intList.add(Integer.valueOf(strArray));
		}
		return intList;
	}

	public static String brandIdToBrandString(Integer brandId) {
		String barnd = "";

		switch (brandId) {
		case 1:
			barnd = "德邦";
			break;
		case 2:
			barnd = "优速";
			break;
		case 3:
			barnd = "龙邦";
			break;
		case 4:
			barnd = "速尔";
			break;
		case 5:
			barnd = "快捷";
			break;
		case 6:
			barnd = "全峰";
			break;
		case 7:
			barnd = "百世快递";
			break;
		case 8:
			barnd = "天天快递";
			break;
		case 9:
			barnd = "中通";
			break;
		case 10:
			barnd = "韵达";
			break;
		case 11:
			barnd = "申通";
			break;
		case 12:
			barnd = "圆通";
			break;
		case 13:
			barnd = "顺丰";
			break;
		case 14:
			barnd = "EMS";
			break;
		case 15:
			barnd = "国通";
			break;
		case 16:
			barnd = "蚂蚁帮";
			break;
		case 17:
			barnd = "邮政小包";
			break;
		case 18:
			barnd = "宅急送";
			break;
		case 19:
			barnd = "跨越";
			break;
		case 20:
			barnd = "京东";
			break;
		case 21:
			barnd = "达达";
			break;
		case 22:
			barnd = "万象";
			break;
		case 23:
			barnd = "妙寄";
			break;
		case 24:
			barnd = "中铁";
			break;
		case 25:
			barnd = "远程";
			break;
		case 26:
			barnd = "安能";
			break;
		case 27:
			barnd = "品骏";
			break;
		case 28:
			barnd = "日日顺";
			break;
		case 29:
			barnd = "如风达";
			break;
		case 32:
			barnd = "其他";
			break;

		}

		return barnd;
	}


	public static String brandIdToBrandJC(Integer brandId) {
		String brandJC = "";
		switch (brandId) {
			case 1:
				brandJC = "DEPPON";
				break;
			case 2:
				brandJC = "UC56";
				break;
			case 3:
				brandJC = "LBEX";
				break;
			case 4:
				brandJC = "SURE";
				break;
			case 5:
				brandJC = "FASTEXPRESS";
				break;
			case 6:
				brandJC = "QFKD";
				break;
			case 7:
				brandJC = "BSO";
				break;
			case 8:
				brandJC = "TTKDEX";
				break;
			case 9:
				brandJC = "ZTO";
				break;
			case 10:
				brandJC = "YUNDA";
				break;
			case 11:
				brandJC = "STO";
				break;
			case 12:
				brandJC = "YTO";
				break;
			case 13:
				brandJC = "SFEXPRESS";
				break;
			case 14:
				brandJC = "EMS";
				break;
			case 15:
				brandJC = "GTO";
				break;
			case 16:
				brandJC = "蚂蚁帮";
				break;
			case 17:
				brandJC = "邮政小包";
				break;
			case 18:
				brandJC = "ZJS";
				break;
			case 19:
				brandJC = "KYEXPRESS";
				break;
			case 20:
				brandJC = "JD";
				break;
			case 21:
				brandJC = "达达";
				break;
			case 22:
				brandJC = "EWINSHINE";
				break;
			case 23:
				brandJC = "MJ";
				break;
			case 24:
				brandJC = "CRE";
				break;
			case 25:
				brandJC = "远程";
				break;
			case 26:
				brandJC = "ANEEX";
				break;
			case 27:
				brandJC = "PJKD";
				break;
			case 28:
				brandJC = "RRS";
				break;
			case 29:
				brandJC = "RFD";
				break;
			case 32:
				brandJC = "OTHER";
				break;
			default:
				brandJC = "OTHER";
				break;
		}
		return brandJC;
	}


	/**
	 * http post请求
	 *
	 * @param url
	 *            请求地址
	 * @param params
	 *            请求参数
	 * @return
	 */
	public static String post(String url, String params) {
		try {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			StringEntity sEntity = new StringEntity(params, "UTF-8");
			httpPost.setEntity(sEntity);
			CloseableHttpResponse response = httpClient.execute(httpPost);
			try {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					return EntityUtils.toString(entity, "UTF-8");
				}
			} finally {
				response.close();
				httpClient.close();
			}
		} catch (Exception e) {
			System.out.println(e);
			throw new RuntimeException(e);
		}
		return null;
	}

	/**
	 *
	 * @author tianyh @Description @param path @param postContent @return
	 * String @throws
	 */
	public static String sendSmsByPost(String path, String postContent) {
		URL url = null;
		try {
			url = new URL(path);
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestMethod("POST");// 提交模式
			httpURLConnection.setConnectTimeout(10000);// 连接超时 单位毫秒
			httpURLConnection.setReadTimeout(10000);// 读取超时 单位毫秒
			// 发送POST请求必须设置如下两行
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setDoInput(true);
			httpURLConnection.setRequestProperty("Charset", "UTF-8");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");

			// PrintWriter printWriter = new
			// PrintWriter(httpURLConnection.getOutputStream());
			// printWriter.write(postContent);
			// printWriter.flush();

			httpURLConnection.connect();
			OutputStream os = httpURLConnection.getOutputStream();
			os.write(postContent.getBytes("UTF-8"));
			os.flush();

			StringBuilder sb = new StringBuilder();
			int httpRspCode = httpURLConnection.getResponseCode();
			if (httpRspCode == HttpURLConnection.HTTP_OK) {
				// 开始获取数据
				BufferedReader br = new BufferedReader(
						new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"));
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
				br.close();
				return sb.toString();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 手机号转邀请码
	 *
	 * @param number
	 * @return
	 */
	public static String mobileToInviteCode(long number) {
		Long rest = number;
		Stack<Character> stack = new Stack<Character>();
		StringBuilder result = new StringBuilder(0);
		while (rest != 0) {
			stack.add(array[new Long((rest - (rest / 62) * 62)).intValue()]);
			rest = rest / 62;
		}
		for (; !stack.isEmpty();) {
			result.append(stack.pop());
		}
		return result.toString();
	}


	/**
	 * 获取某一天的0点时间
	 *
	 * @Title Util.java
	 * @Package com.qd.util
	 * @author liusiyuan
	 * @date 2018年6月22日
	 * @Description TODO
	 */
	public static long getZeroTime(long time) {
		return time = time / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();
	}

	public static String getTimeZone(int hour, int definate) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.HOUR_OF_DAY, definate);
		if (hour >= definate) {
			cal.set(Calendar.HOUR_OF_DAY, definate);
			return cal.getTimeInMillis() + "";
		} else {
			cal.add(Calendar.DAY_OF_YEAR, -1);
			return cal.getTimeInMillis() + "";
		}
	}

	public static Calendar getCalendarInZero() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		return cal;
	}

	/**
	 * 生成秘钥
	 *
	 * @param tm
	 * @param key
	 * @return
	 */
	public static String createSign(TreeMap<String, String> tm, String key) {
		StringBuffer buf = new StringBuffer(key);
		for (Map.Entry<String, String> en : tm.entrySet()) {
			String name = en.getKey();
			String value = en.getValue();
			if (!"sign".equals(name) && !"param".equals(name) && value != null && value.length() > 0
					&& !"null".equals(value)) {
				buf.append(name).append('=').append(value).append('&');
			}
		}
		String _buf = buf.toString();
		return _buf.substring(0, _buf.length() - 1);
	}

	/**
	 * 将文件转成base64 字符串
	 *
	 * @param
	 * @return *
	 * @throws Exception
	 */

	public static String encodeBase64File(String path) throws Exception {
		File file = new File(path);
		;
		FileInputStream inputFile = new FileInputStream(file);
		byte[] buffer = new byte[(int) file.length()];
		inputFile.read(buffer);
		inputFile.close();
		return new BASE64Encoder().encode(buffer);

	}

	public final static String str2MD5(String inStr) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			byte[] strTemp = inStr.getBytes("UTF-8");
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(strTemp);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 字符串加密（移动ascii50码值）
	 *
	 * @Title Util.java
	 * @Package com.miji.util
	 * @author liusiyuan
	 * @date 2019年5月6日
	 * @Description TODO
	 */
	public static String encrypt(String str) {
		String result = str;
		if (!StringUtil.isEmpty(str)) {
			byte[] bytes = str.getBytes();
			// 容错判断
			if (bytes[0] < 70) {
				for (int i = 0; i < bytes.length; i++) {
					bytes[i] = (byte) (bytes[i] + 50);
				}
			}
			result = new String(bytes);
		}
		return result;
	}

	/**
	 * 字符串解密（移动ascii50码值）
	 *
	 * @Title Util.java
	 * @Package com.miji.util
	 * @author liusiyuan
	 * @date 2019年5月6日
	 * @Description TODO
	 */
	public static String decrypt(String str) {
		String result = str;
		if (!StringUtil.isEmpty(str)) {
			byte[] bytes = str.getBytes();
			// 容错判断
			if (bytes[0] > 70) {
				for (int i = 0; i < bytes.length; i++) {
					bytes[i] = (byte) (bytes[i] - 50);
				}
			}
			result = new String(bytes);
		}
		return result;
	}

	public static void main(String[] args) {
		String decrypt = encrypt("15601601206");
		System.out.println(decrypt);
	}
}
