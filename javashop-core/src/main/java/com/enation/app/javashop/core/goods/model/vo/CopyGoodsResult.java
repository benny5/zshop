package com.enation.app.javashop.core.goods.model.vo;

import com.enation.app.javashop.core.aftersale.model.enums.ExceptionClaimsCodeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "返回结果实体")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public  class CopyGoodsResult<T> implements Serializable {


    @ApiModelProperty(name = "code",value = "返回状态码")
    private int code;
    @ApiModelProperty(name = "message",value = "提示信息")
    private String message;
    @ApiModelProperty(name = "data",value = "返回值")
    private T data;

    public CopyGoodsResult(int code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }

    public CopyGoodsResult() {
    }

    public CopyGoodsResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> CopyGoodsResult success(T data){
        return new CopyGoodsResult<>(ExceptionClaimsCodeEnum.S200.getCode(), ExceptionClaimsCodeEnum.S200.getDescription(), data);
    }

    public static CopyGoodsResult successMessage(){
        return new CopyGoodsResult(ExceptionClaimsCodeEnum.S200.getCode(), ExceptionClaimsCodeEnum.S200.getDescription());
    }
}
