package com.enation.app.javashop.core.payment.plugin.alipay;

import com.alipay.api.util.AlipayPayment;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.payment.model.dos.PaymentMethodDO;
import com.enation.app.javashop.core.payment.model.enums.AlipayConfigItem;
import com.enation.app.javashop.core.payment.model.enums.ClientType;
import com.enation.app.javashop.core.payment.model.enums.TradeType;
import com.enation.app.javashop.core.payment.model.vo.*;
import com.enation.app.javashop.core.payment.plugin.alipay.executor.AliPayPaymentAppExecutor;
import com.enation.app.javashop.core.payment.plugin.alipay.executor.AliPayPaymentExecutor;
import com.enation.app.javashop.core.payment.plugin.alipay.executor.AliPayPaymentWapExecutor;
import com.enation.app.javashop.core.payment.plugin.alipay.executor.AlipayRefundExcutor;
import com.enation.app.javashop.core.payment.service.PaymentBillManager;
import com.enation.app.javashop.core.payment.service.PaymentManager;
import com.enation.app.javashop.core.payment.service.PaymentPluginManager;
import com.enation.app.javashop.core.payment.service.RefundManager;
import com.enation.app.javashop.core.trade.order.model.enums.TradeTypeEnum;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fk
 * @version v2.0
 * @Description: 支付宝支付插件
 * @date 2018/4/12 10:25
 * @since v7.0.0
 */
@Service
//@ComponentScan("com.alipay.api")
@ComponentScan(value = "com.alipay.api", excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, value = AlipayPayment.class)
        ,@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = AlipayConfig.class)})
public class AlipayDirectPlugin implements PaymentPluginManager {

    @Autowired
    private AliPayPaymentAppExecutor aliPayPaymentAppExecutor;

    @Autowired
    private AliPayPaymentExecutor aliPayPaymentExecutor;

    @Autowired
    private AliPayPaymentWapExecutor aliPayPaymentWapExecutor;

    @Autowired
    private AlipayRefundExcutor alipayRefundExcutor;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private RefundManager refundManager;

    @Autowired
    private PaymentBillManager paymentBillManager;

    @Override
    public Form pay(PayBill bill) {

        //使用支付客户端判断调用哪个执行者
        if (bill.getClientType().equals(ClientType.PC)) {

            return aliPayPaymentExecutor.onPay(bill);
        }

        if (bill.getClientType().equals(ClientType.WAP)) {

            return aliPayPaymentWapExecutor.onPay(bill);
        }

        if (bill.getClientType().equals(ClientType.NATIVE) || bill.getClientType().equals(ClientType.REACT)) {

            return aliPayPaymentAppExecutor.onPay(bill);
        }
        return null;
    }

    @Override
    public String getPluginId() {
        return "alipayDirectPlugin";
    }

    @Override
    public String getPluginName() {
        return "支付宝";
    }

    @Override
    public List<ClientConfig> definitionClientConfig() {

        List<ClientConfig> resultList = new ArrayList<>();

        ClientConfig config = new ClientConfig();

        List<PayConfigItem> configList = new ArrayList<>();
        for (AlipayConfigItem value : AlipayConfigItem.values()) {
            PayConfigItem item = new PayConfigItem();
            item.setName(value.name());
            item.setText(value.getText());
            configList.add(item);
        }

        config.setKey(ClientType.PC.getDbColumn() + "&" + ClientType.WAP.getDbColumn() + "&" + ClientType.NATIVE.getDbColumn() + "&" + ClientType.REACT.getDbColumn());
        config.setConfigList(configList);
        config.setName("是否开启");

        resultList.add(config);

        return resultList;
    }


    @Override
    public void onReturn(TradeType tradeType) {

        aliPayPaymentExecutor.onReturn(tradeType);
    }

    @Override
    public String onCallback(TradeType tradeType, ClientType clientType) {

        return aliPayPaymentExecutor.onCallback(tradeType, clientType);
    }

    @Override
    public String onQuery(PayBill bill) {

        return aliPayPaymentExecutor.onQuery(bill);
    }

    @Override
    public boolean onTradeRefund(RefundBill bill) {

        return alipayRefundExcutor.refundPay(bill);
    }

    @Override
    public String queryRefundStatus(RefundBill bill) {

        return alipayRefundExcutor.queryRefundStatus(bill);
    }

    @Override
    public Integer getIsRetrace() {

        return 1;
    }

    @Override
    public PayBill createPaymentBill(PayBill payBill, PaymentMethodDO paymentMethod) {
        paymentManager.createPaymentBill(payBill, paymentMethod);
        return payBill;
    }

    @Override
    public void createRefundItem(RefundDO refundDO, OrderDetailDTO orderDetail) {
        String sn = orderDetail.getTradeType().equalsIgnoreCase(TradeTypeEnum.ORDER.name()) ? refundDO.getOrderSn() : refundDO.getTradeSn();
        PaymentBillDO paymentBill = this.paymentBillManager.getBillBySnAndTradeTypeAndPaymentPluginID(sn, orderDetail.getTradeType(), this.getPluginId());
        paymentBill.setTradePrice(refundDO.getRefundPrice());
        refundManager.createRefundItem(refundDO, paymentBill);
    }
}
