package com.enation.app.javashop.core.goods.model.vo;

import com.enation.app.javashop.core.goods.model.dos.DraftGoodsSkuDO;
import com.enation.app.javashop.core.goods.model.dos.GoodsSkuDO;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import java.util.List;

/**
 * 商品sku
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月21日 上午11:50:42
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class GoodsSkuVO extends GoodsSkuDO {

    /**
     *
     */
    private static final long serialVersionUID = -666090547834195127L;

    @ApiModelProperty(name = "spec_list", value = "规格列表", required = false)
    private List<SpecValueVO> specList;

    @ApiModelProperty(name = "goods_transfee_charge", value = "谁承担运费0：买家承担，1：卖家承担", hidden = true)
    private Integer goodsTransfeeCharge;

    @ApiModelProperty(value = "是否被删除 0 删除 1 未删除", hidden = true)
    private Integer disabled;

    @ApiModelProperty(value = "上架状态  0下架 1上架", hidden = true)
    private Integer marketEnable;

    @ApiModelProperty(name = "goods_type", value = "商品类型NORMAL普通POINT积分,VIRTUAL虚拟商品")
    private String goodsType;

    @ApiModelProperty(value = "最后修改时间")
    private Long lastModify;

    @ApiModelProperty(value = "支持快递")
    private Integer isLocal;

    @ApiModelProperty(value = "支持同城")
    private Integer isGlobal;

    @ApiModelProperty(value = "支持自提")
    private Integer isSelf;

    @ApiModelProperty(value = "运费定价方式 1统一价 2模板")
    private Integer freightPricingWay;

    @ApiModelProperty(value = "统一运费")
    private Double freightUnifiedPrice;

    @ApiModelProperty(value = "供应商sku_id")
    private String upSkuId;

    @ApiModelProperty(value = "提货时间")
    private Long pickTime;

    @ApiModelProperty(name = "expiry_day",value = "虚拟商品的使用截止日期")
    private Integer expiryDay;

    @ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
    private String availableDate;

    @ApiModelProperty(name = "add_cart_num", value = "加入购物车数量")
    private Integer addCartNum;

    public List<SpecValueVO> getSpecList() {

        if (this.getSpecs() != null) {
            return JsonUtil.jsonToList(this.getSpecs(), SpecValueVO.class);
        }

        return specList;
    }

    public void setSpecList(List<SpecValueVO> specList) {
        this.specList = specList;
    }

    public GoodsSkuVO() {
    }

    public GoodsSkuVO(DraftGoodsSkuDO draftSku) {
        this.setCost(draftSku.getCost());
        this.setPrice(draftSku.getPrice());
        this.setQuantity(draftSku.getQuantity());
        this.setSkuId(draftSku.getDraftSkuId());
        this.setSn(draftSku.getSn());
        this.setWeight(draftSku.getWeight());
        this.setSpecList(JsonUtil.jsonToList(draftSku.getSpecs(), SpecValueVO.class));
    }

}
