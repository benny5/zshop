package com.enation.app.javashop.core.promotion.luck.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 孙建
 * 抽奖活动奖品DO
 */
@Table(name="es_luck_prize")
@Data
public class LuckPrizeDO implements Serializable {

    /**
     * 抽奖活动奖品ID
     */
    @Id(name="luck_prize_id")
    private Integer luckPrizeId;
    /**
     * 抽奖活动ID
     */
    @Column(name="luck_id")
    private Integer luckId;
    /**
     * 奖品名称
     */
    @Column(name = "prize_name")
    private String prizeName;
    /**
     * 中奖概率
     */
    @Column(name = "probability")
    private BigDecimal probability;
    /**
     * 奖品个数
     */
    @Column(name = "prize_num")
    private Integer prizeNum;
    /**
     * 奖品类型 1实物奖品 2优惠券 3虚拟奖品 4谢谢参与
     */
    @Column(name = "prize_type")
    private Integer prizeType;
    /**
     * 奖品图片url
     */
    @Column(name = "prize_img")
    private String prizeImg;
    /**
     * 出库类型
     */
    @Column(name = "delivery_type")
    private Integer deliveryType;
    /**
     * 优惠券id
     */
    @Column(name = "coupon_id")
    private Integer couponId;
    /**
     * 店铺ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;

}