package com.enation.app.javashop.core.orderbill.model.vo;

/**
 * @author fk
 * @version v2.0
 * @Description: 结算单查询条件
 * @date 2018/4/2810:49
 * @since v7.0.0
 */
public class BillQueryParam {

    private Integer pageNo;

    private Integer pageSize;

    private Integer sellerId;

    private String sn;

    public BillQueryParam(Integer pageNo, Integer pageSize, Integer sellerId, String sn) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.sellerId = sellerId;
        this.sn = sn;
    }

    public BillQueryParam() {
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }
}
