package com.enation.app.javashop.core.payment.model.vo;

import lombok.Data;

import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/10/10
 * @Description: 企业微信转账到微信零钱
 */
@Data
public class TransferBill {

    //转账单号
    private String partnerTradeNo;

    //转账金额(单位是元)
    private Double transferPrice;

    //转账给谁
    private Integer payeeMemberId;

    //真实姓名
    private String realName;

    //转账备注
    private String describe;

}
