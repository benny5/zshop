package com.enation.app.javashop.core.shop.model.enums;
/**
 * 
 * 店铺模版枚举
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 上午11:04:25
 */
public enum ShopTypeEnum {

	PERSONAL("个人店铺"),

	ENTERPRISE("企业店铺");

	private String description;

	ShopTypeEnum(String des) {
		this.description = des;
	}

	public String description() {
		return this.description;
	}

	public String value() {
		return this.name();
	}
}
