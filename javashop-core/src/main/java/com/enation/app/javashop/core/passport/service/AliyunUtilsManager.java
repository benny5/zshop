package com.enation.app.javashop.core.passport.service;

/**
 * @author JFeng
 * @date 2020/7/9 10:23
 */
public interface AliyunUtilsManager {

    Boolean check(String sessionId,String ncToken,String sig);

    String getIPLocation();
}
