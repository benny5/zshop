package com.enation.app.javashop.core.base.message;

import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 社区团购变更
 */

@Data
public class ShetuanChangeMsg implements Serializable {

    private static final long serialVersionUID = 8915428082431868648L;

    private List<Integer> skuIds;

    private Integer shetuanId;

    public ShetuanChangeMsg(Integer shetuanId,List<Integer> skuIds) {
        this.skuIds = skuIds;
        this.shetuanId = shetuanId;
    }

    public ShetuanChangeMsg() {
    }
}
