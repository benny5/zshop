package com.enation.app.javashop.core.member.service.impl;

import com.enation.app.javashop.core.member.model.dos.BuyerLastLeaderDO;
import com.enation.app.javashop.core.member.model.dos.LeaderFansDO;
import com.enation.app.javashop.core.member.service.LeaderFansManager;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class LeaderFansManagerImpl implements LeaderFansManager {

    @Autowired
    @Qualifier("memberDaoSupport")
    private DaoSupport daoSupport;

    @Override
    public boolean band(LeaderFansDO leaderFansDO) {
        leaderFansDO.setCreateTime(DateUtil.getDateline());
        /**如果已绑定测忽略*/
        String sql = "select * from es_leader_fans where leader_id =?  and buyer_id = ?";
        LeaderFansDO leaderFansDO1 = daoSupport.queryForObject(sql,LeaderFansDO.class,leaderFansDO.getLeaderId(),leaderFansDO.getBuyerId());
        // 新建粉丝关系 1：N
        if(leaderFansDO1==null){
            daoSupport.insert(leaderFansDO);
        }
        // 绑定用户和自提点关系 1:1
        this.updateLastLeader(leaderFansDO.getBuyerId(),leaderFansDO.getLeaderId());
        return true;
    }

    @Override
    public BuyerLastLeaderDO getLastLeader(Integer buyerId) {
        String sql = "select * from es_buyer_last_leader where buyer_id=?";
        BuyerLastLeaderDO  lastLeaderDO = this.daoSupport.queryForObject(sql , BuyerLastLeaderDO.class,buyerId);
        return lastLeaderDO;
    }


    @Override
    public Page getListByLeaderId(Integer leaderId, Integer pageNo, Integer pageSize) {
        String sql = "select * from es_leader_fans where leader_id = ?";
        return this.daoSupport.queryForPage(sql, pageNo, pageSize, LeaderFansDO.class, leaderId);
    }




    /**
     * 更新最后的绑定关系，如果已是最后的绑定关系，则忽略，否则更新或插入
     */
    private boolean updateLastLeader(Integer buyerId, Integer leaderId) {
        String sql = "select * from es_buyer_last_leader where buyer_id=?";
        BuyerLastLeaderDO  lastLeaderDO = this.daoSupport.queryForObject(sql , BuyerLastLeaderDO.class,buyerId);
        // 修改了团长自提点
        if(lastLeaderDO != null && !lastLeaderDO.getLeaderId().equals(leaderId)){
            lastLeaderDO.setLeaderId(leaderId);
            this.daoSupport.update(lastLeaderDO,lastLeaderDO.getId());

        }
        // 新增绑定自提点
        if(lastLeaderDO==null){
            //没有最后的绑定记录
            Map map = new HashMap();
            map.put("buyer_id",buyerId);
            map.put("leader_id",leaderId);
            map.put("update_time",DateUtil.getDateline());
            this.daoSupport.insert("es_buyer_last_leader",map);
        }
        return true;
    }
}
