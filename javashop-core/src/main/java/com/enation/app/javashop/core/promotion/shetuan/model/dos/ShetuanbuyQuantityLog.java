package com.enation.app.javashop.core.promotion.shetuan.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 团购商品库存日志表实体
 * @author Snow
 * @version v1.0
 * @since v7.0.0
 * 2018-07-09 15:32:29
 */
@Table(name="es_shetuan_quantity_log")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShetuanbuyQuantityLog implements Serializable {

    private static final long serialVersionUID = 2276297510896449L;

    /**日志id*/
    @Id(name = "log_id")
    @ApiModelProperty(hidden=true)
    private Integer logId;

    /**订单编号*/
    @Column(name = "order_sn")
    @ApiModelProperty(name="order_sn",value="订单编号",required=false)
    private String orderSn;

    /**商品ID*/
    @Column(name = "goods_id")
    @ApiModelProperty(name="goods_id",value="商品ID",required=false)
    private Integer goodsId;

    /**sku_ID*/
    @Column(name = "product_id")
    @ApiModelProperty(name="product_id",value="sku ID",required=false)
    private Integer productId;

    /**数量*/
    @Column(name = "quantity")
    @ApiModelProperty(name="quantity",value="数量",required=false)
    private Integer quantity;

    /**操作时间*/
    @Column(name = "op_time")
    @ApiModelProperty(name="op_time",value="操作时间",required=false)
    private Long opTime;

    /**日志类型*/
    @Column(name = "log_type")
    @ApiModelProperty(name="log_type",value="日志类型",required=false)
    private String logType;

    /**操作原因*/
    @Column(name = "reason")
    @ApiModelProperty(name="reason",value="操作原因",required=false)
    private String reason;

    @Column(name = "st_id")
    @ApiModelProperty(name="st_id",value="团购活动id",required=false)
    private Integer stId;


}
