package com.enation.app.javashop.core.member.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * 团长查询Vo
 * @author john
 * @version v1.0
 * 2020年5月27日 下午8:43:57
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class LeaderQueryVO {

	@ApiModelProperty(name="page_no",value = "页码")
	private Integer pageNo;

	@ApiModelProperty(name="page_size",value = "分页数")
	private Integer pageSize;

    @ApiModelProperty(name="seller_id",value = "卖家id")
	private Integer sellerId;

    @ApiModelProperty(name = "leader_name",value = "团长名称")
    private String leaderName;

    @ApiModelProperty(name = "leader_mobile",value = "团长手机号")
    private String leaderMobile;

    @ApiModelProperty(name = "leader_type",value = "团长类型 0虚拟 1真实")
    private Integer leaderType;

    @ApiModelProperty(name = "cell_name",value = "小区名称")
    private String cellName;

    @ApiModelProperty(name = "site_name",value = "站点名称")
    private String siteName;

    @ApiModelProperty(name = "site_type",value = "站点类型")
    private Integer siteType;

    @ApiModelProperty(name = "status",value = "状态")
    private Integer status;

    /**开始时间*/
    @ApiModelProperty(name="start_time",value="店铺开始时间",required=false)
    private Long startTime;

    /**结束时间*/
    @ApiModelProperty(name="end_time",value="店铺关闭时间",required=false)
    private Long endTime;

    /**关键字*/
    @ApiModelProperty(name="keyword",value="关键字",required=false)
    private String keyword;


    @ApiModelProperty(name = "op_status",value = "营业状态")
    private Integer opStatus;

    /**所在省id*/
    @ApiModelProperty(name="province_id",value="所在省id",required=false)
    private Integer provinceId;

    /**所在市id*/
    @ApiModelProperty(name="city_id",value="所在市id",required=false)
    private Integer cityId;


    /**所在县id*/
    @ApiModelProperty(name="county_id",value="所在县id",required=false)
    private Integer countyId;

    /**所在镇id*/
    @ApiModelProperty(name="town_id",value="所在镇id",required=false)
    private Integer townId;

    /**所在市id*/
    @ApiModelProperty(name="city",value="所在市",required=false)
    private String  city;

    @ApiModelProperty(name = "audit_status",value = "审核状态")
    private Integer auditStatus;


    @ApiModelProperty(name = "origin_site_name",value = "店铺名称")
    private String originSiteName;


}
