package com.enation.app.javashop.core.promotion.tool.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 活动DTO
 *
 * @author Snow create in 2018/7/9
 * @version v2.0
 * @since v7.0.0
 */

@Data
public class PromotionDTO {

    @ApiModelProperty(value="参与的活动ID")
    private Integer actId;

    @ApiModelProperty(value="商品商品")
    private Integer goodsId;

    @ApiModelProperty(value="skuId")
    private Integer productId;

    @ApiModelProperty(value="购买的数量")
    private Integer num;


}
