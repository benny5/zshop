package com.enation.app.javashop.core.promotion.newcomer.model.dos;

import java.io.Serializable;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

/**
 * @Author: zhou
 * @Date 2021-02-25 10:05:27
 * @Description: 新人活动商品
 */
@Data
@Table( name ="es_newcomer_goods" )
public class NewcomerGoodsDO  implements Serializable {
	private static final long serialVersionUID =  4499653316914181842L;

	@Id(name = "id" )
	private Integer id;

	/**
	 * 新人活动ID
	 */
   	@Column(name = "newcomer_id" )
	private Integer newcomerId;

   	@Column(name = "goods_id" )
	private Integer goodsId;

   	@Column(name = "sku_id" )
	private Integer skuId;

	/**
	 * 商品名
	 */
   	@Column(name = "goods_name" )
	private String goodsName;

	/**
	 * 店铺ID
	 */
   	@Column(name = "seller_id" )
	private Integer sellerId;

	/**
	 * 店铺名
	 */
   	@Column(name = "seller_name" )
	private String sellerName;

	/**
	 * 成本价
	 */
   	@Column(name = "cost" )
	private Double cost;

	/**
	 * 店铺价
	 */
   	@Column(name = "seller_price" )
	private Double sellerPrice;

	/**
	 * 售价
	 */
   	@Column(name = "sales_price" )
	private Double salesPrice;

	/**
	 * 商品限购数量
	 */
   	@Column(name = "limit_num" )
	private Integer limitNum;

	/**
	 * 活动商品库存
	 */
   	@Column(name = "sold_quantity" )
	private Integer soldQuantity;

	/**
	 * 已售数量
	 */
   	@Column(name = "sales_num" )
	private Integer salesNum;

	/**
	 * 创建时间
	 */
   	@Column(name = "create_time" )
	private Long createTime;

	/**
	 * 修改时间
	 */
   	@Column(name = "update_time" )
	private Long updateTime;

	/**
	 * 是否删除
	 */
	@Column(name = "is_delete" )
	private Integer isDelete;

}
