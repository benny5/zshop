package com.enation.app.javashop.core.distribution.model.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2021/1/22 15:25
 * 粉丝数据VO
 */
@Data
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class FansDataVO implements Serializable {

    @ApiModelProperty(name = "nickname", value = "粉丝昵称")
    private String nickname;

    @ApiModelProperty(name = "face", value = "粉丝头像")
    private String face;

    @ApiModelProperty(name = "lv1_create_time", value = "团长绑定时间")
    private Long lv1CreateTime;

    @ApiModelProperty(name = "last_order_time", value = "最后下单时间")
    private Long lastOrderTime;

    @ApiModelProperty(name = "last_visit_day", value = "最后访问时间")
    private Long lastVisitDay;

}
