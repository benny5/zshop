package com.enation.app.javashop.core.promotion.seckill.service.impl;

import com.enation.app.javashop.core.base.rabbitmq.TimeExecute;
import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillApplyDO;
import com.enation.app.javashop.core.promotion.seckill.service.SeckillPromotionManager;
import com.enation.app.javashop.core.promotion.tool.model.dos.PromotionGoodsDO;
import com.enation.app.javashop.core.promotion.tool.model.dto.PromotionPriceDTO;
import com.enation.app.javashop.core.promotion.tool.model.enums.PromotionTypeEnum;
import com.enation.app.javashop.core.promotion.tool.service.PromotionGoodsManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.trigger.Interface.TimeTrigger;
import com.enation.app.javashop.framework.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JFeng
 * @date 2020/12/25 11:41
 */
@Service
public class SeckillPromotionManagerImpl implements SeckillPromotionManager {

    @Autowired
    private TimeTrigger timeTrigger;

    @Autowired
    private PromotionGoodsManager promotionGoodsManager;

    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport daoSupport;

    /**
     * 秒杀商品上线通过es_promotion_goods(商品促销信息管理) 所以秒杀商品的基础数据需要关联促销
     * @param apply
     */
    @Override
    public void addPromotionGoods(SeckillApplyDO apply) {
        PromotionGoodsDO promotion = transSeckillApplyToPromotionGoods(apply);
        this.daoSupport.insert("es_promotion_goods", promotion);

        //updateEsGoodsDiscountPrice(apply, startTime, endTime);

    }

    private PromotionGoodsDO transSeckillApplyToPromotionGoods(SeckillApplyDO apply) {
        //促销商品表
        PromotionGoodsDO promotion = new PromotionGoodsDO();
        promotion.setTitle("限时抢购");
        promotion.setGoodsId(apply.getGoodsId());
        promotion.setProductId(apply.getSkuId());
        promotion.setPromotionType(PromotionTypeEnum.SECKILL.name());
        promotion.setActivityId(apply.getApplyId());
        promotion.setNum(apply.getSoldQuantity());
        promotion.setPrice(apply.getPrice());
        promotion.setSellerId(apply.getSellerId());
        //商品活动的开始时间为当前商品的参加时间段
        int timeLine = apply.getTimeLine();
        String date = DateUtil.toString(apply.getStartDay(), "yyyy-MM-dd");
        long startTime = DateUtil.getDateline(date + " " + timeLine + ":00:00", "yyyy-MM-dd HH:mm:ss");
        long endTime = DateUtil.getDateline(date + " 23:59:59", "yyyy-MM-dd HH:mm:ss");

        promotion.setStartTime(startTime);
        promotion.setEndTime(endTime);
        return promotion;
    }

    @Override
    public void batchDeletePromotionGoods(List<Integer> deleteIds) {
        promotionGoodsManager.deleteByActivityIds(deleteIds,PromotionTypeEnum.SECKILL);
    }

    @Override
    public void batchUpdatePromotionGoods(List<SeckillApplyDO> updateSeckillGoods) {
        List<PromotionGoodsDO> promotionGoods = transToPromotionGoods(updateSeckillGoods);
        promotionGoodsManager.batchUpdate(promotionGoods);
    }

    @Override
    public void batchAddPromotionGoods(List<SeckillApplyDO> newSeckillGoods) {
        List<PromotionGoodsDO> promotionGoods = transToPromotionGoods(newSeckillGoods);
        promotionGoodsManager.batchInsert(promotionGoods);
    }

    private List<PromotionGoodsDO> transToPromotionGoods(List<SeckillApplyDO> newSeckillGoods) {
        List<PromotionGoodsDO> promotionGoods = new ArrayList<>();
        for (SeckillApplyDO seckillApplyDO : newSeckillGoods) {
            PromotionGoodsDO promotion = transSeckillApplyToPromotionGoods(seckillApplyDO);
            promotionGoods.add(promotion);
        }
        return promotionGoods;
    }


    /**
     * TODO
     * @param apply
     * @param startTime
     * @param endTime
     */
    private void updateEsGoodsDiscountPrice(SeckillApplyDO apply, long startTime, long endTime) {
        //从缓存读取限时抢购的活动的商品
        //String redisKey = getRedisKey(apply.getStartDay());
        //Map<Integer, List<SeckillGoodsVO>> map = this.cache.getHash(redisKey);
        //如果redis中有当前审核商品参与的限时抢购活动商品信息，就删除掉
        //if (map != null && !map.isEmpty()) {
        //    this.cache.remove(redisKey);
        //}

        //设置延迟加载任务，到活动开始时间后将搜索引擎中的优惠价格设置为0
        PromotionPriceDTO promotionPriceDTO = new PromotionPriceDTO();
        promotionPriceDTO.setGoodsId(apply.getGoodsId());
        promotionPriceDTO.setPrice(apply.getPrice());
        timeTrigger.add(TimeExecute.PROMOTION_EXECUTER, promotionPriceDTO, startTime, null);
        //此活动结束后将索引的优惠价格重置为0
        promotionPriceDTO.setPrice(0.0);
        timeTrigger.add(TimeExecute.PROMOTION_EXECUTER, promotionPriceDTO, endTime, null);
    }

}
