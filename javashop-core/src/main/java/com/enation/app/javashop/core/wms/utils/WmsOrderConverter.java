package com.enation.app.javashop.core.wms.utils;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.member.model.dos.LeaderDO;
import com.enation.app.javashop.core.promotion.tool.model.vo.PromotionVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartPromotionVo;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderTypeEnum;
import com.enation.app.javashop.core.wms.model.dos.WmsOrderDO;
import com.enation.app.javashop.core.wms.model.enums.WmsOrderStatusEnums;
import com.enation.app.javashop.core.wms.model.enums.WmsOrderTypeEnums;
import com.enation.app.javashop.framework.util.DateUtil;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * @author JFeng
 * @date 2020/9/25 10:50
 */
public  class WmsOrderConverter {

    public static WmsOrderDO convertOrder(OrderDO orderDO,LeaderDO leaderDO){
        // 正常待发货订单
        WmsOrderDO wmsOrderDO = new WmsOrderDO();
        // 出库时间 隔日发
        //wmsOrderDO.setDeliverySn("CK-"+ DateUtil.toString(DateUtil.getDateline(), "yyMMddhhmmss")+new Random().nextInt(99));
        wmsOrderDO.setOrderType(WmsOrderTypeEnums.ORDER.value());
        // 订单 orderDO -->>> 出库单 wmsOrderDO
        wmsOrderDO.setShipName(orderDO.getShipName());
        wmsOrderDO.setShipMobile(orderDO.getShipMobile());

        wmsOrderDO.setOrderId(orderDO.getOrderId());
        wmsOrderDO.setOrderSn(orderDO.getSn());

        wmsOrderDO.setSellerId(orderDO.getSellerId());
        wmsOrderDO.setItemsJson(orderDO.getItemsJson());
        wmsOrderDO.setShippingType(orderDO.getShippingType());
        wmsOrderDO.setReceiveTime(orderDO.getReceiveTime());
        wmsOrderDO.setReceiveTimeType(orderDO.getReceiveTimeType());

        if(orderDO.getLeaderId()!=null){
            wmsOrderDO.setSiteId(leaderDO.getLeaderId());
            wmsOrderDO.setSiteName(leaderDO.getLeaderName());
            wmsOrderDO.setSiteMobile(leaderDO.getLeaderMobile());
            wmsOrderDO.setSiteAddr((leaderDO.getProvince()+leaderDO.getCity()+leaderDO.getCounty()+leaderDO.getTown()+leaderDO.getAddress()).replace("null",""));
        }else{
            // 快递配送
            wmsOrderDO.setShipAddr((orderDO.getShipProvince()+orderDO.getShipCity()+orderDO.getShipCounty()+orderDO.getShipTown()+orderDO.getShipAddr()).replace("null",""));
        }

        wmsOrderDO.setCreateTime(DateUtil.getDateline());
        return wmsOrderDO;
    }

    public static WmsOrderDO convertClaims(ClaimsDO claimsDO,OrderDO orderDO,LeaderDO leaderDO){
        WmsOrderDO wmsOrderDO = convertOrder(orderDO, leaderDO);

        wmsOrderDO.setItemsJson(claimsDO.getClaimSkus());
        wmsOrderDO.setClaimsId(claimsDO.getClaimId());
        wmsOrderDO.setOrderSn(claimsDO.getOrderSn());
        wmsOrderDO.setRemarks("理赔异常单");
        wmsOrderDO.setOrderType(WmsOrderTypeEnums.CLAIMS.value());
        return wmsOrderDO;
    }


}
