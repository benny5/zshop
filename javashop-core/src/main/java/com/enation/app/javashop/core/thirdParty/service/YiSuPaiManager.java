package com.enation.app.javashop.core.thirdParty.service;

import com.enation.app.javashop.core.thirdParty.model.dto.yisupai.*;

import java.util.List;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2021/5/8
 * @Description: 易速派接口
 */

public interface YiSuPaiManager {

    // 渲染订单 下单之前必须调用此接口，获取订单拆单，商品是否可售等信息，一般只有一单多品的时候才会有拆单的情况，
    // 注意：只支持同一渠道商品渲染，不同渠道商品需要分开渲染
    List<String> yiSuPaiRendorOrder(Integer orderId);

    // 易速派，下单不支付
    List<String> yiSuPaiCreateOrder(Integer orderId);

    /**
     * 支付订单，一次只能调用一个用户的订单
     * @param orderSnList 需要支付的订单编号列表
     * @param memberId 下单的用户ID
     * @return map<订单编号，支付是否成功>
     */
    Map<String,Boolean> yiSuPaiEnableOrder(List<String> orderSnList,Integer memberId);

    // 取消订单
    Map<String,Boolean> yiSuPaiCancelOrder(List<String> orderSnList,Integer memberId);

    // 保存商品
    void addYiSuPaiGoods();

    // 获取商品列表
    List<YiSuPaiGoodsDTO> getGoodsList(int page ,int pageSize);

    // 获取商品详情
    YiSuPaiGoodsDTO getGoodsDetail(String itemId);

    // 获取商品库存
    List<YiSuPaiGoodsDTO> getGoodsQuantity(List<String> itemIds);

    //获取分类
    Map<String, YiSuPaiCategoryDTO> getCategoryList();

    // 申请匿名用户
    String createAnonyAccount(Integer memberId);

    // 获取订单详情
    YiSuPaiOrderDetailDTO yiSuPaiOrderDetail(String orderSn, Integer memberId);

    // 获取订单列表
    List<YiSuPaiOrderDetailDTO> yiSuPaiOrderDetailList(YiSuPaiQueryOrder queryOrder, Integer memberId);

    // 获取物流信息
    YiSuPaiLogisticsDTO yiSuPaiQueryLogistics(String orderSn, Integer memberId);

    // 确认收货
    Boolean yiSuPaiConfirmDisburse(String orderSn, Integer memberId);
}
