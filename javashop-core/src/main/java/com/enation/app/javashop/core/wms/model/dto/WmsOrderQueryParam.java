package com.enation.app.javashop.core.wms.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/9/21 17:41
 */
@Data
public class WmsOrderQueryParam {


    @ApiModelProperty(name = "page_no",value = "第几页")
    private Integer pageNo;

    @ApiModelProperty(name = "page_size",value = "每页条数")
    private Integer pageSize;

    @ApiModelProperty(name = "seller_id",value = "店铺id")
    private Integer sellerId;

    @ApiModelProperty(name = "sort_field",value = "排序字段")
    private String sortField;

    @ApiModelProperty(name = "start_time",value = "开始创建时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time",value = "结束创建时间")
    private Long endTime;

    @ApiModelProperty(name = "order_sn",value = "订单编号")
    private String orderSn;

    @ApiModelProperty(name = "ship_name",value = "收货人")
    private String shipName;

    @ApiModelProperty(name = "ship_name",value = "收货人电话")
    private String shipMobile;

    @ApiModelProperty(name = "order_type",value = "订单类型")
    private String orderType;

    @ApiModelProperty(name = "order_status",value = "订单状态")
    private String orderStatus;

    @ApiModelProperty(name = "shipping_type",value = "配送方式")
    private String shippingType;

    @ApiModelProperty(name = "sorting_batch",value = "分拣批次")
    private Integer sortingBatch;

    @ApiModelProperty(name = "sorting_seq",value = "分拣序号")
    private Integer sortingSeq;

    @ApiModelProperty(name = "delivery_sn",value = "出库单号")
    private String deliverySn;

    @ApiModelProperty(name = "delivery_date",value = "出库日期")
    private Integer deliveryDate;

    @ApiModelProperty(name = "site_name",value = "自提站点")
    private String siteName;

    @ApiModelProperty(name = "order_status",value = "出库单类型")
    private String wmsOrderType;

    @ApiModelProperty(name = "delivery_status",value = "出库状态")
    private String deliveryStatus;

    @ApiModelProperty(name = "print_times",value = "打印次数  0 未打印 1 已打印")
    private Integer printStatus;

    @ApiModelProperty(name = "delivery_sns",value = "批量出库单号")
    private List<String> deliverySns;

    @ApiModelProperty(name = "delivery_ids",value = "批量出库单id")
    private List<Integer> deliveryIds;

    @ApiModelProperty(name = "export_type",value = "导出类型")
    private String exportType;

    @ApiModelProperty(name = "keyword",value = "关键字")
    private String keyword;

    @ApiModelProperty(name = "delivery_type",value = "分拣类型")
    private String deliveryType;

    @ApiModelProperty(name = "receive_time_seq",value = "出库时间类型")
    private Integer receiveTimeSeq;

    @ApiModelProperty(name = "good_name",value = "商品名称")
    private String goodName;

    private Long receiveTime;

    private Integer receiveTimeType;

    private String receiveTimeName;

    private String ordType;

    private Boolean isDaiShan;

    private Boolean isQuShan;

    private Boolean isBenDao;

    // 是否是衢山镇的超市
    private Boolean isQushanMarket;
}
