package com.enation.app.javashop.core.promotion.activitymessage.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * 活动消息记录表
 * @author xlg
 * 2020/11/05 17:51
 */
@Table(name = "es_activity_message")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ActivityMessageDO implements Serializable{

    /**
     * id
     */
    @Id(name = "id")
    private Integer id;

    /**
     * 活动名称
     */
    @Column(name = "activity_name")
    private String activityName;

    /**
     * 活动内容
     */
    @Column(name = "activity_content")
    private String activityContent;

    /**
     * 活动时间
     */
    @Column(name = "activity_time")
    private Long activityTime;

    /**
     * 温馨提示
     */
    @Column(name = "reminder")
    private String reminder;

    /**
     * 店铺id
     */
    @Column(name = "seller_id")
    private Integer sellerId;

    /**
     * 活动id
     */
    @Column(name = "activity_id")
    private Integer activityId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
}
