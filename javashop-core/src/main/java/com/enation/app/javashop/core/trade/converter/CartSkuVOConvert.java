package com.enation.app.javashop.core.trade.converter;

import com.alibaba.fastjson.JSON;
import com.enation.app.javashop.core.goods.model.enums.GoodsType;
import com.enation.app.javashop.core.promotion.tool.model.vo.PromotionVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartPromotionVo;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuOriginVo;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import com.enation.app.javashop.framework.util.CurrencyUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.springframework.beans.BeanUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author JFeng
 * @date 2020/7/24 16:01
 */
public class CartSkuVOConvert {
    /**
     * 将一个 CartSkuOriginVo转为  CartSkuVO
     * @param originVo
     * @return
     */
    public static CartSkuVO toSkuVo(CartSkuOriginVo originVo ){

        // 生成一个购物项
        CartSkuVO skuVO = new CartSkuVO();
        skuVO.setSellerId(originVo.getSellerId());
        skuVO.setSellerName(originVo.getSellerName());
        skuVO.setGoodsId(originVo.getGoodsId());
        skuVO.setSkuId(originVo.getSkuId());
        skuVO.setCatId(originVo.getCategoryId());
        skuVO.setGoodsImage(originVo.getThumbnail());
        skuVO.setName(originVo.getGoodsName());
        skuVO.setSkuSn(originVo.getSn());
        skuVO.setPurchasePrice(originVo.getPrice());
        skuVO.setOriginalPrice(originVo.getPrice());
        skuVO.setSpecList(originVo.getSpecList());
        skuVO.setIsFreeFreight(originVo.getGoodsTransfeeCharge());
        skuVO.setGoodsWeight(originVo.getWeight());
        skuVO.setTemplateId(originVo.getTemplateId());
        skuVO.setEnableQuantity(originVo.getEnableQuantity());
        skuVO.setLastModify(originVo.getLastModify());
        skuVO.setNum(originVo.getNum());
        skuVO.setChecked(originVo.getChecked());
        skuVO.setGoodsType(originVo.getGoodsType());
        skuVO.setPickTime(originVo.getPickTime());

        //设置可用的活动列表
        skuVO.setSingleList(originVo.getSingleList());
        skuVO.setGroupList(originVo.getGroupList());

        // 本地同城标记
        skuVO.setIsLocal(originVo.getIsLocal());
        skuVO.setIsGlobal(originVo.getIsGlobal());
        skuVO.setLocalTemplateId(originVo.getLocalTemplateId());
        skuVO.setIsSelfTake(originVo.getIsSelf());

        // 统一配送费用
        skuVO.setFreightPricingWay(originVo.getFreightPricingWay());
        skuVO.setFreightUnifiedPrice(originVo.getFreightUnifiedPrice());

        skuVO.setExpiryDay(originVo.getExpiryDay());
        skuVO.setAvailableDate(originVo.getAvailableDate());
        skuVO.setMktprice(originVo.getMktprice());

        // 如果是虚拟商品，返回过期时间
        if (GoodsType.VIRTUAL.name().equals(originVo.getGoodsType())) {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, originVo.getExpiryDay());
            skuVO.setExpiryDayText(sf.format(calendar.getTime()));
        }

        //计算小计
        double subTotal = CurrencyUtil.mul(skuVO.getNum(), skuVO.getOriginalPrice());
        skuVO.setSubtotal(subTotal);

        skuVO.setSkuCost(originVo.getCost());
        return  skuVO;
    }
}
