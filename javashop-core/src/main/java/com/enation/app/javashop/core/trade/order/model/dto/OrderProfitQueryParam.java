package com.enation.app.javashop.core.trade.order.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author chien
 * @since 2020-07-29
 */
@Data
public class OrderProfitQueryParam implements Serializable {


    @ApiModelProperty(value = "第几页")
    private Integer pageNo;

    @ApiModelProperty(value = "每页条数")
    private Integer pageSize;

    @ApiModelProperty(value = "会员Id ")
    private Integer memberId;

    @ApiModelProperty(value = "查询日期时间(月份) 如：202005 ")
    private String dateTime;

    @ApiModelProperty(value = "开始时间(时间戳)")
    private Long startTime;

    @ApiModelProperty(value = "结束时间(时间戳)")
    private Long endTime;

    @ApiModelProperty(value = "统计类型", example = "0:按天统计,1:按月统计 ,2：平台查询 ")
    private Integer type;

    @ApiModelProperty(value = "订单号")
    private String orderSn;

    @ApiModelProperty(value = "佣金状态")
    private String orderStatus;

    @ApiModelProperty(value = "会员姓名")
    private String memberName;

    @ApiModelProperty(value = "买家姓名")
    private String buyerName;

    @ApiModelProperty(value = "佣金类型")
    private Integer commissionType;

    @ApiModelProperty(value = "订单类型")
    private String orderType;

    @ApiModelProperty(value = "推广方式 1直推 2间推")
    private Integer spreadWay;

    @ApiModelProperty(value = "是否查询等于0的佣金")
    private Integer isZero;

    @ApiModelProperty(value = " 收益人姓名")
    private String beneficiaryName;

    @ApiModelProperty(value = " 收益人手机号")
    private String beneficiaryMobile;

}
