package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author 王校长
 * @Date 2020/9/4
 * @Descroption 退补单导出实体
 */
@Data
public class ExportSpreadRefundVO {

    @ExcelProperty(value = "退款流水号", index = 0)
    private String sn;

    @ExcelProperty(value = "订单编号", index = 1)
    private String orderSn;

    @ExcelProperty(value = "商品名称", index = 2)
    private String goodsNames;

    @ExcelProperty(value = "会员名称", index = 3)
    private String memberName;

    @ExcelProperty(value = "售后类型", index = 4)
    private String refundType;

    @ExcelProperty(value = "退款原因", index = 5)
    private String refundReason;

    @ExcelProperty(value = "退款状态", index = 6)
    private String refundStatus;

    @ExcelProperty(value = "退款失败原因", index = 7)
    private String refundFailReason;

    @ExcelProperty(value = "退款金额", index = 8)
    private Double refundPrice;

    @ExcelProperty(value = "操作人", index = 9)
    private String operator;

    @ExcelProperty(value = "退款标题", index = 10)
    private String refundTitle;

    @ExcelProperty(value = "退款说明", index = 11)
    private String refundIntroductions;

    @ExcelProperty(value = "退款描述", index = 12)
    private String refundDescription;

    @ExcelProperty(value = "退款时间", index = 13)
    private String refundTime;


}
