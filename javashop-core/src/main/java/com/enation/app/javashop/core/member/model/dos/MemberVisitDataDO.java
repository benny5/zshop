package com.enation.app.javashop.core.member.model.dos;

import java.io.Serializable;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;

/**
 * @Description:
 * @Date: Created inMon Sep 14 18:22:09 CST 2020
 * @Author: zhou
 * @Modified By:
 */
@Data
@Table(name = "es_sss_member_visit_data")
public class MemberVisitDataDO implements Serializable{

    private static final long serialVersionUID = 4943982612387899443L;

    @Id
    private Integer id;

    @ApiModelProperty(value = "新增用户留存")
    @Column(name = "visit_uv_new")
    private Integer visitUvNew;

    @ApiModelProperty(value = "活跃用户留存")
    @Column(name = "visit_uv")
    private Integer visitUv;

    @ApiModelProperty(value = "时间（当天零点）")
    @Column(name = "create_time")
    @CreatedDate
    private long createTime;

}
