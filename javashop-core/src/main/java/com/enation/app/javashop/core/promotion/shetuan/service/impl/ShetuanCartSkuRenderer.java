package com.enation.app.javashop.core.promotion.shetuan.service.impl;

import com.enation.app.javashop.core.passport.service.AliyunUtilsManager;
import com.enation.app.javashop.core.shop.model.vo.ShopVO;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.core.trade.cart.model.enums.CartSourceType;
import com.enation.app.javashop.core.trade.cart.model.enums.CartType;
import com.enation.app.javashop.core.trade.cart.model.enums.CheckedWay;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuOriginVo;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartVO;
import com.enation.app.javashop.core.trade.cart.service.CartOriginDataManager;
import com.enation.app.javashop.core.trade.cart.service.cartbuilder.CartSkuRenderer;
import com.enation.app.javashop.core.trade.cart.service.cartbuilder.impl.CartSkuFilter;
import com.enation.app.javashop.core.trade.cart.util.CartUtil;
import com.enation.app.javashop.core.trade.converter.CartSkuVOConvert;
import com.enation.app.javashop.core.trade.order.model.vo.CheckoutParamVO;
import com.enation.app.javashop.core.trade.order.service.CheckoutParamManager;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.context.UserContext;
import com.enation.app.javashop.framework.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kingapex on 2019-01-23.
 * 拼团的购物车渲染器
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2019-01-23
 */
@Service
@Slf4j
public class ShetuanCartSkuRenderer implements CartSkuRenderer {
    @Autowired
    private ShopManager shopManager;

    @Autowired
    private CheckoutParamManager checkoutParamManager;

    @Autowired
    private Cache cache;


    @Override
    public void renderSku(List<CartVO> cartList, CartType cartType, CheckedWay way) {
        //获取原始数据
        List<CartSkuOriginVo> originList = this.read(way);

        Integer sellerId = getSellerId();
        //用原始数据渲染购物车
        for (CartSkuOriginVo originVo : originList) {
            if (!originVo.getSellerId().equals(sellerId)) {
                continue;
            }
            // 查询店铺信息（店铺名称修改）
            ShopVO shopVO = this.shopManager.getShop(sellerId);

            //转换为购物车skuvo
            CartSkuVO skuVO = CartSkuVOConvert.toSkuVo(originVo);

            innerRunder(skuVO, cartList, cartType, shopVO);
        }
    }

    private Integer getSellerId() {
        String city = checkoutParamManager.getParam().getCity();
        if (StringUtils.isEmpty(city)) {
            throw new ServiceException("500", "请确认手机定位地址");
        }

        Integer shopByCity = shopManager.getShopByCity(city);
        if (shopByCity == 0) {
            throw new ServiceException("600", "所在城市【" + city + "】,尚未开通社区团购业务");
        }
        return shopByCity;
    }

    @Override
    public void renderSku(List<CartVO> cartList, CartSkuFilter cartSkuFilter, CartType cartType, CheckedWay way) {
        //获取原始数据
        List<CartSkuOriginVo> originList = this.read(way);
        // 查询用户所在城市店铺【购物车结算】
        Integer sellerId=null;
        if(way.equals(CheckedWay.CART)){
            sellerId = getSellerId();
        }
        //用原始数据渲染购物车
        for (CartSkuOriginVo originVo : originList) {

            if (sellerId!=null && sellerId!=0 && !originVo.getSellerId().equals(sellerId) && way.equals(CheckedWay.CART)) {
                continue;
            }

            // 查询店铺信息（店铺名称修改）
            ShopVO shopVO = this.shopManager.getShop(originVo.getSellerId());

            //转换为购物车skuvo
            CartSkuVO skuVO = CartSkuVOConvert.toSkuVo(originVo);

            //如果过滤成功才继续
            if (!cartSkuFilter.accept(skuVO)) {
                continue;
            }

            innerRunder(skuVO, cartList, cartType,shopVO);
        }
    }

    private List<CartSkuOriginVo> read(CheckedWay checkedWay) {
        List<CartSkuOriginVo> originList = (List<CartSkuOriginVo>) cache.get(CartUtil.getCartKey(checkedWay,CartSourceType.SHETUAN_CART));
        if(CollectionUtils.isEmpty(originList)){
            originList=new ArrayList<>();
        }
        return originList;
    }


    /**
     * 内部化用的渲染方法
     *
     * @param skuVO
     * @param cartList
     * @param cartType
     */
    private void  innerRunder( CartSkuVO skuVO, List<CartVO> cartList, CartType cartType,ShopVO shopVO) {
        CartVO cartVO = CartUtil.findCart(skuVO.getSellerId(),cartList);

        if (cartVO == null) {

            cartVO = new CartVO(shopVO.getShopId(), shopVO.getShopName(), shopVO.getShopLogo(), cartType);

            // 购物车类型
            cartVO.setCartSourceType(CartSourceType.SHETUAN_CART.name());

            // 店铺坐标
            cartVO.setShopLat(shopVO.getShopLat());
            cartVO.setShopLng(shopVO.getShopLng());

            cartVO.setCity(shopVO.getShopProvince()+shopVO.getShopCity());

            cartList.add(cartVO);
        }

        if(skuVO.getChecked()==1){
            cartVO.setGoodsNum(skuVO.getNum()+cartVO.getGoodsNum());
        }
        //压入到当前店铺的sku列表中
        cartVO.getSkuList().add(skuVO);
    }

}
