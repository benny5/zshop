package com.enation.app.javashop.core.member.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Table(name = "es_buyer_last_leader")
public class BuyerLastLeaderDO {


    @ApiModelProperty(name = "id",value = "id")
    @Id(name="id")
    private Integer id;


    @ApiModelProperty(name = "leader_id",value = "团长id")
    @Column(name="leader_id")
    private Integer leaderId;

    @ApiModelProperty(name = "buyer_id",value = "客户id")
    @Column(name = "buyer_id")
    private Integer buyerId;

    @ApiModelProperty(name = "update_time",value="更新时间")
    @Column(name="update_time")
    private long updateTime;
}
