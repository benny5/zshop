package com.enation.app.javashop.core.passport.dto;

import lombok.Data;

import java.util.Map;

/**
 * @author ChienSun
 * @date 2019/3/5
 */
@Data
public class MpTemplateMsgDTO {

    private String appid;                   // 公众号appid
    private String template_id;              // 公众号模板id
    private String url;                      // 公众号模板消息所要跳转的url
    private MiniprogramDTO miniprogram;    // 公众号模板消息所要跳转的小程序
    private Map<String, DataDetailDTO> data;  //  公众号模板消息的数据

}
