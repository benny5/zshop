package com.enation.app.javashop.core.pagedata.model.enums;

public enum  PageHomeTypeEnum {

    //默认
    DEFINED(0,"无"),
    //商城首页
    SHOPPAGE(1,"商城首页"),
    //团购首页
    GROUPPAGE(2,"团购首页"),
    //附近首页
    NEARBYPAGE(3,"附近首页"),
    // 发现页
    DISCOVERYPAGE(4,"发现页");

    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    private String description;

    PageHomeTypeEnum(int value ,String des) {
        this.value = value;
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public Integer value() {
        return this.getValue();
    }
}
