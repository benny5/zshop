package com.enation.app.javashop.core.shop.model.dos;

import lombok.Data;

import java.io.Serializable;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/2/26 15:28
 */
@Data
public class DistanceSection implements Serializable {
    private static final long serialVersionUID = 6348162955890093L;

    private Double startDistance;
    private Double endDistance;
    private Double shipPrice;
}
