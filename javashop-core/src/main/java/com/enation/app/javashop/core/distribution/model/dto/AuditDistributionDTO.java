package com.enation.app.javashop.core.distribution.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AuditDistributionDTO {

    // 这里不用id  而是用memberId 因为展示的VO和DO对应不上
    @ApiModelProperty(name = "member_id", value = "会员id")
    private Integer memberId;

    @ApiModelProperty(name = "audit_status", value = "审核状态 2通过 3不通过", required = true)
    private Integer auditStatus;

    @ApiModelProperty(name = "team_name", value = "别名 如 第8团", required = true)
    private String teamName;

    @ApiModelProperty(name = "address", value = "详细地址", required = false)
    private String address;


    @ApiModelProperty(name = "refuse_reason", value = "审核备注", required = true)
    private String refuseReason;

    @ApiModelProperty(name = "operate_areas", value = "运行区域", required = false)
    private String operateAreas;

    @ApiModelProperty(name = "wechat_groups_num", value = "微信群人数", required = false)
    private Integer wechatGroupsNum;

}
