package com.enation.app.javashop.core.promotion.seckill.service;

import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillApplyDO;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillGoodsVO;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

/**
 * 限时抢购促销信息管理
 */
public interface SeckillPromotionManager {

	void addPromotionGoods(SeckillApplyDO apply);

	void batchDeletePromotionGoods(List<Integer>  deleteIds);

	void batchUpdatePromotionGoods(List<SeckillApplyDO> updateSeckillGoods);

	void batchAddPromotionGoods(List<SeckillApplyDO> newSeckillGoods);
}
