package com.enation.app.javashop.core.goods.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * 海报生成请求Vo
 */
@Data
public class PlacardGenVO {

    @NotNull(message = "scene不能为空")
    private String scene ;


    @NotNull(message = "页面类型不能为空")
    /**
     * 如果页面类型为1 ， 代表分享的是商品详情
     * 如果页面类型为2，代表分享的是微页面
     */
    private Integer pageType;

    @NotNull(message = "页面路径不能为空")
    private String pagePath;

    private Integer queryId;
    /** 太阳码生成大小 */
    private Long width;
    /** 太阳码 自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调，默认 false*/
    private boolean autoColor;
    /** lineColor,auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示*/
    private Map<String,Integer>  lineColor;
    /** 是否需要透明底色，为 true 时，生成透明底色的小程序 */
    private boolean isHyaline;

}
