package com.enation.app.javashop.core.aftersale.model.vo;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.member.model.enums.AuditEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author JFeng
 * @date 2020/9/24 23:00
 */
@Data
public class ClaimsChangeMsg   implements Serializable {

    private static final long serialVersionUID = 8172328547919236666L;

    private List<ClaimsDO> claimsDOList;
    private String auditEnum;
}