package com.enation.app.javashop.core.goods.model.enums;

/**
 * 标签类型
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月28日 下午3:00:24
 */
public enum TagType {

    /**
     * 热卖排行
     */
    HOT("hot", "热卖排行", 0, 0, 1, 1, "#999999", "1", 0),
    /**
     * 新品推荐
     */
    NEW("new", "新品推荐", 0, 0, 1, 1, "#999999", "1", 0),
    /**
     * 推荐商品
     */
    RECOMMEND("recommend", "推荐商品", 0, 0, 1, 1, "#999999", "1", 0),
    FREE_SHIPPING("free shipping", "包邮", 1, 1, 0, 0, "#F7AD06", "2", 10),
    ALL_DAY("all_day", "24小时发货", 1, 1, 0, 0, "#F43E14", "2", 20),
    REFUND("refund", "极速退款", 1, 1, 0, 0, "#08A603", "2", 30);

    private String mark;
    private String tagName;
    private int status;
    private int shopHomePageStauts;
    private int goodsListPageStauts;
    private int goodsDetailsPageStauts;
    private String color;
    private String style;
    private int priority;


    TagType(String mark, String tagName, int status, int shopHomePageStauts, int goodsListPageStauts, int goodsDetailsPageStauts, String color, String style, int priority) {

        this.mark = mark;
        this.tagName = tagName;
        this.status = status;
        this.shopHomePageStauts = shopHomePageStauts;
        this.goodsListPageStauts = goodsListPageStauts;
        this.goodsDetailsPageStauts = goodsDetailsPageStauts;
        this.color = color;
        this.style = style;
        this.priority = priority;
    }

    public String mark() {
        return this.mark;
    }

    public String tagName() {
        return this.tagName;
    }

    public int status() {
        return this.status;
    }

    public int shopHomePageStauts() {
        return this.shopHomePageStauts;
    }

    public int goodsListPageStauts() {
        return this.goodsListPageStauts;
    }

    public int goodsDetailsPageStauts() {
        return this.goodsDetailsPageStauts;
    }

    public String color() {
        return this.color;
    }

    public String style() {
        return this.style;
    }

    public int priority() {
        return priority;
    }
}
