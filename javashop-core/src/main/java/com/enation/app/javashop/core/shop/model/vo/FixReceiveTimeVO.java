package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.core.shop.model.dto.FixTimeSection;
import lombok.Data;

import java.util.List;

@Data
public  class FixReceiveTimeVO {


    private List<FixTimeSection> timeSections;

    private String dayText;

    public FixReceiveTimeVO(List<FixTimeSection> timeSections, String dayText) {
        this.timeSections = timeSections;
        this.dayText = dayText;
    }

    public FixReceiveTimeVO() {
    }
}


