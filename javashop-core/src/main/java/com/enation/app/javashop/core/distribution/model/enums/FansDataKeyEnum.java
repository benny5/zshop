package com.enation.app.javashop.core.distribution.model.enums;

/**
 * @author 王志杨
 * @since 2021/1/22 15:09
 * 粉丝留存数据指标key
 */
public enum FansDataKeyEnum {

    today_order_num("今日下单数"),
    week_order_num("一周下单数"),
    two_week_order_num("两周下单数"),
    three_week_order_num("三周下单数"),
    silence_three_day_num("沉默三日数"),
    silence_one_week_num("沉默一周数"),
    silence_two_week_num("沉默两周数"),
    silence_three_week_num("沉默三周数");

    private String description;

    public String getDescription() {
        return description;
    }

    FansDataKeyEnum(String description) {
        this.description = description;
    }
}
