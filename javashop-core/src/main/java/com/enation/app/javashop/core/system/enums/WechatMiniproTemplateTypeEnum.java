package com.enation.app.javashop.core.system.enums;

import java.io.Serializable;

/**
 * 小程序模板消息
 */
public enum WechatMiniproTemplateTypeEnum implements Serializable{

    PAY_NOTICE("3117","待付款提醒","你有1笔订单待支付，5分钟后将自动取消，请尽快支付>>","", "/pages/ucenter/orderDetail/orderDetail?order_sn="),
    SHIP_NOTICE("1227","订单发货提醒","你的订单已发货，请留意查收包裹并及时确认收货，如有疑问请联系客服","", "/pages/ucenter/orderDetail/orderDetail?order_sn="),
    CANCEL_NOTICE_1("1134","订单取消通知","你的订单已取消，已付款项将在1-7个工作内原路退回，请注意查收，有任何疑问请联系客服","", "/pages/ucenter/orderDetail/orderDetail?order_sn="),
    CANCEL_NOTICE_2("1134","订单取消通知","你的订单超时未支付已自动取消，如还需购买，请到商城另行选购>>","", "/pages/ucenter/orderDetail/orderDetail?order_sn="),
    CANCEL_NOTICE_3("1134","订单取消通知","你的订单已被取消，已付款项将在1-7个工作日内原路退回，请注意查收，如有疑问请联系客服", "", "/pages/ucenter/orderDetail/orderDetail?order_sn="),
    RETUND_NOTICE_1("2379","订单售后审核通知","商家已同意您的售后申请，请点击进入查看订单详情>>", "", "/pages/ucenter/orderDetail/orderDetail?order_sn="),
    RETUND_NOTICE_2("2379","订单售后审核通知","商家拒绝了您的售后申请，请点击进入查看订单详情>>", "", "/pages/ucenter/orderDetail/orderDetail?order_sn="),
    PROFITE_NOTICE("4482","订单收益提醒","你获得了一笔佣金，请点击进入查看订单详情>>", "", "/pages/spread/profit/profit"),
    DISTRIBUTION_NOTICE_1("4482","分销审核通知","你的申请已审核通过，请点击进入查看订单详情>>", "", "/pages/leader/apply/apply"),
    DISTRIBUTION_NOTICE_2("4482","分销审核通知","你的申请被驳回，请点击进入查看订单详情>>", "", "/pages/leader/apply/apply"),
    PICK_NOTICE_1("4482","提货点审核通知","你的申请已审核通过，请点击进入查看订单详情>>", "", "/pages/leader/apply/apply"),
    PICK_NOTICE_2("4482","提货点审核通知","你的申请被驳回，请点击进入查看订单详情>>", "", "/pages/leader/apply/apply"),
    WITHDRAWAL_NOTICE("3862","提现审核通知","", "", "/pages/ucenter/wallet/wallet"),
    WITHRESULT_NOTICE("829","提现结果通知","", "", "/pages/ucenter/wallet/wallet"),
    REFUND_NOTICE("1742","退款提醒","商家退补一笔金额，请点击进入查看详情>>", "", "/pages/ucenter/more-return/more-return"),
    PUBLIC_PROFITE_NOTICE("999999", "公众号订单收益提醒", "你的粉丝「NICKNAME」已成功下单，获得一笔SPREAD_WAYCOMMISSION_TYPE。", "订单退款会导致佣金变化，实际收益金额以客户收货为准>>", "/pages/spread/profit/profit"),
    ACTIVITYSTART_NOTICE("274","已预约活动开始提醒","您订阅的活动要结束了，请点击进入查看订单详情>>", "", "/pages/index-group/index/index"),
    GROUP_WORK_NOTICE("1809","拼团结果通知","恭喜您拼团成功，请点击进入查看订单详情>>", "", "/pages/ucenter/order/order"),
    PUBLIC_RECOMMENDED_COUPONS_NOTICE("1000000", "公众号邀请注册成功通知", "恭喜你，成功邀请到1名好友注册，获得优惠劵奖励MYFRIENDSREGISTERREWARD元。", "点击查看更多活动详情>>", "/pages/recommend/recommend"),
    ;


    private String sn;
    private String tmpName;
    private String first;
    private String remark;
    private String page;

    WechatMiniproTemplateTypeEnum(String sn, String tmpName, String first, String remark, String page) {
        this.sn = sn;
        this.tmpName = tmpName;
        this.first = first;
        this.remark = remark;
        this.page = page;
    }

    public String getSn() {
        return this.sn;
    }

    public String getTmpName() {
        return this.tmpName;
    }

    public String getFirst() {
        return this.first;
    }

    public String getRemark() {
        return this.remark;
    }

    public String value() {
        return this.name();
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
