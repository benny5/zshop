package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * 店铺vo
 *
 * @author zhangjiping
 * @version v1.0
 * @since v7.0
 * 2018年3月21日 上午10:32:52
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShopInfoVO {

    /**
     * 店铺Id
     */
    @Column(name = "shop_id")
    @ApiModelProperty(name = "shop_id", value = "店铺Id", required = false)
    private Integer shopId;

    /**
     * 店铺logo
     */
    @Column(name = "shop_logo")
    @ApiModelProperty(name = "shop_logo", value = "店铺logo", required = false)
    private String shopLogo;
    /**
     * 店铺名称
     */
    @Column(name = "shop_name")
    @ApiModelProperty(name = "shop_name", value = "店铺名称", required = true)
    @NotEmpty(message = "店铺名称必填")
    private String shopName;


    /**
     * 法人身份证
     */
    @Column(name = "legal_id")
    @ApiModelProperty(name = "legal_id", value = "法人身份证", required = true)
    @Size(min = 18, max = 18, message = "身份证长度不符")
    private String legalId;

    /**
     * 店铺详细地址
     */
    @Column(name = "shop_add")
    @ApiModelProperty(name = "shop_add", value = "店铺详细地址", required = false)
    private String shopAdd;


    /**
     * 联系人电话
     */
    @Column(name = "link_phone")
    @ApiModelProperty(name = "link_phone", value = "联系人电话", required = true)
    @NotEmpty(message = "联系人电话必填")
    private String linkPhone;

    @ApiModelProperty(name="link_name",value="联系人姓名",required=false)
    private String linkName;
    /**
     * 店铺客服qq
     */
    @Column(name = "shop_qq")
    @ApiModelProperty(name = "shop_qq", value = "店铺客服qq", required = false)
    private String shopQq;

    /**
     * 店铺简介
     */
    @Column(name = "shop_desc")
    @ApiModelProperty(name = "shop_desc", value = "店铺简介", required = false)
    private String shopDesc;


    /**
     * 是否自营
     */
    @Column(name = "self_operated")
    @ApiModelProperty(name = "self_operated", value = "是否自营 1:是 0:否", required = true)
    private Integer selfOperated;


    /**
     * 店铺信用
     */
    @Column(name = "shop_credit")
    @ApiModelProperty(name = "shop_credit", value = "店铺信用", required = false)
    private Double shopCredit;


    /**
     * 店铺所在省
     */
    @Column(name = "shop_province")
    @ApiModelProperty(name = "shop_province", value = "店铺所在省", required = false)
    private String shopProvince;
    /**
     * 店铺所在市
     */
    @Column(name = "shop_city")
    @ApiModelProperty(name = "shop_city", value = "店铺所在市", required = false)
    private String shopCity;
    /**
     * 店铺所在县
     */
    @Column(name = "shop_county")
    @ApiModelProperty(name = "shop_county", value = "店铺所在县", required = false)
    private String shopCounty;
    /**
     * 店铺所在镇
     */
    @Column(name = "shop_town", allowNullUpdate = true)
    @ApiModelProperty(name = "shop_town", value = "店铺所在镇", required = false)
    private String shopTown;


    /**
     * 店铺所在省id
     */
    @Column(name = "shop_province_id")
    @ApiModelProperty(name = "shop_province_id", value = "店铺所在省id", required = false)
    private Integer shopProvinceId;
    /**
     * 店铺所在市id
     */
    @Column(name = "shop_city_id")
    @ApiModelProperty(name = "shop_city_id", value = "店铺所在市id", required = false)
    private Integer shopCityId;
    /**
     * 店铺所在县id
     */
    @Column(name = "shop_county_id")
    @ApiModelProperty(name = "shop_county_id", value = "店铺所在县id", required = false)
    private Integer shopCountyId;
    /**
     * 店铺所在镇id
     */
    @Column(name = "shop_town_id")
    @ApiModelProperty(name = "shop_town_id", value = "店铺所在镇id", required = false)
    private Integer shopTownId;

    /**
     * 店铺描述相符度
     */
    @Column(name = "shop_description_credit")
    @ApiModelProperty(name = "shop_description_credit", value = "店铺描述相符度", required = false)
    private Double shopDescriptionCredit;


    /**
     * 服务态度分数
     */
    @Column(name = "shop_service_credit")
    @ApiModelProperty(name = "shop_service_credit", value = "服务态度分数", required = false)
    private Double shopServiceCredit;

    /**
     * 发货速度分数
     */
    @Column(name = "shop_delivery_credit")
    @ApiModelProperty(name = "shop_delivery_credit", value = "发货速度分数", required = false)
    private Double shopDeliveryCredit;


    /**
     * 货品预警数
     */
    @Column(name = "goods_warning_count")
    @ApiModelProperty(name = "goods_warning_count", value = "货品预警数", required = false)
    private Integer goodsWarningCount;

    /**
     * 是否允许开具增值税普通发票 0：否，1：是
     */
    @Column(name = "ordin_receipt_status")
    @ApiModelProperty(name = "ordin_receipt_status", value = "是否允许开具增值税普通发票 0：否，1：是", required = false, allowableValues = "0,1")
    private Integer ordinReceiptStatus;
    /**
     * 是否允许开具电子普通发票 0：否，1：是
     */
    @Column(name = "elec_receipt_status")
    @ApiModelProperty(name = "elec_receipt_status", value = "是否允许开具电子普通发票 0：否，1：是", required = false, allowableValues = "0,1")
    private Integer elecReceiptStatus;
    /**
     * 是否允许开具增值税专用发票 0：否，1：是
     */
    @Column(name = "tax_receipt_status")
    @ApiModelProperty(name = "tax_receipt_status", value = "是否允许开具增值税专用发票 0：否，1：是", required = false, allowableValues = "0,1")
    private Integer taxReceiptStatus;
    @Column(name = "shop_lat")
    @ApiModelProperty(name="shopLat",value="纬度",required=false)
    private Double shopLat;
    @Column(name = "shop_lng")
    @ApiModelProperty(name="shopLng",value="经度",required=false)
    private Double shopLng;
    // @Column(name = "shop_contact")
    // @ApiModelProperty(name="shopContact",value="门店联系方式",required=false)
    // private String shopContact;

    @Column(name = "open_time_type")
    @ApiModelProperty(name="open_time_type",value="门店营业时间类型 0.全天 1.自定义",required=true)
    private Integer openTimeType;

    @Column(name = "open_start_time")
    @ApiModelProperty(name="open_start_time",value="门店开始营业时间 HH:mm:ss",required=false)
    private String openStartTime;

    @Column(name = "open_end_time")
    @ApiModelProperty(name="open_end_time",value="门店结束营业时间 HH:mm:ss",required=false)
    private String openEndTime;

    @Column(name = "shop_tag")
    @ApiModelProperty(name="shop_tag",value="店铺标签",required=false)
    private String shopTag;

    @Column(name = "ship_type")
    @ApiModelProperty(name="shipType",value="店铺支持配送类型 1 快递 2 同城",required=false)
    private String shipType;

    @Column(name = "legal_back_img")
    @ApiModelProperty(name = "legal_back_img", value = "身份证反面照片", required = false)
    private String legalBackImg;

    @Column(name = "hold_img")
    @ApiModelProperty(name = "hold_img", value = "手持身份证照片", required = false)
    private String holdImg;

    @Column(name = "legal_img")
    @ApiModelProperty(name = "legal_img", value = "身份证正面照片", required = false)
    private String legalImg;

    @Column(name = "licence_img")
    @ApiModelProperty(name = "licence_img", value = "营业执照", required = false)
    private String licenceImg;

    private String categoryNames;

    @ApiModelProperty(name="goods_management_category",value="店铺经营类目",required=false)
    private String goodsManagementCategory;

    @ApiModelProperty(name="community_shop",value="是否社区团购点铺",required=false)
    private Integer communityShop;

    @ApiModelProperty(name="open_time",value="营业时间json",required=false)
    private String openTime;

    @ApiModelProperty(name="business_status",value="店铺营业状态 1营业 0不营业",required=false)
    private Integer businessStatus;


    public ShopInfoVO(ShopVO shopVO) {
        this.setGoodsWarningCount(shopVO.getGoodsWarningCount());
        this.setLegalId(shopVO.getLegalId());
        this.setLinkPhone(shopVO.getLinkPhone());
        this.setSelfOperated(shopVO.getSelfOperated());
        this.setShopAdd(shopVO.getShopAdd());
        this.setShopCity(shopVO.getShopCity());
        this.setShopCityId(shopVO.getShopCityId());
        this.setShopCounty(shopVO.getShopCounty());
        this.setShopCountyId(shopVO.getShopCountyId());
        this.setShopCredit(shopVO.getShopCredit());
        this.setShopDeliveryCredit(shopVO.getShopDeliveryCredit());
        this.setShopDesc(shopVO.getShopDesc());
        this.setShopDescriptionCredit(shopVO.getShopDescriptionCredit());
        this.setShopId(shopVO.getShopId());
        this.setShopLogo(shopVO.getShopLogo());
        this.setShopName(shopVO.getShopName());
        this.setShopProvince(shopVO.getShopProvince());
        this.setShopProvinceId(shopVO.getShopProvinceId());
        this.setShopQq(shopVO.getShopQq());
        this.setShopServiceCredit(shopVO.getShopServiceCredit());
        this.setShopTown(shopVO.getShopTown());
        this.setShopTownId(shopVO.getShopTownId());
        this.setOrdinReceiptStatus(shopVO.getOrdinReceiptStatus());
        this.setElecReceiptStatus(shopVO.getElecReceiptStatus());
        this.setTaxReceiptStatus(shopVO.getTaxReceiptStatus());
        this.setShopLat(shopVO.getShopLat());
        this.setShopLng(shopVO.getShopLng());
        this.setOpenTimeType(shopVO.getOpenTimeType());
        this.setOpenStartTime(shopVO.getOpenStartTime());
        this.setOpenEndTime(shopVO.getOpenEndTime());
        this.setShopTag(shopVO.getShopTag());
        this.setShipType(shopVO.getShipType());
        // 照片
        this.setLegalBackImg(shopVO.getLegalBackImg());
        this.setHoldImg(shopVO.getHoldImg());
        this.setLegalImg(shopVO.getLegalImg());
        this.setLicenceImg(shopVO.getLicenceImg());
        this.setCommunityShop(shopVO.getCommunityShop());
        this.setOpenTime(shopVO.getOpenTime());
        this.setBusinessStatus(shopVO.getBusinessStatus());
    }

    public ShopInfoVO() {

    }



}