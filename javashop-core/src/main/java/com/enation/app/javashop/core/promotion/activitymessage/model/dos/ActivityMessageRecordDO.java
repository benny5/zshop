package com.enation.app.javashop.core.promotion.activitymessage.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 活动订阅消息表
 * @author xlg
 * 2020/11/05 17:51
 */
@Table(name = "es_activity_message_record")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ActivityMessageRecordDO {

    /**
     * id
     */
    @Id(name = "id")
    private Integer id;

    /**
     * 活动id
     */
    @Column(name = "activity_id")
    private Integer activityId;

    /**
     * 活动id
     */
    @Column(name = "seller_id")
    private Integer sellerId;
    /**
     * 会员Id
     */
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 活动消息Id
     */
    @Column(name = "message_id")
    private Integer messageId;

    /**
     * 小程序唯一标识
     */
    @Column(name = "open_id")
    private String openId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
}
