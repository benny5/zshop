package com.enation.app.javashop.core.pagedata.model.enums;

/**
 * @author john
 * @Description 页面状态
 * @ClassName PageTypeEnum
 */
public enum PageStateEnum {
    SCRIPT(1,"草稿"),

    ACTIVE(2,"已上架"),

    NEGATIVE(3,"已下架");
    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    private String description;

    PageStateEnum(int value , String des) {
        this.value = value;
        this.description = des;
    }

    public String description() {
        return this.description;
    }

    public Integer value() {
        return this.getValue();
    }
}
