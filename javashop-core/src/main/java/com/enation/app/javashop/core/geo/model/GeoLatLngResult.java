package com.enation.app.javashop.core.geo.model;

import lombok.Data;

/**
 * @author JFeng
 * @date 2019/1/10 16:54
 */
@Data
public class GeoLatLngResult {
    private String formatted_address;
    private String country;
    private String province;
    private String city;
    private String district;
    private String street;
    private String adcode;
    private String location;

}
