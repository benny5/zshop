package com.enation.app.javashop.core.shop.model.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public  class FixTimeSection implements Serializable {
    private static final long serialVersionUID = 5739331716616203333L;

    private Integer seq;
    private Integer endOrderTime;
    private Integer receiveTime;
    private String halfDay;
    private Integer receiveTimeType;
    private String receiveTimeText;
    private Long receiveTimeStamp;
    private String sortingTimeText;

    public FixTimeSection(Long receiveTimeStamp, Integer receiveTimeType) {
        this.receiveTimeStamp = receiveTimeStamp;
        this.receiveTimeType = receiveTimeType;
    }

    public FixTimeSection() {
    }
}