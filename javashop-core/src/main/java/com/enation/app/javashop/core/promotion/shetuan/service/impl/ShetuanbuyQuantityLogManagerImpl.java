package com.enation.app.javashop.core.promotion.shetuan.service.impl;

import com.enation.app.javashop.core.promotion.groupbuy.model.dos.GroupbuyQuantityLog;
import com.enation.app.javashop.core.promotion.groupbuy.service.GroupbuyQuantityLogManager;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanbuyQuantityLog;
import com.enation.app.javashop.core.promotion.shetuan.service.ShetuanbuyQuantityLogManager;
import com.enation.app.javashop.core.promotion.tool.service.PromotionGoodsManager;
import com.enation.app.javashop.core.statistics.util.DateUtil;
import com.enation.app.javashop.framework.database.DaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 团购商品库存日志表业务类
 *
 * @author xlp
 * @version v1.0
 * @since v7.0.0
 * 2018-07-09 15:32:29
 */
@Service
public class ShetuanbuyQuantityLogManagerImpl implements ShetuanbuyQuantityLogManager {

    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport daoSupport;

    @Autowired
    private PromotionGoodsManager promotionGoodsManager;

    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public List<ShetuanbuyQuantityLog> rollbackReduce(String orderSn) {
        List<ShetuanbuyQuantityLog> logList = daoSupport.queryForList("select * from es_shetuan_quantity_log where order_sn=? ", ShetuanbuyQuantityLog.class, orderSn);
        List<ShetuanbuyQuantityLog> result = new ArrayList<>();
        for (ShetuanbuyQuantityLog log : logList) {
            log.setQuantity(log.getQuantity());
            log.setOpTime(DateUtil.getDateline());
            log.setReason("取消订单，回滚库存");
            log.setLogId(null);
            this.add(log);
            result.add(log);

            promotionGoodsManager.cleanCache(log.getGoodsId());
        }
        return result;
    }

    @Override
    @Transactional(value = "tradeTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ShetuanbuyQuantityLog add(ShetuanbuyQuantityLog shetuanbuyQuantityLog) {
        this.daoSupport.insert(shetuanbuyQuantityLog);
        return shetuanbuyQuantityLog;
    }

}
