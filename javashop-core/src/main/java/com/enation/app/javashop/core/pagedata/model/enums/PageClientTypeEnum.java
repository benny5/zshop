package com.enation.app.javashop.core.pagedata.model.enums;

/**
 * 支付客户端类型
 * @author fk
 * @version v6.4
 * @since v6.4 2017年10月17日 上午10:49:25
 */
public enum PageClientTypeEnum {

	/**
	 * pc客户端
	 */
	PC,
	/**
	 * APP客户端
	 */
	APP,
	/**
	 * WAP
	 */
	WAP;

	PageClientTypeEnum() {

	}

	public String value() {
		return this.name();
	}
	

}
