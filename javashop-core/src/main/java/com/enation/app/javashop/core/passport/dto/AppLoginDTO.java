package com.enation.app.javashop.core.passport.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFeng
 * @date 2020/7/10 11:25
 */

@Data
public class AppLoginDTO {
    @ApiModelProperty(name = "username", value = "用户名-用户名密码登录", example = "Jerry",required = true)
    private String userName;

    @ApiModelProperty(name = "password", value = "密码-用户名密码登录", example = "hm123456",required = true)
    private String password;

    @ApiModelProperty(name = "mobile", value = "手机号", example = "17612166948",required = true)
    private String mobile;

    @ApiModelProperty(name = "sms_code", value = "短信验证码", example = "123456",required = true)
    private String smsCode;

    @ApiModelProperty(name = "uuid", value = "客户端唯一标识", example = "UUID前端生成并缓存",required = true)
    private String uuid;

    @ApiModelProperty(name = "login_type", value = "登录方式", allowableValues= "0-短信登录,1-用户名密码",required = true)
    private Integer loginType;

}
