package com.enation.app.javashop.core.aftersale.model.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.aftersale.model.dto.ClaimSkusDTO;
import com.enation.app.javashop.core.aftersale.model.enums.ClaimsStatusEnum;
import com.enation.app.javashop.core.aftersale.model.enums.ClaimsTypeEnum;
import com.enation.app.javashop.core.aftersale.model.enums.RefundReasonEnum;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.core.trade.order.model.enums.ShipTypeEnum;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.util.StringUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/9/21
 * @Description:
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ClaimsVO extends ClaimsDO implements Serializable {

    private static final long serialVersionUID = -4679176688482697854L;

    @ApiModelProperty(value = "sku列表")
    private List<ClaimSkusDTO> skuList;

    @ApiModelProperty(value = "理赔的售后券")
    private CouponDO couponDO;

    @ApiModelProperty(name = "claim_status_text", value = "理赔状态文字")
    private String claimStatusText;

    @ApiModelProperty(name = "claim_type_text", value = "理赔类型文字")
    private String claimTypeText;

    @ApiModelProperty(name = "refund_reason_text", value = "多退少补原因")
    private String refundReasonText;

    @ApiModelProperty(name = "shipping_type", value = "快递类型")
    private String shippingType;

    @ApiModelProperty(name = "shipping_type_text", value = "快递类型")
    private String shippingTypeText;

    @ApiModelProperty(name = "member_nickname", value = "买家昵称")
    private String memberNickname;

    @ApiModelProperty(name = "ship_name", value = "收货人姓名", required = false)
    private String shipName;

    @ApiModelProperty(name = "ship_mobile", value = "收货人手机", required = false)
    private String shipMobile;

    @ApiModelProperty(name = "ship_addr", value = "收货地址", required = false)
    private String shipAddr;

    @ApiModelProperty(name = "member_realname_lv1", value = "一级团长真实姓名", required = false)
    private String memberRealnameLv1;

    @ApiModelProperty(name = "member_mobile_lv1", value = "一级团长手机号码", required = false)
    private String memberMobileLv1;

    @ApiModelProperty(name = "site_name", value = "站点名", required = false)
    private String siteName;

    @ApiModelProperty(name = "site_leader_name", value = "站长名称", required = false)
    private String siteLeaderName;

    @ApiModelProperty(name = "site_leader_mobile", value = "站长手机号码", required = false)
    private String siteLeaderMobile;

    @ApiModelProperty(name = "delivery_time_string", value = "出库时间字符串")
    private String deliveryTimeString;

    @ApiModelProperty(name = "coupon_id", value = "售后券ID")
    private Integer couponId;

    public List<ClaimSkusDTO> getSkuList() {
        if (!StringUtil.isEmpty(getClaimSkus())) {
            if (getClaimType().equals(ClaimsTypeEnum.COMPENSATION_GOODS.value()) || getClaimType().equals((ClaimsTypeEnum.FREE_GIFT.value()))) {
                skuList = JSON.parseObject(getClaimSkus(), new TypeReference<List<ClaimSkusDTO>>() {
                });
            }
        }

        return skuList;
    }

    public void setSkuList(List<ClaimSkusDTO> skuList) {
        this.skuList = skuList;
    }

    public CouponDO getCouponDO() {
        if (getClaimType().equals(ClaimsTypeEnum.AFTERSALE_COUPON.value()) && !StringUtil.isEmpty(getClaimSkus())) {
            couponDO = JSONObject.parseObject(getClaimSkus(), CouponDO.class);
        }
        return couponDO;
    }

    public void setCouponDO(CouponDO couponDO) {
        this.couponDO = couponDO;
    }

    public String getClaimStatusText() {
        if (getClaimStatus() != null && !getClaimStatus().isEmpty())
            claimStatusText = ClaimsStatusEnum.valueOf(getClaimStatus()).description();
        return claimStatusText;
    }

    public void setClaimStatusText(String claimStatusText) {
        this.claimStatusText = claimStatusText;
    }

    public String getClaimTypeText() {
        if (getClaimType() != null && !getClaimType().isEmpty())
            claimTypeText = ClaimsTypeEnum.valueOf(getClaimType()).description();
        return claimTypeText;
    }

    public void setClaimTypeText(String claimTypeText) {
        this.claimTypeText = claimTypeText;
    }

    public String getRefundReasonText() {
        if (getRefundReason() != null && !getRefundReason().isEmpty())
            claimTypeText = RefundReasonEnum.valueOf(getRefundReason()).description();
        return refundReasonText;
    }

    public void setRefundReasonText(String refundReasonText) {
        this.refundReasonText = refundReasonText;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingTypeText() {
        if (getShippingType() != null && !getShippingType().isEmpty())
            shippingTypeText = ShipTypeEnum.valueOf(getShippingType()).getDesc();
        return shippingTypeText;
    }

    public void setShippingTypeText(String shippingTypeText) {
        this.shippingTypeText = shippingTypeText;
    }

    public String getMemberNickname() {
        return memberNickname;
    }

    public void setMemberNickname(String memberNickname) {
        this.memberNickname = memberNickname;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public String getShipAddr() {
        return shipAddr;
    }

    public void setShipAddr(String shipAddr) {
        this.shipAddr = shipAddr;
    }

    public String getMemberRealnameLv1() {
        return memberRealnameLv1;
    }

    public void setMemberRealnameLv1(String memberRealnameLv1) {
        this.memberRealnameLv1 = memberRealnameLv1;
    }

    public String getMemberMobileLv1() {
        return memberMobileLv1;
    }

    public void setMemberMobileLv1(String memberMobileLv1) {
        this.memberMobileLv1 = memberMobileLv1;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteLeaderName() {
        return siteLeaderName;
    }

    public void setSiteLeaderName(String siteLeaderName) {
        this.siteLeaderName = siteLeaderName;
    }

    public String getSiteLeaderMobile() {
        return siteLeaderMobile;
    }

    public void setSiteLeaderMobile(String siteLeaderMobile) {
        this.siteLeaderMobile = siteLeaderMobile;
    }

    public String getDeliveryTimeString() {
        return deliveryTimeString;
    }

    public void setDeliveryTimeString(String deliveryTimeString) {
        this.deliveryTimeString = deliveryTimeString;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }
}
