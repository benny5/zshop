package com.enation.app.javashop.core.member.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2021/1/22 14:27
 */
@Data
@Table(name = "es_member_pv")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberPvDO implements Serializable {

    // 访问ID
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;
    // 会员ID
    @Column(name = "member_id")
    @ApiModelProperty(name = "member_id", value = "会员ID")
    private Integer memberId;
    // 会员名称
    @Column(name = "member_name")
    @ApiModelProperty(name = "member_name", value = "会员名称")
    private String memberName;
    // 会员访问时间
    @Column(name = "visit_day")
    @ApiModelProperty(name = "visit_day", value = "会员访问时间")
    private String visitDay;
    // 访问次数
    @Column(name = "visit_count")
    @ApiModelProperty(name = "visit_count", value = "访问次数")
    private String visitCount;
    // 团长会员id
    @Column(name = "distributor_id")
    @ApiModelProperty(name = "distributor_id", value = "团长会员id")
    private Integer distributorId;
    // 团长姓名
    @Column(name = "distributor_name")
    @ApiModelProperty(name = "distributor_name", value = "团长姓名")
    private String distributorName;
    // 团名
    @Column(name = "team_name")
    @ApiModelProperty(name = "team_name", value = "团名")
    private String teamName;
    // 访问城市
    @Column(name = "city")
    @ApiModelProperty(name = "city", value = "访问城市")
    private String city;
    // 访问区
    @Column(name = "district")
    @ApiModelProperty(name = "district", value = "访问区")
    private String district;

}
