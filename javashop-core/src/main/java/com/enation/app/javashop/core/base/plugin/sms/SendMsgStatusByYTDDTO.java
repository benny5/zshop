package com.enation.app.javashop.core.base.plugin.sms;

import lombok.Data;

import java.io.Serializable;

/**
 * @author tongzhou
 * @date 2019-12-25 15:50
 **/
@Data
public class SendMsgStatusByYTDDTO implements Serializable{

    private String mobile;
    private String content;
    private String sendTime;
    private String extno;
    private String smsid;

}
