package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2020/10/20
 * @Description:
 */
@Data
public class ImportShetuanGoods {

    //活动ID
    @ExcelProperty(value = "活动ID", index = 0)
    private Integer shetuanId;

    //活动名称
    @ExcelProperty(value = "活动名称", index = 1)
    private String shetuanName;

    //skuID
    @ExcelProperty(value = "skuID", index = 2)
    private Integer skuId;

    @ExcelProperty(value = "商品名称", index = 3)
    private String goodsName;

    @ExcelProperty(value = "优先级", index = 4)
    private Integer priority;

    @ExcelProperty(value = "成本价格", index = 5)
    private Double cost;

    @ExcelProperty(value = "团购价", index = 6)
    private Double shetuanPrice;

    @ExcelProperty(value = "参团数量", index = 7)
    private Integer goodsNum;

    @ExcelProperty(value = "虚拟数量", index = 8)
    private Integer visualNum;

    @ExcelProperty(value = "限购数量", index = 9)
    private Integer limitNum;

    @ExcelProperty(value = "团长佣金比例", index = 10)
    private Double firstRate;

    @ExcelProperty(value = "二级团长佣金", index = 11)
    private Double secondRate;

    @ExcelProperty(value = "自提站点佣金", index = 12)
    private Double selfRaisingRate;

    @ExcelProperty(value = "邀请佣金比例", index = 13)
    private Double inviteRate;

}
