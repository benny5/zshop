package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Data
public class GoodsRateVO {
    @ApiModelProperty(name = "id",value = "id")
    @NotEmpty(message = "id不能为空")
    private Integer id;
    @ApiModelProperty(name = "rate_name",value = "比率名称")
    @NotEmpty(message = "字段名不能为空")
    private String rateName;
    @ApiModelProperty(name = "num",value = "比率值")
    @Min(0)
    private Double rate;
}
