package com.enation.app.javashop.core.wms.model.vo;

import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.core.wms.model.dos.WmsOrderDO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/9/25 19:27
 */
@Data
public class WmsOrderDetail extends WmsOrderDO{

    @ApiModelProperty(value = "sku列表")
    private List<OrderSkuVO> orderSkuList;



}
