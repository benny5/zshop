package com.enation.app.javashop.core.promotion.pintuan.model;

import com.enation.app.javashop.framework.elasticsearch.EsSettings;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Created by kingapex on 2019-01-21.
 * 拼团商品在索引中的文档
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2019-01-21
 */
@Document(indexName = "#{esConfig.indexName}_"+ EsSettings.PINTUAN_INDEX_NAME, type = EsSettings.PINTUAN_TYPE_NAME)
@Data
public class PtGoodsDoc {

    private Integer skuId;

    private Integer goodsId;

    private Integer pintuanId;

    private String goodsName;

    private String thumbnail;

    private Integer buyCount;

    private Double originPrice;

    private Double salesPrice;

    private Integer categoryId;

    private String categoryPath;

    private Boolean isEnableSale;

    private Integer limitNum;

    private String selling;

    private Integer sellerId;

    private Integer requiredNum;

    private Long pickTime;


}
