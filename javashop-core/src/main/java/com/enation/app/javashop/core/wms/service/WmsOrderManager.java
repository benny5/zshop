package com.enation.app.javashop.core.wms.service;

import com.enation.app.javashop.core.aftersale.model.dos.ClaimsDO;
import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.wms.model.dto.SortDirectParam;
import com.enation.app.javashop.core.wms.model.dto.WmsOrderQueryParam;
import com.enation.app.javashop.core.wms.model.vo.WmsOrderDetail;
import com.enation.app.javashop.core.wms.model.vo.WmsOrderVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/9/21 17:28
 */
public interface WmsOrderManager {
    Page<WmsOrderVO> queryDeliverOrderPage(WmsOrderQueryParam param);

    void acceptOrder(OrderDO orderDO);

    void acceptClaims(ClaimsDO claimsDO);

    void cancelOrder(RefundDO refund);

    void createSorting(SortDirectParam sortDirectParam);

    void outStock(Integer[] deliverySns, String deliveryStatus, String refuseReason);

   void updatePrintTimes(Integer[] deliveryId);

    List<WmsOrderVO> queryList(Integer[] deliveryIds);

    void orderSended(OrderDO order);

    void updateReceiveTimeByIds(Integer[] deliveryIds, String receiveDay);

    void delayDispatch(Integer[] deliveryIds);

    void cancelClaims(ClaimsDO claimsDO);
}
