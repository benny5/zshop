package com.enation.app.javashop.core.promotion.luck.model.vo;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/2/24
 * @Description: 可抽奖次数
 */
@Data
public class LuckTotalTime {

    @Column(name = "order_price")
    @ApiModelProperty(value = "订单金额")
    private Double orderPrice;

    @Column(name = "refundable_price")
    @ApiModelProperty(value = "可退款金额")
    private Double refundablePrice;

    /**退(货)款状态*/
    @Column(name = "refund_status")
    @ApiModelProperty(name="refund_status",value="退(货)款状态",required=false)
    private String refundStatus;

}
