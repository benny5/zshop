package com.enation.app.javashop.core.member.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MiniproMobileLoginDTO {

    /**
     * 昵称
     */
    @ApiModelProperty(name = "nickname", value = "昵称", required = false)
    private String nickname;

    /**
     * 会员手机号码
     */
    @NotEmpty(message = "手机号不能为空")
    @ApiModelProperty(name = "mobile", value = "会员手机号码")
    private String mobile;

    /**
     * 手机验证码

    @NotEmpty(message = "验证码不能为空")
    @ApiModelProperty(name = "captcha", value = "手机验证码")
    private String captcha;
     */

    /**
     * 小程序openid
     */
    @NotEmpty(message = "小程序openid不能为空")
    @ApiModelProperty(name = "openid", value = "小程序openid")
    private String openid;

    /**
     * 小程序unionid
     */
    // @NotEmpty(message = "小程序unionid不能为空")
    @ApiModelProperty(name = "unionid", value = "小程序unionid")
    private String unionid;


    @NotEmpty(message = "客户端uuid不能为空")
    @ApiModelProperty(name = "uuid", value = "客户端uuid")
    private String uuid;

    @ApiModelProperty(name = "province", value = "省份" )
    private String province;

    @ApiModelProperty(name = "city", value = "城市" )
    private String city;

    @ApiModelProperty(name = "county", value = "区/县" )
    private String county;

}
