package com.enation.app.javashop.core.goodssearch.model;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2020/11/28
 * @Description:
 */
@Data
public class GoodsTagIndex {
    @Id(name = "tag_id")
    @ApiModelProperty(hidden = true)
    private Integer tagId;
    /**
     * 标签名字
     */
    @Column(name = "tag_name")
    @ApiModelProperty(name = "tag_name", value = "标签名字", required = false)
    private String tagName;
    /**
     * 所属卖家
     */
    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "所属卖家", required = false)
    private Integer sellerId;
    @Column(name = "shop_home_page")
    @ApiModelProperty(name = "shop_home_page", value = "店铺主页显示标签（0显示，1不显示）")
    private Integer shopHomePage;
    @Column(name = "goods_list_page")
    @ApiModelProperty(name = "goods_list_page", value = "商品列表页显示标签（0显示，1不显示）")
    private Integer goodsListPage;
    @Column(name = "goods_details_page")
    @ApiModelProperty(name = "goods_details_page", value = "商品详情页显示标签（0显示，1不显示）")
    private Integer goodsDetailsPage;
    @Column(name = "priority")
    @ApiModelProperty(name = "priority", value = "标签的优先级（数字越大排序越靠前）")
    private Integer priority;
    @Column(name = "color")
    @ApiModelProperty(name = "color", value = "颜色")
    private String color;
    @Column(name = "style")
    @ApiModelProperty(name = "style", value = "样式")
    private String style;
}
