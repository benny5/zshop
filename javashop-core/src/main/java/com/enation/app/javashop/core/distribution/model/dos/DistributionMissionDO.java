package com.enation.app.javashop.core.distribution.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2021/1/22 17:20
 * 团长任务DO
 */

@Table(name="es_distribution_mission")
@Data
public class DistributionMissionDO implements Serializable {

    /**
     * 团长任务ID
     */
    @Id(name="id")
    private Integer id;
    /**
     * 团长任务名称
     */
    @Column(name = "mission_name")
    private String missionName;
    /**
     * 团长任务副标题
     */
    @Column(name = "mission_subtitle")
    private String missionSubtitle;
    /**
     * 团长任务状态
     */
    @Column(name = "mission_status")
    private Integer missionStatus;
    /**
     * 店铺ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 开始时间
     */
    @Column(name = "start_time")
    private Long startTime;
    /**
     * 结束时间
     */
    @Column(name = "end_time")
    private Long endTime;
    /**
     * 奖励类型
     */
    @Column(name = "rewards_type")
    private Integer rewardsType;
    /**
     * 团长任务活动封面
     */
    @Column(name = "cover_img")
    private String coverImg;
    /**
     * 团长活动背景图
     */
    @Column(name = "background_img")
    private String backgroundImg;
    /**
     * 团长任务描述
     */
    @Column(name = "mission_desc")
    private String missionDesc;
    /**
     * 创建人
     */
    @Column(name = "create_name")
    private String createName;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 修改人
     */
    @Column(name = "update_name")
    private String updateName;
    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Long updateTime;
    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

}