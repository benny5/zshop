package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/5/27
 * @Description: 物流信息
 */
@Data
public class YiSuPaiLogisticsDTO {
    // 数据来源：如：菜鸟裹裹
    private String dataProvider;
    // 数据来源说明，如：本数据由菜鸟裹裹提供
    private String dataProviderTitle;
    // 物流单号
    private String mailNo;
    // 物流公司名称
    private String logisticsCompanyName;
    // 物流公司编码
    private String logisticsCompanyCode;
    // 商品列表
    private List<YiSuPaiGoodsDTO> goods;
    // 物流流转列表
    private List<YiSuPaiLogisticsDetailDTO> logisticsDetailList;
}
