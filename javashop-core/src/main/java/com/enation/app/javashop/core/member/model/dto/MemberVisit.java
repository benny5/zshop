package com.enation.app.javashop.core.member.model.dto;

import java.util.List;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/9/14
 * @Description: 微信小程序返回
 */
public class MemberVisit {
    //日期
    private String refDate;
    //新增用户留存
    private List<Map<String,Integer>> visitUvNew;
    //活跃用户留存
    private List<Map<String,Integer>> visitUv;

    public String getRefDate() {
        return refDate;
    }

    public void setRefDate(String refDate) {
        this.refDate = refDate;
    }

    public List<Map<String, Integer>> getVisitUvNew() {
        return visitUvNew;
    }

    public void setVisitUvNew(List<Map<String, Integer>> visitUvNew) {
        this.visitUvNew = visitUvNew;
    }

    public List<Map<String, Integer>> getVisitUv() {
        return visitUv;
    }

    public void setVisitUv(List<Map<String, Integer>> visitUv) {
        this.visitUv = visitUv;
    }
}
