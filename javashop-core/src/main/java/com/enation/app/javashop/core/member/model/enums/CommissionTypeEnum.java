package com.enation.app.javashop.core.member.model.enums;

import com.dag.eagleshop.core.account.model.enums.AccountTypeEnum;

import java.util.HashMap;
import java.util.Map;

public enum CommissionTypeEnum {

    PLATFORM(1, "平台收益"),
    INVITER(2, "邀请佣金"),
    DISTRIBUTION(3, "团长佣金"),
    SITE(4, "服务佣金"),
    SUBSIDY(5, "平台推广补贴");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (AccountTypeEnum item : AccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    CommissionTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }


}
