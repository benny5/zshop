package com.enation.app.javashop.core.goodssearch.enums;


/**
 * @author JFENG
 */

public enum GoodsSearchType {


    LOCAL("本地商品"),

    GLOBAL("商城商品"),

    ALL("所有商品");

    private String description;

    GoodsSearchType(String description) {
        this.description = description;

    }
}
