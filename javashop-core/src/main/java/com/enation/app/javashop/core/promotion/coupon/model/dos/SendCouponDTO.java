package com.enation.app.javashop.core.promotion.coupon.model.dos;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author JFeng
 * @date 2020/9/29 11:28
 */
@Data
public class SendCouponDTO {

    @NotNull
    private Long startTime;

    private Long endTime;

    @NotNull
    private Integer couponId;

}
