package com.enation.app.javashop.core.goods.model.dto;

import com.enation.app.javashop.framework.database.annotation.Column;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SupplierDTO implements Serializable {

    /**
     * 主键
     */
    @Column(name = "id")
    private Integer id;

    /**
     * 供应商编码
     */
    @Column(name = "supplier_code")
    private String supplierCode;

    /**
     * 供应商名称
     */
    @Column(name = "supplier_name")
    private String supplierName;

    /**
     * 关联店铺id
     */
    @Column(name = "shop_id")
    private Integer shopId;

    /**
     * 供应品类
     */
    @Column(name = "supply_category")
    private String supplyCategory;

    /**
     * 供应商类型
     */
    @Column(name = "supplier_type")
    private String supplierType;

    /**
     * 采购方式
     */
    @Column(name = "procurement_method")
    private String procurementMethod;

    /**
     * 供应商等级
     */
    @Column(name = "supplier_level")
    private String supplierLevel;

    /**
     * 结算方式
     */
    @Column(name = "settle_mode")
    private String settleMode;

    /**
     * 对账日期
     */
    @Column(name = "reconciliation_date")
    private String reconciliationDate;

    /**
     * 结算账期
     */
    @Column(name = "settlement_period")
    private String settlementPeriod;

    /**
     * 联系人
     */
    @Column(name = "contacts")
    private String contacts;

    /**
     * 联系方式
     */
    @Column(name = "contact_mobile")
    private String contactMobile;

    /**
     * 省id
     */
    @Column(name = "province_id")
    private Integer provinceId;

    /**
     * 省
     */
    @Column(name = "province")
    private String province;

    /**
     * 城市id
     */
    @Column(name = "city_id")
    private Integer cityId;

    /**
     * 城市
     */
    @Column(name = "city")
    private String city;

    /**
     * 区/县Id
     */
    @Column(name = "county_id")
    private Integer countyId;

    /**
     * 区/县
     */
    @Column(name = "county")
    private String county;

    /**
     * 详细地址
     */
    @Column(name = "detailed_address")
    private String detailedAddress;

    /**
     * 供应商状态 1 启用 0 禁用
     */
    @Column(name = "status")
    private Integer status;

    /**
     * 备注
     */
    @Column(name = "remarks")
    private String remarks;
}
