package com.enation.app.javashop.core.system.model.dos;

import java.io.Serializable;
import java.util.Map;

/**
 * @author 王志杨
 * @since 2020/11/12 13:52
 * 发送微信公众号模板消息请求实体
 */
public class WxPublicTemplate implements Serializable {

    //消息模板ID
    private String template_id;
    //接收消息人的openID
    private String touser;
    //链接地址
    private String url;
    //消息模板标题字体颜色
    private String topcolor;
    //跳转微信小程序所需参数
    private Map<String, String> miniprogram; // 跳小程序所需数据，不需跳小程序可不用传该数据
    //请求参数
    private Map<String,WxPublicTemplateData> data;

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public Map<String,WxPublicTemplateData> getData() {
        return data;
    }

    public void setData(Map<String,WxPublicTemplateData> data) {
        this.data = data;
    }

    public Map<String, String> getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(Map<String, String> miniprogram) {
        this.miniprogram = miniprogram;
    }

    public WxPublicTemplate(String template_id, String touser, String url, String topcolor, Map<String, String> miniprogram, Map<String, WxPublicTemplateData> data) {
        this.template_id = template_id;
        this.touser = touser;
        this.url = url;
        this.topcolor = topcolor;
        this.miniprogram = miniprogram;
        this.data = data;
    }

    public WxPublicTemplate() {
    }
}
