package com.enation.app.javashop.core.pagedata.model;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * 楼层实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@Table(name = "es_page")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PageData implements Serializable {

    private static final long serialVersionUID = 3806389481183972L;

    /**
     * 主键id
     */
    @Id(name = "page_id")
    @ApiModelProperty(hidden = true)
    private Integer pageId;
    /**
     * 楼层名称
     */
    @Column(name = "page_name")
    @ApiModelProperty(name = "page_name", value = "楼层名称", required = true)
    @NotEmpty(message = "名称不能为空")
    private String pageName;
    /**
     * 楼层数据
     */
    @Column(name = "page_data")
    @ApiModelProperty(name = "page_data", value = "楼层数据", required = true)
    @NotEmpty(message = "页面数据不能为空")
    private String pageData;

    @Column(name = "page_type")
    @ApiModelProperty(name = "page_type", value = "页面类型", hidden = true)
    private String pageType;

    @Column(name = "page_type_name")
    @ApiModelProperty(name = "page_type_name", value = "页面类型名称", hidden = true)
    private String pageTypeName;

    @Column(name = "client_type")
    @ApiModelProperty(name = "client_type", value = "客户端类型", hidden = true)
    private String clientType;


    @Column(name = "store_id")
    @ApiModelProperty(name = "store_id", value = "商店id", hidden = true)
    private Integer storeId;

    @Column(name = "agent_id")
    @ApiModelProperty(name = "agent_id", value = "代理", hidden = true)
    private Integer agentId;

    @Column(name = "owner_type")
    @ApiModelProperty(name = "owner_type", value = "拥有者类型", hidden = true)
    private Integer ownerType;

    @Column(name = "background")
    @ApiModelProperty(name = "background", value = "页面背景")
    private String background;

    @Column(name = "page_desc")
    @ApiModelProperty(name = "page_desc", value = "页面描述")
    private String pageDesc;

    @Column(name = "state")
    @ApiModelProperty(name = "state", value = "状态", hidden = true)
    private Integer state;

    @Column(name = "province")
    @ApiModelProperty(name = "province", value = "省")
    private String province;

    @Column(name = "city")
    @ApiModelProperty(name = "city", value = "市", hidden = true)
    private String city;

    @Column(name = "county")
    @ApiModelProperty(name = "county", value = "区/县", hidden = true)
    private String county;

    @Column(name = "town")
    @ApiModelProperty(name = "town", value = "镇", hidden = true)
    private String town;

    @Column(name = "county_id")
    @ApiModelProperty(name = "county_id", value = "区/县Id", hidden = true)
    private Integer countyId;

    public void setIsDiscover(Integer isDiscover) {
        this.isDiscover = isDiscover;
    }

    @Column(name = "is_discover")
    @ApiModelProperty(name = "is_discover", value = "是否发现", hidden = true)
    private Integer  isDiscover;

    @Column(name = "share_image")
    @ApiModelProperty(name = "share_image", value = "首页分享图片", hidden = true)
    private String shareImage;

    public String getPageDesc() {
        return pageDesc;
    }

    public void setPageDesc(String pageDesc) {
        this.pageDesc = pageDesc;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getIsDiscover() {
        return isDiscover;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @PrimaryKeyField
    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageData() {
        return pageData;
    }

    public void setPageData(String pageData) {
        this.pageData = pageData;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(Integer ownerType) {
        this.ownerType = ownerType;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getShareImage() {
        return shareImage;
    }

    public void setShareImage(String shareImage) {
        this.shareImage = shareImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PageData that = (PageData) o;
        if (pageId != null ? !pageId.equals(that.pageId) : that.pageId != null) {
            return false;
        }
        if (pageName != null ? !pageName.equals(that.pageName) : that.pageName != null) {
            return false;
        }
        if (pageData != null ? !pageData.equals(that.pageData) : that.pageData != null) {
            return false;
        }
        if (pageType != null ? !pageType.equals(that.pageType) : that.pageType != null) {
            return false;
        }
        return clientType != null ? clientType.equals(that.clientType) : that.clientType == null;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (pageId != null ? pageId.hashCode() : 0);
        result = 31 * result + (pageName != null ? pageName.hashCode() : 0);
        result = 31 * result + (pageData != null ? pageData.hashCode() : 0);
        result = 31 * result + (pageType != null ? pageType.hashCode() : 0);
        result = 31 * result + (clientType != null ? clientType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PageData{" +
                "pageId=" + pageId +
                ", pageName='" + pageName + '\'' +
                ", pageData='" + pageData + '\'' +
                ", pageType='" + pageType + '\'' +
                ", clientType='" + clientType + '\'' +
                ", storeId='" + storeId + '\'' +
                ", ownerType='" + ownerType + '\'' +
                ", agentId='" + agentId + '\'' +
                ", state='" + state + '\'' +
                '}';
    }


    public String getPageTypeName() {
        return pageTypeName;
    }

    public void setPageTypeName(String pageTypeName) {
        this.pageTypeName = pageTypeName;
    }


    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }
}
