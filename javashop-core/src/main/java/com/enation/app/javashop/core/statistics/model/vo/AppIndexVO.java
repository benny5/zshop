package com.enation.app.javashop.core.statistics.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/4/7 9:56
 */
@Data
public class AppIndexVO extends  ShopDashboardVO {
    @ApiModelProperty(value = "支付订单数")
    private Integer paidOrderNum;

    @ApiModelProperty(value = "交易额")
    private Double tradeAmount;

    @ApiModelProperty(value = "客单价")
    private Double orderAvgPrice;

    @ApiModelProperty(value = "团长总数")
    private Integer distributorNum;

    @ApiModelProperty(value = "团均订单数")
    private Double distributorOrderNum;

    @ApiModelProperty(value = "团长出单率")
    private Double distributorOrderRatio;

    @ApiModelProperty(value = "自提点总数")
    private Integer siteNum;

    @ApiModelProperty(value = "点均订单数")
    private Double siteOrderNum;

    @ApiModelProperty(value = "自提点出单率")
    private Double siteOrderRatio;

    //=============================================

    @ApiModelProperty(value = "支付订单数")
    private Integer lastDayPaidOrderNum;

    @ApiModelProperty(value = "昨日交易额")
    private Double lastDayTradeAmount;

    @ApiModelProperty(value = "昨日客单价")
    private Double lastDayOrderAvgPrice;

    @ApiModelProperty(value = "昨日活跃团长")
    private Integer lastDayActiveDistributorNum;

    @ApiModelProperty(value = "昨日新增团长")
    private Integer lastDayNewDistributorNum;

    @ApiModelProperty(value = "昨日新增自提点")
    private Integer lastDayNewSiteNum;

    @ApiModelProperty(value = "是否社区团购")
    private Integer communityShop;

}
