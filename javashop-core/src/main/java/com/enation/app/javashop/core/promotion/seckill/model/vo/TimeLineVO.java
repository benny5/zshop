package com.enation.app.javashop.core.promotion.seckill.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 时刻表列表元素
 *
 * @author Snow create in 2018/7/30
 * @version v2.0
 * @since v7.0.0
 */

@Data
public class TimeLineVO {


    @ApiModelProperty(value = "距离此组时刻的开始时间，如果为0代表正在进行中。" +
            "例如：如果此组时刻为18点，现在的时间为16点，那么此时间是已秒为单位的2小时的时间")
    private Long distanceTime;

    @ApiModelProperty(value = "距离本组结束时间。")
    private Long endDistanceTime;

    @ApiModelProperty(value = "时刻文字")
    private String timeText;

    @ApiModelProperty(value = "距离下组时刻开始，还差多少时间，仅正在进行中的时刻展示使用。")
    private Long nextDistanceTime;


    @ApiModelProperty(value = "是否开始 0:未开始 1：进行中 2 ：已结束")
    private Integer startStatus;


    @ApiModelProperty(value = "距离活动结束的时间，秒为单位")
    private Long distanceEndTime;

    @ApiModelProperty(value = "距离活动开始的时间，秒为单位。如果活动的开始时间是10点，服务器时间为8点，距离开始还有多少时间")
    private Long distanceStartTime;

    private Integer isCurrent;

}
