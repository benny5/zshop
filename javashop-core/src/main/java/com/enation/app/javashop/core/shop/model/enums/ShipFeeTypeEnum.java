package com.enation.app.javashop.core.shop.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 同城配送费 计价方式
 */
public enum ShipFeeTypeEnum {
    BASE_DISTANCE(1, "基础距离计价"),

    SECTION_DISTANCE(2, "距离区间计价");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (ShipFeeTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    ShipFeeTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }


    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(int index){
        return enumMap.get(index);
    }


}
