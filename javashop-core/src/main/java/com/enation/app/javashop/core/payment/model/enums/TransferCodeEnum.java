package com.enation.app.javashop.core.payment.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/10/12
 * @Description:
 */
public enum TransferCodeEnum {

    E601(601,"微信异常"),
    E602(602,"请联系管理员配置参数"),
    E603(603,"用户未绑定openID"),
    E604(604,"找不到证书路径，请联系管理员正确配置"),
    E605(605,"提现到零钱失败，请联系管理员检查参数配置"),
    E608(608,"查询微信提现状态失败，请联系管理员检查参数配置"),
    E609(609,"系统异常，请联系管理员"),
    E610(610,"请求参数异常"),
    E611(611,"收款人姓名与微信实名不一致"),
    E612(612,"微信未实名认证，无法转账");

    TransferCodeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    private int code;
    //备注
    private String description;

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String value() {
        return this.name();
    }

}
