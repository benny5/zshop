package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/5/14
 * @Description:
 */
@Data
public class YiSuPaiGoodsPageDTO {
    // 总商品数
    private Integer totalRecords;
    // 当前页
    private Integer currentPage;
    // 每页几条
    private Integer pageSize;
    private List<YiSuPaiGoodsDTO> list;

}
