package com.enation.app.javashop.core.wms.model.enums;

/**
 * 出库单状态
 * @author JFeng
 * @date 2020/9/22 14:39
 */
public enum ShipServiceTypeEnums {

    INNER_SOURCE("自主配送"),

    OUT_SOURCE("外包配送");

    private String description;

    ShipServiceTypeEnums(String description) {
        this.description = description;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
