package com.enation.app.javashop.core.promotion.pintuan.model;

import com.enation.app.javashop.core.goods.model.dto.BuyRecord;
import com.enation.app.javashop.core.goodssearch.model.GoodsSearchLine;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by kingapex on 2019-01-22.
 * 拼团商品信息VO
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2019-01-22
 */
@Data
public class PinTuanGoodsVO extends PintuanGoodsDO {

    @ApiModelProperty(name = "required_num", value = "成团人数")
    private Integer requiredNum;

    @ApiModelProperty(name = "start_time_left", value = "开始剩余时间，秒数")
    private Long startTimeLeft;

    @ApiModelProperty(name = "time_left", value = "结束剩余时间，秒数")
    private Long timeLeft;

    @ApiModelProperty(name = "promotion_rule", value = "拼团规则")
    private String promotionRule;

    @ApiModelProperty(name = "selling", value = "商品卖点/商品详情")
    private String selling;

    @ApiModelProperty(hidden = true, value = "开始时间戳")
    private Long startTime;

    @ApiModelProperty(hidden = true, value = "结束时间戳")
    private Long endTime;

    @ApiModelProperty(name = "enable_quantity", value = "可用库存")
    private Integer enableQuantity;

    @ApiModelProperty(name = "goods_detail", value = "商品详情")
    private GoodsSearchLine goodsDetail;

    @ApiModelProperty(name = "cost", value = "成本价格")
    private Double cost;

    private Boolean isEnableSale;

    private List<BuyRecord> buyRecordList;

}
