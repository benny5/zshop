package com.enation.app.javashop.core.statistics.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/4/7 9:56
 */
@Data
public class IncomeDataVO {
    /** 店铺营收数据类型*/
    private Integer incomeType;
    /** 最近一周收入*/
    private Double weekIncome;
    /** 单日最高*/
    private Double dayMax;
    /**日均*/
    private  Double dayAverage;
    /**新增访客数*/
    private SimpleChart simpleChart;

}
