package com.enation.app.javashop.core.trade.order.service;

import com.enation.app.javashop.core.trade.order.model.dos.OrderMemberDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderMetaDO;
import com.enation.app.javashop.core.trade.order.model.enums.OrderMetaKeyEnum;

import java.util.List;

/**
 * 会员订单统计
 */
public interface OrderMemberManager {

    /**
     * 添加
     * @param orderMemberDO
     */
    void add(OrderMemberDO orderMemberDO);

    /**
     * 读取订单统计
     * @param memberId
     * @return
     */
    OrderMemberDO getOrderMember(int memberId);

    /**
     * 更新会员订单统计
     */
    void updateOrderMember(String oldFiled,String newFiled,int memberId);

    void orderStatistic(Integer memberId);
}
