package com.enation.app.javashop.core.promotion.newcomer.service;

import com.enation.app.javashop.core.promotion.newcomer.model.dos.NewcomerGoodsDO;

import java.util.List;
import java.util.Map;

import com.enation.app.javashop.core.promotion.newcomer.model.vos.MiniNewcomerDataVO;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillGoodsVO;
import com.enation.app.javashop.core.promotion.tool.model.dto.PromotionDTO;
import com.enation.app.javashop.framework.database.Page;


/**
 * @Author: zhou
 * @Date: 2021/2/25
 * @Description:
 */
public interface NewcomerGoodsManager {

    // 新增活动商品
    void addNewcomerGoods(List<NewcomerGoodsDO> newcomerGoodsDOList);

    // 获取活动商品列表
    List<NewcomerGoodsDO> getNewcomerGoodsList(Integer newcomerId);

    /**
     * 查询当前活动中的商品
     */
    Map<String,Object> selNewcomerGoods(Integer pageNo, Integer pageSize, String city);

    /**
     * 增加已销售库存数量
     */
    boolean addSalesNum(List<PromotionDTO> promotionDTOList);

    /**
     * 回滚库存
     */
    void rollbackStock(List<PromotionDTO> promotionDTOList);

    NewcomerGoodsDO getModel(Integer id);

    MiniNewcomerDataVO getNewcomerGoodsByNewcomerId(Integer newcomerId,Integer skuId);
}
