package com.enation.app.javashop.core.trade.order.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/4/19 23:11
 */
public enum PeriodTypeEnum {
    /**
     * 7天
     */
    WEEK(0,7),

    /**
     * 15天
     */
    HALF_MONTH(1,15),

    /**
     * 30天
     */
    MONTH(2,30);

    private Integer index;
    private Integer text;

    private static Map<Integer, PeriodTypeEnum> enumMap = new HashMap<>();

    static {
        for (PeriodTypeEnum item : values()) {
            enumMap.put(item.getIndex(), item);
        }
    }

    PeriodTypeEnum(int index, int text){
        this.index = index;
        this.text = text;
    }


    public Integer getIndex() {
        return index;
    }

    public Integer getText() {
        return text;
    }

    public static PeriodTypeEnum resolve(Integer index){
        return enumMap.get(index);
    }


}
