package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.core.shop.model.dos.ShipLocalTemplateVO;
import com.enation.app.javashop.core.shop.model.dos.ShipTemplateDO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author fk
 * @version v2.0
 * @Description: 运费模板VO
 * @date 2018/8/22 15:16
 * @since v7.0.0
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShipTemplateSellerVO extends ShipTemplateDO implements Serializable {

    @ApiModelProperty(name = "items", value = "指定配送区域", required = true)
    private List<ShipTemplateChildSellerVO>  items;

    @ApiModelProperty(name = "localTemplate", value = "指定配送区域", required = true)
    private ShipLocalTemplateVO localTemplate;

    @ApiParam("起送价格")
    private BigDecimal baseShipPrice;

    @ApiParam("起送距离")
    private Integer  shipRange;


}
