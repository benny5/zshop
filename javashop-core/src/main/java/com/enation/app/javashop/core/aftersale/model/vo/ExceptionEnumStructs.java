package com.enation.app.javashop.core.aftersale.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2020/9/24 17:01
 * 异常枚举结构体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionEnumStructs implements Serializable {

    private static final long serialVersionUID = 5611660104636476408L;

    private String label;
    private String value;

}
