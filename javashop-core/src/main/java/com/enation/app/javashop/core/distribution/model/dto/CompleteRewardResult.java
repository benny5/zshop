package com.enation.app.javashop.core.distribution.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompleteRewardResult {

    /**
     * 是否完成
     */
    private boolean success;
    /**
     * 完成阶段的奖金
     */
    private BigDecimal levelReward;
    /**
     * 完成阶段的优惠券id
     */
    private Integer levelCouponId;

    public CompleteRewardResult(boolean success){
        this.success = success;
    }

}
