package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.enation.app.javashop.core.goods.model.enums.StoreTypeEnum;
import lombok.Data;

/**
 * 配送汇总
 *
 * @author JFeng
 * @date 2020/7/17 14:31
 */
@ExcelIgnoreUnannotated
public class ExportSkuSupplyVO {

    @ExcelProperty("商品分类")
    private String categoryName;

    @ExcelProperty("sku名称")
    private String name;

    @ExcelProperty("sku_id")
    private Integer skuId;

    @ExcelProperty("供应商")
    private String supplierName;

    @ExcelProperty("数量")
    private Integer goodsCount;

    @ExcelProperty("配送日期")
    private Integer deliveryDate;

    /**
     * 导出时忽略这个字段
     */
    @ExcelIgnore
    private Integer store;

    @ExcelIgnore
    private Integer preSort;

    @ExcelProperty("贮藏要求")
    private String storeText;

    @ExcelProperty("是否预分拣")
    private String preSortText;

    @ExcelProperty("成本价")
    private Double cost;

    @ExcelProperty("售价")
    private Double price;

    private Integer categoryId;

    private String categoryPath;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Integer getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Integer getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Integer deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getStore() {
        return store;
    }

    public void setStore(Integer store) {
        if (store != null) {
            this.storeText = StoreTypeEnum.getNameByIndex(store);
        }
        this.store = store;
    }

    public Integer getPreSort() {
        return preSort;
    }

    public void setPreSort(Integer preSort) {
        if (preSort != null) {
            this.preSortText = preSort == 1 ? "是" : "否";
        }
        this.preSort = preSort;
    }

    public String getStoreText() {
        return storeText;
    }

    public String getPreSortText() {
        return preSortText;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
