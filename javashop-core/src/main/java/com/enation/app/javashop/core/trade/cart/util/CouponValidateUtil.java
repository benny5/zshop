package com.enation.app.javashop.core.trade.cart.util;

import com.enation.app.javashop.core.goods.model.enums.GoodsType;
import com.enation.app.javashop.core.promotion.tool.model.enums.PromotionTypeEnum;
import com.enation.app.javashop.core.promotion.tool.model.vo.PromotionVO;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import com.enation.app.javashop.core.trade.cart.model.vo.SelectedPromotionVo;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
* @author liuyulei
 * @version 1.0
 * @Description: 检测优惠券是否可用
 * @date 2019/5/7 20:38
 * @since v7.0
 */
public class CouponValidateUtil {



    /**
     * 检测选择的促销活动是否为积分兑换  如果为积分兑换则不能使用优惠券
     * @param selectedPromotionVo
     */
    public static Boolean validateCoupon(SelectedPromotionVo selectedPromotionVo, Integer sellerId, List<CartSkuVO> skuList){
        Map<Integer, List<PromotionVO>> singlePromotionMap = selectedPromotionVo.getSinglePromotionMap();

        List<PromotionVO> promotions = singlePromotionMap.get(sellerId);
        AtomicReference<Boolean> result = new AtomicReference<>(new Boolean(false));
        if(promotions != null && !promotions.isEmpty()){

            promotions.forEach(promotionVO -> {
                for (CartSkuVO cartSkuVO : skuList) {
                    if (promotionVO.getSkuId().equals(cartSkuVO.getSkuId())) {
                        //此时存在积分商品
                        result.set( PromotionTypeEnum.EXCHANGE.name().equals(promotionVO.getPromotionType()));
                        break;
                    }
                }
            });

        }

        return result.get();
    }


    /**
     * 监测购物车中是否包含
     * @param skuList
     * @return
     */
    public static Boolean validateExchange(List<CartSkuVO> skuList){
        for (CartSkuVO cartSkuVO : skuList) {
            //此时存在积分商品
            if(GoodsType.POINT.name().equals(cartSkuVO.getGoodsType()) && cartSkuVO.getChecked().intValue() == 1){
                return true;
            }
        }

        return false;
    }


}
