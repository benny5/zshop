package com.enation.app.javashop.core.member.model.dto;

import com.enation.app.javashop.core.base.context.Region;
import com.enation.app.javashop.core.base.context.RegionFormat;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 买家修改会员DTO
 *
 * @author zh
 * @version v7.0
 * @date 18/4/26 下午10:40
 * @since v7.0
 */
@Data
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberEditDTO {

    /**
     * 会员真实姓名
     */
    @Column(name = "real_name")
    @ApiModelProperty(name = "real_name", value = "会员真实姓名")
    private String realName;
    /**
     * 昵称
     */
    @Column(name = "nickname")
    @ApiModelProperty(name = "nickname", value = "昵称", required = true)
    // @Length(min = 2, max = 20, message = "会员昵称必须为2到20位之间")
    private String nickname;

    @RegionFormat
    @ApiModelProperty(name = "region", value = "地区")
    private Region region;
    /**
     * 会员性别
     */
    @Column(name = "sex")
    @ApiModelProperty(name = "sex", value = "会员性别,1为男，0为女", required = true)
    private Integer sex;
    /**
     * 会员生日
     */
    @Column(name = "birthday")
    @ApiModelProperty(name = "birthday", value = "会员生日")
    private Long birthday;
    /**
     * 详细地址
     */
    @Column(name = "address")
    @ApiModelProperty(name = "address", value = "详细地址")
    private String address;
    /**
     * 邮箱
     */
    @Column(name = "email")
    @Email(message = "邮箱格式不正确")
    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;

    /**
     * 座机号码
     */
    @Column(name = "tel")
    @ApiModelProperty(name = "tel", value = "座机号码")
    private String tel;

    /**
     * 会员头像
     */
    @Column(name = "face")
    @ApiModelProperty(name = "face", value = "会员头像")
    private String face;

    /**
     * 纬度
     */
    @Column(name = "lat")
    @ApiModelProperty(name = "lat", value = "纬度", required = false)
    private Double lat;
    /**
     * 经度
     */
    @Column(name = "lng")
    @ApiModelProperty(name = "lng", value = "经度", required = false)
    private Double lng;

    /**
     * 账户会员id
     */
    @Column(name = "account_member_id")
    @ApiModelProperty(name = "accountMemberId", value = "账户会员id", required = false)
    private String accountMemberId;


}
