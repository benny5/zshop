package com.enation.app.javashop.core.goods.model.dto;

import com.enation.app.javashop.core.goods.model.dos.GoodsGalleryDO;
import com.enation.app.javashop.core.goods.model.dos.GoodsParamsDO;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * 商品vo
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月21日 上午11:25:10
 */
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class GoodsDTO implements Serializable {

    private static final long serialVersionUID = 3922135264669953741L;
    @ApiModelProperty(hidden = true)
    private Integer goodsId;

    @ApiModelProperty(name = "goods_name", value = "商品名称", required = true)
    @NotEmpty(message = "商品名称不能为空")
    private String goodsName;

    @ApiModelProperty(name = "goods_gallery_list", value = "商品相册", required = true)
    @NotNull(message = "商品相册图片不能为空")
    @Size(min = 1,message = "商品相册图片不能为空")
    private List<GoodsGalleryDO> goodsGalleryList;

    @ApiModelProperty(name = "video_url", value = "主图视频", required = false)
    private String videoUrl;

    @ApiModelProperty(name = "category_id", value = "分类id", required = false)
    @NotNull(message = "商城分类不能为空")
    private Integer categoryId;

    @ApiModelProperty(name = "selling", value = "商品卖点", required = false)
    private String selling;

    /**
     * 用作显示
     */
    @ApiModelProperty(name = "category_name", value = "分类名称", required = false)
    private String categoryName;

    @ApiModelProperty(name = "shop_cat_id", value = "店铺分类id", required = false)
    @Min(value = 0, message = "店铺分类值不正确")
    private Integer shopCatId;

    @ApiModelProperty(name = "brand_id", value = "品牌id", required = false)
    @Min(value = 0, message = "品牌值不正确")
    private Integer brandId;

    /** 如果brandId为空 并且brandName非空 说明用户自定义了品牌 */
    @ApiModelProperty(name = "brand_name", value = "品牌名称", required = false)
    private String brandName;

    @ApiModelProperty(name = "price", value = "商品价格", required = true)
    @Min(value = 0, message = "商品价格不能为负数")
    @Max(value = 99999999, message = "商品价格不能超过99999999")
    private Double price;


    @ApiModelProperty(name = "mktprice", value = "市场价格", required = false)
    @Min(value = 0, message = "市场价格不能为负数")
    @Max(value = 99999999, message = "市场价格不能超过99999999")
    private Double mktprice;

    @ApiModelProperty(name = "have_spec", value = "是否有规格0没有1有", hidden = true)
    private Integer haveSpec;

    @ApiModelProperty(name = "sku_list", value = "sku列表", required = false)
    @Valid
    private List<GoodsSkuVO> skuList;

    @ApiModelProperty(name = "has_changed", value = "sku数据变化或者组合变化判断 0:没变化，1：变化", required = true)
    private Integer hasChanged;

    @ApiModelProperty(name = "quantity", value = "库存", required = true)
    @Max(value = 99999999, message = "库存不能超过99999999")
    private Integer quantity;

    @ApiModelProperty(value = "可用库存")
    private Integer enableQuantity;

    @ApiModelProperty(name = "weight", value = "重量", required = false)
    @Min(value = 0, message = "重量不能为负数")
    @Max(value = 99999999,message = "重量不能超过99999999")
    private Double weight;


    @ApiModelProperty(name = "sn", value = "商品编号", required = false)
    @Length(max = 30,message = "商品编号太长，不能超过30个字符")
    private String sn;


    @ApiModelProperty(name = "cost", value = "成本价格", required = true)
    @Min(value = 0, message = "成本价格不能为负数")
    @Max(value = 99999999, message = "成本价格不能超过99999999")
    private Double cost;




    @ApiModelProperty(name = "goods_transfee_charge", value = "谁承担运费0：买家承担，1：卖家承担")
    @Min(value = 0, message = "承担运费值不正确")
    @Max(value = 1, message = "承担运费值不正确")
    private Integer goodsTransfeeCharge;



    @ApiModelProperty(name = "page_title", value = "seo标题", required = false)
    private String pageTitle;

    @ApiModelProperty(name = "meta_keywords", value = "seo关键字", required = false)
    private String metaKeywords;

    @ApiModelProperty(name = "meta_description", value = "seo描述", required = false)
    private String metaDescription;


    @ApiModelProperty(name = "goods_params_list", value = "商品参数", required = false)
    @Valid
    private List<GoodsParamsDO> goodsParamsList;

    @ApiModelProperty(name = "intro", value = "详情", required = false)
    private String intro;

    @ApiModelProperty(name = "market_enable", value = "是否上架，1上架 0下架", required = false)
    @Min(value = 0, message = "是否上架值不正确")
    @Max(value = 1, message = "是否上架值不正确")
    private Integer marketEnable;

    @ApiModelProperty(value = "商品是否审核")
    private Integer isAuth;

    @ApiModelProperty(value = "商品的评论数量", hidden = true)
    private Integer commentNum;

    @ApiModelProperty(name = "exchange", value = "积分兑换对象，不是积分兑换商品可以不传")
    private ExchangeVO exchange;

    @ApiModelProperty(value = "是否本地商品")
    private Boolean isLocal;

    @ApiModelProperty(value = "是否商城商品")
    private Boolean isGlobal;

    @ApiModelProperty(name = "is_self_take", value = "是否自提", required = false)
    private Boolean isSelfTake;

    @ApiModelProperty(name = "is_self_take", value = "运费模板id,不需要运费模板时值是0", required = false)
    @Min(value = 0, message = "运费模板值不正确")
    private Integer templateId;

    @ApiModelProperty(name = "local_template_id", value = "运费模板id,不需要运费模板时值是0", required = false)
    private Integer localTemplateId;


    @ApiModelProperty(name = "freight_pricing_way", value = "运费定价方式 0.统一运费 1.模板运费", required = false)
    private Integer freightPricingWay;

    /** 运费一口价 */
    @ApiModelProperty(name = "freight_unified_price", value = "运费统一价", required = false)
    private Double freightUnifiedPrice;


    @ApiModelProperty(name = "supplier_name", value = "供应商姓名")
    private String  supplierName;

    @ApiModelProperty(name = "up_goods_id", value = "上游商品id")
    private String upGoodsId;

    @ApiModelProperty(name = "commission_display", value = "商品佣金")
    private Double commissionDisplay;

    @ApiModelProperty(name = "local_freight_pricing_way", value = "同城运费定价方式 0.统一运费 1.模板运费", required = false)
    private Integer localFreightPricingWay;

    /** 运费一口价 */
    @ApiModelProperty(name = "local_freight_unified_price", value = "同城运费统一价", required = false)
    private Double localFreightUnifiedPrice;

    @ApiModelProperty(name = "store",value = "贮藏要求")
    private Integer store;

    @ApiModelProperty(name = "pre_sort",value = "是否预分拣")
    private Integer preSort;

    @ApiModelProperty(name = "goods_type", value = "商品类型NORMAL普通,POINT积分,VIRTUAL虚拟商品")
    private String goodsType;

    @ApiModelProperty(name = "expiry_day",value = "虚拟商品的使用截止日期")
    private Integer expiryDay;

    @ApiModelProperty(name = "available_date",value = "虚拟商品的可用日期")
    private String availableDate;

    //@ApiModelProperty(name = "delivery_setting", value = "商品配送设置-json")
    //private DeliverySetting deliverySetting;

}
