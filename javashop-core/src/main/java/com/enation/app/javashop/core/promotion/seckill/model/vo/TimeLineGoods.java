package com.enation.app.javashop.core.promotion.seckill.model.vo;

import com.enation.app.javashop.core.promotion.seckill.model.dos.SeckillApplyDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author JFeng
 * @date 2020/12/24 15:17
 */

@Data
public class TimeLineGoods {

    private Integer timeLine;

    private List<SeckillGoodsVO> seckillGoodsList;

    @ApiModelProperty(value = "距离活动结束的时间，秒为单位")
    private Long distanceEndTime;

    @ApiModelProperty(value = "距离活动开始的时间，秒为单位。如果活动的开始时间是10点，服务器时间为8点，距离开始还有多少时间")
    private Long distanceStartTime;

}
