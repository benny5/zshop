package com.enation.app.javashop.core.promotion.pintuan.model;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.PrimaryKeyField;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.Min;


/**
 * 拼团商品实体
 *
 * @author admin
 * @version vv1.0.0
 * @since vv7.1.0
 * 2019-01-22 11:20:56
 */
@Table(name = "es_pintuan_goods")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class PintuanGoodsDO {

    /**
     * id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;
    /**
     * sku_id
     */
    @Column(name = "sku_id")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "sku_id", value = "sku_id")
    private Integer skuId;

    @Column(name = "goods_id")
    @ApiModelProperty(name = "goods_id", value = "goods_id")
    private Integer goodsId;


    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "卖家id")
    private Integer sellerId;

    /**
     * 卖家名称
     */
    @Column(name = "seller_name")
    @ApiModelProperty(name = "seller_name", value = "卖家名称", hidden = true)
    private String sellerName;


    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    @ApiModelProperty(name = "goods_name", value = "商品名称", required = true)
    private String goodsName;
    /**
     * 原价
     */
    @Column(name = "origin_price")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "origin_price", value = "原价")
    private Double originPrice;
    /**
     * 活动价
     */
    @Column(name = "sales_price")
    @Min(message = "必须为数字", value = 0)
    @ApiModelProperty(name = "sales_price", value = "活动价")
    private Double salesPrice;
    /**
     * sn
     */
    @Column(name = "sn")
    @ApiModelProperty(name = "sn", value = "sn", required = true)
    private String sn;
    /**
     * 已售数量
     */
    @Column(name = "sold_quantity")
    @ApiModelProperty(name = "sold_quantity", value = "已售数量")
    private Integer soldQuantity;
    /**
     * 待发货数量
     */
    @Column(name = "locked_quantity")
    @ApiModelProperty(name = "locked_quantity", value = "待发货数量")
    private Integer lockedQuantity;
    /**
     * 拼团活动id
     */
    @Column(name = "pintuan_id")
    @ApiModelProperty(name = "pintuan_id", value = "拼团活动id")
    private Integer pintuanId;


    @Column(name = "specs")
    @ApiModelProperty(name = "specs", value = "规格信息json")
    @JsonRawValue
    private String specs;

    @Column(name = "thumbnail")
    @ApiModelProperty(name = "thumbnail", value = "商品图片")
    private String thumbnail;

    @Column(name = "limit_num")
    @ApiModelProperty(name = "limit_num", value = "限购数量")
    private Integer limitNum;

    @Column(name = "pick_time")
    @ApiModelProperty(name = "pick_time", value = "提货时间")
    private Long pickTime;

    private String pickTimeShow;

}