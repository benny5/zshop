package com.enation.app.javashop.core.promotion.pintuan.service.impl;

import com.enation.app.javashop.core.base.CachePrefix;
import com.enation.app.javashop.core.client.goods.GoodsClient;
import com.enation.app.javashop.core.goods.model.dos.CategoryDO;
import com.enation.app.javashop.core.goods.model.dos.GoodsDO;
import com.enation.app.javashop.core.goods.model.dto.BuyRecord;
import com.enation.app.javashop.core.goods.model.vo.CacheGoods;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goods.service.GoodsQueryManager;
import com.enation.app.javashop.core.goodssearch.util.HexUtil;
import com.enation.app.javashop.core.promotion.pintuan.model.PinTuanGoodsVO;
import com.enation.app.javashop.core.promotion.pintuan.model.Pintuan;
import com.enation.app.javashop.core.promotion.pintuan.model.PtGoodsDoc;
import com.enation.app.javashop.core.promotion.pintuan.service.PinTuanSearchManager;
import com.enation.app.javashop.core.promotion.tool.support.SkuNameUtil;
import com.enation.app.javashop.core.shop.service.ShopManager;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.elasticsearch.EsConfig;
import com.enation.app.javashop.framework.elasticsearch.EsSettings;
import com.enation.app.javashop.framework.logs.Debugger;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by kingapex on 2019-01-21.
 * 拼团搜索业务实现者
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-21
 */

@Service
public class PinTuanSearchManagerImpl implements PinTuanSearchManager {


    @Autowired
    @Qualifier("tradeDaoSupport")
    private DaoSupport tradeDaoSupport;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private EsConfig esConfig;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private ShopManager shopManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Autowired
    private Cache cache;

    protected final Log logger = LogFactory.getLog(this.getClass());

    public PinTuanSearchManagerImpl() {
    }

    public PinTuanSearchManagerImpl(ElasticsearchTemplate _elasticsearchTemplate, EsConfig _esConfig) {
        this.elasticsearchTemplate = _elasticsearchTemplate;
        this.esConfig = _esConfig;

    }

    @SuppressWarnings("Duplicates")
    @Override
    public List<PtGoodsDoc> search(Integer categoryId, Integer pageNo, Integer pageSize) {

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        // 分类检索
        if (categoryId != null) {

            CategoryDO category = goodsClient.getCategory(categoryId);
            if (category != null) {
                boolQueryBuilder.must(QueryBuilders.wildcardQuery("categoryPath", HexUtil.encode(category.getCategoryPath()) + "*"));
            }

        }
        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        searchQuery.withIndices(indexName).withTypes(EsSettings.PINTUAN_TYPE_NAME).withPageable(pageable).withQuery(boolQueryBuilder);

        List<PtGoodsDoc> resultlist = elasticsearchTemplate.queryForList(searchQuery.build(), PtGoodsDoc.class);

        for (PtGoodsDoc goodsDoc : resultlist) {
            GoodsSkuVO skuVO = goodsClient.getSkuFromCache(goodsDoc.getSkuId());
            if (skuVO.getEnableQuantity() > 0) {
                goodsDoc.setIsEnableSale(true);
            } else {
                goodsDoc.setIsEnableSale(false);
            }
        }

        return resultlist;
    }

    @Override
    public Page<List<PinTuanGoodsVO>> searchByCity(String city, Integer pageNo, Integer pageSize) {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        // 城市查询店铺
        if (city != null) {
            Integer shopId = shopManager.getShopByCity(city);
            if (shopId != null) {
                boolQueryBuilder.must(QueryBuilders.matchQuery("sellerId", shopId.toString()));
            }
        }

        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        searchQuery.withIndices(indexName).withTypes(EsSettings.PINTUAN_TYPE_NAME).withPageable(pageable).withQuery(boolQueryBuilder);

        AggregatedPage<PtGoodsDoc> ptGoodsDocs = elasticsearchTemplate.queryForPage(searchQuery.build(), PtGoodsDoc.class);

        List<PtGoodsDoc> esGoodsList = ptGoodsDocs.getContent();
        List<PinTuanGoodsVO> resultlist = new ArrayList<>();
        if(!CollectionUtils.isEmpty(esGoodsList)){
            for (PtGoodsDoc goodsDoc : esGoodsList) {
                PinTuanGoodsVO pinTuanGoodsVO = new PinTuanGoodsVO();
                BeanUtil.copyProperties(goodsDoc, pinTuanGoodsVO);
                GoodsSkuVO skuVO = goodsClient.getSkuFromCache(goodsDoc.getSkuId());
                if (skuVO.getEnableQuantity() > 0) {
                    pinTuanGoodsVO.setIsEnableSale(true);
                } else {
                    pinTuanGoodsVO.setIsEnableSale(false);
                }

                // 购买记录
                pinTuanGoodsVO.setBuyRecordList( cache.range(CachePrefix.BUY_REC.getPrefix() + goodsDoc.getSkuId(), 0, 5));
                // 提货时间
                String pickTimeShow = Optional.ofNullable(pinTuanGoodsVO.getPickTime())
                        .map(time -> DateUtil.toString(time, "MM月dd日")).orElse("次日送达");
                pinTuanGoodsVO.setPickTimeShow(pickTimeShow);
                resultlist.add(pinTuanGoodsVO);
            }
        }

        return new Page(pageNo, ptGoodsDocs.getTotalElements(), pageSize, resultlist);
    }

    @Override
    public void addIndex(PtGoodsDoc goodsDoc) {

        //对cat path特殊处理
        CategoryDO categoryDO = goodsClient.getCategory(goodsDoc.getCategoryId());
        goodsDoc.setCategoryPath(HexUtil.encode(categoryDO.getCategoryPath()));

        IndexQuery indexQuery = new IndexQuery();
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        indexQuery.setIndexName(indexName);
        indexQuery.setType("pintuan_goods");
        indexQuery.setId(goodsDoc.getSkuId().toString());
        indexQuery.setObject(goodsDoc);

        elasticsearchTemplate.index(indexQuery);
        if (logger.isDebugEnabled()) {
            logger.debug("将拼团商品将拼团商品ID[" + goodsDoc.getGoodsId() + "] " + goodsDoc + " 写入索引");
        }
    }

    @Autowired
    private Debugger debugger;

    @Override
    public boolean addIndex(PinTuanGoodsVO pintuanGoods) {

        String goodsName  = pintuanGoods.getGoodsName() + SkuNameUtil.createSkuName(pintuanGoods.getSpecs());

        try {

            CacheGoods cacheGoods = goodsClient.getFromCache(pintuanGoods.getGoodsId());
            PtGoodsDoc ptGoodsDoc = new PtGoodsDoc();
            ptGoodsDoc.setCategoryId(cacheGoods.getCategoryId());
            ptGoodsDoc.setThumbnail(pintuanGoods.getThumbnail());
            ptGoodsDoc.setSalesPrice(pintuanGoods.getSalesPrice());
            ptGoodsDoc.setOriginPrice(pintuanGoods.getOriginPrice());
            ptGoodsDoc.setGoodsName( goodsName );
            ptGoodsDoc.setBuyCount(pintuanGoods.getSoldQuantity());
            ptGoodsDoc.setGoodsId(pintuanGoods.getGoodsId());
            ptGoodsDoc.setSkuId(pintuanGoods.getSkuId());
            ptGoodsDoc.setPintuanId(pintuanGoods.getPintuanId());
            ptGoodsDoc.setSellerId(pintuanGoods.getSellerId());
            ptGoodsDoc.setSelling(pintuanGoods.getSelling());
            ptGoodsDoc.setLimitNum(pintuanGoods.getLimitNum()==null? 0:pintuanGoods.getLimitNum());
            ptGoodsDoc.setRequiredNum(pintuanGoods.getRequiredNum());
            ptGoodsDoc.setPickTime(pintuanGoods.getPickTime());

            logger.info("【生成拼团商品索引】："+ptGoodsDoc.toString());
            this.addIndex(ptGoodsDoc);

            return true;
        } catch (Exception e) {
            logger.error("为拼团商品[" + goodsName + "]生成索引报错",e);
            debugger.log("为拼团商品[" + goodsName + "]生成索引报错", StringUtil.getStackTrace(e));
            return false;
        }


    }

    /**
     * 向es写入索引
     *
     * @param skuId
     */
    @Override
    public void delIndex(Integer skuId) {
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;

        elasticsearchTemplate.delete(indexName, EsSettings.PINTUAN_TYPE_NAME, "" + skuId);

        if (logger.isDebugEnabled()) {
            logger.debug("将拼团商品ID[" + skuId + "]删除索引");
        }
    }

    @Override
    public void deleteByGoodsId(Integer goodsId) {
        DeleteQuery dq = dq();
        dq.setQuery(QueryBuilders.termQuery("goodsId", goodsId));

        elasticsearchTemplate.delete(dq);
    }

    private DeleteQuery dq() {
        DeleteQuery dq = new DeleteQuery();
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        dq.setIndex(indexName);
        dq.setType(EsSettings.PINTUAN_TYPE_NAME);
        return dq;
    }

    @Override
    public void deleteByPintuanId(Integer pinTuanId) {
        DeleteQuery dq = dq();
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        dq.setIndex(indexName);
        dq.setType(EsSettings.PINTUAN_TYPE_NAME);
        dq.setQuery(QueryBuilders.termQuery("pintuanId", pinTuanId));

        logger.info("【拼团商品索引删除】"+dq.toString());
        elasticsearchTemplate.delete(dq);
    }

    @Override
    public void syncIndexByPinTuanId(Integer pinTuanId, Pintuan pintuan) {
        //查询出数据库中的拼团商品
        String sql = "select * from es_pintuan_goods where pintuan_id=? ";
        List<PinTuanGoodsVO> dbList = tradeDaoSupport.queryForList(sql, PinTuanGoodsVO.class, pinTuanId);

        //查询出索引出的商品
        //BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        //bqb.must(QueryBuilders.termQuery("pinTuanId", pinTuanId));

        //List<PtGoodsDoc> esList = queryList(bqb);

        //同步数据
        this.updateEsPintuanGoods(dbList,pintuan);

    }

    @Override
    public void syncIndexByGoodsId(Integer goodsId) {

        CacheGoods goods = goodsClient.getFromCache(goodsId);
        List<GoodsSkuVO> skuList = goods.getSkuList();

        Long now = DateUtil.getDateline();

        //查询出数据库中的拼团商品
        String sql = "select g.* from es_pintuan_goods g,es_pintuan pt where g.pintuan_id = pt.promotion_id and  g.goods_id=? and   pt.start_time<? and pt.end_time>?  ";
        List<PinTuanGoodsVO> dbTempList = tradeDaoSupport.queryForList(sql, PinTuanGoodsVO.class, goodsId, now, now);

        List<PinTuanGoodsVO> dbList = new ArrayList<>();

        for (PinTuanGoodsVO ptGoods : dbTempList) {
            if (findSku(ptGoods.getSkuId(), skuList)) {
                dbList.add(ptGoods);
            }
        }


        //根据商品id查询出索引中的商品
        BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        bqb.must(QueryBuilders.termQuery("goodsId", goodsId));

        List<PtGoodsDoc> esList = queryList(bqb);
        //同步数据
        sync(dbList, esList);

    }


    private boolean findSku(int skuId, List<GoodsSkuVO> skuList) {
        for (GoodsSkuVO sku : skuList) {
            if (sku.getSkuId().equals(skuId)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 由es中查询拼团sku列表
     *
     * @param bqb BoolQueryBuilder
     * @return
     */
    private List<PtGoodsDoc> queryList(BoolQueryBuilder bqb) {

        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;

        searchQuery.withIndices(indexName).withTypes(EsSettings.PINTUAN_TYPE_NAME).withQuery(bqb);

        List<PtGoodsDoc> esList = elasticsearchTemplate.queryForList(searchQuery.build(), PtGoodsDoc.class);

        return esList;
    }

    private void updateEsPintuanGoods(List<PinTuanGoodsVO> dbList,Pintuan pintuan){
        this.delIndexByPintuanId(pintuan.getPromotionId());
        List<Integer> goodsIds = dbList.stream().map(PinTuanGoodsVO::getGoodsId).collect(Collectors.toList());
        List<GoodsDO> goodsDOS = this.goodsQueryManager.queryByIds(goodsIds);
        Map<Integer, GoodsDO> goodsDOMap = goodsDOS.stream().collect(Collectors.toMap(GoodsDO::getGoodsId, Function.identity()));
        for (PinTuanGoodsVO pinTuanGoodsVO : dbList) {
            pinTuanGoodsVO.setRequiredNum(pintuan.getRequiredNum());
            GoodsDO goodsDO = goodsDOMap.get(pinTuanGoodsVO.getGoodsId());
            pinTuanGoodsVO.setSelling(goodsDO.getSelling());
            this.addIndex(pinTuanGoodsVO);
        }
    }

    private void delIndexByPintuanId(Integer pinTuanId) {
        DeleteQuery deleteQuery = new DeleteQuery();
        String indexName = esConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;

        BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        bqb.must(QueryBuilders.termQuery("pintuanId", pinTuanId));

        deleteQuery.setIndex(indexName);
        deleteQuery.setType(EsSettings.PINTUAN_TYPE_NAME);
        deleteQuery.setQuery(bqb);

        elasticsearchTemplate.delete(deleteQuery);
    }

    /**
     * 对比数据库和es中的两个集合，以数据库的为准，同步es中的数据
     *
     * @param dbList 数据库集合
     * @param esList es中的集合
     */
    private void sync(List<PinTuanGoodsVO> dbList, List<PtGoodsDoc> esList) {

        //按数据库中的来循环
        for (PinTuanGoodsVO dbGoods : dbList) {
            PtGoodsDoc goodsDoc = findFromList(esList, dbGoods.getSkuId());

            //在索引中没找到说明新增了
            if (goodsDoc == null) {
                this.addIndex(dbGoods);

            } else {

                //看看售价或者限购数量变没变
                Double salesPrice = goodsDoc.getSalesPrice();
                Double dbPrice = dbGoods.getSalesPrice();
                Integer esLimitNum = goodsDoc.getLimitNum();
                Integer dbLimitNum = dbGoods.getLimitNum();

                //价格或者限购数量发生变化了，更新索引
                if (!salesPrice.equals(dbPrice) || !esLimitNum.equals(dbLimitNum)) {
                    goodsDoc.setSalesPrice(dbPrice);
                    goodsDoc.setLimitNum(dbLimitNum);
                    IndexQuery indexQuery = new IndexQueryBuilder().withId("" + goodsDoc.getSkuId()).withObject(goodsDoc).build();
                    elasticsearchTemplate.index(indexQuery);
                }
            }
        }

        //大小不一样，说明可能会发生了删除的
        if (esList.size() != dbList.size()) {
            esList.forEach(goodsDoc -> {
                boolean result = findFormList(dbList, goodsDoc.getSkuId());

                //没找到，说明删除了
                if (!result) {
                    delIndex(goodsDoc.getSkuId());
                }
            });

        }

    }

    /**
     * 查询 数据库的列表中没有
     *
     * @param dbList
     * @param skuId
     * @return
     */
    private boolean findFormList(List<PinTuanGoodsVO> dbList, Integer skuId) {
        for (PinTuanGoodsVO goodsVO : dbList) {
            if (goodsVO.getSkuId().equals(skuId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找索引列表中有没有
     *
     * @param list
     * @param skuId
     * @return
     */
    private PtGoodsDoc findFromList(List<PtGoodsDoc> list, Integer skuId) {
        for (PtGoodsDoc goodsDoc : list) {
            if (goodsDoc.getSkuId().equals(skuId)) {
                return goodsDoc;
            }
        }

        return null;
    }
}
