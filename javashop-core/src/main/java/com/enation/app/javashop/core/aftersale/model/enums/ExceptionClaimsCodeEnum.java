package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常理赔返回码
 */
public enum ExceptionClaimsCodeEnum {

    S200(200, "操作成功"),
    E614(614, "请求参数异常"),
    E615(615, "服务器异常"),
    E616(616, "商品库存不足"),
    E617(617, "异常状态判断不正确"),
    E618(618, "存在审核通过的理赔，无法进行异常撤销"),
    E619(619, "赔付现金失败"),
    E620(620, "多退少补失败"),
    E621(621, "包含不能审核的理赔单"),
    E622(622, "已审核通过，不能撤销"),
    E623(623, "未找到需要赔付现金的用户账号"),
    E624(624, "该订单已有异常申请记录"),
    E625(625, "该订单已申请过维权");

    private int code;
    private String description;

    ExceptionClaimsCodeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String value(){
        return this.name();
    }
}
