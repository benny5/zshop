package com.enation.app.javashop.core.promotion.newcomer.service;

import com.enation.app.javashop.core.promotion.newcomer.model.dos.NewcomerDO;
import com.enation.app.javashop.core.promotion.newcomer.model.vos.QueryNewcomerVO;
import com.enation.app.javashop.framework.database.Page;

/**
 * @Author: zhou
 * @Date: 2021/2/25
 * @Description: 新人购活动
 */
public interface NewcomerManager {

    // 查询新人购活动列表
    Page<NewcomerDO> queryNewcomerList(QueryNewcomerVO queryNewcomerVO);

    // 新增或修改新人购活动
    void addOrUpdateNewcomer(NewcomerDO newcomerDO);

    // 删除新人购活动
    void deleteNewcomer(Integer newcomerId);

    // 活动上下线
    void onOffLine(Integer newcomerId, Integer status);
}
