package com.enation.app.javashop.core.aftersale.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 王志杨
 * @since 2020/9/3
 * 多退少补查询VO
 */
@ApiModel
@Data
public class SpreadRefundQueryParamVO implements Serializable {

    @ApiModelProperty(value = "第几页")
    @NotNull(message = "页码不能为空")
    private Integer pageNo;

    @ApiModelProperty(value = "每页条数")
    @NotNull(message = "每页数量不能为空")
    private Integer pageSize;

    @ApiModelProperty(name="refund_sn",value="退款流水号",required=false)
    private String sn;

    @ApiModelProperty(name="order_sn",value="订单编号",required=false)
    private String orderSn;

    @ApiModelProperty(hidden=true, value = "会员名称")
    private String memberName;

    @ApiModelProperty(name="refund_type",value="售后类型(多付退款-SPREAD_REFUND)",required=false)
    @NotBlank(message = "请指定退补类型")
    private String refundType;

    @ApiModelProperty(name="refund_reason",value="退款原因",required=false)
    private String refundReason;

    @ApiModelProperty(name="refund_status",value="退款状态",required=false)
    private String refundStatus;

    @ApiModelProperty(hidden=true, value = "退款失败原因")
    private String refundFailReason;

    @ApiModelProperty(name="operator", value="操作人",required=false)
    private String operator;

    @ApiModelProperty(hidden=true, value="退款金额")
    private Double refundPrice;

    @ApiModelProperty(hidden=true, value="退款标题")
    private String refundTitle;

    @ApiModelProperty(hidden=true, value="退款说明")
    private String refundIntroductions;

    @ApiModelProperty(hidden=true, value="退款描述")
    private String refundDescription;

    @ApiModelProperty(hidden=true, value="退款时间")
    private String refundTime;

    @ApiModelProperty(hidden=true, value = "会员id")
    private Integer memberId;

    @ApiModelProperty(hidden=true, value = "店铺id")
    private Integer sellerId;

}
