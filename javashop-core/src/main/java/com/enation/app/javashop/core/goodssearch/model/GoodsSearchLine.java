package com.enation.app.javashop.core.goodssearch.model;

import com.enation.app.javashop.core.goods.model.dto.TagsDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商品搜索
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-16 16:32:45
 */
@Data
public class GoodsSearchLine {

    /**
     * 商品id
     */
    private int goodsId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 缩略图
     */
    private String thumbnail;

    /**
     * 小图
     */
    private String small;

    /**
     * 商品优惠价格
     */
    private Double discountPrice;

    /**
     * 商品价格
     */
    private Double price;

    /**
     * 商品价格
     */
    private Double mktprice;

    /**
     * 购买数
     */
    private Integer buyCount;

    /**
     * 评论数
     */
    private Integer commentNum;

//	/**库存数*/
//	private Integer quantity;

    /**
     * 商品好評率
     */
    private Double grade;

    /**
     * 卖家id
     */
    private Integer sellerId;
    /**
     * 店铺名字
     */
    private String sellerName;

    /**
     * 是否自营商品 0否 1是
     */
    private Integer selfOperated;

    /**
     * 商品描述
     */
    private String metaDescription;

    private String distance;

    private BigDecimal distanceValue;

    private String shopLogo;

    private Integer isLocal;

    private Integer isGlobal;

    private Integer isSelfTake;

    private String shipFeeSetting;

    private String shipTimeSetting;

    private Double baseShipPrice;

    private Double shipRange;

    private String templateTc;

    private String shipPriceShow;

    private String shipTimeShow;

    private Object[] sortFiled;

    private String sortType;

    private Integer allowLocal;

    private String videoUrl;

    private String selling;

    private Integer communityShop;

    private List<TagsDTO> shopHomeTags;

    private List<TagsDTO> goodsListTags;

    private List<TagsDTO> goodsDetailsTags;

}
