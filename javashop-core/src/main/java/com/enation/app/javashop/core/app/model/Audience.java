package com.enation.app.javashop.core.app.model;

import lombok.Data;

import java.util.List;

/**
 * 设备信息
 */
@Data
public class Audience {
    /**
     * 标签OR
     * 数组。多个标签之间是 OR 的关系，即取并集。
     * 用标签来进行大规模的设备属性、用户属性分群。 一次推送最多 20 个。
     * 有效的 tag 组成：字母（区分大小写）、数字、下划线、汉字、特殊字符@!#$&*+=.|￥。
     * 限制：每一个 tag 的长度限制为 40 字节。（判断长度需采用 UTF-8 编码）
     */
    private String[] tag;

    /**
     * 标签AND
     * 数组。多个标签之间是 AND 关系，即取交集。
     * 注意与 tag 区分。一次推送最多 20 个。
     */
    private String[] tag_and;

    /**
     * 标签NOT
     * 数组。多个标签之间，先取多标签的并集，再对该结果取补集。
     * 一次推送最多 20 个。
     */
    private String[] tag_not;

    /**
     * 别名
     * 数组。多个别名之间是 OR 关系，即取并集
     * 用别名来标识一个用户。一个设备只能绑定一个别名，但多个设备可以绑定同一个别名。一次推送最多 1000 个。
     * 有效的 alias 组成：字母（区分大小写）、数字、下划线、汉字、特殊字符@!#$&*+=.|￥。
     * 限制：每一个 alias 的长度限制为 40 字节。（判断长度需采用 UTF-8 编码）
     */
    private List<String> alias;

    /**
     * 注册ID
     * 数组。多个注册 ID 之间是 OR 关系，即取并集。
     * 设备标识。一次推送最多 1000 个。客户端集成 SDK 后可获取到该值。如果您一次推送的 registration_id 值超过 1000 个，可以直接使用 文件推送 功能。
     */
    private List<String> registration_id;

    /**
     * 用户分群 ID
     * 在页面创建的用户分群的 ID。定义为数组，但目前限制一次只能推送一个。
     * 目前限制是一次只能推送一个。
     */
    private String[] segment;

    /**
     * A/B Test ID
     * 在页面创建的 A/B 测试的 ID。定义为数组，但目前限制是一次只能推送一个。
     * 目前限制一次只能推送一个。
     */
    private String[] abtest;

}
