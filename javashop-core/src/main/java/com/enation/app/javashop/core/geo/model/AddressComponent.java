package com.enation.app.javashop.core.geo.model;

import lombok.Data;

import java.util.List;

@Data
public class AddressComponent {
    //国家
    private String country;
    //省名
    private String province;
    //城市名
    private String city;
    //区县名
    private String district;
    //乡镇名
    private String township;
    //乡镇名 新加
    private String townshipStr;
}
