package com.enation.app.javashop.core.trade.order.model.dto;

import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/4/4 18:14
 */
@Data
public class RefundOrderDTO extends OrderDO {
    /**退(货)款单id*/
    @ApiModelProperty(hidden=true)
    private Integer refundId;
    /**退货(款)单编号*/
    @ApiModelProperty(name="refund_sn",value="退货(款)单编号",required=false)
    private String refundSn;
     /**退(货)款状态*/
    @ApiModelProperty(name="refund_status",value="退(货)款状态",required=false)
    private String refundStatus;
    /**退款金额*/
    @ApiModelProperty(name="refund_price",value="退款金额",required=false)
    private Double refundPrice;
    /**退还积分*/
    @ApiModelProperty(name="refund_point",value="退还积分",required=false)
    private Integer refundPoint;
    /**退款方式(原路退回，在线支付，线下支付)*/
    @ApiModelProperty(name="refund_way",value="退款方式(原路退回，线下支付)",required=false)
    private String refundWay;
    /**退款账户类型*/
    @ApiModelProperty(name="account_type",value="退款账户类型",required=false)
    private String accountType;
    /**退款账户*/
    @ApiModelProperty(name="return_account",value="退款账户",required=false)
    private String returnAccount;
    /**客户备注*/
    @ApiModelProperty(name="customer_remark",value="客户备注",required=false)
    private String customerRemark;
    /**客服备注*/
    @ApiModelProperty(name="seller_remark",value="客服备注",required=false)
    private String sellerRemark;

    @ApiModelProperty(name = "refund_time",value = "退款时间",hidden = true)
    private Long refundTime;
    /**退款原因*/
    @ApiModelProperty(name="refund_reason",value="退款原因",required=false)
    private String refundReason;
    /**拒绝原因*/
    @ApiModelProperty(name="refuse_reason",value="拒绝原因",required=false)
    private String refuseReason;

    /**售后类型(取消订单,申请售后)*/
    @ApiModelProperty(name="refund_type",value="售后类型(取消订单,申请售后)",required=false)
    private String refundType;
    /**订单类型(在线支付,货到付款)*/
    @ApiModelProperty(name="payment_type",value="订单类型(在线支付,货到付款)",required=false)
    private String paymentType;
    /**退(货)款类型(退货，退款)*/
    @ApiModelProperty(name="refuse_type",value="退(货)款类型(退货，退款)",required=false,hidden = true)
    private String refuseType;

    @ApiModelProperty(name = "refund_fail_reason",value = "退款失败原因",required = false)
    private String refundFailReason;
}
