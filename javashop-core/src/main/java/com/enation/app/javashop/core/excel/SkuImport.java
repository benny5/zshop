package com.enation.app.javashop.core.excel;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/7/11 17:31
 */

@Data
public class SkuImport {

    private Integer skuId;

    private String upSkuId;

    private String goodsName;
    // 价格
    private Double price;
    // 库存
    private Integer quantity;

}
