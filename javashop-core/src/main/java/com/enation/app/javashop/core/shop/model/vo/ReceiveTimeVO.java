package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.core.shop.model.dto.FixTimeSection;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public  class ReceiveTimeVO {


    private List<FixReceiveTimeVO> timeSections;

    private Boolean hasReceiveTime;

    private String defaultTimeText;

    private Integer defaultSeq;

    private LocalDate postDay;

    private String pintuanPickTime;
}


