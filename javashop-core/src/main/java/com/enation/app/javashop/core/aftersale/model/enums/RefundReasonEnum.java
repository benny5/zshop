package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @Author 王校长
 * @Date 2020/9/3
 * @Descroption 退补差价原因
 */
public enum RefundReasonEnum {
    //重量误差
    WEIGHT_ERROR("重量误差"),
    //价格错误
    PRICE_ERROR("价格错误"),
    //活动优惠
    PREFERENTIAL_ACTIVITIES("活动优惠");


    private String description;

    RefundReasonEnum(String des){
        this.description=des;
    }

    public String description(){
        return this.description;
    }

    public String value(){
        return this.name();
    }
}
