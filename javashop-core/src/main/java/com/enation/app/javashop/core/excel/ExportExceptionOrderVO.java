package com.enation.app.javashop.core.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author 王校长
 * @since 2020/9/4
 * 退补单导出excel实体
 */
@Data
public class ExportExceptionOrderVO {

    /**异常单号*/
    @ExcelProperty(value = "异常单号", index = 0)
    private String exceptionSn;
    /**订单号*/
    @ExcelProperty(value = "订单号", index = 1)
    private String orderSn;
    /**异常处理状态*/
    @ExcelProperty(value = "处理状态", index = 2)
    private String exceptionStatusText;
    /**上报来源*/
    @ExcelProperty(value = "上报来源", index = 3)
    private String exceptionSourceText;
    /**异常类型*/
    @ExcelProperty(value = "异常类型", index = 4)
    private String exceptionTypeText;
    /**异常描述*/
    @ExcelProperty(value = "异常描述", index = 5)
    private String exceptionDescription;
    /**解决说明*/
    @ExcelProperty(value = "解决说明", index = 6)
    private String solveExplain;
    /**解决方式字符串*/
    @ExcelProperty(value = "解决方式", index = 7)
    private String solveTypes;
    /**客户评分*/
    @ExcelProperty(value = "客户评分", index = 8)
    private Double memberScore;
    /**客户评价*/
    @ExcelProperty(value = "客户评价", index = 9)
    private String memberComment;
    /**订单类型*/
    @ExcelProperty(value = "订单类型", index = 10)
    private String orderTypeText;
    /**配送方式*/
    @ExcelProperty(value = "配送方式", index = 11)
    private String shippingTypeText;
    /**买家昵称*/
    @ExcelProperty(value = "买家昵称", index = 12)
    private String memberNickname;
    /** 收货人姓名*/
    @ExcelProperty(value = "收货人", index = 13)
    private String shipName;
    /**收货地址*/
    @ExcelProperty(value = "收货地址", index = 14)
    private String shipAddr;
    /**收货人手机*/
    @ExcelProperty(value = "收货电话", index = 15)
    private String shipMobile;
    /**一级团长真实姓名*/
    @ExcelProperty(value = "团长姓名", index = 16)
    private String memberRealnameLv1;
    /**一级团长手机号码*/
    @ExcelProperty(value = "团长电话", index = 17)
    private String memberMobileLv1;
    /**站点名*/
    @ExcelProperty(value = "自提站点", index = 18)
    private String siteName;
    /**站长名称*/
    @ExcelProperty(value = "站长姓名", index = 19)
    private String siteLeaderName;
    /**站长手机号码*/
    @ExcelProperty(value = "站长电话", index = 20)
    private String siteLeaderMobile;
    /**上报时间*/
    @ExcelProperty(value = "上报时间", index = 21)
    private String createTime;
    /**上报人*/
    @ExcelProperty(value = "上报人", index = 22)
    private String createName;
    /**接单时间*/
    @ExcelProperty(value = "接单时间", index = 23)
    private String reciveTime;
    /**接单人*/
    @ExcelProperty(value = "接单人", index = 24)
    private String reciveName;
    /**处理时间*/
    @ExcelProperty(value = "处理时间", index = 25)
    private String processTime;
    /**处理人*/
    @ExcelProperty(value = "处理人", index = 26)
    private String processName;
    /**完成时间*/
    @ExcelProperty(value = "完成时间", index = 27)
    private String completeTime;
    /**结单时间*/
    @ExcelProperty(value = "结单时间", index = 28)
    private String finishTime;
    /**结单人*/
    @ExcelProperty(value = "结单人", index = 29)
    private String finishName;

}
