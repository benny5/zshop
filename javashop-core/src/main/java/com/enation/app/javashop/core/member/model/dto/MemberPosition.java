package com.enation.app.javashop.core.member.model.dto;

import lombok.Data;

/**
 * @author JFENG
 * @version 1.0
 * @Description:
 * @date 2020/3/6 16:15
 */
@Data
public class MemberPosition {
    private String province;
    private String city;
    private String district;
    private String street;
    private String address;
    private String addressName;
    private String detailAddress;
    private String buildingPosition;
    private double longitude;
    private double latitude;
}
