package com.enation.app.javashop.core.goodssearch.model;

import com.enation.app.javashop.core.goods.model.dos.TagsDO;
import com.enation.app.javashop.framework.elasticsearch.EsSettings;
import lombok.Data;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.GeoPointField;

import java.util.List;

/**
 * Created by kingapex on 2018/7/19.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/7/19
 */
@Document(indexName = "#{esConfig.indexName}_" + EsSettings.GOODS_INDEX_NAME, type = EsSettings.GOODS_TYPE_NAME)
@Data
public class GoodsIndex {
    public GoodsIndex() {

    }

    private Integer goodsId;

    private String goodsName;

    private String thumbnail;

    private String small;

    private Integer buyCount;

    private Integer sellerId;

    private String sellerName;

    private String videoUrl;

    private String selling;

    private Integer shopCatId;

    private String shopCatPath;

    private Integer commentNum;

    private Double grade;

    private double discountPrice;

    private double price;

    private Integer brand;

    private Integer categoryId;

    private String categoryPath;

    private Integer disabled;

    private Integer marketEnable;

    private Integer isAuth;

    private String intro;

    private Integer priority;

    private String metaDescription;


    /**
     * 是否自营商品 0否 1是
     */
    private Integer selfOperated;

    private List<Param> params;


    @GeoPointField
    private GeoPoint location;

    private String shopLogo;

    private Integer isLocal;

    private Integer isGlobal;

    private Integer isSelfTake;

    private String shipFeeSetting;

    private String shipTimeSetting;

    private Double baseShipPrice;

    private Double shipRange;

    private String localTemplateId;

    private String templateId;

    private double mktprice; // 市场价/划线价

    private String shopCatName;

    private Integer communityShop;

    private Long timestamp;

    private String shopHomeTags;

    private String goodsListTags;

    private String goodsDetailsTags;
}
