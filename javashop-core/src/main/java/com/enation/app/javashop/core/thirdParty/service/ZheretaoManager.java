package com.enation.app.javashop.core.thirdParty.service;

import com.enation.app.javashop.core.thirdParty.model.dto.Result;
import com.enation.app.javashop.core.thirdParty.model.dto.ZheretaoGood;
import com.enation.app.javashop.core.thirdParty.model.vo.ZheretaoRequestVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2020/10/12
 * @Description:
 */
public interface ZheretaoManager {
    //获取存量
    void getZheretaoAllGoods();

    //获取昨天的增量
    void getZheretaoAddGoods();

    //更新商品
    Result updateGoods(ZheretaoRequestVo zheretaoRequestVo);

    /**
     * 生成浙里淘分类映射的json
     *
     * @param uploadFile 映射文件 第一列为浙里淘二级分类的Id，第二列为商城三级分类的名字
     */
    List<Integer> categoryMapping(MultipartFile uploadFile);
}
