package com.enation.app.javashop.core.goodssearch.service.impl;

import com.enation.app.javashop.core.goods.model.dto.ShipCalculateDTO;
import com.enation.app.javashop.core.goodssearch.service.ShipCalculator;
import com.enation.app.javashop.core.shop.model.dos.DistanceSection;
import com.enation.app.javashop.core.shop.model.dos.TemplateFeeSetting;
import com.enation.app.javashop.core.shop.model.dto.PeekTime;
import com.enation.app.javashop.core.shop.model.enums.ShipFeeTypeEnum;
import com.enation.app.javashop.framework.util.DateUtil;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.math.BigDecimal.ROUND_HALF_DOWN;


/**
 * @author JFENG
 */
@Component
public class ShipFeeCalculator implements ShipCalculator {


    @Override
    public void calculate(ShipCalculateDTO shipCalculateDTO) {
        TemplateFeeSetting templateFeeSetting = shipCalculateDTO.getTemplateFeeSetting();
        // 计价方式
        Integer feeType = templateFeeSetting.getFeeType()==null?ShipFeeTypeEnum.BASE_DISTANCE.getIndex():templateFeeSetting.getFeeType();
        BigDecimal distanceTarget = shipCalculateDTO.getDistance();
        BigDecimal shipPrice = BigDecimal.ZERO;
        // 基础距离计价
        if(feeType.equals(ShipFeeTypeEnum.BASE_DISTANCE.getIndex())){
            if (templateFeeSetting.getBaseDistance().compareTo(distanceTarget) < 0) {
                BigDecimal extraFee = distanceTarget.subtract(templateFeeSetting.getBaseDistance()).divide(templateFeeSetting.getUnitDistance(),3,BigDecimal.ROUND_HALF_DOWN).multiply(templateFeeSetting.getUnitPrice());
                shipPrice = templateFeeSetting.getBasePrice().add(extraFee);
            } else {
                shipPrice = templateFeeSetting.getBasePrice();
            }
        }
        // 距离区间计价
        if(feeType.equals(ShipFeeTypeEnum.SECTION_DISTANCE.getIndex())){
            double distance = distanceTarget.doubleValue();
            List<DistanceSection> distanceSections = templateFeeSetting.getDistanceSections();
            for (DistanceSection distanceSection : distanceSections) {
                if(distance>distanceSection.getStartDistance() && distance<=distanceSection.getEndDistance() ){
                    shipPrice=BigDecimal.valueOf(distanceSection.getShipPrice());
                    break;
                }
            }
        }
        // 加收费计算
        if(!CollectionUtils.isEmpty(templateFeeSetting.getPeekTimes())){
            List<PeekTime> peekTimes = templateFeeSetting.getPeekTimes();
            for (PeekTime peekTime : peekTimes) {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
                LocalTime startTime = LocalTime.parse(peekTime.getPeekTimeStart(), dateTimeFormatter);
                LocalTime endTime = LocalTime.parse(peekTime.getPeekTimeEnd(), dateTimeFormatter);
                LocalTime currentTime = LocalTime.now();
                // 当前时间处于加收时间区域内
                if(startTime.isBefore(currentTime)&&endTime.isAfter(currentTime)){
                    shipPrice.add(new BigDecimal(peekTime.getPeekTimePrice()));
                    break;
                }

            }
        }
        shipCalculateDTO.setLocalShipPrice(shipPrice.setScale(2, BigDecimal.ROUND_HALF_DOWN));
    }
}
