package com.enation.app.javashop.core.shop.model.vo;

import com.enation.app.javashop.core.shop.model.dto.NearbyShopCatGoodsDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2021/4/9
 * @Description: 附近店铺详情页
 */
@Data
public class NearbyShopDetailsVO {
    @ApiModelProperty(value = "店铺ID")
    private Integer sellerId;

    @ApiModelProperty(value = "店铺名")
    private String sellerName;

    @ApiModelProperty(value = "店铺logo")
    private String shopLogo;

    @ApiModelProperty(value = "店铺服务态度分数")
    private Double shopServiceCredit;

    @ApiModelProperty(value = "店铺经度")
    private Double shopLat;

    @ApiModelProperty(value = "店铺纬度")
    private Double shopLng;

    @ApiModelProperty(value = "店铺经验类目ID")
    private String goodsManagementCategory;

    @ApiModelProperty(value = "起送价格")
    private Double baseShipPrice;

    @ApiModelProperty(value = "最低配送价格")
    private Double minShipPrice;

    @ApiModelProperty(value = "当前定位与店铺的距离")
    private Double shopDistance;

    @ApiModelProperty(value = "用户是否收藏了店铺")
    private Boolean isCollection;

    @ApiModelProperty(value = "店铺到当前定位的配送时间")
    private Integer shipTime;

    @ApiModelProperty(value = "店铺营业时间(json)")
    private String openTime;

    @ApiModelProperty(value = "店铺商品评价")
    private String content;

    @ApiModelProperty(name="link_phone",value="联系人电话")
    private String linkPhone;

    @ApiModelProperty(name="shop_province",value="店铺所在省",required=false)
    private String shopProvince;

    @ApiModelProperty(name="shop_city",value="店铺所在市",required=false)
    private String shopCity;

    @ApiModelProperty(name="shop_county",value="店铺所在县",required=false)
    private String shopCounty;

    @ApiModelProperty(name="shop_town",value="店铺所在镇",required=false)
    private String shopTown;

    @ApiModelProperty(name="shop_add",value="店铺详细地址",required=false)
    private String shopAdd;

    @ApiModelProperty(value = "活动标签")
    private List<Map> promotionList;

    @ApiModelProperty(value = "店铺经验类目")
    private List<String> categoryNameList;

    @ApiModelProperty(value = "店铺分组")
    private List<NearbyShopCatGoodsDTO> shopCatDOList;
}
