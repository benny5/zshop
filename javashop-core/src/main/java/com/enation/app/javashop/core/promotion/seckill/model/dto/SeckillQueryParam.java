package com.enation.app.javashop.core.promotion.seckill.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Date;

/**
 * 限时抢购查询参数
 * @author Snow create in 2018/6/21
 * @version v2.0
 * @since v7.0.0
 */
@Data
public class SeckillQueryParam {

    @ApiModelProperty(value = "页码")
    private Integer pageNo;

    @ApiModelProperty(value = "每页条数")
    private Integer pageSize;

    @ApiModelProperty(value = "限时抢购活动ID")
    private Integer seckillId;

    @ApiModelProperty(value = "关键字")
    private String keywords;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "时间")
    private Integer timeLine;

    @ApiModelProperty(value = "开始时间")
    private long startDay;

    @ApiModelProperty(value = "店铺id")
    private Integer  sellerId;

    @ApiModelProperty(value = "秒杀类型")
    private String  seckillType;


}
