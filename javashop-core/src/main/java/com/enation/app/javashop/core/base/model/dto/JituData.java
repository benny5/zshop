package com.enation.app.javashop.core.base.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class JituData {
    private String keyword;
    private List<JituTrack> details;
}

