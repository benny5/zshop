package com.enation.app.javashop.core.trade.cart.model.vo;

import lombok.Data;

/**
 * @author JFeng
 * @date 2020/2/18 23:52
 */
@Data
public class ShipWayVO {
    private Integer sellerId;
    // LOCAL GLOBAL
    private String shipWay;

    public ShipWayVO(Integer sellerId, String shipWay) {
        this.sellerId = sellerId;
        this.shipWay = shipWay;
    }

    public ShipWayVO() {
    }
}
