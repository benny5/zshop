package com.enation.app.javashop.core.aftersale.model.enums;

/**
 * @author 王志杨
 * @since 2020年9月21日 14:00:50
 * 异常上报来源
 */
public enum ExceptionSourceEnum {

    OFFLINE("线下沟通"),
    WEIXIN_APP("微信小程序"),
    SERVICE_LINE("客服热线"),
    MOBILE_APP("手机APP"),
    SORTING_OUT_STORAGE("分拣出库");

    private String description;

    ExceptionSourceEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String value(){
        return this.name();
    }
}
