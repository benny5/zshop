package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 配送汇总
 * @author JFeng
 * @date 2020/7/17 14:31
 */

@Data
public class ExportOrderDeliveryVO {

    @ExcelProperty("订单号")
    private String orderSn;

    @ExcelProperty("单号(配送系统用)")
    private String dispatchNo;

    @ExcelProperty("收货人")
    private String shipName;

    @ExcelProperty("收货人联系方式")
    private String shipMobile;

    @ExcelProperty("收货地址")
    private String shipAddr;

    @ExcelProperty("配送方式")
    private String shipWay;


}
