package com.enation.app.javashop.core.promotion.shetuan.model.vo;

import com.enation.app.javashop.core.trade.order.model.enums.*;
import com.enation.app.javashop.core.trade.order.model.vo.GoodsOperateAllowable;
import com.enation.app.javashop.core.trade.order.model.vo.OrderSkuVO;
import com.enation.app.javashop.core.trade.order.support.OrderSpecialStatus;
import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.util.JsonUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JFeng
 * @date 2020/6/3 14:59
 */

@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ShetuanOrderVO {
    /**
     * 主键ID
     */
    @Id(name = "order_id")
    @ApiModelProperty(hidden = true)
    private Integer orderId;
    /**
     * 订单编号
     */
    @Column(name = "sn")
    @ApiModelProperty(name = "sn", value = "订单编号", required = false)
    private String sn;
    /**
     * 店铺ID
     */
    @Column(name = "seller_id")
    @ApiModelProperty(name = "seller_id", value = "店铺ID", required = false)
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    @ApiModelProperty(name = "seller_name", value = "店铺名称", required = false)
    private String sellerName;
    /**
     * 店铺logo
     */
    @Column(name = "seller_logo")
    @ApiModelProperty(name = "seller_logo", value = "店铺logo", required = false)
    private String sellerLogo;
    /**
     * 会员ID
     */
    @Column(name = "member_id")
    @ApiModelProperty(name = "member_id", value = "会员ID", required = false)
    private Integer memberId;
    /**
     * 买家账号
     */
    @Column(name = "member_name")
    @ApiModelProperty(name = "member_name", value = "买家账号", required = false)
    private String memberName;
    /**
     * 订单状态
     */
    @Column(name = "order_status")
    @ApiModelProperty(name = "order_status", value = "订单状态", required = false)
    private String orderStatus;
    /**
     * 付款状态
     */
    @Column(name = "pay_status")
    @ApiModelProperty(name = "pay_status", value = "付款状态", required = false)
    private String payStatus;
    /**
     * 支付方式类型
     */
    @Column(name = "payment_type")
    @ApiModelProperty(name = "payment_type", value = "支付方式类型", required = false)
    private String paymentType;
    /**
     * 货运状态
     */
    @Column(name = "ship_status")
    @ApiModelProperty(name = "ship_status", value = "货运状态", required = false)
    private String shipStatus;
    /**
     * 配送方式ID
     */
    @Column(name = "shipping_id")
    @ApiModelProperty(name = "shipping_id", value = "配送方式ID", required = false)
    private Integer shippingId;
    /**
     * 评论是否完成
     */
    @Column(name = "comment_status")
    @ApiModelProperty(name = "comment_status", value = "评论是否完成", required = false)
    private String commentStatus;
    /**
     * 配送方式
     */
    @Column(name = "shipping_type")
    @ApiModelProperty(name = "shipping_type", value = "配送方式", required = false)
    private String shippingType;
    /**
     * 支付时间
     */
    @Column(name = "payment_time")
    @ApiModelProperty(name = "payment_time", value = "支付时间", required = false)
    private Long paymentTime;
    /**
     * 已支付金额
     */
    @Column(name = "pay_money")
    @ApiModelProperty(name = "pay_money", value = "已支付金额", required = false)
    private Double payMoney;
    /**
     * 收货人姓名
     */
    @Column(name = "ship_name")
    @ApiModelProperty(name = "ship_name", value = "收货人姓名", required = false)
    private String shipName;
    /**
     * 收货地址
     */
    @Column(name = "ship_addr")
    @ApiModelProperty(name = "ship_addr", value = "收货地址", required = false)
    private String shipAddr;
    /**
     * 收货人手机
     */
    @Column(name = "ship_mobile")
    @ApiModelProperty(name = "ship_mobile", value = "收货人手机", required = false)
    private String shipMobile;
    /**
     * 收货时间
     */
    @Column(name = "receive_time")
    @ApiModelProperty(name = "receive_time", value = "收货时间", required = false)
    private String receiveTime;
    /**
     * 配送地区-省份ID
     */
    @Column(name = "ship_province_id")
    @ApiModelProperty(name = "ship_province_id", value = "配送地区-省份ID", required = false)
    private Integer shipProvinceId;

    /**
     * 配送地区-城市ID
     */
    @Column(name = "ship_city_id")
    @ApiModelProperty(name = "ship_city_id", value = "配送地区-城市ID", required = false)
    private Integer shipCityId;

    /**
     * 配送地区-区(县)ID
     */
    @Column(name = "ship_county_id")
    @ApiModelProperty(name = "ship_county_id", value = "配送地区-区(县)ID", required = false)
    private Integer shipCountyId;

    /**
     * 配送街道id
     */
    @Column(name = "ship_town_id")
    @ApiModelProperty(name = "ship_town_id", value = "配送街道id", required = false)
    private Integer shipTownId;
    /**
     * 配送地区-省份
     */
    @Column(name = "ship_province")
    @ApiModelProperty(name = "ship_province", value = "配送地区-省份", required = false)
    private String shipProvince;
    /**
     * 配送地区-城市
     */
    @Column(name = "ship_city")
    @ApiModelProperty(name = "ship_city", value = "配送地区-城市", required = false)
    private String shipCity;
    /**
     * 配送地区-区(县)
     */
    @Column(name = "ship_county")
    @ApiModelProperty(name = "ship_county", value = "配送地区-区(县)", required = false)
    private String shipCounty;
    /**
     * 配送街道
     */
    @Column(name = "ship_town")
    @ApiModelProperty(name = "ship_town", value = "配送街道", required = false)
    private String shipTown;
    /**
     * 订单总额
     */
    @Column(name = "order_price")
    @ApiModelProperty(name = "order_price", value = "订单总额", required = false)
    private Double orderPrice;
    /**
     * 商品总额
     */
    @Column(name = "goods_price")
    @ApiModelProperty(name = "goods_price", value = "商品总额", required = false)
    private Double goodsPrice;
    /**
     * 配送费用
     */
    @Column(name = "shipping_price")
    @ApiModelProperty(name = "shipping_price", value = "配送费用", required = false)
    private Double shippingPrice;
    /**
     * 优惠金额
     */
    @Column(name = "discount_price")
    @ApiModelProperty(name = "discount_price", value = "优惠金额", required = false)
    private Double discountPrice;
    /**
     * 是否被删除
     */
    @Column(name = "disabled")
    @ApiModelProperty(name = "disabled", value = "是否被删除", required = false)
    private Integer disabled;
    /**
     * 订单商品总重量
     */
    @Column(name = "weight")
    @ApiModelProperty(name = "weight", value = "订单商品总重量", required = false)
    private Double weight;
    /**
     * 商品数量
     */
    @Column(name = "goods_num")
    @ApiModelProperty(name = "goods_num", value = "商品数量", required = false)
    private Integer goodsNum;
    /**
     * 订单备注
     */
    @Column(name = "remark")
    @ApiModelProperty(name = "remark", value = "订单备注", required = false)
    private String remark;
    /**
     * 订单取消原因
     */
    @Column(name = "cancel_reason")
    @ApiModelProperty(name = "cancel_reason", value = "订单取消原因", required = false)
    private String cancelReason;
    /**
     * 签收人
     */
    @Column(name = "the_sign")
    @ApiModelProperty(name = "the_sign", value = "签收人", required = false)
    private String theSign;

    /**
     * 转换 List<OrderSkuVO>
     */
    @Column(name = "items_json")
    @ApiModelProperty(name = "items_json", value = "货物列表json", required = false)
    private String itemsJson;

    @Column(name = "warehouse_id")
    @ApiModelProperty(name = "warehouse_id", value = "发货仓库ID", required = false)
    private Integer warehouseId;

    @Column(name = "need_pay_money")
    @ApiModelProperty(name = "need_pay_money", value = "应付金额", required = false)
    private Double needPayMoney;

    @Column(name = "ship_no")
    @ApiModelProperty(name = "ship_no", value = "发货单号", required = false)
    private String shipNo;

    /**
     * 收货地址ID
     */
    @Column(name = "address_id")
    @ApiModelProperty(name = "address_id", value = "收货地址ID", required = false)
    private Integer addressId;
    /**
     * 管理员备注
     */
    @Column(name = "admin_remark")
    @ApiModelProperty(name = "admin_remark", value = "管理员备注", required = false)
    private Integer adminRemark;
    /**
     * 物流公司ID
     */
    @Column(name = "logi_id")
    @ApiModelProperty(name = "logi_id", value = "物流公司ID", required = false)
    private Integer logiId;
    /**
     * 物流公司名称
     */
    @Column(name = "logi_name")
    @ApiModelProperty(name = "logi_name", value = "物流公司名称", required = false)
    private String logiName;

    /**
     * 完成时间
     */
    @Column(name = "complete_time")
    @ApiModelProperty(name = "complete_time", value = "完成时间", required = false)
    private Long completeTime;
    /**
     * 订单创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "订单创建时间", required = false)
    private Long createTime;

    /**
     * 签收时间
     */
    @Column(name = "signing_time")
    @ApiModelProperty(name = "signing_time", value = "签收时间", required = false)
    private Long signingTime;

    /**
     * 送货时间
     */
    @Column(name = "ship_time")
    @ApiModelProperty(name = "ship_time", value = "送货时间", required = false)
    private Long shipTime;

    /**
     * 支付方式返回的交易号
     */
    @Column(name = "pay_order_no")
    @ApiModelProperty(name = "pay_order_no", value = "支付方式返回的交易号", required = false)
    private String payOrderNo;

    /**
     * 售后状态
     */
    @Column(name = "service_status")
    @ApiModelProperty(name = "service_status", value = "售后状态", required = false)
    private String serviceStatus;

    /**
     * 结算状态
     */
    @Column(name = "bill_status")
    @ApiModelProperty(name = "bill_status", value = "结算状态", required = false)
    private Integer billStatus;

    /**
     * 结算单号
     */
    @Column(name = "bill_sn")
    @ApiModelProperty(name = "bill_sn", value = "结算单号", required = false)
    private String billSn;

    /**
     * 订单来源
     */
    @Column(name = "client_type")
    @ApiModelProperty(name = "client_type", value = "订单来源", required = false)
    private String clientType;

    @Column(name = "need_receipt")
    @ApiModelProperty(name = "need_receipt", value = "是否需要发票,0：否，1：是")
    private Integer needReceipt;

    @Column(name = "leader_id")
    @ApiModelProperty(name = "leader_id", value = "社团团长id")
    private Integer leaderId;

    /**
     * @see OrderTypeEnum
     * 因增加拼团业务新增订单类型字段 kingapex 2019/1/28 on v7.1.0
     */
    @ApiModelProperty(value = "订单类型")
    @Column(name = "order_type")
    private String orderType;


    /**
     * 订单的扩展数据
     * 为了增加订单的扩展性，个性化的业务可以将个性化数据（如拼团所差人数）存在此字段 kingapex 2019/1/28 on v7.1.0
     */
    @ApiModelProperty(value = "扩展数据",hidden = true)
    @Column(name = "order_data")
    private String orderData;

    /**
     * id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    @Column(name = "shetuan_id")
    private Integer shetuanId;

    @Column(name = "leader_commission")
    private Double leaderCommission;

    @Column(name = "leader_return_commission")
    private Double leaderReturnCommission;

    @Column(name = "commission_rate")
    private Double commissionRate;

    @Column(name = "pick_address")
    private String pickAddress;

    @Column(name = "buy_num")
    private Integer buyNum;

    @Column(name = "buyer_name")
    private String buyerName;

    @Column(name = "buyer_mobile")
    private String buyerMobile;

    @Column(name = "pick_time")
    private Long pickTime;

    @Column(name = "update_time")
    private Long updateTime;

    @Column(name = "st_order_status")
    private String  stOrderStatus;

    @Column(name = "leader_member_id")
    private Integer leaderMemberId;

    @Column(name = "commission_type")
    private String  commissionType;

    @Column(name = "buyer_member_id")
    private Integer buyerMemberId;

    @Column(name = "buyer_member_name")
    private String  buyerMemberName;

    private String orderStatusText;

    private String payStatusText;

    private String shipStatusText;

    private List<OrderSkuVO> skuList;

    private String leaderName;

    private String leaderSite;

    private String leaderMobile;

    private Integer totalNum;

    private String siteName;

    private Integer siteType;
    @Column(name = "real_name")
    private String realName;
    @Column(name = "mobile")
    private String mobile;

    @Column(name = "payment_method_name")
    private String paymentMethodName;

    private Integer printTimes;

    @Column(name = "sorting_seq")
    private Integer sortingSeq;

    @Column(name = "sorting_batch")
    private Integer sortingBatch;
    // -------------------------  自提点地址 (四级地址 + 详细地址)-------------
    @Column(name = "province")
    private String province;

    @Column(name = "city")
    private String city;

    @Column(name = "county")
    private String county;

    @Column(name = "town")
    private String town;

    @Column(name = "street")
    private String street;

    @Column(name = "address")
    private String address;

    @Column(name = "member_nick_name")
    private String memberNickName;

    public void resetStatusField(){
        //先从特殊的流程-状态显示 定义中读取，如果为空说明不是特殊的状态，直接显示为 状态对应的提示词
        this.orderStatusText = OrderSpecialStatus.getStatusText(this.orderType, this.paymentType, this.orderStatus);
        // 待评论
        String commentStatus = OrderSpecialStatus.getStatusText(this.orderType, this.commentStatus, this.orderStatus);
        this.orderStatusText = StringUtils.isNotEmpty(commentStatus) ?commentStatus:orderStatusText;
        // 售后中
        String serviceStatus = OrderSpecialStatus.getStatusText(this.orderType, this.orderStatus,null);
        this.orderStatusText = StringUtils.isNotEmpty(serviceStatus) ?serviceStatus:orderStatusText;
        // 常规状态
        if (StringUtil.isEmpty(orderStatusText)) {
            this.orderStatusText = OrderStatusEnum.valueOf( this.orderStatus).description();
        }

        this.payStatusText = PayStatusEnum.valueOf( this.payStatus).description();
        this.shipStatusText = ShipStatusEnum.valueOf(this.shipStatus).description();


        this.skuList = JsonUtil.jsonToList(this.itemsJson, OrderSkuVO.class);
        this.itemsJson=null;
        //遍历所有的商品
        this.totalNum=0;
        List<OrderSkuVO> errorSkuList = new ArrayList<>();
        for (OrderSkuVO skuVO : skuList) {
            if(this.serviceStatus.equals(ServiceStatusEnum.NOT_APPLY.name()) && skuVO.getServiceStatus().equals(ServiceStatusEnum.APPLY.name())){
                errorSkuList.add(skuVO);
                continue;
            }
            //设置商品的可操作状态
            skuVO.setGoodsOperateAllowableVO(new GoodsOperateAllowable(PaymentTypeEnum.valueOf(this.paymentType), OrderStatusEnum.valueOf(this.orderStatus),
                    ShipStatusEnum.valueOf(this.shipStatus), ServiceStatusEnum.valueOf(skuVO.getServiceStatus()),
                    PayStatusEnum.valueOf(this.payStatus)));
            totalNum+=skuVO.getNum();
        }
        skuList.removeAll(errorSkuList);

        // 判断四级时候为空  省市区镇/ 省市区街道
        if(StringUtil.notEmpty(province)){
             pickAddress = province;
            if(StringUtil.notEmpty(city)){
                pickAddress = pickAddress.concat(city);
                if(StringUtil.notEmpty(county)){
                    pickAddress = pickAddress.concat(county);

                    if(StringUtil.notEmpty(town)){
                        pickAddress = pickAddress.concat(town);
                    }
                    if(StringUtil.notEmpty(street) && StringUtil.isEmpty(town)){
                        pickAddress = pickAddress.concat(street);
                    }
                }
            }
        }
        // 详细地址
        if(StringUtil.notEmpty(address)){
            pickAddress = pickAddress.concat(address);
        }


    }

}
