package com.enation.app.javashop.core.app.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * 通知内容信息
 */
@Data
public class Notification {
    /**
     * Android信息
     */
    private JSONObject android;

    /**
     * ios信息
     */
    private JSONObject ios;

    /**
     * 平台属性 消息内容
     */
    private String alert;
}
