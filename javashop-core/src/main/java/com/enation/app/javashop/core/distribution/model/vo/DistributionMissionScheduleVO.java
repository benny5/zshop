package com.enation.app.javashop.core.distribution.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: zhou
 * @Date: 2021/1/27
 * @Description: 团长任务进度
 */
@Data
public class DistributionMissionScheduleVO {

    /**
     * 团长进度ID
     */
    @ApiModelProperty(name = "id", value = "团长进度ID")
    private Integer id;
    /**
     * 团长任务名称
     */
    @ApiModelProperty(name = "mission_name", value = "团长任务名称")
    private String missionName;
    /**
     * 任务明细类型（0：每日任务，1：每月任务）
     */
    @ApiModelProperty(name = "mission_type", value = "任务明细类型（0：每日任务，1：每月任务）")
    private Integer missionType;
    /**
     * 任务明细指标（0：下单人数，1：成交金额）
     */
    @ApiModelProperty(name = "mission_target", value = "任务明细指标（0：下单人数，1：成交金额）")
    private Integer missionTarget;
    /**
     * 任务期间
     */
    @ApiModelProperty(name = "mission_time", value = "任务期间")
    private String missionTime;
    /**
     * 任务详情id
     */
    @ApiModelProperty(name = "mission_detail_id", value = "任务详情id")
    private Integer missionDetailId;
    /**
     * 店铺ID
     */
    @ApiModelProperty(name = "seller_id", value = "店铺ID")
    private Integer sellerId;

    /**
     * 店铺名称
     */
    @ApiModelProperty(name ="seller_name", value ="店铺名称")

    private String sellerName;
    /**
     * 会员id
     */
    @ApiModelProperty(name = "member_id", value = "会员id")
    private Integer memberId;
    /**
     * 团长名称
     */
    @ApiModelProperty(name = "member_name", value = "团长名称")
    private String memberName;
    /**
     * 目标值
     */
    @ApiModelProperty(name = "target_value", value = "目标值")
    private BigDecimal targetValue;
    /**
     * 完成值
     */
    @ApiModelProperty(name = "complete_value", value = "完成值")
    private BigDecimal completeValue;
    /**
     * 累计奖励
     */
    @ApiModelProperty(name = "total_reward", value = "累计奖励")
    private BigDecimal totalReward;
    /**
     * 阶段1状态
     */
    @ApiModelProperty(name = "level1_status", value = "阶段1状态")
    private Integer level1Status;
    /**
     * 阶段2状态
     */
    @ApiModelProperty(name = "level2_status", value = "阶段2状态")
    private Integer level2Status;
    /**
     * 阶段3状态
     */
    @ApiModelProperty(name = "level3_status", value = "阶段3状态")
    private Integer level3Status;
    /**
     * 阶段4状态
     */
    @ApiModelProperty(name = "level4_status", value = "阶段4状态")
    private Integer level4Status;
    /**
     * 创建人
     */
    @ApiModelProperty(name = "create_name", value = "创建人")
    private String createName;
    /**
     * 修改时间
     */
    @ApiModelProperty(name = "update_time", value = "修改时间")
    private Long updateTime;
    /**
     * 是否删除
     */
    @ApiModelProperty(name = "is_delete", value = "是否删除")
    private Integer isDelete;

    @ApiModelProperty(name = "leader_mobile", value = "团长电话")
    private String leaderMobile;
}
