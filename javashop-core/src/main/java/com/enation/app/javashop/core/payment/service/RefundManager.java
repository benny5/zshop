package com.enation.app.javashop.core.payment.service;

import com.enation.app.javashop.core.aftersale.model.dos.RefundDO;
import com.enation.app.javashop.core.payment.model.dos.PaymentBillDO;
import com.enation.app.javashop.core.trade.sdk.model.OrderDetailDTO;

import java.util.List;
import java.util.Map;

/**
 * 退款接口
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-04-15
 */

public interface RefundManager {

    /**
     * 原路退回
     *
     * @param returnTradeNo 第三方订单号
     * @param refundSn      退款编号
     * @param refundPrice   退款金额
     * @return resultMap
     */
    Map originRefund(String returnTradeNo, String refundSn, Double refundPrice);


    /**
     * 查询退款状态
     *
     * @param returnTradeNo 第三方订单号
     * @param refundSn      退款编号
     * @return RefundStatus
     */
    String queryRefundStatus(String returnTradeNo, String refundSn);

    /*
     * 创建退款单明细
     */
    void createRefundItem(RefundDO refundDO, PaymentBillDO paymentBill);

}
