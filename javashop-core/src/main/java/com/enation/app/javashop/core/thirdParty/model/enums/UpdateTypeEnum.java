package com.enation.app.javashop.core.thirdParty.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/10/9
 * @Description:
 */
public enum UpdateTypeEnum {
    QUANTITY("更新库存"),

    GOODS("更新商品");

    private String description;

    UpdateTypeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }
}
