package com.enation.app.javashop.core.trade.order.model.enums;

/**
 * 订单状态
 *
 * @author Snow
 * @version 1.0
 * @since v7.0.0
 * 2017年3月31日下午2:44:54
 */
public enum StOrderStatusEnum {

    /**
     * 已付款
     */
    PAID_OFF("已付款"),

    /**
     * 已发货
     */
    SHIPPED("已发货"),

    /**
     * 已生效
     */
    CONFIRM("已生效"),

    /**
     * 已结算
     */
    SETTLED("已结算"),

    /**
     * 已失效
     */
    CANCELLED("已失效");


    private String description;

    StOrderStatusEnum(String description) {
        this.description = description;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return this.description;
    }

    public String value() {
        return this.name();
    }


}
