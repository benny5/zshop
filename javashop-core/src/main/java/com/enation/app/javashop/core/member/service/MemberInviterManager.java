package com.enation.app.javashop.core.member.service;

import com.enation.app.javashop.core.member.model.dos.MemberInviter;

import java.util.List;

/**
 * 会员邀请人业务层
 */
public interface MemberInviterManager {

    /**
     * 邀请人员
     */
    String PREFIX = "{MEMBER_INVITER}_";
    /**
     * 邀请渠道
     */
    String CHANNEL = "{MEMBER_CHANNEL}_";

    void addMemberInviter(MemberInviter memberInviter);

    MemberInviter getByMemberId(Integer memberId);

    List<MemberInviter> getByInviterMemberId(Integer inviterMemberId);

}