package com.enation.app.javashop.core.thirdParty.model.enums;

/**
 * @Author: zhou
 * @Date: 2020/10/13
 * @Description:
 */
public enum ResultCodeEnum {
    S200(200, "操作成功"),
    E614(614, "请求参数异常"),
    E615(615, "服务器异常");

    private int code;
    private String description;

    ResultCodeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String value(){
        return this.name();
    }
}
