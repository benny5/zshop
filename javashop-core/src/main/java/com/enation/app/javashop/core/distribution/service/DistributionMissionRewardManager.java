package com.enation.app.javashop.core.distribution.service;

import com.enation.app.javashop.core.distribution.model.dos.DistributionMissionRewardDO;
import com.enation.app.javashop.core.distribution.model.dto.QueryDistributionMissionRewardDTO;
import com.enation.app.javashop.framework.database.Page;

/**
 * @Author: zhou
 * @Date: 2021/1/26
 * @Description: 团长任务奖励明细
 */
public interface DistributionMissionRewardManager {

    void createDisMisReward(DistributionMissionRewardDO distributionMissionRewardDO);

    Page<DistributionMissionRewardDO> queryDisMisRewList(QueryDistributionMissionRewardDTO queryRewardDTO);

}
