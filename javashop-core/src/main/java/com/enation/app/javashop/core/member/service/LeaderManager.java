package com.enation.app.javashop.core.member.service;

import com.enation.app.javashop.core.member.model.dos.LeaderDO;
import com.enation.app.javashop.core.member.model.dos.MemberAddress;
import com.enation.app.javashop.core.member.model.dto.NearLeaderDTO;
import com.enation.app.javashop.core.member.model.vo.LeaderQueryVO;
import com.enation.app.javashop.core.member.model.vo.LeaderVO;
import com.enation.app.javashop.core.trade.order.model.vo.CheckoutParamVO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

public interface LeaderManager {

    LeaderDO addLeader(LeaderDO leaderDO);

    boolean edit(LeaderDO leaderDO, Integer leaderId);

    Page list(LeaderQueryVO vo);

    Page<NearLeaderDTO>  nearLeaderList(int pageNo , int pageSize, Double lng, Double lat);

    boolean delete(Integer id);

    LeaderDO getById(Integer id);

    LeaderDO getByMemberId(Integer memberId);

    NearLeaderDTO getByIdAndLatLng(Integer id ,Double lng,Double lat);

    LeaderDO  getLeaderByShopIdAndBuyerId(Integer shopId,Integer buyerId);

    NearLeaderDTO getNearestLeader(Double lng, Double lat);

    LeaderDO getBandLeader(Integer uid);

    void updateLeaderStatus(String leaderId, Integer status);

    // 批量查询
    List<LeaderDO> getLeaderDO(String leaderIds);

    LeaderDO getLeaderDOByLeaderId(Integer leaderId);

    void updatePriority(Integer leaderId, Integer shipPriority);

    List<LeaderVO> queryLeaderBySiteName(String siteName);

    LeaderDO getLeaderDO(CheckoutParamVO param, MemberAddress memberAddress, String activeShipWay);
}
