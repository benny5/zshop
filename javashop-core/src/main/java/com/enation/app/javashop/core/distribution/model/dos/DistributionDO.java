package com.enation.app.javashop.core.distribution.model.dos;


import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分销商
 */
@Data
@Table(name = "es_distribution")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class DistributionDO {

    /**
     * 主键id
     */
    @Id(name = "id")
    @ApiModelProperty(hidden = true)
    private Integer id;

    /**
     * 关联会员的id
     */
    @Column(name = "member_id")
    @ApiModelProperty(value = "会员id", required = true)
    private Integer memberId;


    /**
     * 会员名称
     */
    @Column(name = "member_name")
    @ApiModelProperty(value = "会员名称", required = true)
    private String memberName;


    /**
     * 关系路径
     */
    @Column()
    @ApiModelProperty(value = "关系路径", required = true)
    private String path;

    /**
     * 2级分销商id（上上级）
     */
    @Column(name = "member_id_lv2")
    @ApiModelProperty(name = "member_id_lv2", value = "2级分销商id（上上级）", required = true)
    private Integer memberIdLv2;

    /**
     * 1级分销商id（上级）
     */
    @Column(name = "member_id_lv1")
    @ApiModelProperty(name = "member_id_lv1", value = "1级分销商id（上级）", required = true)
    private Integer memberIdLv1;

    /**
     * 下线人数
     */
    @Column(name = "downline")
    @ApiModelProperty(value = "下线人数", required = true)
    private Integer downline = 0;

    /**
     * 提成相关订单数
     */
    @Column(name = "order_num")
    @ApiModelProperty(name = "order_num", value = "提成相关订单数", required = true)
    private Integer orderNum = 0;

    /**
     * 返利总额
     */
    @Column(name = "rebate_total")
    @ApiModelProperty(name = "rebate_total", value = "返利总额", required = true)
    private Double rebateTotal = 0D;

    /**
     * 营业额总额
     */
    @Column(name = "turnover_price")
    @ApiModelProperty(name = "turnover_price", value = "营业额总额", required = true)
    private Double turnoverPrice = 0D;

    /**
     * 可提现金额
     */
    @Column(name = "can_rebate")
    @ApiModelProperty(name = "can_rebate", value = "可提现金额", required = true)
    private Double canRebate = 0D;

    /**
     * 返利金额冻结
     */
    @Column(name = "commission_frozen")
    @ApiModelProperty(name = "commission_frozen", value = "冻结金额", required = true)
    private Double commissionFrozen = 0D;


    /**
     * 提现冻结
     */
    @Column(name = "withdraw_frozen_price")
    @ApiModelProperty(name = "withdraw_frozen_price", value = "冻结金额", required = true)
    private Double withdrawFrozenPrice = 0D;

    /**
     * 使用模板id
     */
    @Column(name = "current_tpl_id")
    @ApiModelProperty(name = "current_tpl_id", value = "使用模板id", required = true)
    private Integer currentTplId;


    /**
     * 使用模板id
     */
    @Column(name = "current_tpl_name")
    @ApiModelProperty(name = "current_tpl_name", value = "使用模板名称", required = true)
    private String currentTplName;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty(name = "create_time", value = "创建时间", required = false)
    private Long createTime;

    @Column(name = "audit_status")
    @ApiModelProperty(name = "audit_status", value = "状态", required = false)
    private Integer auditStatus;

    @Column(name = "status_name")
    @ApiModelProperty(name = "status_name", value = "状态名称", required = false)
    private String statusName;

    @Column(name = "audit_time")
    @ApiModelProperty(name = "audit_time", value = "审核时间", required = false)
    private Long auditTime;

    @Column(name = "apply_reason")
    @ApiModelProperty(name = "apply_reason", value = "申请原因", required = false)
    private String applyReason;

    @Column(name = "audit_remark")
    @ApiModelProperty(name = "audit_remark", value = "审核备注", required = false)
    private String auditRemark;

    @Column(name = "team_name")
    @ApiModelProperty(name = "team_name", value = "别名 如 第8团", required = false)
    private String teamName;

    @Column(name = "province_id")
    @ApiModelProperty(name = "province_id", value = "省Id", required = false)
    private Integer provinceId;

    @Column(name = "province")
    @ApiModelProperty(name = "province", value = "省", required = false)
    private String province;

    @Column(name = "city_id")
    @ApiModelProperty(name = "city_id", value = "市Id", required = false)
    private Integer cityId;

    @Column(name = "city")
    @ApiModelProperty(name = "city", value = "市", required = false)
    private String city;

    @Column(name = "county_id")
    @ApiModelProperty(name = "county_id", value = "区/县Id", required = false)
    private Integer countyId;

    @Column(name = "county")
    @ApiModelProperty(name = "county", value = "区/县", required = false)
    private String county;

    @Column(name = "address")
    @ApiModelProperty(name = "address", value = "详细地址", required = false)
    private String address;

    @Column(name = "distributor_grade")
    @ApiModelProperty(name = "distributor_grade", value = "分销等级", required = false)
    private Integer distributorGrade;

    @Column(name = "distributor_title")
    @ApiModelProperty(name = "distributor_title", value = "分销头衔", required = false)
    private String distributorTitle;

    @Column(name = "status")
    @ApiModelProperty(name = "status", value = "启用/禁用", required = false)
    private Integer status;

    @Column(name = "invite_code")
    @ApiModelProperty(name = "invite_code", value = "分销邀请码", required = false)
    private String inviteCode;

    @Column(name = "lv1_create_time")
    @ApiModelProperty(name = "lv1_create_time", value = "1级关系创建时间", required = false)
    private Long lv1CreateTime;

    @Column(name = "lv1_expire_time")
    @ApiModelProperty(name = "lv1_expire_time", value = "1级关系失效时间", required = false)
    private Long lv1ExpireTime;

    @Column(name = "lv2_expire_time")
    @ApiModelProperty(name = "lv2_expire_time", value = "2级关系失效时间", required = false)
    private Long lv2ExpireTime;

    @Column(name = "last_order_time")
    @ApiModelProperty(name = "last_order_time", value = "最新下单时间", required = false)
    private Long lastOrderTime;

    @Column(name = "lv1_invite_code")
    @ApiModelProperty(name = "lv1_invite_code", value = "上级邀请码", required = false)
    private String lv1InviteCode;

    @Column(name = "operate_areas")
    @ApiModelProperty(name = "operate_areas", value = "运行区域", required = false)
    private String operateAreas;

    @Column(name = "wechat_groups_num")
    @ApiModelProperty(name = "wechat_groups_num", value = "微信群人数", required = false)
    private Integer wechatGroupsNum;

    @Column(name = "settle_mode")
    @ApiModelProperty(name = "settle_mode", value = "结算模式", required = false)
    private Integer settleMode;

    @Column(name = "business_type")
    @ApiModelProperty(name = "business_type", value = "业务类型", required = false)
    private Integer businessType;

    @Column(name = "visit_channel")
    @ApiModelProperty(name = "visit_channel", value = "访问渠道", required = false)
    private Integer visitChannel;

}