package com.enation.app.javashop.core.thirdParty.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.enation.app.javashop.core.base.CachePrefix;
import com.enation.app.javashop.core.base.message.CategoryChangeMsg;
import com.enation.app.javashop.core.base.model.dto.FileDTO;
import com.enation.app.javashop.core.base.model.vo.FileVO;
import com.enation.app.javashop.core.base.plugin.upload.Uploader;
import com.enation.app.javashop.core.base.rabbitmq.AmqpExchange;
import com.enation.app.javashop.core.base.service.FileManager;
import com.enation.app.javashop.core.base.service.SettingManager;
import com.enation.app.javashop.core.distribution.model.dos.DistributionGoods;
import com.enation.app.javashop.core.distribution.service.DistributionGoodsManager;
import com.enation.app.javashop.core.goods.GoodsErrorCode;
import com.enation.app.javashop.core.goods.model.dos.*;
import com.enation.app.javashop.core.goods.model.enums.GoodsType;
import com.enation.app.javashop.core.goods.model.vo.GoodsSkuVO;
import com.enation.app.javashop.core.goods.model.vo.SpecValueVO;
import com.enation.app.javashop.core.goods.service.*;
import com.enation.app.javashop.core.goods.service.impl.CategoryManagerImpl;
import com.enation.app.javashop.core.goodssearch.util.PinYinUtil;
import com.enation.app.javashop.core.system.service.UploadFactory;
import com.enation.app.javashop.core.thirdParty.model.dto.Category;
import com.enation.app.javashop.core.thirdParty.model.dto.GoodsSpecs;
import com.enation.app.javashop.core.thirdParty.model.dto.Result;
import com.enation.app.javashop.core.thirdParty.model.dto.SkuAmt;
import com.enation.app.javashop.core.thirdParty.model.dto.yisupai.*;
import com.enation.app.javashop.core.thirdParty.service.YiSuPaiManager;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderItemsDO;
import com.enation.app.javashop.framework.JavashopConfig;
import com.enation.app.javashop.framework.cache.Cache;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.BeanUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import com.enation.app.javashop.framework.util.StringUtil;
import net.sf.json.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author: zhou
 * @Date: 2021/5/8
 * @Description:
 */
@Service
public class YiSuPaiManagerImpl implements YiSuPaiManager {
    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private FileManager fileManager;

    private final Log logger = LogFactory.getLog(getClass());

    public static final String APP_KEY = "6UD9Lwnq";
    public static final String APP_SECRET = "465b445b467a4b9dbf61840412268b87";
    public static final String THIRD_PARTY_USER_ID = "test001";
    public static final String BASE_URL = "http://scm.yorisun.com:8080/scm/api/app/";

    public static final int SELLER_ID=188;
    public static final String SELLER_NAME="测试店铺";

    @Autowired
    private GoodsGalleryManager goodsGalleryManager;
    @Autowired
    private GoodsSkuManager goodsSkuManager;
    @Autowired
    private CategoryManager categoryManager;

    @Autowired
    private SpecificationManager specificationManager;
    @Autowired
    private SpecValuesManager specValuesManager;
    @Autowired
    private SettingManager settingManager;
    @Autowired
    private DistributionGoodsManager distributionGoodsManager;
    @Autowired
    private GoodsQuantityManager goodsQuantityManager;
    @Autowired
    private JavashopConfig javashopConfig;
    @Autowired
    protected Cache cache;

    @Autowired
    protected AmqpTemplate amqpTemplate;

    @Autowired
    @Qualifier("goodsDaoSupport")
    private DaoSupport daoSupport;

    // 下单，不支付
    @Override
    public List<String> yiSuPaiCreateOrder(Integer orderId) {

        if(orderId==null){
            return null;
        }

        OrderDO order=this.daoSupport.queryForObject(OrderDO.class,orderId);

        if(order==null){
            return null;
        }

        YiSuPaiDeliveryAddress yspAddress = new YiSuPaiDeliveryAddress(order);

        String sql = "select g.up_goods_id item_id,sku.up_sku_id sku_id,oi.num quantity from es_order_items oi \n" +
                "LEFT JOIN es_goods g on oi.goods_id=g.goods_id LEFT JOIN es_goods_sku sku ON oi.promotion_id=sku.sku_id\n" +
                "WHERE oi.order_sn='?'";
        List<YiSuPaiOrderItems> yspOrderItemList = this.daoSupport.queryForList(sql, YiSuPaiOrderItems.class, order.getSn());

        if (CollectionUtils.isEmpty(yspOrderItemList)||createAnonyAccount(order.getMemberId())!=null) {
            return null;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        map.put("deliveryAddress", yspAddress);
        map.put("itemList", yspOrderItemList);
        map.put("postage", order.getShippingPrice());

        logger.info("商品下单不支付请求的数据：" + map.toString());
        String resultString = actualPostRequest(map, getAnonyAccount(order.getMemberId()), "business/createOrder");
        logger.info("商品下单不支付返回的数据：" + resultString);

        Result<List<String>> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<List<String>>>() {
        });

        if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
            return null;
        }

        return yiSuPaiResult.getData();

    }

    @Override
    public List<String> yiSuPaiRendorOrder(Integer orderId) {

        if(orderId==null){
            return null;
        }

        OrderDO order=this.daoSupport.queryForObject(OrderDO.class,orderId);

        if(order==null){
            return null;
        }

        YiSuPaiDeliveryAddress yspAddress = new YiSuPaiDeliveryAddress(order);

        String sql = "select g.up_goods_id item_id,sku.up_sku_id sku_id,oi.num quantity from es_order_items oi \n" +
                "LEFT JOIN es_goods g on oi.goods_id=g.goods_id LEFT JOIN es_goods_sku sku ON oi.promotion_id=sku.sku_id\n" +
                "WHERE oi.order_sn='?'";
        List<YiSuPaiOrderItems> yspOrderItemList = this.daoSupport.queryForList(sql, YiSuPaiOrderItems.class, order.getSn());

        if (CollectionUtils.isEmpty(yspOrderItemList)||createAnonyAccount(order.getMemberId())!=null) {
            return null;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        map.put("deliveryAddress", yspAddress);
        map.put("itemList", yspOrderItemList);

        logger.info("渲染订单请求的数据：" + map.toString());
        String resultString = actualPostRequest(map, getAnonyAccount(order.getMemberId()), "business/rendorOrder");
        logger.info("渲染订单返回的数据：" + resultString);

        Result<List<String>> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<List<String>>>() {
        });

        if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
            return null;
        }

        return yiSuPaiResult.getData();

    }

    @Override
    public Map<String,Boolean> yiSuPaiEnableOrder(List<String> orderSnList,Integer memberId){
        Map<String,Boolean> resultMap=new HashMap<>();

        if(CollectionUtils.isEmpty(orderSnList)){
            return resultMap;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        for(String orderSn:orderSnList){
            map.put("orderNo", orderSn);

            logger.info("订单支付请求的数据：" + map.toString());
            String resultString = actualPostRequest(map, createAnonyAccount(memberId), "business/enableOrder");
            logger.info("订单支付返回的数据：" + resultString);

            Result<YiSuPaiGoodsPageDTO> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<YiSuPaiGoodsPageDTO>>() {
            });

            if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
                resultMap.put(orderSn,false);
            }else {
                resultMap.put(orderSn,true);
            }

        }
        return resultMap;

    }

    @Override
    public YiSuPaiOrderDetailDTO yiSuPaiOrderDetail(String orderSn, Integer memberId) {
        YiSuPaiOrderDetailDTO detail = new YiSuPaiOrderDetailDTO();
        if (StringUtil.isEmpty(orderSn)) {
            return detail;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        map.put("orderNo", orderSn);

        logger.info("获取订单详情请求的数据：" + map.toString());
        String resultString = actualPostRequest(map, createAnonyAccount(memberId), "order/searchOrderDetail");
        logger.info("获取订单详情返回的数据：" + resultString);

        Result<YiSuPaiOrderDetailDTO> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<YiSuPaiOrderDetailDTO>>() {
        });

        if (yiSuPaiResult.getCode() != 200 || yiSuPaiResult.getData() == null) {
            return detail;
        } else {
            return yiSuPaiResult.getData();
        }

    }

    @Override
    public List<YiSuPaiOrderDetailDTO> yiSuPaiOrderDetailList(YiSuPaiQueryOrder queryOrder, Integer memberId) {
        List<YiSuPaiOrderDetailDTO> detailList = new ArrayList<>();
        if (queryOrder==null) {
            return detailList;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        if (StringUtil.isEmpty(queryOrder.getOrderNo())) {
            map.put("orderNo", queryOrder.getOrderNo());
        }
        if (StringUtil.isEmpty(queryOrder.getOrderColumn())) {
            map.put("orderColumn", queryOrder.getOrderColumn());
        }
        if (StringUtil.isEmpty(queryOrder.getOrderType())) {
            map.put("orderType", queryOrder.getOrderType());
        }
        if (queryOrder.getStatus() != null) {
            map.put("status", queryOrder.getStatus());
        }
        Integer currentPage = queryOrder.getCurrentPage() == null ? 1 : queryOrder.getCurrentPage();
        Integer pageSize = queryOrder.getPageSize() == null ? 10 : queryOrder.getPageSize();
        map.put("currentPage", currentPage);
        map.put("pageSize", pageSize);

        logger.info("获取订单列表请求的数据：" + map.toString());
        String resultString = actualPostRequest(map, createAnonyAccount(memberId), "order/searchOrderDetail");
        logger.info("获取订单列表返回的数据：" + resultString);

        Result<List<YiSuPaiOrderDetailDTO>> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<List<YiSuPaiOrderDetailDTO>>>() {
        });

        if (yiSuPaiResult.getCode() != 200 || yiSuPaiResult.getData() == null) {
            return detailList;
        } else {
            return yiSuPaiResult.getData();
        }

    }

    @Override
    public YiSuPaiLogisticsDTO yiSuPaiQueryLogistics(String orderSn, Integer memberId) {
        YiSuPaiLogisticsDTO logisticsDTO = new YiSuPaiLogisticsDTO();
        if (StringUtil.isEmpty(orderSn)) {
            return logisticsDTO;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        map.put("orderNo", orderSn);

        logger.info("获取订单物流信息请求的数据：" + map.toString());
        String resultString = actualPostRequest(map, createAnonyAccount(memberId), "order/queryLogistics");
        logger.info("获取订单物流信息返回的数据：" + resultString);

        Result<YiSuPaiLogisticsDTO> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<YiSuPaiLogisticsDTO>>() {
        });

        if (yiSuPaiResult.getCode() != 200 || yiSuPaiResult.getData() == null) {
            return logisticsDTO;
        } else {
            return yiSuPaiResult.getData();
        }

    }

    @Override
    public Boolean yiSuPaiConfirmDisburse(String orderSn, Integer memberId) {
        if (StringUtil.isEmpty(orderSn)) {
            return false;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        map.put("orderNo", orderSn);

        logger.info("确认收货请求的数据：" + map.toString());
        String resultString = actualPostRequest(map, createAnonyAccount(memberId), "order/confirmDisburse");
        logger.info("确认收货返回的数据：" + resultString);

        Result yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result>() {
        });

        if (yiSuPaiResult.getCode() != 200 ) {
            return false;
        } else {
            return true;
        }

    }

    @Override
    public Map<String,Boolean> yiSuPaiCancelOrder(List<String> orderSnList,Integer memberId){
        Map<String,Boolean> resultMap=new HashMap<>();

        if(CollectionUtils.isEmpty(orderSnList)){
            return resultMap;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        for(String orderSn:orderSnList){
            map.put("orderNo", orderSn);

            logger.info("取消订单请求的数据：" + map.toString());
            String resultString = actualPostRequest(map, createAnonyAccount(memberId), "business/cancelOrder");
            logger.info("取消订单返回的数据：" + resultString);

            Result<YiSuPaiGoodsPageDTO> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<YiSuPaiGoodsPageDTO>>() {
            });

            if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
                resultMap.put(orderSn,false);
            }else {
                resultMap.put(orderSn,true);
            }

        }
        return resultMap;

    }

    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void addYiSuPaiGoods() {


        for(int page =1;;page++){

            long start = System.currentTimeMillis();
            List<YiSuPaiGoodsDTO> yiSuPaiGoodsDTOList = this.getGoodsList(page,50);
            if (CollectionUtils.isEmpty(yiSuPaiGoodsDTOList)|| yiSuPaiGoodsDTOList.size()<1) {
                return;
            }
            System.out.println(page+">>>>"+yiSuPaiGoodsDTOList.size());

            //获取易速派的分类列表
            Map<String, YiSuPaiCategoryDTO> yspCategoryMap = this.getCategoryList();

            for (YiSuPaiGoodsDTO yspGoods : yiSuPaiGoodsDTOList) {
                GoodsDO goodsDO = this.initGoods(yspGoods);

                YiSuPaiCategoryDTO yspCategory = yspCategoryMap.get(yspGoods.getCategoryId());
                CategoryDO parenteCategory = new CategoryDO(null, 0);
                if(yspCategory!=null){

                    if (!StringUtils.isEmpty(yspCategory.getParentId())) {
                        parenteCategory = this.addCategory(new CategoryDO(yspCategoryMap.get(yspCategory.getParentId()).getName(), 0));
                    }
                    CategoryDO categoryDO = this.addCategory(new CategoryDO(yspCategory.getName(), parenteCategory.getCategoryId()));

                    goodsDO.setCategoryId(categoryDO.getCategoryId());
                }


                String sql = "select * from es_goods where up_goods_id = ?";
                GoodsDO goodsDO1 = this.daoSupport.queryForObject(sql,GoodsDO.class,yspGoods.getItemId());

                if(goodsDO1!=null){
                    //如果没有同步过此商品则新增
                    this.daoSupport.update(goodsDO,goodsDO1.getGoodsId());
                }else{
                    this.daoSupport.insert(goodsDO);
                }

                // 获取添加商品的商品ID
                Integer goodsId = this.daoSupport.getLastId("es_goods");
                goodsDO.setGoodsId(goodsId);

                //设置分销返利
                addDistributionGoods(goodsId);

                List<GoodsSkuVO> goodsSkuVOList = this.initGoodsSku(goodsDO, yspGoods.getSkuList());
                // 有没有规格都需要添加商品sku信息
                this.goodsSkuManager.add(goodsSkuVOList, goodsDO);
                // List<GoodsGalleryDO> goodsGalleryList
                // // 添加相册
                // this.goodsGalleryManager.add(yspGoods.getItemImages(), goodsId);
                long end = System.currentTimeMillis();
                logger.debug("新增商品" + goodsId + "耗时=============================================================================" + (end - start));

            }
        }
    }
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public CategoryDO addCategory(CategoryDO category) {

        CategoryDO parent = null;
        //不能添加重复的分类名称
        String sqlQuery = "select * from es_category where name = ? ";
        CategoryDO oldCategory = this.daoSupport.queryForObject(sqlQuery,CategoryDO.class, category.getName());
        if (oldCategory!=null) {
            return oldCategory;
        }

        // 非顶级分类
        if (category.getParentId() != null && category.getParentId() != 0) {
            parent = this.categoryManager.getModel(category.getParentId());
            if (parent == null) {
                throw new ServiceException(GoodsErrorCode.E300.code(), "父分类不存在");
            }
            // 替换catPath 根据catPath规则来匹配级别
            String catPath = parent.getCategoryPath().replace("|", ",");
            String[] str = catPath.split(",");
            // 如果当前的catPath length 大于4 证明当前分类级别大于五级 提示
            if (str.length >= 4) {
                throw new ServiceException(GoodsErrorCode.E300.code(), "最多为三级分类,添加失败");
            }
        }

        this.daoSupport.insert(category);
        int categoryId = this.daoSupport.getLastId("es_category");
        category.setCategoryId(categoryId);

        String sql = "";
        // 判断是否是顶级类似别，如果parentid为空或为0则为顶级类似别
        // 注意末尾都要加|，以防止查询子孙时出错
        // 不是顶级类别，有父
        if (parent != null) {
            category.setCategoryPath(parent.getCategoryPath() + categoryId + "|");
        } else {// 是顶级类别
            category.setCategoryPath("0|" + categoryId + "|");
        }

        sql = "update es_category set  category_path=? where  category_id=?";
        this.daoSupport.execute(sql, category.getCategoryPath(), categoryId);


        cache.remove(CachePrefix.GOODS_CAT.getPrefix() + "ALL");

        CategoryChangeMsg categoryChangeMsg = new CategoryChangeMsg(categoryId, CategoryChangeMsg.ADD_OPERATION);
        this.amqpTemplate.convertAndSend(AmqpExchange.GOODS_CATEGORY_CHANGE, AmqpExchange.GOODS_CATEGORY_CHANGE + "_ROUTING", categoryChangeMsg);

        return category;
    }



    @Override
    public List<YiSuPaiGoodsDTO> getGoodsList(int page,int pageSize) {


        SortedMap<String, Object> mapGood = new TreeMap<>();
        mapGood.put("currentPage", page);
        mapGood.put("pageSize", pageSize);

        String resultString = actualPostRequest(mapGood, THIRD_PARTY_USER_ID, "item/searchItemList");
        System.out.println(resultString);
        logger.info("获取到商品列表：" + resultString);

        Result<YiSuPaiGoodsPageDTO> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<YiSuPaiGoodsPageDTO>>() {
        });

        int totalPage = yiSuPaiResult.getData().getTotalRecords() / pageSize ;
        if(yiSuPaiResult.getData().getTotalRecords() % pageSize !=0){
            totalPage++;
        }
        if(page>totalPage){
            return null;
        }

        if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
            return null;
        }

        List<YiSuPaiGoodsDTO> goodsDTOList = yiSuPaiResult.getData().getList();

        if (CollectionUtils.isEmpty(goodsDTOList)) {
            return null;
        }

        List<YiSuPaiGoodsDTO> goodsDetailList = new ArrayList<>();
        for (YiSuPaiGoodsDTO yiSuPaiGoodsDTO : goodsDTOList) {
            yiSuPaiGoodsDTO = this.getGoodsDetail(yiSuPaiGoodsDTO.getItemId());
            if (yiSuPaiGoodsDTO != null) {
                goodsDetailList.add(yiSuPaiGoodsDTO);
            }
        }

        return goodsDetailList;
    }

    @Override
    public YiSuPaiGoodsDTO getGoodsDetail(String itemId) {
        SortedMap<String, Object> map = new TreeMap<>();
        map.put("itemId", itemId);

        String resultString = actualPostRequest(map, THIRD_PARTY_USER_ID, "item/searchItemDetail");
        System.out.println(resultString);
        logger.info("获取到商品详情：" + resultString);

        Result<YiSuPaiGoodsDTO> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<YiSuPaiGoodsDTO>>() {});

        if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
            return null;
        }

        //TODO 将外部平台同步的过来商品对应的商品图片、sku图片上传至平台的oss，解决前端不能显示指定比例大小图的问题
        this.initPic(yiSuPaiResult.getData());

        return yiSuPaiResult.getData();
    }

    /**
     * 将外部平台同步的过来商品对应的商品图片、sku图片上传至平台的oss，解决前端不能显示指定比例大小图的问题
     * @param dto
     * @return
     */
    private YiSuPaiGoodsDTO  initPic(YiSuPaiGoodsDTO  dto){

        if(!(dto==null || dto.getMainPicUrl()==null || dto.getMainPicUrl().isEmpty())){
            try {
                String url = dto.getMainPicUrl().contains("http:") || dto.getMainPicUrl().contains("https:") ? dto.getMainPicUrl() : "http:"+dto.getMainPicUrl();
                FileVO fileVO = fileManager.upload(this.getFileDTO(url),"goods");
                dto.setMainPicUrl(fileVO.getUrl());
            } catch (Exception e) {
                System.out.println("上传文件失败。。。。。");
                e.printStackTrace();
            }
            //return uploader.getThumbnailUrl(url, width, height);
        }
        return dto;
    }


    public  FileDTO getFileDTO(String urlString) throws Exception {
        // 构造URL
        URL url = new URL(urlString);
        // 打开连接
        URLConnection con = url.openConnection();
        //设置请求超时为5s
        con.setConnectTimeout(5*1000);
        // 输入流
        InputStream is = con.getInputStream();

        // 1K的数据缓冲
        byte[] bs = new byte[1024];
        // 读取到的数据长度
        int len;

        FileDTO fileDTO = new FileDTO();
        fileDTO.setStream(is);
        String ext = urlString.substring(urlString.lastIndexOf("/") + 1, urlString.length());
        fileDTO.setExt(ext);
        fileDTO.setName(UUID.randomUUID().toString());



        return fileDTO;
    }




    @Override
    public List<YiSuPaiGoodsDTO> getGoodsQuantity(List<String> itemIds) {
        List<YiSuPaiGoodsDTO> goodsList=new ArrayList<>();
        if(CollectionUtils.isEmpty(itemIds)){
            return goodsList;
        }

        SortedMap<String, Object> map = new TreeMap<>();
        map.put("itemIds", itemIds);

        logger.info("获取商品库存请求的数据：" + map.toString());
        String resultString = actualPostRequest(map, THIRD_PARTY_USER_ID, "item/batchSearchItemQuantity");
        logger.info("获取商品库存返回的数据：" + map.toString());

        Result<List<YiSuPaiGoodsDTO>> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<List<YiSuPaiGoodsDTO>>>() {});

        if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
            return goodsList;
        }

        return yiSuPaiResult.getData();
    }

    @Override
    public Map<String, YiSuPaiCategoryDTO> getCategoryList() {
        String resultString = actualPostRequest(null, THIRD_PARTY_USER_ID, "item/categoryList");
        System.out.println(resultString);
        logger.info("获取分类列表" + resultString);

        Result<List<YiSuPaiCategoryDTO>> yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result<List<YiSuPaiCategoryDTO>>>() {});

        if (yiSuPaiResult.getCode() != 200||yiSuPaiResult.getData() == null) {
            return null;
        }

        List<YiSuPaiCategoryDTO> categoryDTOList=yiSuPaiResult.getData();

        return categoryDTOList.stream().collect(Collectors.toMap(YiSuPaiCategoryDTO::getCategoryId, Function.identity(),(oldValue, newValue) -> newValue));
    }


    private List<GoodsSkuVO> initGoodsSku(GoodsDO goodsDO, List<YiSuPaiGoodsSkuDTO> yspSkuList) {
        // 需要组装返回的sku列表
        List<GoodsSkuVO> goodsSkuVOList = new ArrayList<>();

        // 商品的分类ID
        Integer categoryId = goodsDO.getCategoryId();
        //查询系统中符合条件的规格名并保存到zheretaoSpecMap里 分类，sellerId,规格名
        String sql = "select s.* from es_specification s inner join es_category_spec cs on s.spec_id = cs.spec_id  where disabled = 1 and category_id = ? and seller_id = ? ";
        List<SpecificationDO> specificationDOList = this.daoSupport.queryForList(sql, SpecificationDO.class, categoryId, SELLER_ID);
        // 保存已经匹配过的规格名  Map<规格名,系统的规格名ID>
        Map<String, Integer> yspSpecMap = specificationDOList.stream().collect(Collectors.toMap(SpecificationDO::getSpecName, SpecificationDO::getSpecId,(oldKey,newKey)->newKey));
        //保存已经匹配过的规格值，Map<规格值,规格值的状态> value值大于0表示已经匹配过
        Map<String, Integer> specValMap = new HashMap<>();


        //当前sku中的规格值列表
        List<SpecValueVO> specValueVOList = new ArrayList<>();
        //批量保存的规格值
        List<SpecValuesDO> specValueDOList = new ArrayList<>();
        for (YiSuPaiGoodsSkuDTO yspSku : yspSkuList) {
            Map<String, String> specMap = JSONObject.fromObject(yspSku.getSkuPropertiesJson());
            for (Map.Entry<String, String> entry : specMap.entrySet()) {
                //规格名
                String specName = entry.getKey();
                //规格值
                String specValue = entry.getValue();

                Integer specId = yspSpecMap.get(specName);
                if (yspSpecMap == null || specId == null) {
                    SpecificationDO newSpec = new SpecificationDO();
                    //存储规格名
                    newSpec.setSellerId(SELLER_ID);
                    newSpec.setSpecName(specName);
                    newSpec.setSpecMemo("易速派商家自定义");
                    newSpec = specificationManager.add(newSpec);
                    specId = newSpec.getSpecId();
                    //保存分类规格的关系
                    CategorySpecDO categorySpec = new CategorySpecDO(categoryId, specId);
                    this.daoSupport.insert(categorySpec);
                    //匹配过的规格名存在map中
                    yspSpecMap.put(specName, specId);
                }

                //没有保存过规格值 或者数据库中没有
                if (specValMap == null || specValMap.size() == 0) {
                    sql = "select * from es_spec_values where spec_id = ? and seller_id = ? group by spec_value";
                    //系统中该规格名下的所有规格值
                    List<SpecValuesDO> specValuesDOList = this.daoSupport.queryForList(sql, SpecValuesDO.class, specId, SELLER_ID);
                    Map<String, Integer> specValueMap = specValuesDOList.stream().collect(Collectors.toMap(SpecValuesDO::getSpecValue, SpecValuesDO::getSpecValueId));
                    if (specValueMap != null && specValueMap.size() > 0)
                        specValMap.putAll(specValueMap);
                }

                SpecValuesDO specValuesDO = new SpecValuesDO();
                specValuesDO.setSpecId(specId);
                specValuesDO.setSpecName(specName);
                specValuesDO.setSpecValue(specValue);
                specValuesDO.setSellerId(SELLER_ID);
                if (specValMap.get(specValue) == null) {
                    //当前规格名下没有规格值，或规格值没有重复，则保存规格值
                    specValueDOList.add(specValuesDO);
                }
                SpecValueVO specValueVO = new SpecValueVO();
                BeanUtils.copyProperties(specValuesDO, specValueVO);
                specValueVOList.add(specValueVO);
                specValMap.put(specValue, 0);

            }

            GoodsSkuVO skuVO = new GoodsSkuVO();
            if (!CollectionUtils.isEmpty(specValueVOList)) {
                skuVO.setSpecList(specValueVOList);
                BeanUtils.copyProperties(goodsDO, skuVO);
                //上游skuId
                skuVO.setUpSkuId(yspSku.getSkuId());
                Integer priceCent = yspSku.getPriceCent();
                skuVO.setPrice(priceCent == null ? 999d : priceCent / 100);
                goodsSkuVOList.add(skuVO);
            }
        }

        //批量保存规格值
        if (!CollectionUtils.isEmpty(specValueDOList)) {
            specValuesManager.addSpecValuesList(specValueDOList);
        }
        return goodsSkuVOList;
    }

    private GoodsDO initGoods(YiSuPaiGoodsDTO yspGoods){
        GoodsDO goodsDO = new GoodsDO();

        // 自动生成商品编码
        goodsDO.setSn(PinYinUtil.getPinYinHeadChar(SELLER_NAME) + "_" + DateUtil.toString(new Date(),
                "yyyyMMdd") + "_" + ((int) (Math.random() * 900) + 100));

        goodsDO.setUpGoodsId(yspGoods.getItemId());
        goodsDO.setGoodsName(yspGoods.getItemTitle());
        goodsDO.setSupplierName(yspGoods.getSource());
        goodsDO.setBuyCount(yspGoods.getTotalSoldQuantity());

        Integer reservePrice = yspGoods.getReservePrice();
        goodsDO.setMktprice(reservePrice == null ? 999d : (double) reservePrice / 100);
        Integer priceCent = yspGoods.getPriceCent();
        goodsDO.setPrice(priceCent == null ? 999d : priceCent / 100);
        goodsDO.setCost(priceCent == null ? 999d : priceCent / 100);

        goodsDO.setQuantity(yspGoods.getQuantity());
        goodsDO.setEnableQuantity(yspGoods.getQuantity());
        goodsDO.setIntro(yspGoods.getDescOption());
        if (CollectionUtils.isEmpty(yspGoods.getSkuList())) {
            goodsDO.setHaveSpec(0);
        } else {
            goodsDO.setHaveSpec(1);
        }
        goodsDO.setIsGlobal(1);
        goodsDO.setIsLocal(0);
        goodsDO.setIsSelfTake(0);
        // 商品状态 是否可用
        goodsDO.setDisabled(1);
        // 上架状态 1上架  0下架
        goodsDO.setMarketEnable(0);
        // 不需要审核
        goodsDO.setIsAuth(1);

        // 商品创建时间
        goodsDO.setCreateTime(DateUtil.getDateline());
        // 商品最后更新时间
        goodsDO.setLastModify(DateUtil.getDateline());
        // 商品浏览次数
        goodsDO.setViewCount(0);
        // 评论次数
        goodsDO.setCommentNum(0);
        // 商品评分
        goodsDO.setGrade(100.0);
        goodsDO.setGoodsType(GoodsType.NORMAL.name());
        goodsDO.setSellerId(SELLER_ID);
        goodsDO.setSellerName(SELLER_NAME);

        // 向goods加入图片
        GoodsGalleryDO goodsGalley = goodsGalleryManager
                .getGoodsGallery(yspGoods.getMainPicUrl());
        goodsDO.setOriginal(goodsGalley.getOriginal());
        goodsDO.setBig(goodsGalley.getBig());
        goodsDO.setSmall(goodsGalley.getSmall());
        goodsDO.setThumbnail(goodsGalley.getThumbnail());

        return goodsDO;
    }



    @Override
    public String createAnonyAccount(Integer memberId) {
        String resultString = actualPostRequest(null, getAnonyAccount(memberId), "account/createAnonyAccount");
        System.out.println(resultString);
        Result yiSuPaiResult = JSON.parseObject(resultString, new TypeReference<Result>() {
        });
        if(yiSuPaiResult.getCode()==200){
            return getAnonyAccount(memberId);
        }else {
            return null;
        }
    }

    private String actualPostRequest(SortedMap<String, Object> map, String thirPartyUserId, String url) {
        if (map == null) {
            map = new TreeMap<>();
        }

        map.put("thirdPartyUserId", thirPartyUserId);
        map.put("timestamp", Calendar.getInstance().getTimeInMillis());

        String sign = getMD5Sign(map, APP_SECRET);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("appKey", APP_KEY);
        headers.add("sign", sign);
        HttpEntity httpEntity = new HttpEntity<>(map, headers);

        url = BASE_URL + url;
        String result = restTemplate.postForObject(url, httpEntity, String.class);
        return result;
    }

    private String getAnonyAccount(Integer memberId){
        return APP_KEY + memberId;
    }

    private String getMD5Sign(SortedMap<String, Object> params, String appSecret) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            if (!entry.getKey().equals("sign")) { // 拼装参数,排除sign
                if (!StringUtils.isEmpty(entry.getKey())) {//过滤key为空或者value为null的
                    if (entry.getValue() != null) {
                        String value;
                        if (entry.getValue() instanceof String) {
                            value = entry.getValue().toString();
                        } else {
                            value = JSON.toJSONString(entry.getValue(), SerializerFeature.MapSortField);
                        }
                        //log.info("key:"+entry.getKey()+", value:"+value);
                        sb.append(entry.getKey()).append("=").append(value).append("&");
                    }
                }
            }
        }
        sb.append(appSecret);
        // logger.info("all param str:{}" + sb.toString());
        return DigestUtils.md5Hex(sb.toString());
    }


    //设置分销返利
    private void addDistributionGoods(Integer goodsId) {
        DistributionGoods distributionGoods = new DistributionGoods();
        distributionGoods.setGoodsId(goodsId);
        distributionGoods.setGrade1Rebate(8.00);
        distributionGoods.setGrade2Rebate(1.00);
        distributionGoods.setInviterRate(1.00);
        distributionGoodsManager.edit(distributionGoods);
    }
}
