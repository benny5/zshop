package com.enation.app.javashop.core.trade.cart.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 购物车视图<br/>
 * 购物车构建器最终要构建的成品
 * 文档请参考：<br>
 * <a href="http://doc.javamall.com.cn/current/achitecture/jia-gou/ding-dan/cart-and-checkout.html#购物车显示" >购物车显示</a>
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/12/10
 */

@Data
public class CartView {

    /**
     * 购物车列表
     */
    @ApiModelProperty(value = "购物车列表")
    private List<CartVO> cartList;

    /**
     * 购物车计算后的总价
     */
    @ApiModelProperty(value = "车计算后的总价")
    private PriceDetailVO totalPrice;

    private int totalGoodsNum;

    //社团购物车商品数量
    private int shetuanCartGoodsNum;

    //社团购物车sku数量
    private int shetuanCartSkuNum;


    /* 权限 {@link com.enation.app.javashop.core.trade.cart.model.dos.OrderPermission}*/
    private String cartViewType;

    public CartView(List<CartVO> cartList, PriceDetailVO totalPrice) {
        this.cartList = cartList;
        this.totalPrice = totalPrice;
    }

    public void statisticSkuNum(){
        List<CartVO> cartList = this.cartList;
        if(cartList.size()>1){
            List<CartSkuVO> skuList = cartList.get(0).getSkuList();
            for (int i = 1; i < cartList.size(); i++) {
                skuList.addAll(cartList.get(i).getSkuList());
            }
            cartList.get(0).setPrice(this.totalPrice);
            this.cartList=Arrays.asList(cartList.get(0));
        }

        //社团购物车商品数量
        int shetuanCartGoodsNum = 0;
        //社团购物车中的sku数量
        int shetuanCartSkuNum = 0;
        if (!CollectionUtils.isEmpty(cartList)) {
            CartVO cartVO = cartList.get(0);
            shetuanCartSkuNum = cartVO.getGoodsNum();
            List<CartSkuVO> skuList = cartVO.getSkuList();
            if (!CollectionUtils.isEmpty(skuList)) {
                skuList = skuList.stream().filter(cartSkuVO -> cartSkuVO.getChecked() == 1 &&  cartSkuVO.getInvalid() != 1 ).collect(Collectors.toList());
                shetuanCartGoodsNum = skuList.size();
            }
        }
       this.shetuanCartGoodsNum=shetuanCartGoodsNum;
        this.shetuanCartSkuNum=shetuanCartSkuNum;
    }


}
