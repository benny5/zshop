package com.enation.app.javashop.core.passport.dto;

import lombok.Data;

/**
 * @author ChienSun
 * @date 2019/3/5
 */
@Data
public class MiniprogramDTO {

    private String appid;
    private String pagepath;

}
