package com.enation.app.javashop.core.passport.service.impl;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.afs.model.v20180112.AuthenticateSigRequest;
import com.aliyuncs.afs.model.v20180112.AuthenticateSigResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.geoip.model.v20200101.DescribeIpv4LocationRequest;
import com.aliyuncs.geoip.model.v20200101.DescribeIpv4LocationResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.enation.app.javashop.core.passport.service.AliyunUtilsManager;
import com.enation.app.javashop.core.system.SystemErrorCode;
import com.enation.app.javashop.framework.context.ThreadContextHolder;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 阿里云滑动验证
 * @author JFeng
 * @date 2020/7/9 10:24
 */
@Component
@Slf4j
public class AliyunUtilsManagerImpl implements AliyunUtilsManager {
    private static final String  ACCESSKEY_ID = "LTAI4FdditDg35n86fHtJbbV";
    private static final String  ACCESSKEY_SECRET = "Sp3RyO8UK6q76zP5UjKpZpgNQJpUFk";
    private static final String  APP_KEY = "FFFF0N0000000000941B";
    private static final String  SCENE = "nc_register_h5";
    private DefaultAcsClient client =null;

    @PostConstruct
    public void init() {
       this.initClient();
    }

    /**
     * 滑动验证
     * @param sessionId
     * @param ncToken
     * @param sig
     * @return
     */
    @Override
    public Boolean check(String sessionId,String ncToken,String sig) {
        try {
            AuthenticateSigRequest request = new AuthenticateSigRequest();
            request.setSessionId(sessionId);// 必填参数，从前端获取，不可更改，android和ios只传这个参数即可
            request.setSig(sig);// 必填参数，从前端获取，不可更改
            request.setToken(ncToken);// 必填参数，从前端获取，不可更改
            request.setScene(SCENE);// 必填参数，从前端获取，不可更改
            request.setAppKey(APP_KEY);// 必填参数，后端填写
            request.setRemoteIp(ThreadContextHolder.getHttpRequest().getRemoteHost());// 必填参数，后端填写

            AuthenticateSigResponse response = client.getAcsResponse(request);
            if(response.getCode() == 100) {
                System.out.println("验签通过");
                return true;
            } else {
                System.out.println("验签失败");
                return false;
            }

        } catch (ClientException ce) {
            log.error("ClientException异常");
            ce.printStackTrace();
            throw new ServiceException(SystemErrorCode.E928.code(), "滑动验证失败！");
        }
    }

    @Override
    public String getIPLocation() {
        String ipAddr = IpUtil.getIpAddr(ThreadContextHolder.getHttpRequest());
        DescribeIpv4LocationRequest request = new DescribeIpv4LocationRequest();
        request.setIp(ipAddr);

        try {
            DescribeIpv4LocationResponse response = client.getAcsResponse(request);

            if (StringUtils.isNotEmpty(response.getCity())) {
                return response.getCity();
            }
            return "";
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            System.out.println("ErrCode:" + e.getErrCode());
            System.out.println("ErrMsg:" + e.getErrMsg());
            System.out.println("RequestId:" + e.getRequestId());
            throw new ServiceException(SystemErrorCode.E928.code(), "IP解析失败！");
        }
        return "";
    }

    private void initClient()  {
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESSKEY_ID, ACCESSKEY_SECRET);
        client = new DefaultAcsClient(profile);

        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", "afs", "afs.aliyuncs.com");
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
