package com.enation.app.javashop.core.trade.order.model.enums;

/**
 * Created by kingapex on 2019-01-27.
 * 订单类型
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-27
 */
public enum OrderTypeEnum {

    /**
     * 普通订单
     */
    normal,

    /**
     * 拼团订单
     */
    pintuan,

    /**
     * 社区团购订单
     */
    shetuan,

    /**
     * 服务订单
     */
    fuwu

}
