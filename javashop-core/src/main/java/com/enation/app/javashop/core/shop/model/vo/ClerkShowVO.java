package com.enation.app.javashop.core.shop.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 店员实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-08-04 18:48:39
 */


@Data
public class ClerkShowVO implements Serializable {
    /**
     * 店员id
     */
    @ApiModelProperty(value = "店员id")
    private Integer clerkId;
    /**
     * 店员名称
     */
    @ApiModelProperty(value = "账号名称")
    private String uname;
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String mobile;
    /**
     * 权限
     */
    @ApiModelProperty(value = "权限")
    private String role;
    /**
     * 权限id
     */
    @ApiModelProperty(value = "权限id")
    private Integer roleId;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;
    /**
     * 会员id
     */
    @ApiModelProperty(value = "会员id")
    private Integer memberId;
    /**
     * 店铺id
     */
    @ApiModelProperty(value = "店铺id")
    private Integer shopId;
    /**
     * 角色标识
     */
    @ApiModelProperty(value = "1:超级店员 0:普通店员")
    private Integer founder;

    /**
     * 店员状态，-1为禁用，1为正常
     */
    @ApiModelProperty(name = "user_state", value = "店员状态，-1为禁用，0为正常")
    private Integer userState;

    @ApiModelProperty(name = "shop_name", value = "店铺名称")
    private String shopName;

    @ApiModelProperty(name = "founder_text", value = "角色名称")
    private String founderText;

    private String face;

}