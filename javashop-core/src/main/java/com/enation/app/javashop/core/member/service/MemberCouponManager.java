package com.enation.app.javashop.core.member.service;

import com.enation.app.javashop.core.member.model.dos.MemberCoupon;
import com.enation.app.javashop.core.member.model.dto.MemberCouponQueryParam;
import com.enation.app.javashop.core.member.model.vo.MemberCouponNumVO;
import com.enation.app.javashop.core.promotion.coupon.model.dos.CouponDO;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;

/**
 * 会员优惠券
 *
 * @author Snow create in 2018/5/24
 * @version v2.0
 * @since v7.0.0
 */
public interface MemberCouponManager {

    /**
     * 领取优惠券
     *
     * @param memberCoupon 优惠券
     */
    void receiveBonus(MemberCoupon memberCoupon);


    /**
     * 查询我的所有优惠券
     *
     * @param param
     * @return
     */
    Page list(MemberCouponQueryParam param);


    /**
     * 读取我的优惠券
     *
     * @param memberId
     * @param mcId
     * @return
     */
    MemberCoupon getModel(Integer memberId, Integer mcId);

    /**
     * 将优惠券变为已使用
     *
     * @param mcId
     */
    void usedCoupon(Integer mcId,String orderSn);


    /**
     * 检测已领取数量
     *
     * @param couponId
     */
    void checkLimitNum(Integer couponId);

    /**
     * 结算页—查询会员优惠券
     *
     * @param sellerId 商家id
     * @param memberId  会员id
     * @return
     */
    List<MemberCoupon> listByCheckout(Integer sellerId, Integer memberId);

    /**
     * 优惠券各个状态数量
     *
     * @return
     */
    MemberCouponNumVO statusNum();

    CouponDO getOrderShareCoupon(String orderSn,Integer sellerId);

    void retrieveCoupon(String orderSn);

    /**
     * 根据会员id 查询会员新人优惠券  有没有失效
     */
    MemberCoupon getNewCoupon(Integer memberId,Integer sellerId,String[] couponType);

    /**
     * 根据会员id 和优惠卷id 查询会员首单优惠券  getCouponNum
     */
    Integer getCouponNum (Integer memberId,Integer sellerId,String userType);

    /**
     * 查询领取的优惠券
     */
    List<MemberCoupon> getMemberCoupon (Integer memberId,Integer[] couperId);


    void retroBackCoupon(String sn);
}
