package com.enation.app.javashop.core.wms.model.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 配送汇总
 * @author JFeng
 * @date 2020/7/17 14:31
 */

@Data
public class ExportSiteSkuVO {

    @ExcelProperty("自提点")
    private String siteName;

    @ExcelProperty("商品名称")
    private String name;

    @ExcelProperty("单品数量")
    private Integer skuTotal;

}
