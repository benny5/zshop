package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/5/14
 * @Description:
 */
@Data
public class YiSuPaiGoodsDTO {

    private String itemId;
    // 商品名
    private String itemTitle;
    // 商品名（在物流信息中使用）
    private String goodsName;
    private String categoryId;
    private String sellerId;
    // 主图地址
    private String mainPicUrl;
    // 轮播图地址
    private List<String> itemImages;
    // 商品详情
    private String descOption;

    // 商品来源
    private String source;
    // 商品来源链接
    private String goodsSouceUrl;
    // 商详页显示的划线价
    private Integer reservePrice;
    // 销售价格（分）
    private Integer priceCent;

    // 是否可售卖
    private Boolean canSell;
    // 是否包邮
    private Boolean sellerPayPostfee;
    // 库存
    private Integer quantity;
    // linkedmall销量
    private Integer totalSoldQuantity;
    // 最小差价（所有sku中划线价与售价的最小差值）
    private Integer minPriceDiff;
    // 最大差价（所有sku中划线价与售价的最大差值）
    private Integer maxPriceDiff;
    // 商品差价率至多值,4位小数（0.6000 -》 60%）
    private Double maxPriceDiffRate;
    // 商品差价率至少值,,4位小数（0.6000 -》 60%）
    private Double minPriceDiffRate;

    private List<YiSuPaiGoodsSkuDTO> skuList;
}
