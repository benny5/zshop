package com.enation.app.javashop.core.base.plugin.sms;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.enation.app.javashop.core.base.model.vo.ConfigItem;
import com.enation.app.javashop.core.base.plugin.express.util.MD5;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 壹通道短信工具类
 **/
@Component
public class SmsYtdPluginNew implements SmsPlatform{

    private static Logger logger = Logger.getLogger(SmsYtdPluginNew.class);

    // 生产URL
    private static final String MULTIURL = "http://open.yitd.cn/sms/multiUTF8";

    // 企业id
    private static final String USERID = "487880";
    // 账号
    private static final String USERNAME = "487880";
    // 密码
    private static final String PASSWORD = "5jJJsJ";

    @Override
    public List<ConfigItem> definitionConfigItem() {
        List<ConfigItem> list = new ArrayList<>();

        ConfigItem userid = new ConfigItem();
        userid.setType("text");
        userid.setName("userid");
        userid.setText("企业id");

        ConfigItem username = new ConfigItem();
        username.setType("text");
        username.setName("username");
        username.setText("用户名");

        ConfigItem password = new ConfigItem();
        password.setType("text");
        password.setName("password");
        password.setText("密码");

        list.add(userid);
        list.add(username);
        list.add(password);
        return list;
    }

    @Override
    public boolean onSend(String mobile, String content, Map param) {
        // 头信息
        Map<String, String> header = new LinkedHashMap<>();
        header.put("userid", (String) param.get("userid"));
        header.put("account", (String) param.get("username"));
        header.put("password", MD5.encode((String) param.get("password")));

        // 短信内容
        String msgId = "ytd" + UUID.randomUUID().toString().replace("-", "");
        SendMsgStatusByYTDDTO sendMsgStatusByYTDDTO = new SendMsgStatusByYTDDTO();
        sendMsgStatusByYTDDTO.setMobile(mobile);
        sendMsgStatusByYTDDTO.setContent(content);
        sendMsgStatusByYTDDTO.setSmsid(msgId);

        // body
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("messages", Lists.newArrayList(sendMsgStatusByYTDDTO));
        try {
            logger.info("壹通道短信请求报文：" + JSON.toJSON(body).toString());
            String result = SmsLczssUtil.sendHttpClientPostJSON(MULTIURL, header, JSON.toJSON(body).toString());
            logger.info("壹通道短信响应报文:" + result);
            JSONObject jsonObject = JSON.parseObject(result);
            String code = jsonObject.getString("code");
            if("success".equalsIgnoreCase(code)){
                JSONArray messageJsonArray = jsonObject.getJSONArray("messages");
                JSONObject messageJSONObject = messageJsonArray.getJSONObject(0);
                String returnstatus = messageJSONObject.getString("returnstatus");
                if("success".equalsIgnoreCase(returnstatus)){
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("壹通道短信发送失败", e);
        }
        return false;
    }

    @Override
    public String getPluginId() {
        return "smsYtdPluginNew";
    }

    @Override
    public String getPluginName() {
        return "壹通道短信插件";
    }

    @Override
    public Integer getIsOpen() {
        return null;
    }


    public static void main(String[] args) {
        SmsYtdPluginNew smsYtdPlugin = new SmsYtdPluginNew();
        Map map = new HashMap();
        map.put("userid", USERID);
        map.put("username", USERNAME);
        map.put("password", PASSWORD);
        boolean stringObjectMap = smsYtdPlugin.onSend("18255348391",
                "【妙寄】编号A-0-0001中通快件4023已到快递站，11点前可自提，联系电话13761822094",map);
        System.out.println(stringObjectMap);
    }
}
