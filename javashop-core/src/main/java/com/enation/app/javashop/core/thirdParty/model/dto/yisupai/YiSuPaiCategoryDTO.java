package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import lombok.Data;

/**
 * @Author: zhou
 * @Date: 2021/5/15
 * @Description: 易速派分类
 */
@Data
public class YiSuPaiCategoryDTO {
    private int id;
    private String categoryId;
    private String parentId;
    private String name;
    private String source;
}
