package com.enation.app.javashop.core.distribution.service.pattern;

import com.enation.app.javashop.core.distribution.model.dos.DistributionOrderDO;
import com.enation.app.javashop.core.trade.order.model.dos.OrderDO;

/**
 * 分销策略
 */
public interface DistributionStrategy {

    /**
     *  确定分销商
     */
    void distributors(OrderDO orderDO, DistributionOrderDO distributionOrderDO);

    /**
     *  计算分销商们的收益
     */
    void countProfit(OrderDO orderDO, DistributionOrderDO distributionOrderDO);


}
