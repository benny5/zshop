package com.enation.app.javashop.core.payment.model.enums;

/**
 * 支付插件
 *
 * @author 王志杨
 * @version 1.0
 * @since 2020年10月15日 10:28:27
 */
public enum PaymentPluginEnum {

    /**
     * 微信支付插件
     */
    weixinPayPlugin("微信"),

    /**
     * 钱包支付插件
     */
    walletPayPlugin("钱包"),

    /**
     * 支付宝支付插件
     */
    alipayDirectPlugin("支付宝"),

    /**
     * 组合支付插件
     */
    mergePayPlugin("组合支付");


    private String description;

    PaymentPluginEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
