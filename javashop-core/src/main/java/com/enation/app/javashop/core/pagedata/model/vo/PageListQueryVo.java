package com.enation.app.javashop.core.pagedata.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class PageListQueryVo implements Serializable {


    @ApiModelProperty(name = "client_type", value = "客户端类型" )
    private String clientType;

    @ApiModelProperty(name = "page_name", value = "页面名称" )
    private String pageName;

    @ApiModelProperty(name = "page_id", value = "页面id" )
    private Integer pageId;

    @ApiModelProperty(name = "page_type", value = "页面类型" )
    private String pageType;

    @ApiModelProperty(name = "owner_type", value = "首页类型" )
    private String ownerType;

    @ApiModelProperty(name = "state", value = "页面状态" )
    private Integer state;

    @ApiModelProperty(name = "city", value = "定制城市" )
    private String city;

    @ApiModelProperty(name = "page_no", value = "页号" )
    private int pageNo;

    @ApiModelProperty(name = "page_size", value = "单页数据" )
    private int pageSize;

    @ApiModelProperty(name = "province", value = "省" )
    private String province;

    @ApiModelProperty(name = "county", value = "区/县" )
    private String county;


}
