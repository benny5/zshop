package com.enation.app.javashop.core.goodssearch.service;

import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsGroupVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsSearchDTO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.StGoodsDoc;
import com.enation.app.javashop.framework.database.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by kingapex on 2019-01-21.
 * 拼团搜索业务接口
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-01-21
 */
public interface ShetuanSearchManager {

    Page search(ShetuanGoodsSearchDTO goodsSearch);


    boolean addIndex(ShetuanGoodsVO shetuanGoodsVO, Map<String, Object> goods);

    boolean delIndex(Integer shetuanId,Integer skuId);

    boolean deleteByShetuanId(Integer shetuanId);

    List<ShetuanGoodsGroupVO> searchByShopCat(Integer shopCatId);

    /**
     * 根据拼团id同步es中的拼团商品<br/>
     * 当拼团活动商品发生变化时调用此方法
     * @param pinTuanId
     */
    //void syncIndexByPinTuanId(Integer pinTuanId);

    /**
     * 根据商品id同步es中的拼团商品<br>
     * @param goodsId 商品id
     */
    //void syncIndexByGoodsId(Integer goodsId);
}
