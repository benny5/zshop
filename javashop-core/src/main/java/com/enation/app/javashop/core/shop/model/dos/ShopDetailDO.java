package com.enation.app.javashop.core.shop.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;


/**
 * 店铺详细实体
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-21 10:06:38
 */
@Table(name="es_shop_detail")
@ApiModel
@Data
public class ShopDetailDO implements Serializable {
			
    private static final long serialVersionUID = 1810092990127648L;
    
    /**店铺详细id*/
    @Id(name = "id")
    @ApiModelProperty(hidden=true)
    private Integer id;
    /**店铺id*/
    @Column(name = "shop_id")
    @ApiModelProperty(name="shop_id",value="店铺id",required=false)
    private Integer shopId;
    /**店铺所在省id*/
    @Column(name = "shop_province_id")
    @ApiModelProperty(name="shop_province_id",value="店铺所在省id",required=false)
    private Integer shopProvinceId;
    /**店铺所在市id*/
    @Column(name = "shop_city_id")
    @ApiModelProperty(name="shop_city_id",value="店铺所在市id",required=false)
    private Integer shopCityId;
    /**店铺所在县id*/
    @Column(name = "shop_county_id")
    @ApiModelProperty(name="shop_county_id",value="店铺所在县id",required=false)
    private Integer shopCountyId;
    /**店铺所在镇id*/
    @Column(name = "shop_town_id")
    @ApiModelProperty(name="shop_town_id",value="店铺所在镇id",required=false)
    private Integer shopTownId;
    /**店铺所在省*/
    @Column(name = "shop_province")
    @ApiModelProperty(name="shop_province",value="店铺所在省",required=false)
    private String shopProvince;
    /**店铺所在市*/
    @Column(name = "shop_city")
    @ApiModelProperty(name="shop_city",value="店铺所在市",required=false)
    private String shopCity;
    /**店铺所在县*/
    @Column(name = "shop_county")
    @ApiModelProperty(name="shop_county",value="店铺所在县",required=false)
    private String shopCounty;
    /**店铺所在镇*/
    @Column(name = "shop_town",allowNullUpdate = true)
    @ApiModelProperty(name="shop_town",value="店铺所在镇",required=false)
    private String shopTown;
    /**店铺详细地址*/
    @Column(name = "shop_add")
    @ApiModelProperty(name="shop_add",value="店铺详细地址",required=false)
    private String shopAdd;
    /**公司名称*/
    @Column(name = "company_name")
    @ApiModelProperty(name="company_name",value="公司名称",required=false)
    private String companyName;
    /**公司地址*/
    @Column(name = "company_address")
    @ApiModelProperty(name="company_address",value="公司地址",required=false)
    private String companyAddress;
    /**公司电话*/
    @Column(name = "company_phone")
    @ApiModelProperty(name="company_phone",value="公司电话",required=false)
    private String companyPhone;
    /**电子邮箱*/
    @Column(name = "company_email")
    @ApiModelProperty(name="company_email",value="电子邮箱",required=false)
    private String companyEmail;
    /**员工总数*/
    @Column(name = "employee_num")
    @ApiModelProperty(name="employee_num",value="员工总数",required=false)
    private Integer employeeNum;
    /**注册资金*/
    @Column(name = "reg_money")
    @ApiModelProperty(name="reg_money",value="注册资金",required=false)
    private Double regMoney;
    /**联系人姓名*/
    @Column(name = "link_name")
    @ApiModelProperty(name="link_name",value="联系人姓名",required=false)
    private String linkName;
    /**联系人电话*/
    @Column(name = "link_phone")
    @ApiModelProperty(name="link_phone",value="联系人电话",required=false)
    private String linkPhone;
    /**法人姓名*/
    @Column(name = "legal_name")
    @ApiModelProperty(name="legal_name",value="法人姓名",required=false)
    private String legalName;
    /**法人身份证*/
    @Column(name = "legal_id")
    @ApiModelProperty(name="legal_id",value="法人身份证",required=false)
    private String legalId;
    /**法人身份证照片*/
    @Column(name = "legal_img")
    @ApiModelProperty(name="legal_img",value="法人身份证照片",required=false)
    private String legalImg;
    /**营业执照号*/
    @Column(name = "license_num")
    @ApiModelProperty(name="license_num",value="营业执照号",required=false)
    private String licenseNum;
    /**营业执照所在省id*/
    @Column(name = "license_province_id")
    @ApiModelProperty(name="license_province_id",value="营业执照所在省id",required=false)
    private Integer licenseProvinceId;
    /**营业执照所在市id*/
    @Column(name = "license_city_id")
    @ApiModelProperty(name="license_city_id",value="营业执照所在市id",required=false)
    private Integer licenseCityId;
    /**营业执照所在县id*/
    @Column(name = "license_county_id")
    @ApiModelProperty(name="license_county_id",value="营业执照所在县id",required=false)
    private Integer licenseCountyId;
    /**营业执照所在镇id*/
    @Column(name = "license_town_id")
    @ApiModelProperty(name="license_town_id",value="营业执照所在镇id",required=false)
    private Integer licenseTownId;
    /**营业执照所在省*/
    @Column(name = "license_province")
    @ApiModelProperty(name="license_province",value="营业执照所在省",required=false)
    private String licenseProvince;
    /**营业执照所在市*/
    @Column(name = "license_city")
    @ApiModelProperty(name="license_city",value="营业执照所在市",required=false)
    private String licenseCity;
    /**营业执照所在县*/
    @Column(name = "license_county")
    @ApiModelProperty(name="license_county",value="营业执照所在县",required=false)
    private String licenseCounty;
    /**营业执照所在镇*/
    @Column(name = "license_town",allowNullUpdate = true)
    @ApiModelProperty(name="license_town",value="营业执照所在镇",required=false)
    private String licenseTown;
    /**营业执照详细地址*/
    @Column(name = "license_add")
    @ApiModelProperty(name="license_add",value="营业执照详细地址",required=false)
    private String licenseAdd;
    /**成立日期*/
    @Column(name = "establish_date")
    @ApiModelProperty(name="establish_date",value="成立日期",required=false)
    private Long establishDate;
    /**营业执照有效期开始*/
    @Column(name = "licence_start")
    @ApiModelProperty(name="licence_start",value="营业执照有效期开始",required=false)
    private Long licenceStart;
    /**营业执照有效期结束*/
    @Column(name = "licence_end")
    @ApiModelProperty(name="licence_end",value="营业执照有效期结束",required=false)
    private Long licenceEnd;
    /**法定经营范围*/
    @Column(name = "scope")
    @ApiModelProperty(name="scope",value="法定经营范围",required=false)
    private String scope;
    /**营业执照电子版*/
    @Column(name = "licence_img")
    @ApiModelProperty(name="licence_img",value="营业执照电子版",required=false)
    private String licenceImg;
    /**组织机构代码*/
    @Column(name = "organization_code")
    @ApiModelProperty(name="organization_code",value="组织机构代码",required=false)
    private String organizationCode;
    /**组织机构电子版*/
    @Column(name = "code_img")
    @ApiModelProperty(name="code_img",value="组织机构电子版",required=false)
    private String codeImg;
    /**一般纳税人证明电子版*/
    @Column(name = "taxes_img")
    @ApiModelProperty(name="taxes_img",value="一般纳税人证明电子版",required=false)
    private String taxesImg;
    /**银行开户名*/
    @Column(name = "bank_account_name")
    @ApiModelProperty(name="bank_account_name",value="银行开户名",required=false)
    private String bankAccountName;
    /**银行开户账号*/
    @Column(name = "bank_number")
    @ApiModelProperty(name="bank_number",value="银行开户账号",required=false)
    private String bankNumber;
    /**开户银行支行名称*/
    @Column(name = "bank_name")
    @ApiModelProperty(name="bank_name",value="开户银行支行名称",required=false)
    private String bankName;
    /**开户银行所在省id*/
    @Column(name = "bank_province_id")
    @ApiModelProperty(name="bank_province_id",value="开户银行所在省id",required=false)
    private Integer bankProvinceId;
    /**开户银行所在市id*/
    @Column(name = "bank_city_id")
    @ApiModelProperty(name="bank_city_id",value="开户银行所在市id",required=false)
    private Integer bankCityId;
    /**开户银行所在县id*/
    @Column(name = "bank_county_id")
    @ApiModelProperty(name="bank_county_id",value="开户银行所在县id",required=false)
    private Integer bankCountyId;
    /**开户银行所在镇id*/
    @Column(name = "bank_town_id")
    @ApiModelProperty(name="bank_town_id",value="开户银行所在镇id",required=false)
    private Integer bankTownId;
    /**开户银行所在省*/
    @Column(name = "bank_province")
    @ApiModelProperty(name="bank_province",value="开户银行所在省",required=false)
    private String bankProvince;
    /**开户银行所在市*/
    @Column(name = "bank_city")
    @ApiModelProperty(name="bank_city",value="开户银行所在市",required=false)
    private String bankCity;
    /**开户银行所在县*/
    @Column(name = "bank_county")
    @ApiModelProperty(name="bank_county",value="开户银行所在县",required=false)
    private String bankCounty;
    /**开户银行所在镇*/
    @Column(name = "bank_town",allowNullUpdate = true)
    @ApiModelProperty(name="bank_town",value="开户银行所在镇",required=false)
    private String bankTown;
    /**开户银行许可证电子版*/
    @Column(name = "bank_img")
    @ApiModelProperty(name="bank_img",value="开户银行许可证电子版",required=false)
    private String bankImg;
    /**税务登记证号*/
    @Column(name = "taxes_certificate_num")
    @ApiModelProperty(name="taxes_certificate_num",value="税务登记证号",required=false)
    private String taxesCertificateNum;
    /**纳税人识别号*/
    @Column(name = "taxes_distinguish_num")
    @ApiModelProperty(name="taxes_distinguish_num",value="纳税人识别号",required=false)
    private String taxesDistinguishNum;
    /**税务登记证书*/
    @Column(name = "taxes_certificate_img")
    @ApiModelProperty(name="taxes_certificate_img",value="税务登记证书",required=false)
    private String taxesCertificateImg;
    /**店铺经营类目*/
    @Column(name = "goods_management_category")
    @ApiModelProperty(name="goods_management_category",value="店铺经营类目",required=false)
    private String goodsManagementCategory;
    /**店铺等级*/
    @Column(name = "shop_level")
    @ApiModelProperty(name="shop_level",value="店铺等级",required=false)
    private Integer shopLevel;
    /**店铺等级申请*/
    @Column(name = "shop_level_apply")
    @ApiModelProperty(name="shop_level_apply",value="店铺等级申请",required=false)
    private Integer shopLevelApply;
    /**店铺相册已用存储量*/
    @Column(name = "store_space_capacity")
    @ApiModelProperty(name="store_space_capacity",value="店铺相册已用存储量",required=false)
    private Double storeSpaceCapacity;
    /**店铺logo*/
    @Column(name = "shop_logo")
    @ApiModelProperty(name="shop_logo",value="店铺logo",required=false)
    private String shopLogo;
    /**店铺横幅*/
    @Column(name = "shop_banner")
    @ApiModelProperty(name="shop_banner",value="店铺横幅",required=false)
    private String shopBanner;
    /**店铺简介*/
    @Column(name = "shop_desc")
    @Size(max=200,message = "店铺简介长度不符，不能超过200")
    @ApiModelProperty(name="shop_desc",value="店铺简介",required=false)
    private String shopDesc;
    /**是否推荐*/
    @Column(name = "shop_recommend")
    @ApiModelProperty(name="shop_recommend",value="是否推荐",required=false)
    private Integer shopRecommend;
    /**店铺主题id*/
    @Column(name = "shop_themeid")
    @ApiModelProperty(name="shop_themeid",value="店铺主题id",required=false)
    private Integer shopThemeid;
    /**店铺主题模版路径*/
    @Column(name = "shop_theme_path")
    @ApiModelProperty(name="shop_theme_path",value="店铺主题模版路径",required=false)
    private String shopThemePath;
    /**店铺主题id*/
    @Column(name = "wap_themeid")
    @ApiModelProperty(name="wap_themeid",value="店铺主题id",required=false)
    private Integer wapThemeid;
    /**wap店铺主题*/
    @Column(name = "wap_theme_path")
    @ApiModelProperty(name="wap_theme_path",value="wap店铺主题",required=false)
    private String wapThemePath;
    /**店铺信用*/
    @Column(name = "shop_credit")
    @ApiModelProperty(name="shop_credit",value="店铺信用",required=false)
    private Double shopCredit;
    /**店铺好评率*/
    @Column(name = "shop_praise_rate")
    @ApiModelProperty(name="shop_praise_rate",value="店铺好评率",required=false)
    private Double shopPraiseRate;
    /**店铺描述相符度*/
    @Column(name = "shop_description_credit")
    @ApiModelProperty(name="shop_description_credit",value="店铺描述相符度",required=false)
    private Double shopDescriptionCredit;
    /**服务态度分数*/
    @Column(name = "shop_service_credit")
    @ApiModelProperty(name="shop_service_credit",value="服务态度分数",required=false)
    private Double shopServiceCredit;
    /**发货速度分数*/
    @Column(name = "shop_delivery_credit")
    @ApiModelProperty(name="shop_delivery_credit",value="发货速度分数",required=false)
    private Double shopDeliveryCredit;
    /**店铺收藏数*/
    @Column(name = "shop_collect")
    @ApiModelProperty(name="shop_collect",value="店铺收藏数",required=false)
    private Integer shopCollect;
    /**店铺商品数*/
    @Column(name = "goods_num")
    @ApiModelProperty(name="goods_num",value="店铺商品数",required=false)
    private Integer goodsNum;
    /**店铺客服qq*/
    @Column(name = "shop_qq")
    @ApiModelProperty(name="shop_qq",value="店铺客服qq",required=false)
    private String shopQq;
    /**店铺佣金比例*/
    @Column(name = "shop_commission")
    @ApiModelProperty(name="shop_commission",value="店铺佣金比例",required=false)
    private Double shopCommission;
    /**货品预警数*/
    @Column(name = "goods_warning_count")
    @ApiModelProperty(name="goods_warning_count",value="货品预警数",required=false)
    private Integer goodsWarningCount;
    /**是否自营*/
    @Column(name = "self_operated")
    @ApiModelProperty(name="self_operated",value="是否自营",required=true)
    private Integer selfOperated;
    /**申请开店进度*/
    @Column(name = "step")
    @ApiModelProperty(name="step",value="申请开店进度：1,2,3,4",required=false)
    private Integer step;
    /**是否允许开具增值税普通发票 0：否，1：是*/
    @Column(name = "ordin_receipt_status")
    @ApiModelProperty(name="ordin_receipt_status",value="是否允许开具增值税普通发票 0：否，1：是",required=false, allowableValues = "0,1")
    private Integer ordinReceiptStatus;
    /**是否允许开具电子普通发票 0：否，1：是*/
    @Column(name = "elec_receipt_status")
    @ApiModelProperty(name="elec_receipt_status",value="是否允许开具电子普通发票 0：否，1：是",required=false, allowableValues = "0,1")
    private Integer elecReceiptStatus;
    /**是否允许开具增值税专用发票 0：否，1：是*/
    @Column(name = "tax_receipt_status")
    @ApiModelProperty(name="tax_receipt_status",value="是否允许开具增值税专用发票 0：否，1：是",required=false, allowableValues = "0,1")
    private Integer taxReceiptStatus;

    /**店铺纬度*/
    @Column(name = "shop_lat")
    @ApiModelProperty(name="shop_lat",value="店铺纬度",required=false)
    private Double shopLat;
    /**店铺经度*/
    @Column(name = "shop_lng")
    @ApiModelProperty(name="shop_lng",value="店铺经度",required=false)
    private Double shopLng;
    // @Column(name = "shop_contact")
    // @ApiModelProperty(name="shop_contact",value="门店联系电话",required=false)
    // private String shopContact;
    @Column(name = "open_time_type")
    @ApiModelProperty(name="open_time_type",value="门店营业时间类型 0.全天 1.自定义",required=false)
    private Integer openTimeType;
    @Column(name = "open_time")
    @ApiModelProperty(name="open_time",value="店铺营业时间json",required=false)
    private String openTime;
    @Column(name = "business_status")
    @ApiModelProperty(name="business_status",value="店铺营业状态 1营业 0不营业",required=false)
    private Integer businessStatus;
    @Column(name = "open_start_time")
    @ApiModelProperty(name="open_start_time",value="门店开始营业时间 HH:mm:ss",required=false)
    private String openStartTime;
    @Column(name = "open_end_time")
    @ApiModelProperty(name="open_end_time",value="门店结束营业时间 HH:mm:ss",required=false)
    private String openEndTime;
    @Column(name = "ship_type")
    @ApiModelProperty(name="ship_type",value="店铺配送方式支持",required=false)
    private String shipType;
    @Column(name = "shop_tag")
    @ApiModelProperty(name="shop_tag",value="店铺标签",required=false)
    private String shopTag;
    @Column(name = "legal_back_img")
    @ApiModelProperty(name = "legal_back_img", value = "身份证反面照片", required = false)
    private String legalBackImg;
    @Column(name = "hold_img")
    @ApiModelProperty(name = "hold_img", value = "手持身份证照片", required = false)
    private String holdImg;
    @Column(name = "community_shop")
    private Integer communityShop;

    @Column(name = "cooperation_mode")
    @ApiModelProperty(name = "cooperation_mode", value = "合作模式 PLATFORM_CONSIGNMENT 平台代销 SELF_SALE 商家自销", required = true)
    private String cooperationMode;

    @Column(name = "settlement_method")
    @ApiModelProperty(name = "settlement_method", value = "结算方式 CASH_SETTLEMENT 现结  WEEK_SETTLEMENT 周结  MONTHLY_SETTLEMENT 月结", required = true)
    private String settlementMethod;

    @Column(name = "shop_type")
    @ApiModelProperty(name = "shop_type", value = "店铺类型-1:商店 2:团购 3本地生活", required = true)
    private Integer shopType;

}