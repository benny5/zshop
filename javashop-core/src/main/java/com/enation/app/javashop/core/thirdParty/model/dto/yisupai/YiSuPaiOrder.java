package com.enation.app.javashop.core.thirdParty.model.dto.yisupai;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: zhou
 * @Date: 2021/5/24
 * @Description: 易速派下单实体类
 */
@Data
public class YiSuPaiOrder {


    @ApiModelProperty(value = "收货信息")
    private YiSuPaiDeliveryAddress yspDeliveryAddress;

    @ApiModelProperty(value = "商品列表")
    private List<YiSuPaiOrder> yspOrderList;

    @ApiModelProperty(value = "邮费")
    private Integer postage;
}
