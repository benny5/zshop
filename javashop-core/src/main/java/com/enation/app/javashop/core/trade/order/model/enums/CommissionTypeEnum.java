package com.enation.app.javashop.core.trade.order.model.enums;

/**
 * 交易类型
 * @author Snow
 * @version 1.0
 * @since v7.0.0
 * 2017年4月5日下午5:12:55
 */
public enum CommissionTypeEnum {

	/** 团长佣金 */
	LEADER_COMMISSION("团长佣金"),

	/** 分销佣金 */
	DISTRIBUTOR_COMMISSION("分销佣金");

	private String description;

	CommissionTypeEnum(String description){
		  this.description=description;
	}
}
