package com.enation.app.javashop.core.shop.model.vo.operator;

import com.enation.app.javashop.framework.validation.annotation.Mobile;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;


/**
 * 店员实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-08-04 18:48:39
 */

@Data
public class ClerkUpdateVO implements Serializable {
    /**
     * 店员名称
     */
    @ApiModelProperty(value = "账号名称")
    private String uname;
    /**
     * 账号密码
     */
    @Pattern(regexp = "[a-fA-F0-9]{32}", message = "密码格式不正确")
    @ApiModelProperty(value = "新账号密码")
    private String password;

    @ApiModelProperty(value = "旧账号密码")
    private String oldPassword;

    /**
     * 手机号码
     */
    @Mobile(message = "手机格式不正确")
    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "头像")
    private String face;

    @ApiModelProperty(value = "短信验证码")
    private String smsCode;
}