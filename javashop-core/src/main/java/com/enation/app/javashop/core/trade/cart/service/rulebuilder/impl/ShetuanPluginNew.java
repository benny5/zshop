package com.enation.app.javashop.core.trade.cart.service.rulebuilder.impl;

import com.enation.app.javashop.core.promotion.exchange.model.dos.ExchangeDO;
import com.enation.app.javashop.core.promotion.seckill.model.vo.SeckillGoodsVO;
import com.enation.app.javashop.core.promotion.shetuan.model.dos.ShetuanGoodsVO;
import com.enation.app.javashop.core.promotion.tool.model.enums.PromotionTypeEnum;
import com.enation.app.javashop.core.promotion.tool.model.vo.PromotionVO;
import com.enation.app.javashop.core.trade.cart.model.enums.PromotionTarget;
import com.enation.app.javashop.core.trade.cart.model.vo.CartSkuVO;
import com.enation.app.javashop.core.trade.cart.model.vo.PromotionRule;
import com.enation.app.javashop.core.trade.cart.service.rulebuilder.SkuPromotionRuleBuilder;
import com.enation.app.javashop.framework.util.CurrencyUtil;
import com.enation.app.javashop.framework.util.DateUtil;
import org.springframework.stereotype.Component;

/**
 * 积分兑换插件
 * @author Snow create in 2018/5/25
 * @version v2.0
 * @since v7.0.0
 */
@Component
public class ShetuanPluginNew implements SkuPromotionRuleBuilder {



    @Override
    public PromotionRule build(CartSkuVO skuVO, PromotionVO promotionVO) {
        PromotionRule rule = new PromotionRule(PromotionTarget.SKU);

        ShetuanGoodsVO shetuanGoodsVO = promotionVO.getShetuanGoodsVO();
        if (shetuanGoodsVO == null) {
            return rule;
        }

        //默认商品标签
        String tag = "社区团购";

        //活动部分价格
        double totalActivityPrice = CurrencyUtil.mul(shetuanGoodsVO.getShetuanPrice(), skuVO.getNum());

        double reducedTotalPrice = CurrencyUtil.sub(skuVO.getSubtotal(), totalActivityPrice);
        double reducedPrice = CurrencyUtil.sub(skuVO.getOriginalPrice(), shetuanGoodsVO.getShetuanPrice());

        //如果商品金额 小于 优惠金额    则减免金额 = 需要支付的金额

        if (skuVO.getSubtotal() < reducedTotalPrice) {
            reducedTotalPrice = totalActivityPrice;
        }

        rule.setReducedTotalPrice(reducedTotalPrice);


        rule.setReducedPrice(reducedPrice);
        rule.setTips("社区团购价[" + shetuanGoodsVO.getShetuanPrice() + "]元");
        rule.setTag(tag);

        skuVO.setShetuanGoodsId(shetuanGoodsVO.getId());
        skuVO.setPurchaseNum(skuVO.getNum());
        return rule;

    }


    @Override
    public PromotionTypeEnum getPromotionType() {

        return PromotionTypeEnum.SHETUAN;
    }

}
