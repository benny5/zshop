package com.enation.app.javashop.core.promotion.luck.model.dos;

import com.enation.app.javashop.framework.database.annotation.Column;
import com.enation.app.javashop.framework.database.annotation.Id;
import com.enation.app.javashop.framework.database.annotation.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 孙建
 * 抽奖记录DO
 */
@Table(name="es_luck_record")
@Data
public class LuckRecordDO implements Serializable {

    /**
     * 抽奖记录id
     */
    @Id(name="luck_record_id")
    private Integer luckRecordId;
    /**
     * 抽奖活动ID
     */
    @Column(name="luck_id")
    private Integer luckId;
    /**
     * 会员id
     */
    @Column(name = "member_id")
    private Integer memberId;
    /**
     * 会员昵称
     */
    @Column(name = "member_nickname")
    private String memberNickname;
    /**
     * 会员真实姓名
     */
    @Column(name = "member_real_name")
    private String memberRealName;
    /**
     * 会员头像
     */
    @Column(name = "member_face")
    private String memberFace;
    /**
     * 抽奖奖品id
     */
    @Column(name = "luck_prize_id")
    private Integer luckPrizeId;
    /**
     * 奖品名称
     */
    @Column(name = "prize_name")
    private String prizeName;
    /**
     * 奖品类型
     */
    @Column(name = "prize_type")
    private Integer prizeType;
    /**
     * 奖品图片url
     */
    @Column(name = "prize_img")
    private String prizeImg;
    /**
     * 中奖奖品个数
     */
    @Column(name = "win_prize_num")
    private Integer winPrizeNum;
    /**
     * 是否中奖
     */
    @Column(name = "is_hit")
    private Integer isHit;
    /**
     * 出库类型
     */
    @Column(name = "delivery_type")
    private Integer deliveryType;
    /**
     * 兑奖方式 0系统自动 1人工处理
     */
    @Column(name = "exchange_type")
    private Integer exchangeType;
    /**
     * 兑奖状态 0未兑奖 1已兑奖
     */
    @Column(name = "exchange_status")
    private Integer exchangeStatus;
    /**
     * 兑奖时间
     */
    @Column(name = "exchange_time")
    private Long exchangeTime;
    /**
     * 店铺ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;
    /**
     * 店铺名称
     */
    @Column(name = "seller_name")
    private String sellerName;
    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Long createTime;
    /**
     * 是否删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

}