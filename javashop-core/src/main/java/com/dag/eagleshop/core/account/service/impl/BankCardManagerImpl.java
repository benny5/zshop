package com.dag.eagleshop.core.account.service.impl;


import com.dag.eagleshop.core.account.model.dto.bankcard.BankCardDTO;
import com.dag.eagleshop.core.account.model.dto.bankcard.BindBankCardDTO;
import com.dag.eagleshop.core.account.model.dto.bankcard.QueryBankCardDTO;
import com.dag.eagleshop.core.account.model.dto.base.IdDTO;
import com.dag.eagleshop.core.account.model.dto.base.PageDTO;
import com.dag.eagleshop.core.account.model.dto.base.ViewPage;
import com.dag.eagleshop.core.account.model.enums.AccountConstants;
import com.dag.eagleshop.core.account.service.BankCardManager;
import com.dag.eagleshop.core.account.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.recycler.Recycler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账户服务
 */
@Slf4j
@Service
public class BankCardManagerImpl implements BankCardManager {

    @Value("${service.account.url}")
    private String url;


    @Override
    public BankCardDTO bind(BindBankCardDTO bindBankCardDTO) {
        String requestPath = url + AccountConstants.BIND_BANK_CARD;
        return HttpUtils.httpPostByJson(requestPath, bindBankCardDTO, BankCardDTO.class);
    }

    @Override
    public void unbind(IdDTO<String> idDTO) {
        String requestPath = url + AccountConstants.UNBIND_BANK_CARD;
        HttpUtils.httpPostByJson(requestPath, idDTO, Recycler.V.class);
    }

    @Override
    public BankCardDTO queryById(IdDTO<String> idDTO) {
        String requestPath = url + AccountConstants.QUERY_BANK_CARD_BY_ID;
        return HttpUtils.httpPostByJson(requestPath, idDTO, BankCardDTO.class);
    }

    @Override
    public BankCardDTO queryByCardNo(String cardNo) {
        String requestPath = url + AccountConstants.QUERY_BY_CARD_NO + "?cardNo=" + cardNo;
        return HttpUtils.httpGetByJson(requestPath, BankCardDTO.class);
    }

    @Override
    public List<BankCardDTO> queryByCardNo(List<String> cardNoList) {
        String requestPath = url + AccountConstants.QUERY_BY_CARD_NOS;
        return HttpUtils.httpPostByJson(requestPath, cardNoList, List.class);
    }

    @Override
    public Integer countByMemberId(String memberId) {
        String requestPath = url + AccountConstants.COUNT_BY_MEMBER_ID + "?memberId=" + memberId;
        return HttpUtils.httpGetByJson(requestPath, Integer.class);
    }

    @Override
    public ViewPage<BankCardDTO> queryList(PageDTO<QueryBankCardDTO> pageDTO) {
        String requestPath = url + AccountConstants.QUERY_BANK_CARD_LIST;
        return HttpUtils.httpPostByJson(requestPath, pageDTO, ViewPage.class);
    }

    @Override
    public Boolean updateBankCard(BankCardDTO bankCardDTO) {
        String requestPath = url + AccountConstants.UPDATE_BANK_CARD;
        return HttpUtils.httpPostByJson(requestPath, bankCardDTO, Boolean.class);
    }
}
