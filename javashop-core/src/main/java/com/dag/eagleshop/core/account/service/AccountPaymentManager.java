package com.dag.eagleshop.core.account.service;


import com.dag.eagleshop.core.account.model.dto.payment.*;
import org.springframework.web.bind.annotation.RequestBody;

public interface AccountPaymentManager {

    /**
     * 向钱包服务发起支付请求（立即支付场景）
     * @param payRequestDTO 钱包支付请求DTO
     * @return 钱包支付响应DTO
     */
    UnifiedPayRespDTO unifiedPay(UnifiedPayReqDTO payRequestDTO);

    /**
     * 账户冻结预支付（先冻结后支付场景）
     * @param freezePrePayReqDTO 冻结支付请求DTO
     * @return 冻结支付响应DTO
     */
    FreezePrePayRespDTO freezePrePay(@RequestBody FreezePrePayReqDTO freezePrePayReqDTO);

    /**
     * 账户解冻并确认支付（先冻结后支付场景）
     * @param unfreezeConfirmPayReqDTO 冻结支付请求DTO
     * @return 冻结支付响应DTO
     */
    UnfreezeConfirmPayRespDTO unfreezeConfirmPay(@RequestBody UnfreezeConfirmPayReqDTO unfreezeConfirmPayReqDTO);

    /**
     * 账户解冻并取消支付（先冻结后支付场景）
     * @param unfreezeCancelPayReqDTO 取消冻结支付请求DTO
     * @return 取消冻结字符响应DTO
     */
    UnfreezeCancelPayRespDTO unfreezeCancelPay(@RequestBody UnfreezeCancelPayReqDTO unfreezeCancelPayReqDTO);

}
