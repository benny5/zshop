package com.dag.eagleshop.core.account.service.impl;

import com.dag.eagleshop.core.account.model.dto.payment.UnifiedPayRespDTO;
import com.dag.eagleshop.core.account.model.dto.refund.UnifiedRefundReqDTO;
import com.dag.eagleshop.core.account.model.dto.refund.UnifiedRefundRespDTO;
import com.dag.eagleshop.core.account.model.enums.AccountConstants;
import com.dag.eagleshop.core.account.service.AccountRefundManager;
import com.dag.eagleshop.core.account.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author 王志杨
 * @since 2020/10/26 14:42
 * 账户钱包退款实现
 */
@Slf4j
@Service
public class AccountRefundManagerImpl implements AccountRefundManager {

    @Value("${service.account.url}")
    private String url;

    @Override
    public UnifiedRefundRespDTO unifiedRefund(UnifiedRefundReqDTO refundRequestDTO) {
        String requestPath = url + AccountConstants.UNIFIED_REFUND;
        return HttpUtils.httpPostByJson(requestPath, refundRequestDTO, UnifiedRefundRespDTO.class);
    }
}
