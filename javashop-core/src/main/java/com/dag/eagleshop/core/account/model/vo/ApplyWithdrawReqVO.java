package com.dag.eagleshop.core.account.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 申请提现VO
 */
@Data
public class ApplyWithdrawReqVO extends BaseDTO {

    /**
     * 账户id
     */
    @ApiModelProperty(name ="accountId", value = "账户id", required = true)
    @NotNull(message = "账户id不能为空")
    private String accountId;
    /**
     * 申请提现金额
     */
    @ApiModelProperty(name ="amount", value = "申请提现金额", required = true)
    @NotNull(message = "申请提现金额不能为空")
    private BigDecimal amount;
    /**
     * 提现渠道
     */
    @ApiModelProperty(name ="withdrawChannel", value = "提现渠道，1银行卡 2支付宝 3微信", required = true)
    @NotNull(message = "提现渠道不能为空")
    private Integer withdrawChannel;
    /**
     * 银行名称
     */
    @ApiModelProperty(name ="bankName", value = "银行名称")
    private String bankName;
    /**
     * 支行名
     */
    @ApiModelProperty(name ="subBankName", value = "支行名")
    private String subBankName;
    /**
     * 银行卡号
     */
    @ApiModelProperty(name ="cardNo", value = "银行卡号")
    private String cardNo;
    /**
     * 开户名
     */
    @ApiModelProperty(name ="openAccountName", value = "开户名")
    private String openAccountName;

    /**
     * 其他账户类型名称
     */
    @ApiModelProperty(name ="otherAccountTypeName", value = "其他账户类型名称")
    private String otherAccountTypeName;
    /**
     * 其他账户名
     */
    @ApiModelProperty(name ="otherAccountName", value = "其他账户名")
    private String otherAccountName;
    /**
     * 其他账号
     */
    @ApiModelProperty(name ="otherAccountNo", value = "其他账号")
    private String otherAccountNo;

    /**
     * 申请说明
     */
    @ApiModelProperty(name ="applyRemark", value = "申请说明", required = false)
    private String applyRemark;

    /**
     * 提现方式
     */
    @ApiModelProperty(name ="withdrawType", value = "提现方式", required = true)
    private Integer withdrawType;

}
