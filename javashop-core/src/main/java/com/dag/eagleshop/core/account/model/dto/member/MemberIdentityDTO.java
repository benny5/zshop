package com.dag.eagleshop.core.account.model.dto.member;

import com.dag.eagleshop.core.account.model.dto.base.DataEntity;
import lombok.Data;

/**
 * 会员身份
 */
@Data
public class MemberIdentityDTO extends DataEntity {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 身份类型
     */
    private Integer identityType;
    /**
     * 身份类型名称
     */
    private String identityName;
    /**
     * 审核状态
     */
    private Integer auditStatus;


}
