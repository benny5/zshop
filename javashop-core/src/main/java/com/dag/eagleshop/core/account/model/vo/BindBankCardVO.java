package com.dag.eagleshop.core.account.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 绑定银行卡DTO
 */
@Data
@ApiModel(value = "绑定银行卡实体")
public class BindBankCardVO extends SaaSDTO {

    /**
     * 开户行名称
     */
    @ApiModelProperty(name ="bankName", value = "开户行名称")
    @NotNull(message = "开户行名称不能为空")
    private String bankName;
    /**
     * 支行名
     */
    @ApiModelProperty(name ="subBankName", value = "支行名称")
    private String subBankName;
    /**
     * 银行卡号
     */
    @ApiModelProperty(name ="cardNo", value = "银行卡号")
    @NotNull(message = "银行卡号不能为空")
    private String cardNo;
    /**
     * 开户名
     */
    @ApiModelProperty(name ="openAccountName", value = "开户名")
    @NotNull(message = "开户名不能为空")
    private String openAccountName;
    /**
     * 银行卡类型 0对私 1对公
     */
    @ApiModelProperty(name ="bankCardType", value = "银行卡类型，0对私 1对公")
    private Integer bankCardType;
    /**
     * 是否默认
     */
    @ApiModelProperty(name ="isDefault", value = "是否默认，0不是 1是")
    private Integer isDefault;

    /**
     * 开户省份
     */
    @ApiModelProperty(name ="bank_province", value = "开户省份")
    private String bankProvince;
    /**
     * 开户城市
     */
    @ApiModelProperty(name ="bank_city", value = "开户城市")
    private String bankCity;
    /**
     * 开户省份ID
     */
    @ApiModelProperty(name ="bank_province_id", value = "开户省份ID")
    private Integer bankProvinceId;
    /**
     * 开户城市ID
     */
    @ApiModelProperty(name ="bank_city_id", value = "开户城市ID")
    private Integer bankCityId;

}
