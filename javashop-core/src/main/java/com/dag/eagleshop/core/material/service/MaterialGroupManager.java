package com.dag.eagleshop.core.material.service;

import com.dag.eagleshop.core.material.model.dos.MaterialGroupDO;
import com.enation.app.javashop.framework.database.Page;

public interface MaterialGroupManager {

    /**
     * 查询素材分组列表
     */
    Page list(int pageNo, int pageSize, Integer sellerId, String materialGroupName);

    /**
     * 添加分组
     */
    MaterialGroupDO add(MaterialGroupDO materialGroup);

    /**
     * 修改分组
     */
    MaterialGroupDO edit(MaterialGroupDO materialGroup, Integer id);

    /**
     * 根据id查询分组
     */
    MaterialGroupDO getModel(Integer id);

    /**
     * 删除分组
     */
    void delete(Integer[] ids);
}
