package com.dag.eagleshop.core.account.model.dto.payment;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 取消冻结字符响应DTO
 */

@Data
public class UnfreezeCancelPayRespDTO implements Serializable {

    private static final long serialVersionUID = -3184444468213486471L;
    /**
     * 支付流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 支付金额
     */
    private BigDecimal amount;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static UnfreezeCancelPayRespDTO bySuccess(String tradeVoucherNo, String message){
        UnfreezeCancelPayRespDTO unfreezeConfirmPayRespDTO = by(tradeVoucherNo , message);
        unfreezeConfirmPayRespDTO.setSuccess(true);
        return unfreezeConfirmPayRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static UnfreezeCancelPayRespDTO byFail(String tradeVoucherNo, String message){
        UnfreezeCancelPayRespDTO unfreezeConfirmPayRespDTO = by(tradeVoucherNo, message);
        unfreezeConfirmPayRespDTO.setSuccess(false);
        return unfreezeConfirmPayRespDTO;
    }


    private static UnfreezeCancelPayRespDTO by(String tradeVoucherNo, String message){
        UnfreezeCancelPayRespDTO unfreezeConfirmPayRespDTO = new UnfreezeCancelPayRespDTO();
        unfreezeConfirmPayRespDTO.setTradeVoucherNo(tradeVoucherNo);
        unfreezeConfirmPayRespDTO.setMessage(message);
        return unfreezeConfirmPayRespDTO;
    }

}
