package com.dag.eagleshop.core.account.service;

import com.dag.eagleshop.core.account.model.dto.account.*;
import com.dag.eagleshop.core.account.model.dto.base.IdDTO;
import com.dag.eagleshop.core.account.model.dto.base.IdsDTO;
import com.dag.eagleshop.core.account.model.dto.base.PageDTO;
import com.dag.eagleshop.core.account.model.dto.base.ViewPage;
import com.dag.eagleshop.core.account.model.dto.member.MemberDTO;
import com.dag.eagleshop.core.account.model.dto.member.OpenMemberDTO;
import com.dag.eagleshop.core.account.model.dto.payment.PaymentRecordDTO;
import com.dag.eagleshop.core.account.model.dto.payment.RefundRecordDTO;
import com.dag.eagleshop.core.account.model.dto.withdraw.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface AccountManager {

    MemberDTO openMember(OpenMemberDTO openMemberDTO);

    MemberDTO queryMemberById(String memberId);

    AccountDTO queryByMemberIdAndAccountType(String memberId, Integer accountType);

    AccountDTO openAccount(OpenAccountDTO openAccountDTO);

    CountAccountDTO summary(String memberId);

    TransferRespDTO transfer(TransferReqDTO transferReqDTO);

    BusinessTransferRespDTO businessTransfer(BusinessTransferReqDTO transferReqDTO);

    ViewPage<WithdrawBillDTO> queryWithdrawList(PageDTO<QueryWithdrawBillDTO> pageDTO);

    /** 根据提现单号查询提现账号列表 */
    List<WithdrawBill> queryListByIds(IdsDTO<String> idsDTO);

    ApplyWithdrawRespDTO applyWithdraw(ApplyWithdrawReqDTO applyWithdrawReqDTO);

    boolean auditing(AuditingDTO auditingDTO);

    MarkTransferredDTO markTransfer(MarkTransferredDTO markTransferredDTO);

    //查询并更新微信转账状态
    void updateTransferStatus();

    boolean paymentRecord(PaymentRecordDTO paymentRecordDTO);

    boolean refundRecord(RefundRecordDTO refundRecordDTO);

    ViewPage<TradeRecordDTO> queryTradeRecordList(PageDTO<QueryTradeRecordDTO> pageDTO);

    MemberDTO queryPlatformMember();

    AccountDTO queryPlatformAccount(Integer accountType);


    /********************************************* 提现账户管理 **************************************************/

    @ApiOperation(value = "查询提现账号列表", notes = "查询提现账号列表")
    ViewPage<WithdrawAccountDTO> queryWithdrawAccountList(@RequestBody PageDTO<QueryWithdrawAccountDTO> pageDTO);

    @ApiOperation(value = "添加提现账号", notes = "添加提现账号")
    boolean addWithdrawAccount(@RequestBody AddWithdrawAccountDTO addWithdrawAccountDTO);

    @ApiOperation(value = "根据id更新提现账号", notes = "根据id更新提现账号")
    boolean updateWithdrawAccountById(@RequestBody UpdateWithdrawAccountDTO updateWithdrawAccountDTO);

    @ApiOperation(value = "删除提现账户", notes = "删除提现账户")
    void delWithdrawAccountById(@RequestBody IdDTO<String> idDTO);

}
