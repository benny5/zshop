package com.dag.eagleshop.core.account.model.dto.member;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 开通会员身份DTO
 */
@Data
public class OpenMemberIdentityDTO extends BaseDTO {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 身份类型
     */
    @NotNull(message = "身份类型不能为空")
    private Integer identityType;
    /**
     * 身份类型名称
     */
    @NotNull(message = "身份类型名称不能为空")
    private String identityName;


}
