package com.dag.eagleshop.core.account.model.dto.withdraw;

import com.dag.eagleshop.core.account.model.dto.base.UpdateDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author chien
 * @since 2020-07-22
 */
@Data
public class UpdateWithdrawAccountDTO extends UpdateDTO {

    /**
     * 会员id
     */
    @ApiModelProperty(name ="memberId", value = "会员id", hidden = true)
    private String memberId;
    /**
     * 账号类型 1支付宝 2微信
     */
    @ApiModelProperty(name ="type", value = "账号类型, 1支付宝 2微信", required = true)
    @NotNull(message = "账号类型不能为空")
    private Integer type;
    /**
     * 类型名称
     */
    @ApiModelProperty(name ="typeName", value = "类型名称", hidden = true)
    private String typeName;
    /**
     * 账户名
     */
    @ApiModelProperty(name ="accountName", value = "账户名", required = true)
    @NotNull(message = "账户名不能为空")
    private String accountName;
    /**
     * 账号
     */
    @ApiModelProperty(name ="accountNo", value = "账号", required = true)
    @NotNull(message = "账号不能为空")
    private String accountNo;
    /**
     * 是否对公 0否 1是
     */
    @ApiModelProperty(name ="ofPublic", value = "是否对公 0否 1是", required = false)
    private Integer ofPublic;

}
