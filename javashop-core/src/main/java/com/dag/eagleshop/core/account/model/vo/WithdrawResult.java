package com.dag.eagleshop.core.account.model.vo;

import com.enation.app.javashop.core.aftersale.model.enums.ExceptionClaimsCodeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: zhou
 * @Date: 2020/10/19
 * @Description:
 */
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class WithdrawResult<T> implements Serializable {

    @ApiModelProperty(name = "code",value = "返回状态码")
    private int code;
    @ApiModelProperty(name = "data",value = "返回值")
    private T data;

    public WithdrawResult(int code, T data) {
        this.code = code;
        this.data = data;
    }
}
