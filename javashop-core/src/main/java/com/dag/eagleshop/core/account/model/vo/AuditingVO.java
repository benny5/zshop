package com.dag.eagleshop.core.account.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 提现审核VO
 */
@Data
public class AuditingVO extends SaaSDTO {

    /**
     * 提现单id
     */
    @NotNull(message = "提现单id不能为空")
    private String withdrawBillId;
    /**
     * 审核备注
     */
    private String inspectRemark;
    /**
     * 提现审核状态
     */
    @NotNull(message = "提现审核状态不能为空")
    private Integer status;

}
