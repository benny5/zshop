package com.dag.eagleshop.core.account.model.dto.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * 新增对象DTO 所有新增对象请求类必须继承此DTO
 * 使用场景
 * 1、Feign请求DTO继承
 */
@Data
@Component
public class AddDTO extends SaaSDTO{

    /** 创建人id */
    @ApiModelProperty(name ="createrId", value = "创建人id", hidden = true)
    private String createrId;

    /** 创建人姓名 */
    @ApiModelProperty(name ="createrName", value = "创建人姓名", hidden = true)
    private String createrName;

}
