package com.dag.eagleshop.core.account.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 提现账号类型
 **/
public enum WithdrawAccountTypeEnum {

    ALIPAY(1, "支付宝"),
    WECHAT(2, "微信");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (WithdrawAccountTypeEnum item : WithdrawAccountTypeEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    WithdrawAccountTypeEnum(int index, String text){
        this.index = index;
        this.text = text;
    }

    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return index == null ? "" : enumMap.get(index);
    }


}
