package com.dag.eagleshop.core.account.model.dto.account;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 账户
 **/
@Data
public class CountAccountDTO extends BaseDTO {

    /**
     * 会员id
     */
    private String memberId;
    /**
     * 总金额
     */
    private BigDecimal totalAmount;
    /**
     * 可用余额
     */
    private BigDecimal balanceAmount;
    /**
     * 冻结金额
     */
    private BigDecimal freezeAmount;
    /**
     * 账户状态
     */
    private Integer status;

}
