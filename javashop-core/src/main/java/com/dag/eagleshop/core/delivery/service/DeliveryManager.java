package com.dag.eagleshop.core.delivery.service;

import com.dag.eagleshop.core.delivery.model.dto.AddOrderDTO;
import com.dag.eagleshop.core.delivery.model.dto.CancelOrderDTO;
import com.dag.eagleshop.core.delivery.model.dto.JsonBean;
import com.dag.eagleshop.core.delivery.model.dto.UpdateOrderDTO;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface DeliveryManager {

    JsonBean addOrder(@RequestBody AddOrderDTO addOrderDTO);

    JsonBean updateOrder(@RequestBody UpdateOrderDTO updateOrderDTO);

    JsonBean queryRider(@RequestBody Map<String,Object> map);

    JsonBean cancelOrder(@RequestBody CancelOrderDTO cancelOrderDTO);
}
