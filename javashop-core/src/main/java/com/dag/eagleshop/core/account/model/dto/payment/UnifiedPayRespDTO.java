package com.dag.eagleshop.core.account.model.dto.payment;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UnifiedPayRespDTO implements Serializable {

    private static final long serialVersionUID = -3579477216654577301L;
    /**
     * 支付流水号
     */
    private String serialNo;
    /**
     * 交易凭证单号
     */
    private String tradeVoucherNo;
    /**
     * 支付金额
     */
    private BigDecimal amount;
    /**
     * 支付结果
     */
    private boolean success;
    /**
     * 支付描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static UnifiedPayRespDTO bySuccess(String serialNo, String tradeVoucherNo, String message){
        UnifiedPayRespDTO unifiedPayResponseDTO = by(serialNo, tradeVoucherNo , message);
        unifiedPayResponseDTO.setSuccess(true);
        return unifiedPayResponseDTO;
    }

    /**
     * 构建失败结果
     */
    public static UnifiedPayRespDTO byFail(String serialNo, String tradeVoucherNo, String message){
        UnifiedPayRespDTO unifiedPayResponseDTO = by(serialNo, tradeVoucherNo, message);
        unifiedPayResponseDTO.setSuccess(false);
        return unifiedPayResponseDTO;
    }


    private static UnifiedPayRespDTO by(String serialNo, String tradeVoucherNo, String message){
        UnifiedPayRespDTO unifiedPayResponseDTO = new UnifiedPayRespDTO();
        unifiedPayResponseDTO.setSerialNo(serialNo);
        unifiedPayResponseDTO.setTradeVoucherNo(tradeVoucherNo);
        unifiedPayResponseDTO.setMessage(message);
        return unifiedPayResponseDTO;
    }

}
