package com.dag.eagleshop.core.account.model.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2020/10/19
 * @Description:
 */
public enum WithdrawStatusEnum {
    FAIL(-1,"提现失败"),
    UNDERWAY(0, "提现中"),
    AGREE(1, "审核通过"),
    REFUSE(2, "审核拒绝"),
    SUCCESS(3, "提现成功"),
    PAY_UNDERWAY(4, "支付中");

    private int index;
    private String text;

    private static Map<Integer, String> enumMap = new HashMap<>();

    static {
        for (WithdrawStatusEnum item : WithdrawStatusEnum.values()) {
            enumMap.put(item.getIndex(), item.getText());
        }
    }

    WithdrawStatusEnum(int index, String text){
        this.index = index;
        this.text = text;
    }

    public int getIndex() {
        return index;
    }

    public String getText() {
        return text;
    }

    public static String getTextByIndex(Integer index){
        return enumMap.get(index);
    }

    public String getName() {
        return this.name();
    }
}
