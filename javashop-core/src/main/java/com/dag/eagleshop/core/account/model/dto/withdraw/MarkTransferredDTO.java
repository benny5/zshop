package com.dag.eagleshop.core.account.model.dto.withdraw;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 标记转账DTO
 */
@Data
public class MarkTransferredDTO extends SaaSDTO {

    /**
     * 提现单id
     */
    @NotNull(message = "提现单id不能为空")
    private String withdrawBillId;
    /**
     * 转账备注
     */
    private String transferRemark;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;

    /**
     * 提现单状态
     */
    @NotNull(message = "提现单状态不能为空")
    private Integer status;

    /**
     * 真正的失败原因，用于页面展示，并不保存
     */
    private String realFailReason;

}
