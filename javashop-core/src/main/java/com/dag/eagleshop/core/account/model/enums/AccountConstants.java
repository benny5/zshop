package com.dag.eagleshop.core.account.model.enums;

/**
 * 账户服务常量类
 */
public class AccountConstants {

    /** 开通会员 */
    public static final String OPEN_MEMBER = "/account/member/openMember";
    /** 根据ID查询成员 */
    public static final String QUERY_BY_ID = "/account/member/queryById";
    /** 根据会员和账户类型查询账户 */
    public static final String QUERY_BY_MEMBER_ID_AND_ACCOUNT_TYPE = "/account/queryByMemberIdAndAccountType";
    /** 开通账户 */
    public static final String OPEN_ACCOUNT = "/account/openAccount";
    /** 账户汇总查询 */
    public static final String SUMMARY = "/account/summary";
    /** 账户转账 */
    public static final String TRANSFER = "/account/transfer";
    /** 业务转账 */
    public static final String BUSINESS_TRANSFER = "/account/businessTransfer";
    /** 申请提现 */
    public static final String APPLY_WITHDRAW = "/account/withdrawBill/applyWithdraw";
    /** 根据提现单号查询提现账号列表 */
    public static final String QUERY_LIST_BY_IDS="/account/withdrawBill/queryListByIds";
    /** 提现审核 */
    public static final String AUDITING = "/account/withdrawBill/auditing";
    /** 标记为已转账 */
    public static final String MARK_TRANSFERRED = "/account/withdrawBill/markTransferred";
    /** 提现记录查询 */
    public static final String QUERY_WITHDRAW_LIST = "/account/withdrawBill/queryWithdrawList";
    /** 支付记录 */
    public static final String PAYMENT_RECORD = "/account/payment/paymentRecord";
    /** 退款记录 */
    public static final String REFUND_RECORD = "/account/payment/refundRecord";
    /** 交易记录查询 */
    public static final String QUERY_TRADE_RECORD_LIST = "/account/queryTradeRecordList";
    /** 向钱包服务发起支付请求（立即支付场景） */
    public static final String UNIFIED_PAY = "/account/payment/unifiedPay";
    /** 账户冻结预支付（先冻结后支付场景） */
    public static final String FREEZE_PRE_PAY = "/account/payment/freezePrePay";
    /** 账户解冻并确认支付（先冻结后支付场景） */
    public static final String UNFREEZE_CONFIRM_PAY = "/account/payment/unfreezeConfirmPay";
    /** 账户解冻并取消支付（先冻结后支付场景） */
    public static final String UNFREEZE_CANCEL_PAY = "/account/payment/unfreezeCancelPay";
    /** 账户统一退款 */
    public static final String UNIFIED_REFUND = "/account/refund/unifiedRefund";

    /** 查询平台会员 */
    public static final String QUERY_PLATFORM_MEMBER = "/platform/queryPlatformMember";
    /** 查询平台账户 */
    public static final String QUERY_PLATFORM_ACCOUNT = "/platform/queryPlatformAccount";


    /********************************************* 提现账户管理 **************************************************/

    /** 查询提现账号列表 */
    public static final String QUERY_WITHDRAW_ACCOUNT_LIST = "/account/withdrawAccount/queryWithdrawAccountList";
    /** 添加提现账号 */
    public static final String ADD_WITHDRAW_ACCOUNT = "/account/withdrawAccount/addWithdrawAccount";
    /** 根据id更新提现账号 */
    public static final String UPDATE_WITHDRAW_ACCOUNT_BY_ID = "/account/withdrawAccount/updateWithdrawAccountById";
    /** 删除提现账户 */
    public static final String DEL_WITHDRAW_ACCOUNT_BY_ID = "/account/withdrawAccount/delWithdrawAccountById";


    /********************************************* 提现账户管理 **************************************************/

    /** 绑定银行卡 */
    public static final String BIND_BANK_CARD = "/account/bankcard/bind";
    /** 解绑银行卡 */
    public static final String UNBIND_BANK_CARD = "/account/bankcard/unbind";
    /** 根据id查询银行卡 */
    public static final String QUERY_BANK_CARD_BY_ID = "/account/bankcard/queryById";
    /** 根据卡号查询银行卡 */
    public static final String QUERY_BY_CARD_NO = "/account/bankcard/queryByCardNo";
    /** 根据卡号查询银行卡 */
    public static final String QUERY_BY_CARD_NOS = "/account/bankcard/queryByCardNos";
    /** 根据memberId查询银行卡数量 */
    public static final String COUNT_BY_MEMBER_ID = "/account/bankcard/countByMemberId";
    /** 根据条件查询银行卡列表 */
    public static final String QUERY_BANK_CARD_LIST = "/account/bankcard/queryList";
    /** 根据条件查询银行卡列表 */
    public static final String UPDATE_BANK_CARD = "/account/bankcard/updateById";


}
