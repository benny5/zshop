package com.dag.eagleshop.core.material.service.impl;

import com.dag.eagleshop.core.material.MaterialErrorCode;
import com.dag.eagleshop.core.material.model.dos.MaterialDO;
import com.dag.eagleshop.core.material.model.dos.MaterialGroupDO;
import com.dag.eagleshop.core.material.service.MaterialGroupManager;
import com.dag.eagleshop.core.material.service.MaterialManager;
import com.enation.app.javashop.framework.database.DaoSupport;
import com.enation.app.javashop.framework.database.Page;
import com.enation.app.javashop.framework.exception.ServiceException;
import com.enation.app.javashop.framework.util.SqlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MaterialManagerImpl implements MaterialManager {

    @Autowired
    @Qualifier("systemDaoSupport")
    private DaoSupport daoSupport;

    @Autowired
    private MaterialGroupManager materialGroupManager;


    @Override
    public Page list(int pageNo, int pageSize, Integer sellerId, String materialName) {
        StringBuilder sqlBuilder = new StringBuilder("select * from mat_material ");
        sqlBuilder.append(" where seller_id = " + sellerId);
        List<Object> term = new ArrayList<>();
        if (materialName != null) {
            sqlBuilder.append(" and material_name like ? ");
            term.add("%" + materialName + "%");
        }

        sqlBuilder.append(" order by material_id desc");

        return this.daoSupport.queryForPage(sqlBuilder.toString(), pageNo, pageSize,
                MaterialDO.class, term.toArray());
    }

    @Override
    public MaterialDO add(MaterialDO materialGroup) {
        MaterialGroupDO materialGroupDO = materialGroupManager.getModel(materialGroup.getMaterialGroupId());
        if (materialGroupDO == null) {
            throw new ServiceException(MaterialErrorCode.E1000.code(), "素材分组不存在");
        }

        this.daoSupport.insert(materialGroup);
        return materialGroup;
    }


    @Override
    public MaterialDO edit(MaterialDO materialGroup, Integer id) {
        MaterialDO materialDO = this.getModel(id);
        if (materialDO == null) {
            throw new ServiceException(MaterialErrorCode.E1000.code(), "素材不存在");
        }

        MaterialGroupDO materialGroupDO = materialGroupManager.getModel(materialGroup.getMaterialGroupId());
        if (materialGroupDO == null) {
            throw new ServiceException(MaterialErrorCode.E1000.code(), "素材分组不存在");
        }

        this.daoSupport.update(materialGroup, id);
        return materialGroup;
    }


    @Override
    public MaterialDO getModel(Integer id) {
        return this.daoSupport.queryForObject(MaterialDO.class, id);
    }

    @Override
    public void delete(Integer[] ids) {
        List term = new ArrayList<>();

        String idsStr = SqlUtil.getInSql(ids, term);

        String sql = "delete from mat_material where material_id in (" + idsStr + ") ";

        this.daoSupport.execute(sql, term.toArray());
    }


    @Override
    public void batchEditGroup(Integer[] ids, Integer materialGroupId) {
        this.daoSupport.batchUpdate();
    }
}
