package com.dag.eagleshop.core.account.model.dto.payment;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 取消冻结支付请求DTO
 */
@Data
public class UnfreezeCancelPayReqDTO implements Serializable {

    private static final long serialVersionUID = 1864914767450793510L;
    /**
     * 交易凭证单号
     */
    @NotNull(message = "交易凭证单号不能为空")
    private String tradeVoucherNo;
    /**
     * 付款人id
     */
    @NotNull(message = "付款人id不能为空")
    private String draweeMemberId;
    /**
     * 付款账户类型
     */
    @NotNull(message = "付款账户类型")
    private Integer accountType;
    /**
     * 付款金额
     */
    @NotNull(message = "付款金额不能为空")
    private BigDecimal amount;
    /**
     * 操作人id
     */
    @NotNull(message = "操作人id不能为空")
    private String operatorId;
    /**
     * 操作人姓名
     */
    @NotNull(message = "操作人姓名不能为空")
    private String operatorName;

}
