package com.dag.eagleshop.core.account.model.dto.withdraw;

import com.dag.eagleshop.core.account.model.dto.base.SaaSDTO;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 申请提现DTO
 */
@Data
public class ApplyWithdrawReqDTO extends SaaSDTO {

    /**
     * 会员id
     */
    @NotNull(message = "会员id不能为空")
    private String memberId;
    /**
     * 会员名称
     */
    @NotNull(message = "会员名称不能为空")
    private String memberName;
    /**
     * 账户id
     */
    @NotNull(message = "账户id不能为空")
    private String accountId;
    /**
     * 提现渠道
     */
    @NotNull(message = "提现渠道不能为空")
    private Integer withdrawChannel;
    /**
     * 申请提现金额
     */
    @NotNull(message = "申请提现金额不能为空")
    private BigDecimal amount;
    /**
     * 银行名称
     */
    private String bankName;
    /**
     * 支行名
     */
    private String subBankName;
    /**
     * 银行卡号
     */
    private String cardNo;
    /**
     * 开户名
     */
    private String openAccountName;
    /**
     * 其他账户类型名称
     */
    private String otherAccountTypeName;
    /**
     * 其他账户名
     */
    private String otherAccountName;
    /**
     * 其他账号
     */
    private String otherAccountNo;
    /**
     * 提现方式
     */
    @NotNull(message = "提现方式不能为空")
    private Integer withdrawType;
    /**
     * 申请备注
     */
    @NotNull(message = "申请备注不能为空")
    private String applyRemark;

}
