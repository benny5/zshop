package com.dag.eagleshop.core.account.service;

import com.dag.eagleshop.core.account.model.dto.bankcard.BankCardDTO;
import com.dag.eagleshop.core.account.model.dto.bankcard.BindBankCardDTO;
import com.dag.eagleshop.core.account.model.dto.bankcard.QueryBankCardDTO;
import com.dag.eagleshop.core.account.model.dto.base.IdDTO;
import com.dag.eagleshop.core.account.model.dto.base.PageDTO;
import com.dag.eagleshop.core.account.model.dto.base.ViewPage;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 账户服务
 */
public interface BankCardManager {

    @ApiOperation(value = "绑定银行卡", notes = "绑定银行卡")
    BankCardDTO bind(@RequestBody BindBankCardDTO bindBankCardDTO);

    @ApiOperation(value = "解绑银行卡", notes = "解绑银行卡")
    void unbind(@RequestBody IdDTO<String> idDTO);

    @ApiOperation(value = "根据id查询银行卡", notes = "根据id查询银行卡")
    BankCardDTO queryById(@RequestBody IdDTO<String> idDTO);

    @ApiOperation(value = "根据卡号查询银行卡", notes = "根据卡号查询银行卡")
    BankCardDTO queryByCardNo(@RequestParam(value = "cardNo") String cardNo);

    @ApiOperation(value = "根据卡号查询银行卡", notes = "根据卡号查询银行卡")
    List<BankCardDTO> queryByCardNo(@RequestBody List<String> cardNoList);

    @ApiOperation(value = "根据memberId查询银行卡数量", notes = "根据memberId查询银行卡数量")
    Integer countByMemberId(@RequestParam(value = "memberId") String memberId);

    @ApiOperation(value = "根据条件查询银行卡列表", notes = "根据条件查询银行卡列表")
    ViewPage<BankCardDTO> queryList(@RequestBody PageDTO<QueryBankCardDTO> pageDTO);

    @ApiOperation(value = "根据id更新银行卡", notes = "根据id更新银行卡")
    Boolean updateBankCard(BankCardDTO bankCardDTO);
}
