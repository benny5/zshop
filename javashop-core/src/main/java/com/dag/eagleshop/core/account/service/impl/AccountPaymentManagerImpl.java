package com.dag.eagleshop.core.account.service.impl;

import com.dag.eagleshop.core.account.model.dto.payment.*;
import com.dag.eagleshop.core.account.model.enums.AccountConstants;
import com.dag.eagleshop.core.account.service.AccountPaymentManager;
import com.dag.eagleshop.core.account.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author 王志杨
 * @since 2020/10/13 20:04
 */

@Slf4j
@Service
public class AccountPaymentManagerImpl implements AccountPaymentManager {

    @Value("${service.account.url}")
    private String url;

    @Override
    public UnifiedPayRespDTO unifiedPay(UnifiedPayReqDTO payRequestDTO) {
        String requestPath = url + AccountConstants.UNIFIED_PAY;
        return HttpUtils.httpPostByJson(requestPath, payRequestDTO, UnifiedPayRespDTO.class);
    }

    @Override
    public FreezePrePayRespDTO freezePrePay(FreezePrePayReqDTO freezePrePayReqDTO) {
        String requestPath = url + AccountConstants.FREEZE_PRE_PAY;
        return HttpUtils.httpPostByJson(requestPath, freezePrePayReqDTO, FreezePrePayRespDTO.class);
    }

    @Override
    public UnfreezeConfirmPayRespDTO unfreezeConfirmPay(UnfreezeConfirmPayReqDTO unfreezeConfirmPayReqDTO) {
        String requestPath = url + AccountConstants.UNFREEZE_CONFIRM_PAY;
        return HttpUtils.httpPostByJson(requestPath, unfreezeConfirmPayReqDTO, UnfreezeConfirmPayRespDTO.class);
    }

    @Override
    public UnfreezeCancelPayRespDTO unfreezeCancelPay(UnfreezeCancelPayReqDTO unfreezeCancelPayReqDTO) {
        String requestPath = url + AccountConstants.UNFREEZE_CANCEL_PAY;
        return HttpUtils.httpPostByJson(requestPath, unfreezeCancelPayReqDTO, UnfreezeCancelPayRespDTO.class);
    }
}
