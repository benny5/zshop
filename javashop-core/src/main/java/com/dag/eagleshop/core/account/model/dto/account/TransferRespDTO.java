package com.dag.eagleshop.core.account.model.dto.account;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import lombok.Data;

/**
 * 转账结果DTO
 */
@Data
public class TransferRespDTO extends BaseDTO {

    /**
     * 付款账户id
     */
    private String draweeAccountId;
    /**
     * 收款账户id
     */
    private String payeeAccountId;
    /**
     * 转账结果
     */
    private boolean success;
    /**
     * 转账描述
     */
    private String message;


    /**
     * 构建成功结果
     */
    public static TransferRespDTO bySuccess(String draweeAccountId, String payeeAccountId, String message){
        TransferRespDTO transferRespDTO = by(draweeAccountId, payeeAccountId, message);
        transferRespDTO.setSuccess(true);
        return transferRespDTO;
    }

    /**
     * 构建失败结果
     */
    public static TransferRespDTO byFail(String draweeAccountId, String payeeAccountId, String message){
        TransferRespDTO transferRespDTO = by(draweeAccountId, payeeAccountId, message);
        transferRespDTO.setSuccess(false);
        return transferRespDTO;
    }


    private static TransferRespDTO by(String draweeAccountId, String payeeAccountId, String message){
        TransferRespDTO transferRespDTO = new TransferRespDTO();
        transferRespDTO.setDraweeAccountId(draweeAccountId);
        transferRespDTO.setPayeeAccountId(payeeAccountId);
        transferRespDTO.setMessage(message);
        return transferRespDTO;
    }

}
