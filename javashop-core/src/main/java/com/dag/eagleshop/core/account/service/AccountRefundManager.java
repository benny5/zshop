package com.dag.eagleshop.core.account.service;

import com.dag.eagleshop.core.account.model.dto.refund.UnifiedRefundReqDTO;
import com.dag.eagleshop.core.account.model.dto.refund.UnifiedRefundRespDTO;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author 王志杨
 * @since 2020/10/22 16:04
 * 账户钱包退款
 */
public interface AccountRefundManager {

    /**
     * 账户统一退款
     * @param refundRequestDTO
     * @return
     */
    UnifiedRefundRespDTO unifiedRefund(@RequestBody UnifiedRefundReqDTO refundRequestDTO);

}
