package com.dag.eagleshop.core.account.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 账户DTO
 **/
@Data
public class AccountVO extends BaseDTO {

    @ApiModelProperty(name ="id", value = "账户id")
    private String id;
    /**
     * 会员id
     */
    @ApiModelProperty(name ="memberId", value = "会员id")
    private String memberId;
    /**
     * 账户类型 目前就一个主账户 AccountTypeEnum
     */
    @ApiModelProperty(name ="accountType", value = "账户类型")
    private Integer accountType;
    /**
     * 账户类型名称 目前就一个主账户 AccountTypeEnum
     */
    @ApiModelProperty(name ="accountTypeName", value = "账户类型名称")
    private String accountTypeName;
    /**
     * 账户性质
     */
    @ApiModelProperty(name ="nature", value = "账户性质")
    private Integer nature;
    /**
     * 总金额
     */
    @ApiModelProperty(name ="totalAmount", value = "总金额")
    private BigDecimal totalAmount;
    /**
     * 可用余额
     */
    @ApiModelProperty(name ="balanceAmount", value = "可用余额")
    private BigDecimal balanceAmount;
    /**
     * 冻结金额
     */
    @ApiModelProperty(name ="freezeAmount", value = "冻结金额")
    private BigDecimal freezeAmount;
    /**
     * 账户状态
     */
    @ApiModelProperty(name ="status", value = "账户状态")
    private Integer status;

}
