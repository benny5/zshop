package com.dag.eagleshop.core.account.model.dto.base;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 接收一个id数组
 * @param <T>
 */
@Data
public final class IdsDTO<T> extends BaseDTO {

    @NotNull(message = "ids参数不能为空！")
    private List<T> ids;

    private IdsDTO(){}

    private IdsDTO(List<T> ids){
        this.ids = ids;
    }

    public static <T> IdsDTO<T> by(List<T> ids){
        return new IdsDTO<>(ids);
    }

}
