package com.dag.eagleshop.core.account.model.dto.base;

import lombok.Data;

/**
 * 分页实体类
 * @param <T>
 */
@Data
public class PageDTO<T> extends BaseDTO{

    // 业务对象
    private T body;

    // 排序字段
    private SortField[] orderBys;

    // 页码
    private Integer pageNum = 1;

    // 页大小
    private Integer pageSize = 10;

    public PageDTO(){}

    public PageDTO(Integer pageNum, Integer pageSize){
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public PageDTO(Integer pageNum, Integer pageSize, T body){
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.body = body;
    }

}
