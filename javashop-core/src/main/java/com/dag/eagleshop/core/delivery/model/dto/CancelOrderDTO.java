package com.dag.eagleshop.core.delivery.model.dto;


import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class CancelOrderDTO implements Serializable {
    @NotNull(message = "orderSn不能为空")
    private List<String> ids;

    // 取消原因
    private String reason;

    // 卖家id
    private String updaterId;

    //卖家姓名
    private String updaterName;

}
