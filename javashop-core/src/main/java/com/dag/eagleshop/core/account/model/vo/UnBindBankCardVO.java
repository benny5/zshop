package com.dag.eagleshop.core.account.model.vo;

import com.dag.eagleshop.core.account.model.dto.base.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UnBindBankCardVO extends BaseDTO {

    @ApiModelProperty(name ="bankCardId", value = "银行卡id")
    @NotNull(message = "银行卡id不能为空")
    private String bankCardId;

}
