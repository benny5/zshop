package com.dag.eagleshop.core.account.model.dto.member;

import com.dag.eagleshop.core.account.model.dto.base.DataEntity;
import lombok.Data;

import java.util.List;

@Data
public class MemberDTO extends DataEntity {

    /**
     * 会员姓名
     */
    private String memberName;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String area;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 会员状态
     */
    private Integer memberStatus;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 身份证正面url
     */
    private String idCardFrontUrl;
    /**
     * 身份证反面url
     */
    private String idCardBackUrl;
    /**
     * 手持身份证url
     */
    private String handIdCardUrl;
    /**
     * 会员身份列表
     */
    private List<MemberIdentityDTO> identityList;

    // 头像
    private String face;

    // 会员昵称
    private String nickName;

    // 会员id
    private Integer memberId;

    // 描述一
    private String describe1;

}
