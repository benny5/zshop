package com.enation.app.javashop.framework.redis.transactional;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RedisTransactional {

    /**
     * 获取锁的最长等待时间，以秒为单位
     * 如果不指定，则会一直等待
     */
    int acquireTimeout() default 0;

    /**
     * 重试的间隔时间,设置GIVEUP忽略此项
     * @return
     */
    long sleepMills() default 200;

    /**
     * 重试次数
     * @return
     */
    int retryTimes() default 0;

    /**
     * 锁的超时时间，以秒为单位
     * 如果不指定，则不会超时，只能手工解锁时才会释放
     * 持锁时间,单位毫秒
     */
    int lockTimeout() default 0 ;


    /**
     * 锁名字，如果不指定，则使用当前方法的全路径名
     * @return
     */
    String lockName() default "";

    /**
     * 锁的资源，key支持spring El表达式
     */
    @AliasFor("key")
    String value() default "'default'";

}
