
package com.enation.app.javashop.framework.util;

import com.enation.app.javashop.framework.entity.DictDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 字典工具类
 * @author 孙建
 */
public final class DictUtils {

	private DictUtils(){}

	// 项目字典MAP的key
	public static final String DICT_MAP = "DICT_MAP";

	public static final int SYSTEM = 1;	// 系统字典 不可删除
	public static final int GENERAL = 2;	// 通用字典 商户共享
	public static final int PRIVATE = 3;	// 私有字典

	// 空字符
	private static final String EMPTY = "";

	// 本地缓存
	private static Map<String, List<DictDTO>> dictMap = new ConcurrentHashMap<>();

	// 初始化缓存
	public synchronized static void initDictLocalCache(){
		// 清除
		dictMap.clear();

		// 获取
		Map<String, List<DictDTO>> redisDictMap = RedisUtils.get(DICT_MAP, new TypeReference<ConcurrentHashMap<String, List<DictDTO>>>() {});
		if(redisDictMap != null){
			dictMap.putAll(redisDictMap);
		}
	}

	// 启动后执行
	static {
		initDictLocalCache();
	}


	/**
	 * 根据type获取字典列表
	 */
	public static List<DictDTO> getDictList(String merchantId, String type){
		List<DictDTO> dictList = dictMap.get(type);
		if(StringUtils.isEmpty(merchantId)){
			return dictList;
		}
		if(IDataUtils.isNotEmpty(dictList)){
			List<DictDTO> resultList = new ArrayList<>();
			for (DictDTO dictDTO : dictList) {
				Integer classify = dictDTO.getClassify();
				// 系统字典必须有
				if(SYSTEM == classify){
					resultList.add(dictDTO);
				}else if(GENERAL == classify){
					// 通用字典需要判断该商户是否删除了 TODO
					resultList.add(dictDTO);
				}else{
					// 私有字典
					if(merchantId.equals(dictDTO.getMerchantId())){
						resultList.add(dictDTO);
					}
				}
			}
			return resultList;
		}
		return null;
	}


	/**
	 * 根据value和type获取label
	 */
	public static String getDictLabel(String merchantId, String value, String type){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(value)){
			List<DictDTO> dictList = getDictList(merchantId, type);
			if(dictList != null && dictList.size() > 0){
				for (DictDTO dict : dictList){
					if (type.equals(dict.getType()) && value.equals(dict.getValue())){
						return dict.getLabel();
					}
				}
			}
		}
		return EMPTY;
	}


	/**
	 * 根据value和type获取label
	 */
	public static String getDictLabel(String merchantId, Integer value, String type){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(String.valueOf(value))){
			List<DictDTO> dictList = getDictList(merchantId, type);
			if(dictList != null && dictList.size() > 0){
				for (DictDTO dict : dictList){
					if (type.equals(dict.getType()) && String.valueOf(value).equals(dict.getValue())){
						return dict.getLabel();
					}
				}
			}
		}
		return EMPTY;
	}


	/**
	 * 根据values和type获取labels
	 */
	public static String getDictLabels(String merchantId, String values, String type){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(values)){
			List<String> valueList = Lists.newArrayList();
			for (String value : StringUtils.split(values, ",")){
				valueList.add(getDictLabel(merchantId, value, type));
			}
			return StringUtils.join(valueList, ",");
		}
		return EMPTY;
	}


	/**
	 * 根据label和type获取value
	 */
	public static String getDictValue(String merchantId, String label, String type){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(label)){
			List<DictDTO> dictList = getDictList(merchantId, type);
			if(dictList != null && dictList.size() > 0) {
				for (DictDTO dict : dictList) {
					if (type.equals(dict.getType()) && label.equals(dict.getLabel())) {
						return dict.getValue();
					}
				}
			}
		}
		return EMPTY;
	}


	/**
	 * 根据类型和值获取字典
	 */
	public static DictDTO getDictByTypeAndValue(String merchantId, String type, String value){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(value)){
			List<DictDTO> dictList = getDictList(merchantId, type);
			if(dictList != null && dictList.size() > 0) {
				for (DictDTO dict : dictList){
					if (type.equals(dict.getType()) && value.equals(dict.getValue())){
						return dict;
					}
				}
			}
		}
		return null;
	}



	/**
	 * 根据类型和描述获取字典列表
	 */
	public static List<DictDTO> getListByTypeAndDesc(String merchantId, String type, String desc){
		List<DictDTO> resultList =  new ArrayList<DictDTO>();
		if(StringUtils.isBlank(desc) || StringUtils.isBlank(type)){
			return null;
		}
		List<DictDTO> dictList = getDictList(merchantId, type);
		if(dictList == null || dictList.size() == 0){
			return dictList;
		}

		for(DictDTO dictDTO : dictList){
			if(null != dictDTO && desc.equals(dictDTO.getDescription())){
				resultList.add(dictDTO);
			}
		}
		return resultList;
	}
	
}
