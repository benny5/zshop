package com.enation.app.javashop.framework.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class EnvironmentUtils {

    // 系统环境
    private static Environment environment;

    /**
     * 判断是否是非生产环境
     */
    public static boolean isProd(){
        String profile = environment.getActiveProfiles()[0];
        if("prod".equalsIgnoreCase(profile)){
            return true;
        }
        return false;
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        EnvironmentUtils.environment = environment;
    }

}
