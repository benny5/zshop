package com.enation.app.javashop.framework.redis.listener;

/**
 * Redis监听器接口
 * @author ChienSun
 * @date 2019/1/3
 */
public interface RedisMonitor {

    void receiveMessage(String message);

}
