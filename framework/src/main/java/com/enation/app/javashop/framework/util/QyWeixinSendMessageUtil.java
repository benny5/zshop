package com.enation.app.javashop.framework.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author: zhou
 * @Date: 2021/1/6
 * @Description: 企业微信推送消息
 */
public class QyWeixinSendMessageUtil {
    protected final static Log logger = LogFactory.getLog(QyWeixinSendMessageUtil.class);

    public final static String corpid = "wwba93c67a842235ea";
    public final static String corpsecret = "wJ3reEd4YcDh7SJAd6yX4e3XBksZFbei5pd0mDA4rRY";

    // 获取token的url
    public static String getTokenUrl = "https://qyapi.weixin.qq.com/cgi-bin/gettoken";
    // 发送到群聊的url
    public static String sendToChatUrl = "https://qyapi.weixin.qq.com/cgi-bin/appchat/send";

    //每日业绩播报 群聊
    public final static String OperationData = "OperationData";
    // 业务运营预警 群聊
    public final static String EarlyWarning = "EarlyWarning";
    // 客服发送退款消息 群聊
    public final static String RefundMessage = "RefundMessage";

    public static boolean sendMessageToGroupChat(String chatName, String message) {
        if (StringUtils.isEmpty(chatName) || StringUtils.isEmpty(message)) {
            logger.error("推送群聊消息时获取企业微信的数据为空");
            return false;
        }

        // 获取token
        String token = getToken();
        if (token == null) {
            // 日志
            logger.error("推送群聊消息时获取企业微信的token失败");
            return false;
        }

        // 封装url
        String url = sendToChatUrl + "?access_token=" + token;
        logger.info("推送消息到企业微信群聊请求地址：" + url);

        Map<String, Object> apiParams = new LinkedHashMap<>();
        apiParams.put("chatid", chatName);
        apiParams.put("msgtype", "text");
        apiParams.put("safe", 0);
        Map<String, String> text = new LinkedHashMap<>();
        text.put("content", message);
        apiParams.put("text", text);

        logger.info("推送消息到企业微信群聊请求地址：" + url + "   请求参数：" + apiParams.toString());
        try {
            // 推送消息
            String responseString=HttpUtils.doPostWithJson(url,apiParams);
            Map resMap=JsonUtil.toMap(responseString);
            logger.info("推送消息到企业微信群聊的返回参数：" + responseString);
            if (resMap.get("errcode").equals(0)) {

                return true;
            }
        } catch (Exception e) {
            logger.info("推送消息到企业微信群聊请求错误：" + e);
        }
        return false;
    }

    private static String getToken() {
        String url = getTokenUrl + "?corpid=" + corpid + "&corpsecret=" + corpsecret;
        logger.info("获取企业微信的token请求地址：" + url);
        try {
            String responseString = HttpUtils.doGet(url);
            logger.info("获取企业微信的token返回参数：" + responseString);

            Map resMap=JsonUtil.toMap(responseString);
            if (Integer.parseInt(resMap.get("errcode").toString())==0) {
                return (String)resMap.get("access_token");
            }

        } catch (Exception e) {
            logger.error("获取企业微信的token请求错误：" + e);
        }

        return null;
    }

}
