package com.enation.app.javashop.framework.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * 对象格式化工具类
 */
public final class ObjectMapperUtils {

    /** 对象格式化 */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    /** 漂亮的对象格式化 */
    private static final ObjectWriter OBJECT_WRITER = OBJECT_MAPPER.writerWithDefaultPrettyPrinter();

    /**
     * 转换为普通Json对象
     */
    public static String toJson(Object object){
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 格式化对象为Json
     */
    public static String prettyToJson(Object object){
        try {
            return OBJECT_WRITER.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取ObjectMapper对象
     */
    public static ObjectMapper mapper(){
        return OBJECT_MAPPER;
    }

    /**
     * 获取ObjectWriter对象
     */
    public static ObjectWriter writer(){
        return OBJECT_WRITER;
    }

}
